import sys
import os
import numpy as np
import glob
import time
import h5py
import matplotlib as mpl
import imageio
import matplotlib.pyplot as plt
import pyqtgraph as pg
pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))
sys.path.append(pathogen("/software/odysseus/submodules/microjet_analysis/"))

from microjet_analysis import gdvn
from microjet_analysis import liveanalysis as ana
from microjet_analysis import dataio


r""" Analyzes GDVN jet data on the fly.

    When gathering data for jets, it is useful to analyze data live to help guide the data collection.
    This script will:
        1. Gather any data already recorded in a given folder
        2. Plot that pre-recorded data
        3. Start the LiveAnalysis class 
            --> This handles the watching of newly recorded data
        4. Update the phase diagram
        5. Wait until new data is recorded

    Params the User Must Update:
        1. The folders in the master_config

"""

# TO DO:  Need a few methods to handle plotting better. 

config = {"basepath": pathogen(os.path.join("/data/microjets/")),
<<<<<<< HEAD
                "year": "2022",
                "date": "20220308",
                "nozzle_id": "mh_cpf_03",
=======
                "year": "2023",
                "date": "20230714",
                "nozzle_id": "B1_LY59_79_01",
>>>>>>> develop
                'plot_jetspeed': True,
                'plot_jetdeviation': True,
                'plot_jetdiameter': True,
                'plot_jetstability': True,
<<<<<<< HEAD
                'interactive': False ,
=======
                'interactive': False,
>>>>>>> develop
                'make_gifs': False
                }

# the config with all the analysis options
analysis_config = dataio.analysis_config()
<<<<<<< HEAD
analysis_config['max_frames'] = 130
analysis_config['brightfield'] = True

=======
analysis_config['max_frames'] = 999
analysis_config['brightfield'] = False
analysis_config['filter_method'] = 'gausshp'
analysis_config['filter_parameters']['sigma'] = 10
analysis_config['processing_steps']['remove_small_objects'] = True
analysis_config['processing_steps']['min_pixel_size'] = 4
analysis_config['processing_steps']['binary_closing'] = True
analysis_config['processing_steps']['n_closing_iterations'] = 2
analysis_config['processing_steps']['inflate_small_pixels'] = True
analysis_config['processing_steps']['inflate_amount'] = 3
>>>>>>> develop
analysis_config['threshold_method'] = 'hysteresis'
if analysis_config['brightfield'] is True:
    analysis_config['threshold_parameters']['h_low'] = 0.15
    analysis_config['threshold_parameters']['h_high'] = 0.5 
else: 
    analysis_config['threshold_parameters']['h_low'] = 2
    analysis_config['threshold_parameters']['h_high'] = 5
analysis_config['save_stacks']['filtered_stack'] = False
analysis_config['save_stacks']['thresholded_stack'] = False
analysis_config['extra_filter_step'] = False
analysis_config['parallelize'] = True
analysis_config['process_stack'] = True
analysis_config['nozzle_tip_finder_method'] = 'provided'
analysis_config['nozzle_tip_position'] = 1003
analysis_config['processing_steps']['erode_1'] = False
analysis_config['processing_steps']['n_erosion_1_iteration'] = 1
analysis_config['processing_steps']['erode_2'] = False
analysis_config['processing_steps']['n_erosion_2_iteration'] = 1

# make the master config passed throughout the script -- don't change this.
master_config = {'data_path': pathogen(os.path.join(config["basepath"], config['year'], config['date'], config['nozzle_id'])),
                    'save_path': pathogen(os.path.join(config["basepath"], config['year'], config['date'], config['nozzle_id']+"_analysis", "data")),
                    'log_path': pathogen(os.path.join(config["basepath"], "logs", "sensirion_SLI-0430_FTSTT5NA")),
                    'figure_save_path': pathogen(os.path.join(config["basepath"], config['year'],  config['date'], config['nozzle_id']+"_analysis", "figures")),
                    'gif_save_path': pathogen(os.path.join(config["basepath"], config['year'],  config['date'], config['nozzle_id']+"_analysis", "gifs")),
                    'csv_data': pathogen(os.path.join(config["basepath"], config['year'], config['date'], config['nozzle_id']+"_analysis")),
                    'bad_data':pathogen(os.path.join(config["basepath"], config['year'], config['date'], config['nozzle_id'], "bad"))}
master_config.update(config)
master_config.update(analysis_config)

nozzle = config['nozzle_id']

print("==========================", flush=True)
print("Live jet analysis started!", flush=True)
print("==========================", flush=True)

print("\n", flush=True)

print("Looking for data in this folder:", flush=True)
print(f"\t{master_config['data_path']}", flush=True)
print("Looking for logs in this folder:", flush=True)
print(f"\t{master_config['log_path']}", flush=True)
print("Saving the analysis here:", flush=True)
print(f"\t{master_config['save_path']}", flush=True)
print("Figures can be found in this folder:", flush=True)
print(f"\t{master_config['figure_save_path']}", flush=True)
if config['make_gifs']:
    print("Saving the GIFs here:", flush=True)
    print(f"\t{master_config['gif_save_path']}", flush=True)
print(f"Saving bad data here:", flush=True)
print(f"\t{master_config['bad_data']}", flush=True)


print("\n", flush=True)

def plot_phase_diagram(plots, savepath:str, nozzle_id:str,
                        x, xr, y, yr, z, zr, d, cbar_title:str, 
                        clip_negative:bool=False, cmap_reverse=False, level_step=5):

    if clip_negative:
        for i in range(len(z)):
            if z[i] < 0:
                z[i] = 0
        zidx = np.where(z != 0)
        xi, yi, zi, levels = plots.interpolate_ranges(ranges=[xr, yr, zr], data=[x[zidx], y[zidx], z[zidx]], 
                                                        level_step=level_step)
    else:
        xi, yi, zi, levels = plots.interpolate_ranges(ranges=[xr, yr, zr], data=[x, y, z], level_step=level_step)
    fig, ax = plt.subplots(1,1, tight_layout=True)
    if cmap_reverse:
        cmap = mpl.cm.get_cmap('RdBu')
    else:
        cmap = mpl.cm.get_cmap('RdBu_r')
    contourf = ax.contourf(xi, yi, zi, levels=levels, cmap=cmap)
    fig.colorbar(contourf, ax=ax, label=cbar_title)
    ax.scatter(x, y, s=4*d, marker='o', 
                    color=cmap((z-zr[0])/(zr[1]-zr[0])),
                    edgecolors=(0, 0, 0), linewidths=0.3)
    ax.set(xlim=xr, ylim=yr)
    ax.set_xlabel('Liquid Flow (ul/min)')
    ax.set_ylabel('Gas Flow (mg/min)')
    fig.suptitle(nozzle_id)

    plt.savefig(savepath)
    plt.close()

def make_gif(datafile, save_file, frame_limit:int=30, fps:int=10, verbose=True):
    with h5py.File(datafile, "r") as h5:
    # h5 = h5py.File(datafile, "r")
        frames = h5['frames'][::2][:frame_limit]
        # os.makedirs(os.path.basename(save_file), exist_ok=True)
        imageio.mimwrite(save_file, frames, fps=fps)
        if verbose:
            print(f"Saved gif version: fps={fps}, nframes={frame_limit}", flush=True)

rawdata = np.sort(glob.glob(pathogen(os.path.join(master_config['data_path'], "*.h5"))))
if config['make_gifs']:
    for dat in rawdata:
        x = pathogen(os.path.join(master_config['gif_save_path'], dat.split(os.sep)[-1].split('.')[0] + '.gif'))
        if os.path.exists(x):
            pass
        else:
            print(f"Making gif: {x}")
            os.makedirs(os.path.dirname(x), exist_ok=True)
            make_gif(datafile=dat, save_file=x, verbose=True)




prerecorded_data = np.sort(glob.glob(os.path.join(master_config['save_path'], "*.h5")))
sp_js = os.path.join(master_config['figure_save_path'], f"{config['nozzle_id']}_jetspeed_plot.png")
sp_jd = os.path.join(master_config['figure_save_path'], f"{config['nozzle_id']}_jetdeviation_plot.png")
sp_dd = os.path.join(master_config['figure_save_path'], f"{config['nozzle_id']}_jetdiameter_plot.png")
sp_st = os.path.join(master_config['figure_save_path'], f"{config['nozzle_id']}_jetstability_plot.png")
stack_data = {}
for pdat in prerecorded_data:
    num = pdat.split('/')[-1]
    if os.path.exists(pdat):
        f = h5py.File(pdat, 'r')
        d = {}
        for k,v in f.items():
            if k != "2D_images":
                for ki,vi in v.items():
                    d[ki] = vi[()]
        stack_data[num] = d
        f.close()



gdvn_plots = ana.PlotJetData(nozzle_id=nozzle, stack_data=stack_data)
if len(stack_data) > 3:
    x = gdvn_plots.liquid_flow
    xr = (0,40)
    y = gdvn_plots.gas_flow
    yr = (0,85)
    if master_config['plot_jetspeed']:
        z = gdvn_plots.jet_speed
        zr = (0,250)
        d = gdvn_plots.jet_diameter * 1e6
        plot_phase_diagram(plots=gdvn_plots, savepath=sp_js, nozzle_id=nozzle,
                x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d,
                cbar_title="Jet Speed (m/s)", clip_negative=True)
    if master_config['plot_jetdeviation']:
        z = np.abs(gdvn_plots.jet_deviation)
        zr = (0,15)
        plot_phase_diagram(plots=gdvn_plots, savepath=sp_jd, nozzle_id=nozzle,
                            x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d,
                            cbar_title="Jet Deviation (Degree)")
    if master_config['plot_jetdiameter']:
        z = gdvn_plots.jet_diameter * 1e6
        zr = (0,4)
        plot_phase_diagram(plots=gdvn_plots, savepath=sp_dd, nozzle_id=nozzle,
                            x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d, level_step=25,
                            cbar_title=r"Jet Diameter [$\mu$m]")
    if master_config['plot_jetstability']:
        z = gdvn_plots.jet_stability
        zr = (0, 2)
        for i in range(len(z)):
            if z[i] is not None:
                if z[i] > 2:
                    z[i] = 2
                if z[i] < 0:
                    z[i] = 0
            plot_phase_diagram(plots=gdvn_plots, savepath=sp_st, nozzle_id=nozzle,
                                x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d, cmap_reverse=True,
                                cbar_title=r"Jet Stability")

    print(f"\tUpdated plot! -- \n\t\t\t {master_config['figure_save_path']}", flush=True)

fg = ana.LiveAnalysis(master_config)
os.makedirs(master_config['bad_data'], exist_ok=True)

live_analysis = True
idle_time = 0
while live_analysis:
    if fg.current_data is None:
        print("Waiting for a new file...", end="\r", flush=True)

    if fg.current_data is not None:
        run_number = fg.current_data.split(os.sep)[-1].split('.')[0]
        if config['make_gifs']:
            x = pathogen(os.path.join(master_config['gif_save_path'], fg.current_data.split(os.sep)[-1].split('.')[0] + '.gif'))
            print(f"\nMaking GIF: \n\t {x}", flush=True)
            make_gif(datafile=fg.current_data, save_file=x, verbose=True)

        print(f"\nAnalyzing Video: \n\t {fg.current_data}", flush=True)
        try:
            if config['interactive']:
                jetana_viewer = gdvn.AnalysisViewer(filepath=fg.current_data, config=master_config)
                jetana = jetana_viewer.jetana
                pg.mkQApp().exec_()
            else:
                s, m = dataio.h5_metadata(fg.current_data)
                try:
                    jetana = gdvn.JetAnalysis(image_stack=s, image_metadata=m, config=master_config)
                except:
                    print("Something was wrong with this dataset, moving to the bad folder.")
                    os.rename(fg.current_data, pathogen(os.path.join(master_config['bad_data'], run_number+'.h5')))
                    os.rename(pathogen(os.path.join(master_config['data_path'], run_number+'.txt')), 
                                pathogen(os.path.join(master_config['bad_data'], run_number+'.txt')))
                    jetana = None
        except OSError: # in case the file isn't fully transferred....
            time.sleep(10)
            jetana = None


        if jetana is not None:
            # update the master data dictionary -- will rewrite keys with the same name!
            vid = fg.current_data.split(os.sep)[-1]
            stack_data[vid] = jetana.to_dict()


            if config['interactive']:
                uinput = input("Save this dataset? [y/n]:  ")
            else:
                uinput = 'y'

            if (uinput == 'y') or (uinput == 'yes'):
                p = pathogen(os.path.join(master_config['save_path'], vid))
                print(f"\tSaved data here: \n\t\t\t {p} \n\n", flush=True)
                plot = True
                try:
                    jetana.to_h5(p)
                    fg.update_csv()
                except OSError:
                    print('h5 saving error!')

            elif (uinput == 'n') or (uinput == 'no'):
                uinput = input("Move to the bad data folder? [y/n]:  ")
                plot = False
                if (uinput == 'y') or (uinput == 'yes'):
                    print(os.path.join(master_config['bad_data'], run_number))
                    os.rename(fg.current_data, pathogen(os.path.join(master_config['bad_data'], run_number+'.h5')))
                    os.rename(pathogen(os.path.join(master_config['data_path'], run_number+'.txt')), 
                                pathogen(os.path.join(master_config['bad_data'], run_number+'.txt')))

            if plot:
                gdvn_plots.update(stack_data[vid]) # update the plots
                if len(stack_data) > 3:
                    x = gdvn_plots.liquid_flow
                    xr = (0,40)
                    y = gdvn_plots.gas_flow
                    yr = (0,85)
                    if master_config['plot_jetspeed']:
                        z = gdvn_plots.jet_speed
                        zr = (0,250)
                        d = gdvn_plots.jet_diameter * 1e6
                        plot_phase_diagram(plots=gdvn_plots, savepath=sp_js, nozzle_id=nozzle,
                                x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d,
                                cbar_title="Jet Speed (m/s)")
                    if master_config['plot_jetdeviation']:
                        z = np.abs(gdvn_plots.jet_deviation)
                        zr = (0,15)
                        plot_phase_diagram(plots=gdvn_plots, savepath=sp_jd, nozzle_id=nozzle,
                                            x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d,
                                            cbar_title="Jet Deviation (Degree)")
                    if master_config['plot_jetdiameter']:
                        z = gdvn_plots.jet_diameter * 1e6
                        zr = (0,4)
                        plot_phase_diagram(plots=gdvn_plots, savepath=sp_dd, nozzle_id=nozzle,
                                            x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d, level_step=25, 
                                            cbar_title=r"Jet Diameter [$\mu$m]")
                    if master_config['plot_jetstability']:
                        z = gdvn_plots.jet_stability
                        zr = (0, 2)
                        try:
                            for i in range(len(z)):
                                if z[i] > 2:
                                    z[i] = 2
                                if z[i] < 0:
                                    z[i] = 0
                            plot_phase_diagram(plots=gdvn_plots, savepath=sp_st, nozzle_id=nozzle,
                                            x=x, xr=xr, y=y, yr=yr, z=z, zr=zr, d=d, cmap_reverse=True,
                                            cbar_title=r"Jet Stability")
                        except TypeError:
                            print("Something's wrong with the plotting of jet stability. Skipping it for now.")
                    print(f"\tUpdated plot! -- \n\t\t\t {master_config['figure_save_path']}", flush=True)

            if config['interactive']:
                uinput = input("Continue to next run? [y/n]:  ")
            if (uinput == 'y') or (uinput == 'yes'):
                print("Moving on to next run...", flush=True)
            elif (uinput == 'n') or (uinput == 'no'):
                sys.exit()

        fg.update_data()

        idle_time = 0 # reset the idle clock
    else:
        fg.update_data()
        print(f"Waited {idle_time:04} seconds....", end="\r", flush=True)

    try: # delete the jetana initialization since the memory grows each loop for some reason
        del jetana
    except NameError:
        pass

    time.sleep(1)
    idle_time += 1
    if idle_time == 1800:
        print("System idle for 30 minutes... stopping the script. ", flush=True)
        sys.exit()


