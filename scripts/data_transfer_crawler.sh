#!/bin/bash

# What to move off of xfel1 (from within microjets)
dir='2021/20210803/'

# from xfel1 to analysis laptop
SOURCEDIR="raklab@10.206.48.2:/data/rawdata/microjets/./$dir"
DESTDIR="/home/kkarpos/rawdata_temp/microjets"

while [ true ]; do
    rsync -aRv --stats --progress $SOURCEDIR $DESTDIR
    sleep 10
done
