import sys
import os
import numpy as np
import glob
import time
import h5py
import matplotlib as mpl
import imageio
import matplotlib.pyplot as plt
import pyqtgraph as pg
from microjet_analysis import gdvn

import skimage
from microjet_analysis.viewers import ImageView
from microjet_analysis import dataio, filters
from microjet import jet_analysis

import scipy

import time

from microjet_analysis.viewers import image_view


filepath = "/Users/kkarpos/Desktop/NozzleData/20221214/79V1-DB-50G-50L-221205-02/054.h5" # darkfield, good data
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221215/79V1-DB-50G-50L-221205-02_G100/002.h5" # darkfield, sketchy data
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221104/282_79_water/005.h5"
# filepath = "/Users/kkarpos/Desktop/NozzleData/20230110/79M_AA_50L_100G/071.h5"
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221025/GN_glass_50_water/001.h5" # angled nozzle tip causing issues
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221025/R346_79_PSIBuffer/001.h5" # Brightfield
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221215/79V1-DB-50G-50L-221205-02_G100/036.h5" # darkfield, whipping
# filepath = "/Users/kkarpos/Desktop/NozzleData/20230201/79V1-DB-50G-50L-221206-04_glycerol_50/001.h5"
# filepath = "/Users/kkarpos/repos/projects/microjet_analysis/examples/example_data/example_binary.h5"


# the config with all the analysis options
config = dataio.analysis_config()
config['log_path'] = "/Users/kkarpos/Desktop/NozzleData/logs/sensirion_SLI-0430_FTSTT5NA"
config['filter'] = True
config['threshold'] = True
config['process'] = True

config['brightfield'] = False

config['threshold_method'] = 'hysteresis'
config['filter_method'] = 'gausshp'
if config['brightfield'] is True:
    config['threshold_parameters']['h_low'] = 0.15
    config['threshold_parameters']['h_high'] = 0.5 
else: 
    config['threshold_parameters']['h_low'] = 1
    config['threshold_parameters']['h_high'] = 3

config['nozzle_tip_finder_method'] = 'simple'
config['nozzle_tip_position'] = 900


config['processing_steps']['erode_1'] = False
config['processing_steps']['n_erosion_1_iteration'] = 1
config['processing_steps']['remove_small_objects'] = True
config['processing_steps']['min_pixel_size'] = 5
config['processing_steps']['binary_closing'] = False
config['processing_steps']['n_closing_iterations'] = 1
config['processing_steps']['erode_2'] = False
config['processing_steps']['n_erosion_2_iteration'] = 1

config['processing_steps']['fill_holes'] = True


# load the data and analyze

stack, metadata = dataio.h5_metadata(filepath)
for i in range(5):
    jetana = jet_analysis.ImageProcess(stack[i], config=config)
    pg.image(jetana._processed[0])
    pg.mkQApp().exec_()










