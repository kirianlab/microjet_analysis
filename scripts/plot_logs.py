from PyQt5 import QtWidgets
import pyqtgraph as pg
import numpy as np
import pandas as pd
import glob
from loggers import LogParser
import loggers
import os


class LogGui(QtWidgets.QWidget):
    def __init__(self, logpath:str="/Users/kkarpos/Desktop/logs"):
        super().__init__()

        self.epoch_start, self.epoch_stop = 1689276910, 1689367226
        self.logpath = logpath

        # Initialize GUI components
        self.init_ui()

        # Default paths (placeholders)
        self.bronkhorst_path = "./logs/bronkhorst_EL-Flow_COM4/data.log*"
        self.sensirion_path = "./logs/sensirion_SLI-0430_FTSTT5NA/data.log*"
        self.hplc_path = "./logs/shimadzu_LC-20AD_192.168.200.99/data.log*"

        # Connect to the viewRangeChanged signal
        self.left_plot.sigRangeChanged.connect(self.on_range_changed)
        self.right_plot.sigRangeChanged.connect(self.on_range_changed)


        self.hplc_plot_item = pg.PlotDataItem(pen=pg.mkPen('b', width=2))
        self.left_plot.addItem(self.hplc_plot_item)

        # Create PlotDataItems for Sensirion and HPLC data (initialized empty for now)
        self.sensirion_plot_item = pg.PlotDataItem(pen=None, symbol='o', symbolBrush='b', symbolSize=3)
        self.left_plot.addItem(self.sensirion_plot_item)
        


        self.bronk_plot_item = pg.PlotDataItem(pen=None, symbol='o', symbolBrush='g', symbolSize=3)
        self.right_plot.addItem(self.bronk_plot_item)

        self.update_plots()

    def init_ui(self):
        # Set Layout
        layout = QtWidgets.QVBoxLayout()

        # Timestamp input field
        self.timestamp_layout = QtWidgets.QHBoxLayout()
        self.start_timestamp_label = QtWidgets.QLabel("Start Timestamp:")
        self.start_timestamp_input = QtWidgets.QLineEdit()
        self.start_timestamp_input.setText(str(self.epoch_start))  # Set default value
        self.end_timestamp_label = QtWidgets.QLabel("End Timestamp:")
        self.end_timestamp_input = QtWidgets.QLineEdit()
        self.end_timestamp_input.setText(str(self.epoch_stop))    # Set default value
        self.update_button = QtWidgets.QPushButton("Update")
        self.update_button.clicked.connect(self.update_plots)
        
        self.timestamp_layout.addWidget(self.start_timestamp_label)
        self.timestamp_layout.addWidget(self.start_timestamp_input)
        self.timestamp_layout.addWidget(self.end_timestamp_label)
        self.timestamp_layout.addWidget(self.end_timestamp_input)
        self.timestamp_layout.addWidget(self.update_button)
        
        layout.addLayout(self.timestamp_layout)
        
        # Create 1x2 grid of plots
        self.plot_widget = pg.GraphicsLayoutWidget()
        
        self.left_plot = self.plot_widget.addPlot(title="Sensirion & HPLC Data")
        self.left_plot.setLabel('left', "Liquid Flow Rate", units='μL/min')

        self.plot_widget.nextRow()
        self.right_plot = self.plot_widget.addPlot(title="Bronkhorst Gas Mass Flow")
        self.right_plot.setLabel('left', "Gas Mass Flow Rate", units='mg/min')

        # Link x-axes of the plots
        self.right_plot.setXLink(self.left_plot)

        layout.addWidget(self.plot_widget)
        self.setLayout(layout)

    def on_range_changed(self):
        """Called whenever the view range (zoom level) changes."""
        
        # Get the current view range
        x_range, y_range = self.left_plot.viewRange()  # For the left plot, similar for the right plot

        # Decimate the Sensirion data based on the current view range
        decimated_sens_data = self.decimate_data(self.sens_df, x_range, N=1000)
        
        # Update the sensirion_plot_item data
        if not decimated_sens_data.empty:
            self.sensirion_plot_item.setData(decimated_sens_data['epoch_time'], decimated_sens_data['flow_rate'])
        else:
            print("No Sensirion data to display for the current view range.")
        
        # If you want to apply similar decimation to HPLC data:
        decimated_hplc_data = self.decimate_data(self.hplc_df, x_range, N=1000)
        if not decimated_hplc_data.empty:
            self.hplc_plot_item.setData(decimated_hplc_data['epoch_time'], decimated_hplc_data['flow_setpoint'] * 1000)
        else:
            print("No HPLC data to display for the current view range.")


    def decimate_data(self, data, x_range, N=1000):
        """Decimate the data to have a maximum of N points."""
        
        # Determine the indices to keep based on the current view range and the desired number of points
        start, stop = x_range
        mask = (data['epoch_time'] >= start) & (data['epoch_time'] <= stop)
        filtered_data = data[mask]
        
        step = max(1, len(filtered_data) // N)
        decimated_data = filtered_data.iloc[::step]
        
        # Reset the index
        return decimated_data.reset_index(drop=True)

    def load_all_logs(self):
        
        # load the corresponding bronkhorst logs
        try:
            bronk_parser = LogParser(os.path.join(self.logpath, "bronk*/data.log*"))
            bronk_parser.load_time_range(self.epoch_start, self.epoch_stop)
            self.bronk_df = bronk_parser.df
        except TypeError:
            print("No bronkhorst logs! Returning None for this.")
            self.bronk_df = None

        # load the corresponding sensirion logs
        sens_parser = LogParser(os.path.join(self.logpath, "sensirion_SLI-0430*/data.log*"))
        sens_parser.load_time_range(self.epoch_start, self.epoch_stop)
        self.sens_df = sens_parser.df

        hplc_parser = LogParser(os.path.join(self.logpath, "shimadzu_LC-20AD_*/data.log*"))
        hplc_parser.load_time_range(self.epoch_start, self.epoch_stop)
        hplc_df = hplc_parser.df
        hplc_df['flow_setpoint'] = pd.to_numeric(hplc_df['flow_setpoint'], errors='coerce') 
        hplc_df['epoch_time'] = pd.to_numeric(hplc_df['epoch_time'], errors='coerce')
        self.hplc_df = hplc_df

    def update_plots(self):
        start_timestamp = int(self.start_timestamp_input.text())
        end_timestamp = int(self.end_timestamp_input.text())

        # Ensure start time is before end time
        if start_timestamp > end_timestamp:
            start_timestamp, end_timestamp = end_timestamp, start_timestamp
            # Update the input fields with corrected values
            self.start_timestamp_input.setText(str(start_timestamp))
            self.end_timestamp_input.setText(str(end_timestamp))

        # Update epoch_start and epoch_stop
        self.epoch_start = start_timestamp
        self.epoch_stop = end_timestamp
        
        # Try loading the logs and handle potential error
        try:
            self.load_all_logs()
        except TypeError:
            print("No data found for this time range.")
            return

        # Update data for Sensirion PlotDataItem
        if self.sens_df is not None:
            self.sensirion_plot_item.setData(self.sens_df['epoch_time'], self.sens_df['flow_rate'])

        # Update data for HPLC PlotDataItem
        if self.hplc_df is not None:
            self.hplc_plot_item.setData(self.hplc_df['epoch_time'], self.hplc_df['flow_setpoint'] * 1000)

        # Plot Bronkhorst data on the right plot
        if self.bronk_df is not None:
            self.bronk_plot_item.setData(self.bronk_df['epoch_time'], self.bronk_df['flow_rate'])

        # Display warning messages if data is not available
        if self.sens_df is None:
            print("No logs available for Sensirion in this time range.")
        
        if self.bronk_df is None:
            print("No logs available for Bronkhorst in this time range.")

        if self.hplc_df is None:
            print("No logs available for HPLC in this time range.")


        # Auto-scale the plots
        self.left_plot.autoRange()
        self.right_plot.autoRange()




if __name__ == "__main__":

    app = QtWidgets.QApplication([])  # Create an instance of QApplication
    logshow = LogGui()
    logshow.show()
    app.exec_()  # Start the event loop



















