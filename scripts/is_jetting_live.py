import numpy as np
import matplotlib.pyplot as plt
from matplotlib import tri, colors
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from loggers import LogParser
import loggers
import os
from matplotlib.ticker import MaxNLocator
from scipy.stats import binned_statistic_dd

pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))


class GDVNPlots:
    _epoch_start, _epoch_stop = 0, 0
    _time_range = (0,0)
    def __init__(self, datasheet, time_range, logpath):
        self.all_data = pd.read_csv(datasheet)
        self.all_data['stability'] = pd.to_numeric(self.all_data['stability'], errors='coerce')
        self.model = None
        self.fit_pressure_model()
        self._time_range = time_range
        self.epoch_start, self.epoch_stop = self._time_range
        self.data = self.all_data[(self.all_data['epoch_time_start'] >= self.epoch_start) & (self.all_data['epoch_time_stop'] <= self.epoch_stop)]
        self.logpath = logpath
        self.load_all_logs()
        self.get_corr_logs()



    @property
    def time_range(self):
        return self._time_range

    @time_range.setter
    def time_range(self, time_range):
        self._time_range = time_range
        self.epoch_start, self.epoch_stop = self._time_range
        self.data = self.all_data[(self.all_data['epoch_time_start'] >= self.epoch_start) & (self.all_data['epoch_time_stop'] <= self.epoch_stop)]
        self.load_all_logs()
        self.get_corr_logs()
    
    def load_time_range(self, is_jet_avg=30):
        self.data = self.all_data[(self.all_data['epoch_time_start'] >= self.epoch_start) & (self.all_data['epoch_time_stop'] <= self.epoch_stop)]
        self.get_corr_logs(is_jet_avg=is_jet_avg)
        # self.load_all_logs()

    def load_all_logs(self):

        # load the sfa logs
        sfa_parser = LogParser(os.path.join(self.logpath, "single_frame_analysis/logs/*.csv"))
        sfa_parser.load_time_range(self.epoch_start, self.epoch_stop)
        epoch_time = sfa_parser.df["epoch_time"].values
        self._is_jet = sfa_parser.df["is_jet"].values
        self._jet_length = sfa_parser.df["jet_length_mean"].values
        self.logs_df = sfa_parser.df

        # load the corresponding bronkhorst logs
        try:
            bronk_parser = LogParser(os.path.join(self.logpath, "bronk_EL-Flow_COM4/data"))
            bronk_parser.load_time_range(self.epoch_start, self.epoch_stop)
            df = bronk_parser.df
            self._gas = np.interp(epoch_time, df["epoch_time"].values, df["flow_rate"].values)
        except TypeError:
            print("No bronkhorst logs! Returning None for this.")
            return None

        # load the corresponding sensirion logs
        sens_parser = LogParser(os.path.join(self.logpath, "sensirion_SLI-0430*/data"))
        sens_parser.load_time_range(self.epoch_start, self.epoch_stop)
        df = sens_parser.df
        self._sens_high_flow = np.interp(epoch_time, df["epoch_time"].values, df["flow_rate"].values)

        self._times = (epoch_time - np.min(epoch_time)) / 60

    def get_corr_logs(self, is_jet_avg=30):
        # Convert is_jet to a pandas Series to use the rolling function
        is_jet_series = pd.Series(self._is_jet)
        # Create a new boolean array that is True if the last N points are all True
        av = is_jet_series.rolling(is_jet_avg).apply(all).fillna(False).values
        self._is_jet_idx = np.where(av == True)
        self._no_jet_idx = np.where(av == False)

    def plot_is_jetting(self):
        self.load_time_range()
        liq = self._sens_high_flow
        gas = self._gas
        is_jet_idx = self._is_jet_idx
        no_jet_idx = self._no_jet_idx
        norm = colors.Normalize(vmin=0, vmax=120)
        cmap = plt.cm.plasma  # choose a colormap
        fig, ax = plt.subplots()
        ax.scatter(liq[no_jet_idx], gas[no_jet_idx], c='gray', marker="x", s=1, alpha=0.1, label="No Jet")
        ax.scatter(liq[is_jet_idx], gas[is_jet_idx], c='green', marker=".", s=1, alpha=0.25, label="Jet")
        # ax.scatter(self.data['liquid_flow_mean'], self.data['gas_flow'], 
        #                 c=cmap(norm(self.data['jet_speed'])), edgecolors='black', 
        #                 linewidths=0.5, marker="o", s=self.data['jet_diameter']*4e6, label="Recorded Data Point")
        ax.set_ylabel("Gas Flow (mg/min)")
        ax.set_xlabel("Liquid Flow (ul/min)")
        ax.set_xlim(0,60)
        ax.set_ylim(0,80)
        # ax.legend()
        plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, label='Jet Speed')
        plt.show()

    def fit_pressure_model(self, pressure_data_file="pressure_at_the_tip/data/PressureTest_79_atmo.csv", degree=2):
        data = pd.read_csv(pressure_data_file)
        X = data[['Gas Flow Rate [mg/min]', 'Gas Capillary Length [m]']]
        y = data['Pressure Reading [psi]']
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        self.model = make_pipeline(PolynomialFeatures(degree), LinearRegression())
        self.model.fit(X_train, y_train)

    def predict_pressure(self, gas_flow_rate, capillary_length):
        if self.model is None:
            raise Exception("Pressure model is not fit yet. Call fit_pressure_model first.")
        input_features = pd.DataFrame({'Gas Flow Rate [mg/min]': gas_flow_rate, 'Gas Capillary Length [m]': capillary_length})
        return self.model.predict(input_features)

    def predict_jet_speed(self, predicted_pressure):
        rho_water = 997  # density of water in kg/m^3
        predicted_pressure_pa = predicted_pressure * 6894.76  # convert from psi to Pa
        expected_jet_speed = np.sqrt((2 * predicted_pressure_pa) / rho_water)
        return expected_jet_speed

    def interpolate_ranges(self, ranges, data, level_step: int = 2):
        n_levels = int((ranges[2][1] - ranges[2][0]) / level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(n_levels + 20))

        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        interpolator = tri.LinearTriInterpolator(tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels

    def _get_cbar_label(self, z_key):
        if z_key == "jet_speed":
            return "Jet Speed [m/s]"
        elif z_key == "stability" or z_key == "jet_stability":
            return "Jet Stability"
        elif z_key == "jet_diameter":
            return r"Jet Diameter [$\mu$m]"
        elif z_key == "jet_length":
            return r"Jet Length [$\mu$m]"

    def plot_liquid_vs_jetspeed(self, xr=(0, 20), yr=(0, 120), zr=(0, 90)):
        data = self.data

        x = data['liquid_flow_mean'].to_numpy()
        y = data['jet_speed'].to_numpy()
        z = data['gas_flow'].to_numpy()

        # Clip the z values between the specified range
        z = np.clip(z, zr[0], zr[1])

        # Find the indices where z is not zero
        zidx = np.where(z != 0)[0].tolist()

        fig, ax = plt.subplots()
        cmap = plt.get_cmap('RdBu_r')

        # Use the model to predict the jet speed at the corresponding gas flow rate for each data point
        predicted_speeds = self.predict_jet_speed(self.predict_pressure(z, 0))

        # Plot the predicted jet speeds against the liquid flow rate
        ax.scatter(x, predicted_speeds, c=cmap((z[zidx]-zr[0])/(zr[1]-zr[0])), s=10, 
                    marker='X', label=r'Predicted from $\Delta$P', edgecolors=(0, 0, 0), linewidths=0.3)


        im = ax.scatter(x, y, c=cmap((z[zidx]-zr[0])/(zr[1]-zr[0])), s=data['jet_diameter'].iloc[zidx]*4e6, 
                        edgecolors=(0, 0, 0), linewidths=0.3, marker='o', label="Calculated from Images")
        ax.set(xlim=xr, ylim=yr)
        ax.set_xlabel(r'Liquid Flow Rate [$\mu$l/min]')
        ax.set_ylabel('Jet Speed [m/s]')
        fig.suptitle("Nozzle ID: " + str(data['nozzle_id'].iloc[0]))
        norm = colors.Normalize(vmin=zr[0], vmax=zr[1])  # normalization for the colormap
        fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, label='Gas Mass Flow Rate')
        ax.grid(alpha=0.5)
        ax.legend()


        # Show the plot
        plt.show()


    def plot_phase_diagram(self, z_key='jet_speed', xr=(0, 20), yr=(0, 60), zr=(0, 200)):
        self.load_time_range()
        data = self.data

        # Prepare the data
        x = data['liquid_flow_mean'].to_numpy()
        y = data['gas_flow'].to_numpy()
        if z_key == 'stability' or z_key == 'jet_stability':
            z = 1 / data['jet_stability'].to_numpy()
        elif z_key == 'jet_diameter' or z_key == 'diameter':
            z = data['jet_diameter'].to_numpy() * 1e6
        else:
            z = data[z_key].to_numpy()

        # Clip the z values between the specified range
        z = np.clip(z, zr[0], zr[1])

        # Find the indices where z is not zero
        zidx = np.where(z != 0)[0].tolist()

        # Call the interpolation function
        xi, yi, zi, levels = self.interpolate_ranges(ranges=[xr, yr, zr], data=[x[zidx], y[zidx], z[zidx]])

        # Prepare the plot
        fig, ax = plt.subplots(1,1, tight_layout=True)
        cmap = plt.get_cmap('RdBu_r')
        contourf = ax.contourf(xi, yi, zi, levels=levels, cmap=cmap)
        cbar_label = self._get_cbar_label(z_key)

        cbar = fig.colorbar(contourf, ax=ax, label=cbar_label)
        cbar.locator = MaxNLocator(integer=True)  # Force integer values on the colorbar
        cbar.update_ticks()  # Update the colorbar ticks

        # Scatter plot
        ax.scatter(x[zidx], y[zidx], marker='o', s=data['jet_diameter'].iloc[zidx]*4e6, 
                   color=cmap((z[zidx]-zr[0])/(zr[1]-zr[0])),
                   edgecolors=(0, 0, 0), linewidths=0.3)
        ax.set(xlim=xr, ylim=yr)
        ax.set_xlabel('Liquid Flow Rate [μl/min]')
        ax.set_ylabel('Gas Mass Flow Rate [mg/min]')
        fig.suptitle("Nozzle ID: " + str(data['nozzle_id'].iloc[0]))

        # Show the plot
        plt.show()

    def binned_stats(self, value, mask, stat='mean'):
        # Apply the mask to the sample and value arrays
        masked_sample = self._times[mask].reshape(-1, 1)
        masked_value = value[mask]

        # Bin the 'is_jet' variable by time intervals and calculate the mean value in each bin
        bins = [np.linspace(np.min(self._times), np.max(self._times), int(len(self._is_jet)/5))]  # define N time bins
        statistic, bin_edges, bin_number = binned_statistic_dd(masked_sample, 
                                                                masked_value, 
                                                                statistic=stat, 
                                                                bins=bins)
        # Calculate the center of each time bin
        bin_centers = 0.5 * (bin_edges[0][1:] + bin_edges[0][:-1])

        # Map the binned statistic values back to the original data points
        binned_statistic_values = statistic[bin_number - 1]

        return binned_statistic_values, bin_centers

    def plot_jetlength_logs(self, N=10, smoothing_window=50, xlim=(0,100), ylim=(0,80), **kwargs):
        """
        Plots the 2D histogram of 'is_jet' values based on the flow rate data.
        
        The method uses the internal data attributes `_sens_high_flow`, `_gas`, and `_is_jet` to compute 
        a 2D histogram. It uses a moving average to smooth the `_sens_high_flow` data and then computes 
        the histogram based on custom bin edges. The resulting histogram is displayed using the `pcolormesh` 
        function from matplotlib.

        Args:
            N (int, optional): Determines the number of last data points in the smoothed data to check for 
                increasing values. Defaults to 10.
            smoothing_window (int, optional): Size of the moving average window used to smooth the `_sens_high_flow` data. 
                Defaults to 50.
            **kwargs: Arbitrary keyword arguments. These are passed directly to the `plt.pcolormesh` function. 
                The following are commonly used:
                    - xlim (tuple): Tuple of the form (xmin, xmax) specifying the x-axis limits. Defaults to (0, 30).
                    - ylim (tuple): Tuple of the form (ymin, ymax) specifying the y-axis limits. Defaults to (0, 80).
                    - vmin (float): Minimum data value that corresponds to the first color in the colormap. Defaults to 0.
                    - cmap (Colormap): A Colormap instance or registered colormap name. Defaults to 'inferno'.

        Returns:
            None

        Example:
            >>> object_name.plot_isjet(N=15, smoothing_window=7, cmap='inferno', xlim=(5, 50))

        Note:
            Ensure that the data attributes `_sens_high_flow`, `_gas`, and `_is_jet` are properly initialized 
            before calling this method.
        """
        self.load_time_range()
        # Extract relevant data
        x = self._sens_high_flow
        y = self._gas
        z = self._jet_length

        # Convert boolean values to integer for mean computation
        z_int = [int(val) for val in z]

        # Smooth the x data using moving average
        x_smoothed = moving_average(x, smoothing_window)

        # Adjust other data arrays to match the length of the smoothed array
        y_adjusted = y[smoothing_window-1:]
        z_int_adjusted = z_int[smoothing_window-1:]
        x_adjusted = x[smoothing_window-1:]

        # Create a mask based on increasing values over the last N points in the smoothed data
        mask = [np.all(np.diff(x_smoothed[max(0, i-N):i+1]) > 0) for i in range(len(x_smoothed))]

        # Filter the data based on the mask
        x_filtered = np.array(x_adjusted)[mask]
        y_filtered = np.array(y_adjusted)[mask]
        z_filtered = np.array(z_int_adjusted)[mask]

        # Define custom bin edges for x-axis (liquid flow rates)
        x_bin_edges = np.concatenate([
                                        np.linspace(min(x_filtered), 0.1*max(x_filtered), 40),
                                        np.linspace(0.1*max(x_filtered), 0.25*max(x_filtered), 25),
                                        np.linspace(0.25*max(x_filtered), max(x_filtered), 5)
                                        ])
        y_bin_edges = np.linspace(min(y_filtered), max(y_filtered), 10)

        # Compute the 2D histograms
        histogram, _, _, = np.histogram2d(x_filtered, y_filtered, bins=[x_bin_edges, y_bin_edges])
        weighted_histogram, _, _, = np.histogram2d(x_filtered, y_filtered, bins=[x_bin_edges, y_bin_edges], weights=z_filtered)

        # Compute mean is_jet values for each bin
        with np.errstate(divide='ignore', invalid='ignore'):  # Handle division by zero
            mean_is_jet = np.where(histogram > 0, weighted_histogram / histogram, np.nan)

        # Extract or set default values for keyword arguments
        vmin = kwargs.get('vmin', 0)
        cmap = kwargs.get('cmap', plt.get_cmap('inferno'))

        # Plot the 2D histogram using pcolormesh
        plt.pcolormesh(x_bin_edges, y_bin_edges, mean_is_jet.T, cmap=cmap, vmin=vmin, **kwargs)
        plt.colorbar(label=r'Jet Length [$\mu$m]')
        plt.xlabel(r'Liquid Flow Rate [$\mu$L/min]')
        plt.ylabel('Gas Mass Flow Rate [mg/min]')
        plt.xlim(*xlim)
        plt.ylim(*ylim)
        plt.show()

    def plot_isjet(self, N=10, smoothing_window=50, **kwargs):
        """
        Plots the 2D histogram of 'is_jet' values based on the flow rate data.
        
        The method uses the internal data attributes `_sens_high_flow`, `_gas`, and `_is_jet` to compute 
        a 2D histogram. It uses a moving average to smooth the `_sens_high_flow` data and then computes 
        the histogram based on custom bin edges. The resulting histogram is displayed using the `pcolormesh` 
        function from matplotlib.

        Args:
            N (int, optional): Determines the number of last data points in the smoothed data to check for 
                increasing values. Defaults to 10.
            smoothing_window (int, optional): Size of the moving average window used to smooth the `_sens_high_flow` data. 
                Defaults to 50.
            **kwargs: Arbitrary keyword arguments. These are passed directly to the `plt.pcolormesh` function. 
                The following are commonly used:
                    - xlim (tuple): Tuple of the form (xmin, xmax) specifying the x-axis limits. Defaults to (0, 30).
                    - ylim (tuple): Tuple of the form (ymin, ymax) specifying the y-axis limits. Defaults to (0, 80).
                    - vmin (float): Minimum data value that corresponds to the first color in the colormap. Defaults to 0.
                    - cmap (Colormap): A Colormap instance or registered colormap name. Defaults to 'inferno'.

        Returns:
            None

        Example:
            >>> object_name.plot_isjet(N=15, smoothing_window=7, cmap='inferno', xlim=(5, 50))

        Note:
            Ensure that the data attributes `_sens_high_flow`, `_gas`, and `_is_jet` are properly initialized 
            before calling this method.
        """
        self.load_time_range()
        # Extract relevant data
        x = self._sens_high_flow
        y = self._gas
        z = self._is_jet

        # Convert boolean values to integer for mean computation
        z_int = [int(val) for val in z]

        # Smooth the x data using moving average
        x_smoothed = moving_average(x, smoothing_window)

        # Adjust other data arrays to match the length of the smoothed array
        y_adjusted = y[smoothing_window-1:]
        z_int_adjusted = z_int[smoothing_window-1:]
        x_adjusted = x[smoothing_window-1:]

        # Create a mask based on increasing values over the last N points in the smoothed data
        mask = [np.all(np.diff(x_smoothed[max(0, i-N):i+1]) > 0) for i in range(len(x_smoothed))]

        # Filter the data based on the mask
        x_filtered = np.array(x_adjusted)[mask]
        y_filtered = np.array(y_adjusted)[mask]
        z_filtered = np.array(z_int_adjusted)[mask]

        # Define custom bin edges for x-axis (liquid flow rates)
        x_bin_edges = np.concatenate([
                                        np.linspace(min(x_filtered), 0.1*max(x_filtered), 40),
                                        np.linspace(0.1*max(x_filtered), 0.25*max(x_filtered), 25),
                                        np.linspace(0.25*max(x_filtered), max(x_filtered), 5)
                                        ])
        y_bin_edges = np.linspace(min(y_filtered), max(y_filtered), 10)

        # Compute the 2D histograms
        histogram, _, _, = np.histogram2d(x_filtered, y_filtered, bins=[x_bin_edges, y_bin_edges])
        weighted_histogram, _, _, = np.histogram2d(x_filtered, y_filtered, bins=[x_bin_edges, y_bin_edges], weights=z_filtered)

        # Compute mean is_jet values for each bin
        with np.errstate(divide='ignore', invalid='ignore'):  # Handle division by zero
            mean_is_jet = np.where(histogram > 0, weighted_histogram / histogram, np.nan)

        # Extract or set default values for keyword arguments
        xlim = kwargs.get('xlim', (0, 30))
        ylim = kwargs.get('ylim', (0, 80))
        vmin = kwargs.get('vmin', 0)
        cmap = kwargs.get('cmap', plt.get_cmap('inferno'))

        # Plot the 2D histogram using pcolormesh
        plt.pcolormesh(x_bin_edges, y_bin_edges, mean_is_jet.T, cmap=cmap, vmin=vmin, **kwargs)
        plt.colorbar(label='Mean Jetting Value')
        plt.xlabel(r'Liquid Flow Rate [$\mu$L/min]')
        plt.ylabel('Gas Mass Flow Rate [mg/min]')
        plt.xlim(*xlim)
        plt.ylim(*ylim)
        plt.show()


    def plot_timeseries(self, value, cbar_title="", stat='mean', is_jet_avg=30):
        self.load_time_range(is_jet_avg=120)
                # Create masks for jetting and non-jetting data
        is_jet_mask = np.array(self._is_jet, dtype=bool)
        no_jet_mask = ~is_jet_mask

        # Calculate binned statistics for jetting and non-jetting data
        jet_stat, jet_bin_centers = self.binned_stats(value=value, mask=is_jet_mask, stat=stat)
        no_jet_stat, no_jet_bin_centers = self.binned_stats(value=value, mask=no_jet_mask, stat=stat)
        cmap = plt.cm.coolwarm_r

        fig, ax = plt.subplots()

        # Use self._times for color data
        sc_no_jet = ax.scatter(self._sens_high_flow[no_jet_mask], self._gas[no_jet_mask], 
                               c=self._times[no_jet_mask], s=30, cmap=cmap, alpha=0.25, 
                               marker='x')

        sc_jet = ax.scatter(self._sens_high_flow[is_jet_mask], self._gas[is_jet_mask], 
                            c=self._times[is_jet_mask], s=30, cmap=cmap, alpha=0.5, 
                            marker='.', edgecolors=(0, 0, 0), linewidths=0.1)

        ax.scatter(self.data['liquid_flow_mean'], self.data['gas_flow'], c='black', 
                    marker="o", s=15, label="Recorded Data Point")

        ax.set_xlim(-1)
        # Adjust the colormap normalization to the range of self._times
        norm = colors.Normalize(vmin=np.min(self._times), vmax=np.max(self._times))
        fig.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, label=cbar_title)
        ax.set_ylabel("Gas Mass Flow [mg/min]")
        ax.set_xlabel(r"Liquid Volumetric Flow Rate [$\mu$L/min]")
        ax.grid(alpha=0.5)
        plt.show()

    def update_to_latest_timestamp(self):
        """Updates the time range of GDVNPlots with the latest UNIX timestamp from the logs."""
        
        # load the sfa logs
        sfa_parser = LogParser(os.path.join(self.logpath, "single_frame_analysis/logs/*.csv"))
        latest_timestamp = sfa_parser.get_latest_timestamp()
        print(latest_timestamp)
        # Update the time range of the GDVNPlots instance with the latest timestamp
        if latest_timestamp:
            self.time_range = (self._epoch_start, latest_timestamp)

    

def moving_average(data, window_size):
    """Compute moving average of a 1D array."""
    return np.convolve(data, np.ones(window_size)/window_size, mode='valid')



if __name__ == "__main__":

    datapath = pathogen("/data/microjets/2023/20230908/63_DD_01_analaysis/63_DD_01.csv")
    logpath = pathogen("/data/microjets/logs")
    # logpath = "data/master_logs.csv"
    # tp = [1689276910, 1689367226] # 177R_63, Water (has all logs)
    tp = [1689091288, 1689201955] # 322R_79 # No bronkhorst logs for this data?
    # tp = [1689381413, 1689404037]



    gp = GDVNPlots(datapath, time_range=tp, logpath=logpath)
    # self.plot_phase_diagram(xr=(0,100), yr=(0,90), zr=(0,200))
    # self.plot_liquid_vs_jetspeed(xr=(0,100), yr=(0,120), zr=(0,90))








