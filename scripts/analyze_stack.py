import numpy as np
import matplotlib.pyplot as plt
import os, warnings, logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from microjet import filters, regions, process, dataio, measure, gdvn, gdvnview, figures
import pyqtgraph as pg
# from viewers.ImageStackViewer import ImageStackViewer


r"""
    This script analyzes a single image stack using the microjet.gdvn module and displays the results.

    To Do: Create a GUI for this script.

"""

base_path = "/Volumes/kkarpos_ES/PhD_Work/NozzleData/"
year = "2023"
date = "20230110"
config = {'sample': 'water',
            "nozzle_id": "79M_AA_50L_100G",
            "file_id": "001.h5"}



# the config with all the analysis options
analysis_config = dataio.config()

# Set the nozzle tip position - this is now a required parameter
# It's advisable to view your data stack before running the analysis 
# and determine the nozzle tip position
analysis_config['nozzle_tip_position'] = 912

# Set the filter method, default is 'gausshp'
analysis_config['filter_method'] = 'gausshp'
analysis_config['filter_parameters']['sigma'] = 10

# Set the thresholding method and parameters
analysis_config['threshold_method'] = 'hysteresis'
# The hysteresis parameters of 2 and 5 seem to work best for dark field, 
# numbers under 1 work best for bright field
analysis_config['threshold_parameters']['h_low'] = 1
analysis_config['threshold_parameters']['h_high'] = 2

analysis_config['outer_product_method'] = 'centroid'
analysis_config['droplet_area_threshold'] = 5

analysis_config['cross_correlation_projection_threshold'] = 0.2

analysis_config['parallelize'] = True
analysis_config['process_stack'] = True

# Set the number of cores used in the processing steps
# Keep in mind that setting n_cores to the max doesn't necessarily 
# mean faster. There is some overhead when creating the parallel 
# processing, and for this application it may make the processing slower
analysis_config['n_cores'] = 7

config.update(analysis_config)

file_path = os.path.join(base_path, year, date, config['nozzle_id'], config['file_id'])
image_stack, metadata = dataio.open_data(file_path)
log_path = os.path.join(base_path, "logs")

jana = gdvn.JetAnalysis(image_stack, metadata, log_path=log_path, config=config)

# for k, v in jana.output_dict().items():
#     print(f"{k}:  ", v)
#     print("\n")




figures.birds_eye_view_binary(jana.image_stack, 
                                savefig=False, 
                                ntip_idx=jana.config['nozzle_tip_position'], 
                                treg_idx=jana.transition_region_idx, 
                                separation=20, mod=1, dpi=300)























