import sys
import os
import numpy as np
import glob
import time
import h5py
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QLabel, QWidget, QHBoxLayout, QTableWidget, QTableWidgetItem, QHeaderView

# from microjet_analysis.microjet 
import regions, filters, dataio, utils, liveanalysis, measure
from process import ImageProcess, MedianSubtractionFilter, RunningStats

import scipy
import time

r"""

Use this script to troubleshoot! Change the filepath parameter and
update the config if you want to change the thresholding (etc) parameters.

"""
pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))

# Add your filepath here! Use the same format and variable name as below, just change your path 

# filepath = "/Users/kkarpos/Desktop/NozzleData/20221214/79V1-DB-50G-50L-221205-02/054.h5" # darkfield, good data
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221215/79V1-DB-50G-50L-221205-02_G100/002.h5" # darkfield, sketchy data
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221104/282_79_water/005.h5"
# filepath = "/Users/kkarpos/Desktop/NozzleData/20230110/79M_AA_50L_100G/071.h5"
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221025/GN_glass_50_water/001.h5" # angled nozzle tip causing issues
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221025/R346_79_PSIBuffer/001.h5" # Brightfield
# filepath = "/Users/kkarpos/Desktop/NozzleData/20221215/79V1-DB-50G-50L-221205-02_G100/036.h5" # darkfield, whipping
# filepath = "/Users/kkarpos/Desktop/NozzleData/20230201/79V1-DB-50G-50L-221206-04_glycerol_50/001.h5"
# filepath = "/Users/kkarpos/repos/projects/microjet_analysis/examples/example_data/example_binary.h5"
filepath = pathogen("D:/data/microjets/2023/20230712/322R_79/189.h5")

process_config = dataio.process_config()
process_config['brightfield'] = True
process_config['filter'] = True
process_config['threshold'] = True
process_config['process'] = True

process_config['filter_method'] = 'gausshp'
process_config['threshold_method'] = 'hysteresis'

process_config['filter_parameters']['sigma'] = 4
process_config['filter_parameters']['truncate'] = 4

if process_config['brightfield'] is True:
    process_config['threshold_parameters']['h_low'] = 3 #0.15
    process_config['threshold_parameters']['h_high'] = 5 #0.5 
else: 
    process_config['threshold_parameters']['h_low'] = 1
    process_config['threshold_parameters']['h_high'] = 3

process_config['processing_steps']['remove_small_objects'] = True
process_config['processing_steps']['min_pixel_size'] = 5
process_config['processing_steps']['binary_closing'] = True
process_config['processing_steps']['n_closing_iterations'] = 1
process_config['processing_steps']['fill_holes'] = True
process_config['processing_steps']['inflate_small_pixels'] = False
process_config['processing_steps']['inflate_amount'] = 1
process_config['processing_steps']['erode_1'] = False
process_config['processing_steps']['n_erosion_1_iteration'] = 1
process_config['processing_steps']['erode_2'] = False
process_config['processing_steps']['n_erosion_2_iteration'] = 1


regions_config = dataio.jetregions_config()
regions_config['nozzle_tip_finder_method'] = 'column' # 'column', 'intensity', or 'provided'
regions_config['provided_nozzle_tip_idx'] = None      # only works if above is 'provided'
regions_config['transition_region_finder_method'] = 'nonzero' # 'row_diff', 'nonzero', or 'provided'
regions_config['provided_transition_region_idx'] = None       # only works if above is 'provided'




# # the config with all the analysis options
# config = dataio.analysis_config()
# config['log_path'] = "/Users/kkarpos/Desktop/NozzleData/logs/sensirion_SLI-0430_FTSTT5NA"
# config['brightfield'] = False
# config['filter'] = True
# config['threshold'] = True
# config['threshold_method'] = 'hysteresis'
# config['filter_method'] = 'gausshp'
# if config['brightfield'] is True:
#     config['threshold_parameters']['h_low'] = 0.15
#     config['threshold_parameters']['h_high'] = 0.5 
# else: 
#     config['threshold_parameters']['h_low'] = 1
#     config['threshold_parameters']['h_high'] = 3
# config['extra_filter_step'] = False

# config['process'] = True
# config['nozzle_tip_finder_method'] = 'simple'
# config['nozzle_tip_idx'] = 900
# config['processing_steps']['erode_1'] = False
# config['processing_steps']['n_erosion_1_iteration'] = 1
# config['processing_steps']['erode_2'] = False
# config['processing_steps']['n_erosion_2_iteration'] = 1
# config['processing_steps']['remove_small_objects'] = True
# config['processing_steps']['binary_closing'] = True
# config['processing_steps']['n_closing_iterations'] = 1
# config['processing_steps']['fill_holes'] = True


def insert_bad_frames(dataset, num_bad_frames, bad_frame_probability=0.01, frame_shape=(1024,512)):
    """
    Inserts "bad" frames into an existing dataset.
    
    :param dataset: List of frames (numpy arrays) with shape (1024, 512)
    :param num_bad_frames: Number of "bad" frames to insert
    :param bad_frame_probability: Probability of a "bad" frame being inserted at each position (default: 0.01)
    :return: Updated dataset with "bad" frames inserted
    """
    bad_frame = np.zeros(frame_shape)
    bad_frame[-100:,:] = 10
    dataset_length = dataset.shape[0]
    inserted_bad_frames = 0

    for i in range(dataset_length):
        if inserted_bad_frames >= num_bad_frames:
            break

        if np.random.rand() < bad_frame_probability:
            dataset = np.insert(dataset, i, bad_frame, axis=0)
            inserted_bad_frames += 1

    # If not all "bad" frames were inserted, insert the remaining ones at random positions
    while inserted_bad_frames < num_bad_frames:
        random_position = np.random.randint(0, dataset_length)
        dataset = np.insert(dataset, random_position, bad_frame, axis=0)
        inserted_bad_frames += 1

    return dataset


# -----------
# PYQT STUFF
# -----------
# Set up the PyQt5 application
app = QApplication([])
win = QMainWindow()
win.setWindowTitle('Jet Analysis')

# Create a central widget with a QVBoxLayout
central_widget = QWidget()
layout = QVBoxLayout()
central_widget.setLayout(layout)
win.setCentralWidget(central_widget)

# Create QLabel widget to display the number of frames in the buffer
buffer_label = QLabel()
layout.addWidget(buffer_label)

# Create a QHBoxLayout to hold the two ImageView widgets
view_layout = QHBoxLayout()
layout.addLayout(view_layout)

# Create ImageView widget for the raw frame
raw_layout = QVBoxLayout()
raw_view = pg.ImageView()
raw_layout.addWidget(raw_view)
view_title = QLabel("Raw Frame")
raw_layout.addWidget(view_title)
view_layout.addLayout(raw_layout)
raw_view.ui.histogram.hide()
raw_view.ui.roiBtn.hide()
raw_view.ui.menuBtn.hide()

# Create ImageView widget for the processed frame
p_layout = QVBoxLayout()
p_view = pg.ImageView()
p_layout.addWidget(p_view)
view_title = QLabel("Processed Frame")
p_layout.addWidget(view_title)
view_layout.addLayout(p_layout)
p_view.ui.histogram.hide()
p_view.ui.roiBtn.hide()
p_view.ui.menuBtn.hide()

# Create a QWidget to hold the two new ImageView widgets in the third column
third_col_layout = QVBoxLayout()

# Create ImageView widget for the median subtracted frame (proj_line)
proj_layout = QVBoxLayout()
proj_view = pg.ImageView()
proj_layout.addWidget(proj_view)
proj_title = QLabel("Cross-Correlation Matrix and Projection")
proj_layout.addWidget(proj_title)
third_col_layout.addLayout(proj_layout)
proj_view.ui.histogram.hide()
proj_view.ui.roiBtn.hide()
proj_view.ui.menuBtn.hide()

# Create ImageView widget for the correlation frame (corr_view)
corr_layout = QVBoxLayout()
corr_view = pg.ImageView()
corr_layout.addWidget(corr_view)
# corr_title = QLabel("Correlation Frame")
# corr_layout.addWidget(corr_title)
third_col_layout.addLayout(corr_layout)
corr_view.ui.histogram.hide()
corr_view.ui.roiBtn.hide()
corr_view.ui.menuBtn.hide()

# Add third_col_layout to view_layout
view_layout.addLayout(third_col_layout)

# Create a PlotDataItem for the nozzle tip index position
green_line = pg.PlotDataItem(pen=pg.mkPen(color='g', width=3))
p_view.addItem(green_line)

# Create a PlotDataItem for the nozzle tip index position
red_line = pg.PlotDataItem(pen=pg.mkPen(color='r', width=3))
p_view.addItem(red_line)

# Create a PlotDataItem for the jet fit position
blue_line = pg.PlotDataItem(pen=pg.mkPen(color='b', width=3))
p_view.addItem(blue_line)

# Create a PlotDataItem for the cross correlation viewer
proj_line = pg.PlotDataItem(pen=pg.mkPen(color='r', width=3))
proj_view.addItem(proj_line)

# Create a QTableWidget to display the stats
stats_table = QTableWidget()
stats_table.setRowCount(5)
stats_table.setColumnCount(2)
# Hide second column
stats_table.setColumnHidden(1, True)  # First argument is the index of the column, second argument is whether to hide it or not
# Hide the header
stats_table.horizontalHeader().setVisible(False)
# stats_table.setHorizontalHeaderLabels(['Mean', 'Sdev'])
stats_table.setVerticalHeaderLabels(['Jet Speed [m/s]', r'Jet Diameter [μm]', 'Jet Deviation [deg]', r'Jet Length [μm]', 'Stability Measure'])
header = stats_table.horizontalHeader()       
header.setSectionResizeMode(0, QHeaderView.Stretch)
header.setSectionResizeMode(1, QHeaderView.Stretch)
layout.addWidget(stats_table) # Add the stats_table to your layout
# Stretch the table so it's only on the bottom 1/3 of the screen
layout.setStretch(0, 2)  # The first argument is the index of the widget (view_layout is at 0), and the second argument is the stretch factor
layout.setStretch(1, 1)  # stats_table is at index 1

win.resize(1024, 512)  # Set the window size
win.show()  # Show the QMainWindow
# win.showMaximized()
# -----------
# -----------



# Load the testing data
stack, metadata = dataio.h5_metadata(filepath)
# Initialize the live median subtraction
print("stack shape: ", np.shape(stack))
medsub = MedianSubtractionFilter(buffer_size=5, frame_shape=np.shape(stack[0]))
# Insert some bad frames (it's just a zero's array for now, eventually add in real no-jet data)
stack = insert_bad_frames(stack, 20, bad_frame_probability=0.01)

# initialize the cross-correlation class
stats = RunningStats(frame_shape=np.shape(stack[0]))
cross = measure.RunningCrossCorrelation(frame_shape=np.shape(stack[0]))

# create the config passed into the JetCharacterstics class, note that everything needs to be in SI!
calc_config = {'pixel_size': metadata['pixel_size'] * 1e-6, # pixel size in meters
                'delay': metadata['delay'] * 1e-9, # laser flash delay in meters
                'droplet_translation': None,
                'liquid_flow': metadata['liquid_flow'] * 1e-9/60, # liquid flow rate in m^3 / s, multiple ul/min by 1e-9/60 to convert to m^3/s
                'nozzle_tip_idx': None,
                'transition_region_idx': None,
                'jet_fit': None,
                'interaction_point_method': 'percent_of_nozzle_length',
                'percent_dist': 0.5,
                'provided_distance': None
                }
calcs = measure.AllJetCharacteristics(calc_config, buffer_size=50)

# TO DO: For the liquid and gas flow rates, it needs to be updated live according to the data stream.
# To troubleshoot, we'll just use the "global" flow rate values from the metadata file.


# This is the main process below, the analysis steps should be in this order when we start building the 
# actual live analysis class.
# Process incoming frames one by one and display the original and processed frames
for i, frame in enumerate(stack):
    # display the raw image 
    raw_view.setImage(frame, levels=(0,20))

    # run through the processing steps
    frame = ImageProcess(frame, process_config).image

    is_jet = liveanalysis.identify_jetting(frame, threshold=0.9)

    if is_jet:
        # segment the image and get the important locations
        regs = regions.IDJetRegions(frame, regions_config)
        ntip_idx = regs._ntip_idx
        treg_idx = regs._treg_idx

        # add to the running median subtraction
        droplet_region = frame.copy()
        droplet_region[treg_idx:,:] = 0
        processed_frame = medsub.process_frame(droplet_region) # Perform the subtraction ONLY on the droplet region!

        stat = stats.process_frame(processed_frame)

        cross.update(processed_frame)#, mean_image=stat[0])

        # update the cross correlation image
        try:
            fts = cross.shifted_cc
            corr_view.setImage(fts)
            corr_view.setLevels(0, 1)
            projection = cross.projection()
            proj_line.setData(np.linspace(0, fts.shape[0], fts.shape[0]), -30*projection + 5*np.max(10*projection)/2)
        except Exception:
            pass

        displacement, _ = cross.calculate_displacement()

        calc_config['droplet_translation'] = displacement
        calc_config['nozzle_tip_idx'] = ntip_idx
        calc_config['transition_region_idx'] = treg_idx
        calc_config['jet_fit'] = regs.jet_fit

        data = calcs.update_and_get_stats(calc_config)

        # Update the stats_table with the new data
        # stats_table.setItem(0, 0, QTableWidgetItem(f"{data['jet_speed'][0]:.2f} +/- {data['jet_speed'][1]:.2f}"))
        # stats_table.setItem(1, 0, QTableWidgetItem(f"{data['jet_diameter'][0] * 1e6:.2f} +/- {data['jet_diameter'][1] * 1e6:.2f}"))
        stats_table.setItem(2, 0, QTableWidgetItem(f"{data['jet_deviation'][0]:.2f} +/- {data['jet_deviation'][1]:.2f}"))
        stats_table.setItem(3, 0, QTableWidgetItem(f"{data['jet_length'][0] * 1e6:.2f} +/- {data['jet_length'][1] * 1e6:.2f}"))
        stats_table.setItem(4, 0, QTableWidgetItem(f"{data['stability_measure'][0]} -- {data['stability_measure'][1]:.2f}"))

        # update the processed image display
        if ntip_idx >= 0:
            green_line.setData(x=[ntip_idx, ntip_idx], y=[0, frame.shape[1] - 1])
        else:
            green_line.setData(x=[], y=[])
        if treg_idx >= 0:
            red_line.setData(x=[treg_idx, treg_idx], y=[0, frame.shape[1] - 1])
        else:
            red_line.setData(x=[], y=[])

        x_coords = regs.jet_fit
        try:
            y_coords = np.linspace(0, frame.shape[0]-1, num=len(x_coords))  # create corresponding y-coordinates
            blue_line.setData(y=x_coords, x=y_coords)
        except TypeError:
            pass
    else:
        displacement = 0


    p_view.setImage(frame)
    label1 = f"Total Frames: {i:03d}, "
    if is_jet:
        label2 = f"Status: Jetting, "
    else:
        label2 = f"Status: No Jet , "
    label3 = f"Frames in buffer: {len(medsub.buffer)}, "
    # label4 = f"Calculated Displacement: {int(displacement)} pixels"
    buffer_label.setText(label1 + label2 + label3)# + label4)
    app.processEvents()  # Update the display

    if not win.isVisible():
        sys.exit()  # Exit if the window is closed








