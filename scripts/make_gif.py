import os
import h5py
import numpy as np
import imageio
import argparse
import glob

def normalize_frames(frames):
    """Normalize a stack of frames according to global min and max."""
    global_min = np.min(frames)
    global_max = np.max(frames)
    
    normalized_frames = ((frames - global_min) / (global_max - global_min) * 255).astype(np.uint8)
    return normalized_frames

def h5_to_gif(h5_filename, gif_filename:str="", fps=30, skip_nframes:int=2, **kwargs):
    """Convert frames in h5 file to a GIF."""
    
    if gif_filename == "":
        gif_filename = get_gif_output_path(h5_filename)

    # Read frames from h5 file
    with h5py.File(h5_filename, 'r') as f:
        frames = f['frames'][::skip_nframes]
    
    # Calculate duration from fps
    duration = 1.0 / fps

    # Normalize frames
    normalized_frames = normalize_frames(frames)
    
    # Create a GIF using imageio
    imageio.mimsave(gif_filename, normalized_frames, duration=duration, **kwargs)

def generate_gif_name(filepath):
    """Generate gif name from given filepath."""
    parts = os.path.normpath(filepath).split(os.sep)
    gif_name = "-".join(parts[-3:])  # Extract last 3 parts of the path
    return gif_name.replace('.h5', '.gif')

def get_gif_output_path(filepath):
    """Generate the full output path for the gif within a 'gifs' directory."""
    dir_path = os.path.dirname(filepath)
    gif_dir = os.path.join(dir_path, 'gifs')
    
    # Create the gifs directory if it doesn't exist
    os.makedirs(gif_dir, exist_ok=True)
    
    gif_name = generate_gif_name(filepath)
    return os.path.join(gif_dir, gif_name)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Turn a stack of frames into a gif.')
    parser.add_argument('filepath', type=str, nargs='+', help='Path to data file or wildcard pattern.')
    parser.add_argument('--fps', type=int, default=30, help='fps of the gif (default is 30)')
    parser.add_argument('--skip_nframes', type=int, default=2, help="How many interleaved frames to skip in the gif")
    args = parser.parse_args()

    pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))

    for path in args.filepath:
        expanded_path = pathogen(path)
        h5_to_gif(expanded_path, fps=args.fps, skip_nframes=args.skip_nframes)
