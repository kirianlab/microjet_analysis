import numpy as np
import h5py
import glob
import os
import json

pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))

def zoom_fixer(filepath:str, pixel_size:float):
    r"""Updates the zoom value of the h5 and text files.
    
        Sometimes we forget to update the zoom value in Odysseus. That's okay. 
        This function fixes the zoom value in the event you forget to update it 
        during data collection. 

        Note that in order for this to work, you must know the correct zoom value.
        All this function does is replace a value in an existing h5 file.

        TO DO: Edit this to accept a subset of runs, rather than the full data list.

        Args:
            filepath (str): the full path to the data, not including the h5 file
                                e.g., "/path/to/the/data/"
            pixel_size (float): the correct pixel size, used to update the metadata

    """

    all_data = sorted(glob.glob(os.path.join(filepath, "*.h5")))
    for i,im in enumerate(all_data):
        with h5py.File(im, 'r+') as f:
            x = f['metadata'][()]
            y = json.loads(x)
            y['optics']['pixelsize_um'] = pixel_size
            f['metadata'][()] = json.dumps(y, indent=4)


basepath = '/d/data/microjets/2023/20230714/322R_79'
h5s_to_fix = np.arange(224,235)

correct_pixelsize = 1.0585

for p in range(len(h5s_to_fix)):
    path = os.path.join(basepath, str(h5s_to_fix[p]))
    zoom_fixer(path, correct_pixelsize)


