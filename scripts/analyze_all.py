import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, warnings, logging, glob, gc
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from microjet import filters, regions, process, dataio, measure, gdvn, utils, figures
import pyqtgraph as pg
from microjet.gdvn_plots import GDVNPlots



r"""
    This script analyzes a complete data collection using the microjet.gdvn module,
    saves the results as an h5, and displays the results for each stack. 

    To Do: Create a GUI for this script.

"""

base_path = "/Volumes/kkarpos_ES/PhD_Work/NozzleData/"
year = "2023"
date = "20230714"
config = {'sample': 'water',
            "nozzle_id": "177R_63"}
overwrite = False

# the config with all the analysis options
analysis_config = dataio.config()

# Set the filter method, default is 'gausshp'
analysis_config['filter_method'] = 'gausshp'
analysis_config['filter_parameters']['sigma'] = 10

# Set the thresholding method and parameters
analysis_config['threshold_method'] = 'hysteresis'
# The hysteresis parameters of 2 and 5 seem to work best for dark field, 
# numbers under 1 work best for bright field
analysis_config['threshold_parameters']['h_low'] = 1.5
analysis_config['threshold_parameters']['h_high'] = 3

analysis_config['processing_steps']['binary_erosion'] = False

analysis_config['outer_product_method'] = 'centroid'
analysis_config['droplet_area_threshold'] = 5

analysis_config['cross_correlation_projection_threshold'] = 0.2

analysis_config['parallelize'] = True
analysis_config['process_stack'] = False

# Set the number of cores used in the processing steps
# Keep in mind that setting n_cores to the max doesn't necessarily 
# mean faster. There is some overhead when creating the parallel 
# processing, and for this application it may make the processing slower
analysis_config['n_cores'] = 7

config.update(analysis_config)


# Glob all the raw data files
all_data = sorted(glob.glob(os.path.join(base_path, year, date, config['nozzle_id'], "*.h5")))
if len(all_data) == 0:
    raise ValueError(f"No data was found in the following folder: {os.path.join(base_path, year, date, config['nozzle_id'])}")

print(f"Found {len(all_data)} raw data files.")

ntip_path = os.path.join(base_path, year, date, config['nozzle_id'], "nozzle_tip_positions.txt")
if os.path.exists(ntip_path):
    all_ntips = pd.read_csv(ntip_path, header=None, names=['file_id', 'nozzle_tip_idx'])
else:
    raise ValueError("No file for nozzle tips provided, did you make one?")

if len(all_ntips) != len(all_data):
    raise ValueError("The number of data sets does not match the number of provided nozzle tips. Double check that all files have a nozzle tip idx.")


# Setup Paths
log_path = os.path.join(base_path, "logs")
output_base_path = os.path.join(base_path, year, date, config['nozzle_id']+"_analysis")
os.makedirs(output_base_path, exist_ok=True)

analysis_folders = ["data", "figures", "gifs"]
for i, folder in enumerate(analysis_folders):
    os.makedirs(os.path.join(output_base_path, folder), exist_ok=True)

print("Made the analysis folder here: ")
print(f"\t {output_base_path}")
print("\n")
print(20*"-")
print("\n")

csv_path = os.path.join(output_base_path, "data.csv")
csv_updater = utils.CSVUpdater(csv_path)
# if os.path.exists(csv_path):
gdvn_plots = GDVNPlots(csv_path, os.path.join(output_base_path, "figures"), dpi=300)
if csv_updater.number_of_rows > 4:
    gdvn_plots.update_plots()

birds_eye_view_savepath = os.path.join(output_base_path, "figures", "birds_eye_view")
os.makedirs(birds_eye_view_savepath, exist_ok=True)
def save_birds_eye_view_binary(analysis, savepath, file_id):
    figures.birds_eye_view_binary(analysis.image_stack, savepath=os.path.join(savepath, f"{file_id}.png"),
                                                savefig=True, 
                                                ntip_idx=analysis.config['nozzle_tip_position'], 
                                                treg_idx=analysis.transition_region_idx, 
                                                separation=20, mod=1, dpi=150)
    plt.close()



# Loop over each raw data file and analyze
count = 1
for i, full_path in enumerate(all_data):
    print(f"Analyzing file: ")
    print(f"\t {full_path}")

    # path = os.path.join(full_path)
    file_id = full_path.split(os.sep)[-1]
    config["file_id"] = file_id

    config['nozzle_tip_position'] = all_ntips.loc[all_ntips['file_id'] == file_id, 'nozzle_tip_idx'].iat[0]

    if not overwrite:
        dat = os.path.join(output_base_path, 'data', file_id)
        if os.path.exists(dat) and csv_updater.file_id_exists(file_id):
            print(f"\t\t Overwrite is False and the file already exists. Skipping: {file_id}")
            count += 1
            continue
        elif os.path.exists(dat) and not csv_updater.file_id_exists(file_id):
            # Read data from h5 file and update the csv
            output_dict = utils.read_dict_from_hdf5(dat)
            csv_updater.update_csv(output_dict)
            print("\t\t csv file updated with data from h5!")
            # if csv_updater.number_of_rows > 4:
            #     gdvn_plots.update_plots()
            count += 1
            continue

    image_stack, metadata = dataio.open_data(full_path)

    jana = gdvn.JetAnalysis(image_stack, metadata, log_path=log_path, config=config)
    output_dict = jana.output_dict()

    if jana.stack_quality:
        utils.save_dict_to_hdf5(output_dict, os.path.join(output_base_path, "data", file_id))
        print("\t\t h5 file saved!")
        csv_updater.update_csv(output_dict)
        print("\t\t csv file updated!")

        save_birds_eye_view_binary(analysis=jana, savepath=birds_eye_view_savepath, file_id=file_id)

        if csv_updater.number_of_rows > 4:
            gdvn_plots.update_plots()
        print("\t\t figures updated!")
        count += 1
    else:
        print(f"\t\t Stack quality returned as False, something went wrong with the analysis. Skipping saving the analysis for this run.")

    print("\n")
    gc.collect()  # Explicitly invoke garbage collection


print("\n\n")
print(f"Analyzed all {len(all_data)} files, and {count} were good.")
print("\n\n")































