import sys
import os
import numpy as np
import glob
import time
import h5py
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QLabel, QWidget, QHBoxLayout

from microjet.process import ImageProcess, MedianSubtractionFilter
from microjet import regions, filters, dataio, utils, liveanalysis

import scipy
import time

class JetViewer:
    def __init__(self):
        # Set up the PyQt5 application
        self.win = QMainWindow()
        self.win.setWindowTitle('Jet Analysis')

        # Create a central widget with a QVBoxLayout
        self.central_widget = QWidget()
        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)
        self.win.setCentralWidget(self.central_widget)

        # Create QLabel widget to display the number of frames in the buffer
        self.buffer_label = QLabel()
        self.layout.addWidget(self.buffer_label)

        # Create a QHBoxLayout to hold the two ImageView widgets
        self.view_layout = QHBoxLayout()
        self.layout.addLayout(self.view_layout)

        # Create ImageView widget for the raw frame
        self.raw_layout = QVBoxLayout()
        self.raw_view = pg.ImageView()
        self.raw_layout.addWidget(self.raw_view)
        self.view_title = QLabel("Raw Frame")
        self.raw_layout.addWidget(self.view_title)
        self.view_layout.addLayout(self.raw_layout)
        self.hide_ui_elements(self.raw_view)

        # Create ImageView widget for the processed frame
        self.p_layout = QVBoxLayout()
        self.p_view = pg.ImageView()
        self.p_layout.addWidget(self.p_view)
        self.view_title = QLabel("Processed Frame")
        self.p_layout.addWidget(self.view_title)
        self.view_layout.addLayout(self.p_layout)
        self.hide_ui_elements(self.p_view)

        # Create ImageView widget for the median subtracted frame
        self.med_layout = QVBoxLayout()
        self.medsub_view = pg.ImageView()
        self.med_layout.addWidget(self.medsub_view)
        self.view_title = QLabel("Running Median Subtraction")
        self.med_layout.addWidget(self.view_title)
        self.view_layout.addLayout(self.med_layout)
        self.hide_ui_elements(self.medsub_view)

        # Create a PlotDataItem for the nozzle tip index position
        self.blue_line = pg.PlotDataItem(pen=pg.mkPen(color='b', width=3))
        self.p_view.addItem(self.blue_line)

        # Create a PlotDataItem for the nozzle tip index position
        self.red_line = pg.PlotDataItem(pen=pg.mkPen(color='r', width=3))
        self.p_view.addItem(self.red_line)

        self.win.resize(1024, 512)  # Set the window size

    def hide_ui_elements(self, view):
        view.ui.histogram.hide()
        view.ui.roiBtn.hide()
        view.ui.menuBtn.hide()

    def show(self):
        self.win.show()

    def update(self, raw_frame, processed_frame, medsub_frame, ntip_idx, treg_idx, buffer_len):
        # update the raw image display
        self.raw_view.setImage(raw_frame.T, levels=(0,20))

        # update the processed image display
        if ntip_idx >= 0:
            self.blue_line.setData(y=[ntip_idx, ntip_idx], x=[0, processed_frame.shape[2] - 1])
        else:
            self.blue_line.setData(x=[], y=[])
        if treg_idx >= 0:
            self.red_line.setData(y=[treg_idx, treg_idx], x=[0, processed_frame.shape[2] - 1])
        else:
            self.red_line.setData(x=[], y=[])
        self.p_view.setImage(processed_frame.T)

        # update the running median subtraction display
        self.medsub_view.setImage(medsub_frame.T)
    
        self.buffer_label.setText(f"Frames in buffer: {buffer_len}")

    def close_event(self):
        # Exit if the window is closed
        if not self.win.isVisible():
            sys.exit() 

filepath = "/Users/kkarpos/Desktop/NozzleData/20230201/79V1-DB-50G-50L-221206-04_glycerol_50/001.h5"
# filepath = "/Users/kkarpos/repos/projects/microjet_analysis/examples/example_data/example_binary.h5"

process_config = dataio.process_config()
process_config['brightfield'] = False
process_config['filter'] = True
process_config['threshold'] = True
process_config['process'] = True

process_config['filter_method'] = 'gausshp'
process_config['threshold_method'] = 'hysteresis'

if process_config['brightfield'] is True:
    process_config['threshold_parameters']['h_low'] = 0.15
    process_config['threshold_parameters']['h_high'] = 0.5 
else: 
    process_config['threshold_parameters']['h_low'] = 1
    process_config['threshold_parameters']['h_high'] = 3

process_config['processing_steps']['remove_small_objects'] = True
process_config['processing_steps']['min_pixel_size'] = 5
process_config['processing_steps']['binary_closing'] = True
process_config['processing_steps']['n_closing_iterations'] = 2
process_config['processing_steps']['fill_holes'] = True
process_config['processing_steps']['inflate_small_pixels'] = False
process_config['processing_steps']['inflate_amount'] = 3
process_config['processing_steps']['erode_1'] = False
process_config['processing_steps']['n_erosion_1_iteration'] = 1
process_config['processing_steps']['erode_2'] = False
process_config['processing_steps']['n_erosion_2_iteration'] = 1


regions_config = dataio.jetregions_config()
regions_config['nozzle_tip_finder_method'] = 'column' # 'column', 'intensity', or 'provided'
regions_config['provided_nozzle_tip_idx'] = None      # only works if above is 'provided'
regions_config['transition_region_finder_method'] = 'nonzero' # 'row_diff', 'nonzero', or 'provided'
regions_config['provided_transition_region_idx'] = None      # only works if above is 'provided'

def insert_bad_frames(dataset, num_bad_frames, bad_frame_probability=0.01, frame_shape=(1024,512)):
    """
    Inserts "bad" frames into an existing dataset.
    
    :param dataset: List of frames (numpy arrays) with shape (1024, 512)
    :param num_bad_frames: Number of "bad" frames to insert
    :param bad_frame_probability: Probability of a "bad" frame being inserted at each position (default: 0.01)
    :return: Updated dataset with "bad" frames inserted
    """
    bad_frame = np.zeros(frame_shape)
    bad_frame[-100:,:] = 10
    dataset_length = dataset.shape[0]
    inserted_bad_frames = 0

    for i in range(dataset_length):
        if inserted_bad_frames >= num_bad_frames:
            break

        if np.random.rand() < bad_frame_probability:
            dataset = np.insert(dataset, i, bad_frame, axis=0)
            inserted_bad_frames += 1

    # If not all "bad" frames were inserted, insert the remaining ones at random positions
    while inserted_bad_frames < num_bad_frames:
        random_position = np.random.randint(0, dataset_length)
        dataset = np.insert(dataset, random_position, bad_frame, axis=0)
        inserted_bad_frames += 1

    return dataset

# Load the testing data
stack, metadata = dataio.h5_metadata(filepath)
# Initialize the live median subtraction
medsub = MedianSubtractionFilter(buffer_size=30, frame_shape=np.shape(stack[0]))
# Insert some bad frames (it's just a zero's array for now, eventually add in real no-jet data)
stack = insert_bad_frames(stack, 20, bad_frame_probability=0.01)


app = QApplication([])

# Initialize the GUI
viewer = JetViewer()

# Show the window
viewer.show()

# Process incoming frames one by one and display the original and processed frames
for i, frame in enumerate(stack):
    # Run through the processing steps
    frame = ImageProcess(frame, process_config).image

    is_jet = liveanalysis.identify_jetting(frame[0], threshold=0.9)
    if is_jet:
        print(f"Frame {i}: Jetting! :) ")
    elif is_jet is False:
        print(f"Frame {i}: No jet.")

    # Segment the image and get the important locations
    regs = regions.IDJetRegions(frame, regions_config)
    ntip_idx = regs._ntip_idx
    treg_idx = regs._treg_idx

    # Add to the running median subtraction
    processed_frame = medsub.process_frame(frame)

    # Update the display
    viewer.update(frame, processed_frame, processed_frame, ntip_idx, treg_idx, len(medsub.buffer))
    
    # Allow PyQt to process its event loop
    app.processEvents()

    viewer.close_event()


# Run the application event loop
app.exec_()



