import argparse
import os
from microjet import dataio, utils, gdvnview


r"""
Use this script to quickly view the raw h5 data
Useful to get a quick understanding of nozzle position, where the
jet breakup region is, if it's a dark/brightfield image, etc.
"""

parser = argparse.ArgumentParser(description='View raw GDVN data')
parser.add_argument('filepath', type=str, help='Path to data file.')
args = parser.parse_args()


stack, _ = dataio.h5_metadata(utils.pathogen(args.filepath))
gdvnview.gdvn_view(stack, data_dict=None, hold=True, show=True)