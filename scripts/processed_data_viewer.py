import argparse
import numpy as np
import os, h5py
from microjet import utils, gdvnview


r"""
Use this script to quickly view the raw h5 data
Useful to get a quick understanding of nozzle position, where the
jet breakup region is, if it's a dark/brightfield image, etc.
"""

parser = argparse.ArgumentParser(description='View processed GDVN data')
parser.add_argument('filepath', type=str, help='Path to data file.')
args = parser.parse_args()

f = h5py.File(args.filepath, 'r')
stack = np.array(f['image_stack'])


gdvnview.gdvn_view(stack, data_dict=f, hold=True, show=True)