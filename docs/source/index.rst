.. Microjet Analysis documentation master file, created by
   sphinx-quickstart on Fri Sep 16 08:25:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Microjet Analysis Python Package
================================

Welcome to the |microjet_analysis| python package documentation.

This package is developed and used by the Kirian group at ASU to quantify the quality of liquid microjets, nanodrops,
viscous microextrusions, aerosol beams, and other mechanisms for putting samples into the path of X-ray Free-Electron
Lasers.

We will add a "quick overview" page to show what the package can do, so that potential users can determine if it will
be useful for them.

Next, users should visit the installation page to get set up.

Users who want to write python scripts should look at the |api|.

We will also add a page for developers.


Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   self
   installation
   api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
