# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import datetime

print('Loading conf.py')
print("Python executable:", sys.executable)

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'Microjet Analysis'
copyright = '2022, Richard A. Kirian, Konstantinos Karpos, Vivek Krishnan, Sahba Zaare, Roberto Alvarez'
author = 'Richard A. Kirian, Konstantinos Karpos, Vivek Krishnan, Sahba Zaare, Roberto Alvarez'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
import sphinx_rtd_theme
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# The version info for the project you're documenting, acts as replacement for |version| and |release|, also used in
# various other places throughout the built documents.
#
# The short X.Y version.
version = datetime.date.today().strftime('%Y.%m.%d').replace('.0', '.')  # Yields string like '2019.6.30'
# The full version, including alpha/beta/rc tags.
release = version

rst_epilog = """
.. |microjet_analysis|  replace:: `microjet_analysis <https://kirianlab.gitlab.io/microjet_analysis/>`__
.. |ndarray|  replace:: :class:`ndarray <numpy.ndarray>`
.. |slice|  replace:: `slice <https://docs.python.org/dev/library/functions.html#slice>`__
.. |sliced|  replace:: `sliced <https://docs.python.org/dev/library/functions.html#slice>`__
.. |pytest|  replace:: `pytest <http://doc.pytest.org/>`__
.. |Miniconda|  replace:: `Miniconda <https://conda.io/miniconda.html>`__
.. |scipy|  replace:: `scipy <https://www.scipy.org>`__
.. |pyqtgraph|  replace:: `pyqtgraph <http://www.pyqtgraph.org>`__
.. |numpy|  replace:: `numpy <https://numpy.org>`__
.. |git|  replace:: `git <https://git-scm.com>`__
.. |matplotlib|  replace:: `matplotlib <https://matplotlib.org/>`__
.. |pycodestyle|  replace:: `pycodestyle <https://pypi.org/project/pycodestyle/>`__
.. |pyqt5|  replace:: `pyqt5 <https://pypi.org/project/PyQt5/>`__
.. |bash| replace:: `bash <https://www.gnu.org/software/bash/manual/html_node/What-is-Bash_003f.html>`__
.. |f2py| replace:: `f2py <https://numpy.org/doc/stable/f2py/>`__
.. |mamba|  replace:: `mamba <https://mamba.readthedocs.io/en/latest>`__
.. |conda|  replace:: `conda <https://docs.conda.io/en/latest>`__
.. |installation page|  replace:: `installation page <installation_anchor>`__ 
.. |api|  replace:: `API <api_anchor>`__
"""
