.. _installation_anchor:

Installation
============

The "installation" of |microjet_analysis| consists of installing all the dependencies and setting up your Python path.

Download the |microjet_analysis| package
----------------------------------------

To download the package, you can either mouse-click the gitlab page to download a zip file (gitlab.com/kirianlab/microjet_analysis/), or, assuming you have |git| installed, you should clone |microjet_analysis| on your computer:

.. code-block:: bash

    git clone https://gitlab.com/kirianlab/microjet_analysis.git

The preferred method is |git|, since you can then keep the software up-to-date very easily.


Dependencies
------------

As of 2023, microjet_analysis is only tested with Python 3.
`Never use Python 2 <https://www.python.org/doc/sunset-python-2/>`_.
The `environment.yml` file lists all of the packages that are installed upon regular testing of microjet_analysis.
Here are the current contents of that file:

.. literalinclude:: ../../environment.yml


Setting up Python with Miniconda
--------------------------------

|Miniconda| is a reliable and lightweight distribution of python that is known to work well with |microjet_analysis|.
The |conda| package manager that comes with it makes it fast and easy to install and maintain
the dependencies of |microjet_analysis|.
It is recommended that you first make a trial
`conda environment <https://conda.io/docs/user-guide/tasks/manage-environments.html>`_ to check that there are no
conflicts between dependencies.
One way to setup a conda environment with all the needed |microjet_analysis| dependencies is to execute the
following command in the base directory of the |microjet_analysis| git repository:

.. code-block:: bash

    conda env create --file environment.yml

The above line will create the conda environment named ``microjet``.  You will need to activate that
environment whenever you want to use it:

.. code-block:: bash

    source activate microjet

If you wish, you can choose a different name for the environment, or you can install the dependencies into an existing
environment.  For example, to install them in the default ``base`` conda environment:

.. code-block:: bash

    conda env update --name base --file environment.yml

It is notable that |conda| can be *extremely slow* to resolve the above environment.  We have found that the |mamba|
package manager is much faster and acts as a drop-in replacement of |conda|.

In our developer directory, there is a script ``setup_conda.sh``  to set up a standard environment for testing.  The
script uses |mamba|.



Adding the |microjet_analysis| Repository to Your Python Path
--------------------------------------------------------------

In order to run the basic scripts, you will need to add the |microjet_analysis| repository to your python path. To do so, add the following line to your `bash startup script
<https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html>`_:

.. code-block:: bash

    export PYTHONPATH='/path/to/your/repo/microjet_analysis'

A quick way to activate the python environment, add the repository to your python path, and navigate to the directory is to make an alias. That is, 

.. code-block:: bash

    alias microjet="conda activate microjet; export PYTHONPATH='/path/to/your/repo/microjet_analysis'; cd /path/to/your/repo/microjet_analysis"




































