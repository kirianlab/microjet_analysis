# This is very old documentation... read at your own risk...

# How to analyize liquid microjet data and produce phase diagrams

Firstly, you need to know where your data is.  The data should be structured as follows:

```bash
/some/path/to/rawdata/2021/20210727/mh_cut2_01
```

The `/some/path/to/rawdata` part does not really matter, but we generally organize our data in nested directories 
corresponding to years, date, nozzle ID.  This makes it very easy to find new data, and to archive old data.

If you are working on Agave, you should use nomachine in order to see image displays, and you should start an 
interactive session before running any scripts.

Let's set up a directory to contain our analysis.  For example, 

```bash
mkdir -p /bioxfel/user/rkirian/ana
cd /bioxfel/user/rkirian/ana
```

Now let's clone the git repository:

```bash
git clone git@gitlab.com:rkirian/microjet-analysis.git
```

The raw data collected in the Kirian lab should be found in the following directory:

```bash
/bioxfel/data/kirianlab/projects/microjets
```

Rick likes to make symbolic links to the raw data path.  Here's how to do that:

```bash
ln -s /bioxfel/data/kirianlab/projects/microjets rawdata
```

The jet analysis scripts are written in Python.  If you don't have your own Python set up on Agave, you can try Rick's
installation:

```bash
alias python=/bioxfel/user/rkirian/miniconda3/bin/python
```

Now let's try some jet analysis.  If you want to know what the options are for the jet_analysis script, you can use the 
help flag:

```bash
python microjet-analysis/jet_analysis/analyze_jet.py -h
```

The following should work if you followed the above steps:

```bash
python microjet-analysis/jet_analysis/analyze_jet.py --base_directory=./rawdata --save_directory=./tempdata --glob='2021/**/mh_cut2_01/*.h5'
```

Note that the `--glob` argument is quite powerful -- it uses regular expressions according to the glob python package 
(Google search if you don't know what regular expressions are).

A few windows should appear.  There is some limited amount of interaction that is possible

- If the correlation matrix looks good, you can hit the spacebar to accept the results and move on to the next dataset.
- If you want to retry and adjust the threshold and jet region, you can hit the "r" key.
- If you think the current dataframe is not good, you can hit the "q" key to move on to the next dataframe.  
- If you want to totally exit the analysis, hit the "k" key.

The results of the analysis will be saved to the save_directory noted in the command above.  These results will contain
the jet speed, diameter, and other statistics which can be gathered by another program (to be added to this tutorial
later...).