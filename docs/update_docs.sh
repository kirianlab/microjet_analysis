#!/bin/bash
home=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$home" || exit
PYTHONPATH="$PYTHONPATH:$(cd "$home/.." && pwd)"
export PYTHONPATH
echo "PYTHONPATH=$PYTHONPATH"
if [ ! "$(python -c 'import microjet')" = "" ]; then
  echo "Cannot import microjet!"
  exit
fi
#source activate microjet_analysis
[ -d source/api ] && rm -r source/api
sphinx-apidoc --maxdepth 10 --output-dir source/api ../microjet_analysis
echo 'spinx-apidoc finished'
# FIXME: How do we properly change the title of the auto-generated API page?  Below we do it brute-force...
tail -n+3 source/api/modules.rst > tmp.rst
echo '.. _api_anchor:' > source/api/modules.rst
echo ' ' >> source/api/modules.rst
echo 'Package API' >> source/api/modules.rst
echo '===========' >> source/api/modules.rst
cat tmp.rst >> source/api/modules.rst
rm tmp.rst &> /dev/null
#make doctest
make html
#cp -r source/files build/html
#sed -i.bak '/>*Submodules</d' build/html/api/modules.html
#sed -i.bak '/>Module contents</d' build/html/api/modules.html
#sed -i.bak 's/ package</</g' build/html/api/modules.html
#sed -i.bak 's/ module</</g' build/html/api/modules.html
#exit
# Attempting to make the ugly API more readable... failed
#sed -i.bak '/>*package</d' build/html/api/modules.html
#sed -i.bak '/>*contents</d' build/html/api/modules.html
#sed -i.bak '/>*Subpackages</d' build/html/api/modules.html
#sed -i.bak '/>*package</d' build/html/api/modules.html
#sed -i.bak 's/-l3/-l2/g' build/html/api/modules.html
#sed -i.bak 's/-l4/-l3/g' build/html/api/modules.html
#sed -i.bak 's/-l5/-l4/g' build/html/api/modules.html
#sed -i.bak 's/-l6/-l5/g' build/html/api/modules.html
#perl -p -i -e 's{<head>\n}{<head>\n  <meta name="robots" content="noindex, nofollow" />\n}' build/html/*.html
#perl -p -i -e 's{toctree-l2}{toctree-l1}' build/html/api/modules.html
#perl -p -i -e 's{toctree-l3}{toctree-l1}' build/html/api/modules.html
#perl -p -i -e 's{toctree-l4}{toctree-l1}' build/html/api/modules.html
#perl -p -i -e 's{>reborn.*</a>}{}'
