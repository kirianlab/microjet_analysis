#!/bin/bash
home=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$home" || exit
[ -d source/api ] && rm -r source/api
make clean
