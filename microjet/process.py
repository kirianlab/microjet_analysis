import numpy as np
from scipy import ndimage
import skimage
from skimage import morphology, measure, segmentation, feature
# from sklearn.cluster import DBSCAN
try:
    from .utils import check_shapes
except:
    from utils import check_shapes
try:
    from . import filters, dataio
except:
    import filters, dataio
    
from collections import deque

def binary_erosion(data, iterations:int):
    r"""Wrapper for the scipy.ndimage.binary_erosion method

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed data whose shape is the same as the incoming data
    """
    data = data.copy()
    data = check_shapes(data)
    for i in range(data.shape[0]):
        data[i, :, :] = ndimage.binary_erosion(data[i, :, :], iterations=iterations)
    return data

def binary_dilation(data, iterations:int):
    r"""Wrapper for the scipy.ndimage.binary_dilation method

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed data whose shape is the same as the incoming data
    """
    data = data.copy()
    data = check_shapes(data)
    for i in range(data.shape[0]):
        data[i, :, :] = ndimage.binary_dilation(data[i, :, :], iterations=iterations)
    return data

# def remove_small_objects(data, small):
#     r"""Labels an image data and removes small pixels

#         Note: Incoming image data should be thresholded so the background is all zeros

#         Args:
#             data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
#             small (int): removes pixels below this size

#         Returns:
#             Processed data whose shape is the same as the incoming data
#     """
#     data = data.copy()
#     data = check_shapes(data)
#     for i in range(data.shape[0]):
#         labeled = measure.label(data[i, :, :], background=0)
#         unique_labels = np.unique(labeled)
#         if len(unique_labels) > 2:
#             data[i, :, :] = morphology.remove_small_objects(labeled, small)
#     return data

def remove_small_objects(data, small):
    """
    Labels an image data and removes small pixels.

    Args:
        data (ndarray): The image data of shape (n_frames, n_fs, n_ss).
        small (int): Minimum size of objects to keep.

    Returns:
        ndarray: Processed data whose shape is the same as the incoming data.
    """
    data = data.copy()
    for i in range(data.shape[0]):
        # Label the image
        labeled = measure.label(data[i, :, :], background=0)
        # Get the number of pixels in each label
        num_pixels = np.bincount(labeled.ravel())
        # Identify small objects
        small_objects = num_pixels < small
        # Set pixels of small objects to zero
        data[i, :, :][small_objects[labeled]] = 0
    return data

def binary_closing(data, footprint=None, iterations:int=3):
    r"""Wrapper for the scipy.ndimage.binary_closing method

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
            footprint (ndarray): structure kernel passed to the binary closing methods
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed data whose shape is the same as the incoming data
    """
    data = data.copy()
    data = check_shapes(data)
    for i in range(data.shape[0]):
        data[i, :, :] = ndimage.binary_closing(data[i, :, :], 
                                                structure=footprint, iterations=iterations)
    return data

def binary_fill_holes(data):
    r"""Wrapper for the scipy.ndimage.binary_fill_holes method

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)

        Returns:
            Processed data whose shape is the same as the incoming data
    """
    data = data.copy()
    data = check_shapes(data)
    for i in range(data.shape[0]):
        data[i, :, :] = ndimage.binary_fill_holes(data[i, :, :])
    return data

def label_watershed(data):
    r""" Uses a watershed method to do morphology labeling

        Args: 
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)

        Returns:
            The labeled image data of the same shape as the incoming data
    """
    data = data.copy()
    data = check_shapes(data)
    dist = [ndimage.distance_transform_edt(x) for x in data]
    coords = [feature.peak_local_max(dist[i], footprint=np.ones((3, 3)), labels=data[i]) for i, im in enumerate(data)]

    mask = np.zeros(np.shape(dist), dtype=bool)
    for i, c in enumerate(coords):
        mask[i, tuple(c.T)] = True

    markers = [ndimage.label(m)[0] for m in mask]
    labels = np.array([segmentation.watershed(-dist[i], markers[i], mask=data[i]) for i in range(len(data))])
    return labels

def label_standard(data):
    r""" Uses the standard method to do morphology labeling

        Args: 
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)

        Returns:
            The labeled image data of the same shape as the incoming data
    """
    data = check_shapes(data)
    return np.array([measure.label(sf, background=0) for sf in data])

def subtract_data_median(data):
    data = check_shapes(data)
    med = np.median(data, axis=0).astype(int)
    diff = np.subtract(data, med)
    return diff

def get_centroids(data, area_threshold=40, properties=None):
    """
    Uses regionprops to gather centroid and area values for each droplet in each 2D image of a stack.

    Args:
        data (list or ndarray): Stack of 2D images.
        area_threshold (float): Minimum area threshold for considering a droplet.
        properties (tuple): Properties to extract using regionprops.

    Returns:
        list: List of arrays, each containing the centroids for each 2D image in the stack.
        list: List of arrays, each containing the areas of droplets for each 2D image in the stack.
    """
    if properties is None:
        properties = ("area", "centroid")

    centroids_per_image = []
    areas_per_image = []
    
    for im in data:
        reg_table = skimage.measure.regionprops_table(im, properties=properties)
        valid_idx = reg_table['area'] > area_threshold
        centroids = np.column_stack((reg_table['centroid-0'][valid_idx], reg_table['centroid-1'][valid_idx]))
        areas = reg_table['area'][valid_idx]

        centroids_per_image.append(centroids)
        areas_per_image.append(areas)

    return centroids_per_image, areas_per_image

def outlier_filter(image_stack, outlier_threshold:int=3):
    # Finds the bad images in a stack, returns list of frame numbers
    stack = image_stack.copy()
    xs = np.array([np.sum(i) for i in stack])
    xm = np.array([np.mean(i) for i in xs])
    xm_idx = np.where(xm > outlier_threshold * np.median(xm))
    bad_frames = xm_idx[0]
    return bad_frames

def subtract_stack_median(stack):
    med = np.median(stack, axis=0).astype(int)
    diff = np.subtract(stack, med)
    x = np.where(diff < 0)
    diff[x] = 0
    return diff


def normalize_stack(image_stack):
    for i in range(image_stack.shape[0]):
        # only use nonzero values for the normalization
        nonz = np.nonzero(image_stack[i,:,:])
        # divide by the median pixel value of the whole stack
        image_stack[i, :, :] /= np.median(image_stack[i][nonz])




class ImageProcess:
    r""" A class to process a single GDVN microjet image.
    
        Notes:
            1. This class should be independent of any flow conditions.
            2. Only clean up the image using thresholding, filtering, etc. No segmenting.
            

        Assumptions:
            The nozzle is on the right-most edge of the image.
                --> To Do: an automatic 'rotate' method should be included here to ensure this

        Necessary config params:
            1. bright/darkfield (bool)
            2. filter (bool)
            3. threshold (bool)
            4. process (bool)
            5. normalize raw (bool)  ----> ?? Not needed?
            6. nozzle tip finder method (str)
            7. filter_method (str)
            8. filter_parameters (dict)
                8a. sigma (float)
                8b. truncate (int)
            9. threshold_method (str)
            10. threshold_parameters (dict)
                10a. h_low (float)
                10b. h_high (float)
            11. processing_steps (dict)
                11a. footprint (ndarray)
                11b. n_erosion_1_iteration (int)
                11c. remove_small_objects (bool)
                11d. min_pixel_size (int)
                11e. binary_closing (bool)
                11f. n_closing_iterations (int)
                11g. fill_holes (bool)
                11h. erode_2 (bool)
                11i. n_erosion_2_iteration (int)
                11j. inflate_small_pixels (bool)
                11k. inflate_amount (int)

    """
    thresh = None
    def __init__(self, image, config:dict=None):

        # =============================
        # User defined properties
        # =============================

        if config is None:
            config = dict()

        # Load and confirm the incoming config is correct
        self.config = dataio.process_config(config)

        # Grab the raw image
        self._image_raw = image.astype(np.float32)

        # save the shape as a class property
        self._image_shape = np.shape(self._image_raw)

        # make a copy of the raw image so we have access to the original copy
        self._image = self._image_raw.copy()
        if self.config['brightfield'] is False:
            # Flip the image if the data is taken in the darkfield
            self._image *= -1

        self._image = self.image_cleanup(self._image)[0]

    @property
    def image(self):
        return self._image


    def _filter_image(self, image):
        r""" Detects edges and filters background
            
            To Do:
                Add more options for background filtering, only the 'sobel' and 'gausshp' filters are available.

            Args:
                image: The images that make up your movie. This will also work on a single image.
                filter_method (str): The filtering method of choice. 

            Returns:
                image (ndarray): a filtered image of the same shape as the input image
        """

        method = self.config['filter_method']
        params = self.config['filter_parameters']

        image = image.copy()

        if method == 'gausshp':
            image = filters.gausshp(image, params['sigma'], truncate=params['truncate'])
        elif method == 'sobel':
            image = filters.sobel(image)
        else:
            raise KeyError(f"filter_method {method} is not recognized.")
        return np.array(image)

    def _threshold_image(self, image):
        r""" Will threshold an image using the provided method
            
            Current thresholding options are:
                'otsu', 'otsu_clipped', 'hysteresis'

            To Do:
                Add the ability to choose any thresholding method from the filters.py module.

            Args:
                image (array): The image, np.ndarray or not

            Returns:
                (ndarray): The thresholded image
        """
        method = self.config['threshold_method']
        if method == 'otsu':
            thresh = filters.threshold_otsu(image)
        elif method == 'otsu_clipped':
            thresh = filters.threshold_otsu_clipped(image)
        elif method == 'hysteresis':
            low = self.config['threshold_parameters']['h_low']
            high = self.config['threshold_parameters']['h_high']
            thresh = filters.threshold_hysteresis_clipped(image, low, high)
            thresh = np.array(thresh, dtype=int)
        else:
            raise ValueError(f"threshold_method {method} is not recognized.")
        return np.array(thresh)

    def _apply_processing_methods(self, image):
        r"""Apply morphology methods to the image, gathering a host of connected component parameters

            Args:
                image: Your image, it should be filtered and thresholded before running this function
        """
        # add an analysis footprint for the morphology features
        if self.config['brightfield']:
            footprint = self.config['processing_steps']['footprint']
        else:
            footprint = None

        # image = remove_outliers_by_dbscan(image, eps=130, min_samples=4)


        if self.config['processing_steps']['erode_1']:
            iter = self.config['processing_steps']['n_erosion_1_iteration']
            if iter > 0:
                image = binary_erosion(image, iterations=iter)

        # Remove all pixels that are smaller than some user-defined size (default = 5 pixels)
        if self.config['processing_steps']['remove_small_objects']:
            small = self.config['processing_steps']['min_pixel_size']
            if small == 0:
                print("remove_small_objects key and a size of zero were given. This is redundant.", flush=True)
            elif small > 0:
                image = remove_small_objects(image, small)

        if self.config['processing_steps']['binary_closing']:
            iter = self.config['processing_steps']['n_closing_iterations']
            if iter > 0:
                image = binary_closing(image, footprint=footprint, iterations=iter)

        if self.config['processing_steps']['fill_holes']:
            image = binary_fill_holes(image)

        if self.config['processing_steps']['erode_2']:
            iter = self.config['processing_steps']['n_erosion_2_iteration']
            if iter > 0:
                image = binary_erosion(image, iterations=iter)

        if self.config['processing_steps']['inflate_small_pixels']:
            iter = self.config['processing_steps']['inflate_amount']
            if iter > 0:
                image = binary_dilation(image, iterations=iter)


        return image

    def image_cleanup(self, image):
        r"""Cleans up an image based on config inputs
        """
        filtered = None
        thresholded = None
        processed = None

        if self.config['filter']:
            self._filtered = self._filter_image(image)
            image = self._filtered

        if self.config['threshold']:
            self._thresholded = self._threshold_image(image)
            image = self._thresholded

        if self.config['process']:
            self._processed = self._apply_processing_methods(image)
            image = self._processed

        return image

class MedianSubtractionFilter:
    # TO DO: This should only be applied to the region used in the cross correlation!
    def __init__(self, buffer_size, frame_shape):
        self.buffer_size = buffer_size
        self.frame_shape = frame_shape
        self.buffer = deque(maxlen=buffer_size)

    def process_frame(self, frame):
        self.buffer.append(frame)
        # Stack frames along a new axis for easy median calculation
        stacked_frames = np.stack(self.buffer, axis=0)
        median_frame = np.median(stacked_frames, axis=0).astype(int)
        return np.subtract(frame, median_frame)

class RunningStats:
    """
    A class used to calculate running statistics including mean, standard deviation, and variance for image frames.
    
    This class can be useful for analyzing a sequence of images (frames), where the statistics of the image 
    pixel values are calculated on-the-fly with each new frame.

    Attributes:
        frame_shape (tuple): Shape of the frames for which statistics are being calculated.
        n (int): Number of frames processed.
        mean (np.ndarray): Running mean of the frames processed.
        M2 (np.ndarray): Second moment of the frames processed, used in calculating variance.

    Methods:
        reset_statistics():
            Resets the running statistics to zero.
        process_frame(frame: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
            Processes a frame, updating the running mean, standard deviation, and variance.
    """

    def __init__(self, frame_shape):
        """
        Initializes the RunningStats object with a given frame shape.

        Args:
            frame_shape (tuple): Shape of the frames for which statistics are being calculated.
        """
        self.frame_shape = frame_shape
        self.reset_statistics()

    def reset_statistics(self):
        """
        Resets the running statistics to zero.
        
        This includes resetting the number of processed frames, the running mean, and the second moment to zero.
        """
        self.n = 0  # Now a scalar
        self.mean = np.zeros(self.frame_shape)
        self.M2 = np.zeros(self.frame_shape)  # Second moment

    def process_frame(self, frame):
        """
        Processes a frame, updating the running mean, standard deviation, and variance.
        
        The method updates the running mean and second moment for each pixel in the frame, then uses these 
        statistics to calculate the running standard deviation and variance. If less than two frames have been 
        processed, the standard deviation and variance will be zero.

        Args:
            frame (np.ndarray): An input frame of shape matching `self.frame_shape`.

        Returns:
            tuple: Three np.ndarrays of the same shape as the input frame representing the running mean, standard 
            deviation, and variance, in that order.
        """
        self.n += 1
        delta = frame - self.mean
        self.mean += delta / self.n
        delta2 = frame - self.mean
        self.M2 += delta * delta2

        if self.n < 2:
            return self.mean, np.zeros(self.frame_shape), np.zeros(self.frame_shape)
        else:
            variance = self.M2 / (self.n - 1)
            return self.mean, np.sqrt(variance), variance










