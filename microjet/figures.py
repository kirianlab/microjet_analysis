import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib import tri, colors
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor, KernelDensity
from scipy.spatial import ConvexHull, Delaunay
import os, gc

# Force the LaTeX font for all figures
plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral'


def compute_density_weights(x, y, bandwidth=0.5):
    """
    Compute density weights for each data point using Kernel Density Estimation.
    
    Parameters:
    - x, y: data arrays.
    - bandwidth: bandwidth of the KDE.
    
    Returns:
    - An array of density weights for each data point.
    """
    # Prepare data for KDE
    data_points = np.vstack([x, y]).T
    kde = KernelDensity(bandwidth=bandwidth)
    kde.fit(data_points)
    
    # Compute log density
    log_density = kde.score_samples(data_points)
    
    # Convert log density to actual density
    density = np.exp(log_density)
    
    # Return inverse of the density as weights
    return 1.0 / density

def _get_label(key):
    if key == "jet_speed":
        return "Jet Speed [m/s]"
    elif key == "stability" or key == "jet_stability":
        return "Jet Stability"
    elif key == "jet_diameter":
        return r"Jet Diameter [$\mu$m]"
    elif key == "jet_length":
        return r"Jet Length [$\mu$m]"
    elif key == "Capillary" or key == 'capillary':
        return r"Ca"
    elif key == "Ohnesorge" or key == 'ohnesorge':
        return r"Oh"
    elif key == "Weber" or key == 'weber':
        return "We"
    elif key == "Reynolds" or key == 'reynolds':
        return "Re"
    else:
        print("key not recognized, defaulting to empty colorbar title.")
        return r""

def plot_flow_regions(df, z_key='jet_speed', xr=(0, 100), yr=(0, 85), zr=(0, 120), n_neighbors=20,
                            knn_weights='density',  **kwargs):
    """
    Plots a contour map for microjet properties against operational parameters.

    The function uses Delaunay triangulation and Convex Hull methods to define the boundary of the dataset.
    Within this boundary, a k-nearest neighbors algorithm is used to predict values over a meshgrid which 
    are visualized using contour levels.

    Args:
        df (pandas dataframe): The dataframe of your data, should be in the KirianLab format.
        z_key (str, optional): Key for the z-axis data. Defaults to 'jet_speed'.
        xr (tuple, optional): Range for x-axis data. Defaults to (0, 100).
        yr (tuple, optional): Range for y-axis data. Defaults to (0, 85).
        zr (tuple, optional): Range for z-axis data. Defaults to (0, 120).
        n_neighbors (int, optional): Number of neighbors to use for k-NN. Defaults to 20.
        savefig (bool, optional): If True, the plot will be saved as a file. Defaults to False.
        savepath (str, optional): Path to save the figure. Required if `savefig` is True. Defaults to "".
        knn_weights (str, optional): Weight function to use in prediction. Choices are 'density' and 'distance'. Defaults to 'density'.
        **kwargs: Arbitrary keyword arguments for other functionalities.

    Raises:
        ValueError: If the choice of `knn_weights` is not 'density' or 'distance'.

    Returns:
        None. Draws or saves the contour plot. If drawn, plt.show() must be called outside of this method.
    """
    df_filtered = df.dropna(subset=['liquid_flow_mean', 'gas_flow', z_key])

    if len(df_filtered) < n_neighbors:
        n_neighbors = len(df_filtered)

    # Prepare the data
    x = df_filtered['liquid_flow_mean'].to_numpy()
    y = df_filtered['gas_flow'].to_numpy()
    if z_key == 'stability' or z_key == 'jet_stability':
        z = df_filtered['jet_stability'].to_numpy()
        # z = df['stability'].to_numpy()
    elif z_key == 'jet_diameter' or z_key == 'diameter':
        z = df_filtered['jet_diameter'].to_numpy()
    else:
        z = df_filtered[z_key].to_numpy()

    points = np.column_stack((x, y))
    hull = ConvexHull(points)

    # Create a grid of values
    grid_x, grid_y = np.meshgrid(np.linspace(xr[0], xr[1], 150), np.linspace(yr[0], yr[1], 150))
    tri = Delaunay(points[hull.vertices])
    outside_mask = tri.find_simplex(np.column_stack((grid_x.ravel(), grid_y.ravel()))) < 0

    mask = tri.find_simplex(np.column_stack((grid_x.ravel(), grid_y.ravel()))) >= 0

    zidx = np.where(z > 0)[0].tolist()

    if knn_weights == 'density':
        # Compute the density weights
        density_weights = compute_density_weights(x, y)
    
        # Define a custom weight function
        def custom_weights(distances):
            """Compute weights based on inverse distance and density weights."""
            # Get inverse distances (1.0 / distance)
            inv_distances = 1.0 / (distances + 1e-8)  # Adding a small value to prevent division by zero
            
            # Multiply inverse distances by density weights
            weights = inv_distances * density_weights[:distances.shape[1]]
            return weights

    elif knn_weights == 'distance':
        custom_weights = 'distance'
    else:
        raise ValueError("Incorrect choice for knn_weights, options are 'distance' and 'density'.")

    # Use k-NN regression for prediction with custom weights
    knn = KNeighborsRegressor(n_neighbors=n_neighbors, weights=custom_weights)
    knn.fit(np.column_stack((x, y)), z[zidx])
    
    # Predict values on the grid
    grid_z = knn.predict(np.column_stack((grid_x.ravel(), grid_y.ravel()))).reshape(grid_x.shape)
    grid_z[outside_mask.reshape(grid_x.shape)] = np.nan

    # Clip the z values
    grid_z = np.clip(grid_z, zr[0], zr[1])

    # Plot the contour
    dpi = kwargs.get('dpi', 150)
    fig, ax = plt.subplots(figsize=(5,4), dpi=dpi, tight_layout=True)
    cmap = kwargs.get('cmap', plt.get_cmap('RdBu_r'))
    contour = ax.contourf(grid_x, grid_y, grid_z, levels=100, cmap=cmap, vmin=zr[0], vmax=zr[1])

    # Scatter plot with the z values colored according to the colormap and z-range
    norm = plt.Normalize(zr[0], zr[1])
    ax.scatter(x, y, color=cmap((z[zidx]-zr[0])/(zr[1]-zr[0])),
               s=df_filtered['jet_diameter'] * 4, edgecolors='k', linewidth=0.15)
    ax.tick_params(axis='both', which='major', labelsize=12)

    # Create a mappable object for the colorbar
    mappable = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    mappable.set_array([])  # This is required, but the data is not used

    # Add the colorbar using the mappable object
    cbar = plt.colorbar(mappable, ax=ax)
    cbar.set_ticks(np.linspace(zr[0], zr[1], num=5))
    cbar_label = _get_label(z_key)
    cbar.set_label(cbar_label, fontsize=16)

    ax.set(xlim=xr, ylim=yr)
    ax.set_xlabel('Liquid Flow Rate [μl/min]', fontsize=16)
    ax.set_ylabel('Gas Mass Flow Rate [mg/min]', fontsize=16)

    return fig, ax


def dimensionless_numbers(df, y_key, xr=(0, 100), yr=(0, 85), **kwargs):
    dpi = kwargs.get('dpi', 150)
    cmap = kwargs.get('cmap', plt.get_cmap('RdBu_r'))
    fig, ax = plt.subplots(figsize=(5,4), dpi=dpi, tight_layout=True)

    ddf = df.sort_values(by='liquid_flow_mean', ascending=False)
    norm = colors.Normalize(vmin=ddf['gas_flow'].min(), vmax=60)
    for i in range(len(ddf)):
        im = ax.scatter(ddf['liquid_flow_mean'].iloc[i], ddf[y_key].iloc[i], norm=norm, 
                            edgecolors=(0.3,0.3,0.3), linewidths=0.25,
                            s=ddf['jet_diameter'].iloc[i] * 10, c=ddf['gas_flow'].iloc[i], cmap='RdBu_r',
                            marker='o')
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label('Gas Mass Flow [mg/min]', fontsize=16)
    ax.tick_params(axis='both', labelsize=14)
    ax.set(xlim=xr, ylim=yr)
    ax.set_xlabel(r'Liquid Flow Rate [$\mu$L/min]', fontsize=16)
    ax.set_ylabel(_get_label(y_key), fontsize=16)

    return fig, ax


def _birds_eye_view_binary(stack, ntip_idx:int, treg_idx, separation=20, mod=1):
    r'''Make a birds-eye-view image of the full raw image stack
        Useful for a quick view of the full image stack
    '''
    n, nx, ny = stack.shape
    g = 0
    for i in range(0, n, mod):
        im = stack[i, :, :].copy()
        im[:int(treg_idx[i]), :] *= 200
        im[int(treg_idx[i]):ntip_idx, :] *= 40
        im[:ntip_idx, :] *= 1
        a = np.pad(im.T, [[0, separation*n], [0, 0]])
        g += np.roll(a, i*separation, axis=0)
    x = np.where((g < 40) & (g > 0))
    g[x] = 1
    y = np.where((g >= 40) & (g < 200))
    g[y] = 2
    z = np.where(g >= 200)
    g[z] = 3
    return np.array(g)

def birds_eye_view_binary(stack, ntip_idx:int, treg_idx, savepath=None,
                            separation=20, mod=1, 
                            savefig=False,  **kwargs):
    dpi = kwargs.get('dpi', 150)
    image = _birds_eye_view_binary(stack, ntip_idx=ntip_idx, 
                                    treg_idx=treg_idx, separation=20, mod=1).T

    cmap = colors.ListedColormap(['black', 'red', 'blue', 'orange'])
    fig, ax = plt.subplots(figsize=(15,5), dpi=dpi, tight_layout=True)
    ax.imshow(image, cmap='gray', vmin=0, vmax=1, interpolation='none', alpha=1)
    ax.imshow(image, cmap=cmap, vmin=0, vmax=3, interpolation='none', alpha=0.5)

    ax.tick_params(axis='both', which='both', left=False, labelleft=False)

    if savefig and savepath is not None:
        plt.savefig(savepath)
    else:
        plt.show()

    plt.close()  # Close the figure to free memory
    gc.collect()  # Explicitly invoke garbage collection















