import sys
import numpy as np
import pyqtgraph as pg
from microjet import viewers
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QVBoxLayout, QWidget, QLabel
from PyQt5.QtCore import Qt, pyqtSignal





class GDVNView(viewers.ImageView):

    nozzle_tip_idx = None
    jet_breakup_idx = None
    drop_centroids = None
    jet_angle = None
    fit_plot = None
    data_dict = None
    nozzle_tip_index = 0

    nozzleTipIndexChanged = pyqtSignal(int)

    def __init__(self, *args, data_dict=None, **kwargs):
        """ A customized subclass of the pyqtgraph ImageView class that is specialized for looking at GDVN image
        stacks.  It makes use of the output of JetAnalysis and can show centroids, line fits, etc."""
        # super().__init__(*args, **kwargs)
        viewers.ImageView.__init__(self, *args, **kwargs)
        self.app = pg.mkQApp()
        self.frame_names = kwargs.get('frame_names', None)
        self.set_data_dict(data_dict)
        self._frame_changed(0, None)
        self.sigTimeChanged.connect(self._frame_changed)

        # Add a movable horizontal line
        self.horizontalLine = pg.InfiniteLine(angle=90, movable=True)
        self.addItem(self.horizontalLine)
        
        # Connect the line's position change signal
        self.horizontalLine.sigPositionChanged.connect(self._line_moved)

        # Label to display the index position
        self.indexLabel = QLabel("Nozzle Tip Index Position: ")
        self.ui.gridLayout.addWidget(self.indexLabel, 1, 0, 1, 1)  # Adjust layout as needed
        self.initialHistogramLevels = None

    def _line_moved(self):
        # Update the label with the line's current position
        self.nozzle_tip_index = int(self.horizontalLine.value())
        self.nozzleTipIndexChanged.emit(self.nozzle_tip_index)

    def get_nozzle_tip_index(self):
        return self.nozzle_tip_index
    
    def set_nozzle_tip_index(self, index):
        """Set the position of the nozzle tip line."""
        if self.horizontalLine:
            self.horizontalLine.setValue(index)
            self.nozzle_tip_index = index  # Update the internal nozzle tip index

    def set_data_dict(self, data_dict):
        if data_dict is None:  # Because sometimes we want to open the viewer and mouse-click to locate the file...
            return
        try:
            self.nozzle_tip_idx = data_dict['nozzle_tip_position']
        except:
            self.nozzle_tip_idx = data_dict['nozzle_tip_position'][()]

        self.jet_breakup_idx = np.array(data_dict['transition_region_idx'])
        self.jet_fit = np.array(data_dict['jet_fits'])
        
        try:
            self.pixel_size = data_dict['pixel_size']
        except:
            self.pixel_size = data_dict['pixel_size'][()]

        self.nozzle_tip_line = None
        self.jet_breakup_line = None
        self.drop_plot = None
        self.fit_plot = None
        
    def set_image_stack(self, image_stack):
        """
        Update the GDVNView with a new image stack.

        Args:
            image_stack (np.ndarray): A 3D numpy array representing the image stack.
        """
        if image_stack is not None and image_stack.ndim == 3:
            # Update the internal data representation
            self.image_stack = image_stack

            self.initialHistogramLevels = self.ui.histogram.item.getLevels()

            # Update the display with the first frame of the new stack
            self.setImage(image_stack, autoLevels=False, levels=self.initialHistogramLevels)

            # Reset or update other related visual elements as needed
            # For example, resetting lines and plots
            self.nozzle_tip_line = None
            self.jet_breakup_line = None
            self.fit_plot = None

            # Reconnect the frame changed signal to update the display for new stack
            self.sigTimeChanged.connect(self._frame_changed)


            # Redraw or refresh the view
            self.app.processEvents()

    def _frame_changed(self, ind, time):
        # TODO make two versions of this, onewith jet and one with ntip
        # check setVisible(True or False)
        if self.data_dict is None:
            return
        pix = self.data_dict['pixel_size'][()]
        if self.nozzle_tip_idx is not None:
            p = self.nozzle_tip_idx #* pix
            if self.nozzle_tip_line is None:
                self.nozzle_tip_line = self.add_line(angle=90, position=p, movable=False, pen='r')
            self.nozzle_tip_line.setPos([p, p])

        if self.jet_breakup_idx is not None:
            p = self.jet_breakup_idx[ind]# * pix
            if self.jet_breakup_line is None:
                self.jet_breakup_line = self.add_line(angle=90, position=p, movable=False, pen='b')
            self.jet_breakup_line.setPos([p, p])

        if self.jet_fit is not None:
            if np.all(np.isnan(self.jet_fit[ind])):
                pass
            else:
                p = self.jet_fit[ind]
                px = np.arange(np.shape(self.data_dict['image_stack'])[1])  #image_shape[0])
                if self.fit_plot is None:
                    self.fit_plot = self.add_plot(pen='c')
                self.fit_plot.setData(px, p)

        if self.frame_names is not None:
            self.view.setTitle(self.frame_names[ind])
        self.app.processEvents()


def gdvn_view(*args, **kwargs):
    r""" A common problem arises when one tries to create an ImageView instance: if you have not already
    created a pyqt Application instance, you will get an error.  This function will create the app first and then
    create the pyqt Application.

    In addition to the positional and keyword Args accepted by ImageView, the following Args are accepted:

    Args:
        show (bool): Set to False if you do NOT want the ImageView to be visible (i.e. ImageView.show() called).
        hold (bool): Set to False if you do NOT want the ImageView to be activated (i.e. Application.exec_() called).
    """
    hold = kwargs.pop('hold', True)
    show = kwargs.pop('show', True)
    app = pg.mkQApp()
    imv = GDVNView(*args, **kwargs)

    if show:
        imv.show()
    if hold:
        app.exec_()
    return imv



# class TableWindow(QMainWindow):
#     def __init__(self, data_dict):
#         super().__init__()
#         self.setWindowTitle("Dataframe Display")
#         self.setGeometry(100, 100, 800, 600)

#         # Create Table
#         self.tableWidget = QTableWidget()
#         self.setCentralWidget(self.tableWidget)

#         # Setup Table
#         self.tableWidget.setColumnCount(2)
#         self.tableWidget.setHorizontalHeaderLabels(["Key", "Value"])
        
#         # Populate Table
#         self.populateTable(data_dict)

#     def populateTable(self, data):
#         self.tableWidget.setRowCount(len(data))
#         for i, (key, value) in enumerate(data.items()):
#             self.tableWidget.setItem(i, 0, QTableWidgetItem(str(key)))
#             self.tableWidget.setItem(i, 1, QTableWidgetItem(str(value)))


