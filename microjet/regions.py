import numpy as np
from scipy import ndimage
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
try:
    from .utils import check_shapes
except:
    from utils import check_shapes
    
import matplotlib.pyplot as plt

try:
    from . import filters, dataio
except:
    import filters, dataio

def find_nozzle_tip_by_intensity(image):
    """
    Determines the location of the nozzle tip for a single image.

    Given a single image with the nozzle tip in view, will
    attempt to find the nozzle tip. 
    Args:
        image (|ndarray|): The 2D image containing the nozzle tip region.

    Returns:
        int: The index value of the nozzle tip position.
    """
    x = np.sum(image, axis=1)
    maxx = np.max(x)
    if maxx > np.percentile(x, 90):
        idx = np.where(x == np.max(x))[0]
        if len(idx) > 1:
            idx = idx[0]
        ntip_idx = idx
    else:
        # If the previous condition failed, we assume no nozzle tip is in view
        ntip_idx = image.shape[0]
    return int(ntip_idx)

def find_nozzle_tip_by_column(image):
    """
    Given a binary image, finds the nozzle tip index position.

    Args:
        image (|ndarray|): The binary image containing the nozzle tip region.

    Returns:
        int: The index position of the nozzle tip.
    """
    image = check_shapes(image)
    white_pixel_columns = np.nonzero(image)[1]

    if white_pixel_columns.size == 0:
        return -1

    # Filter out columns with very few white pixels
    unique_columns, counts = np.unique(white_pixel_columns, return_counts=True)
    filtered_columns = unique_columns[counts > np.percentile(counts, 90)]

    if filtered_columns.size == 0:
        return -1

    rightmost_white_pixel_column = np.max(filtered_columns)

    return rightmost_white_pixel_column

def find_nozzle_tip_simple(image):
    r"""Determines the location of the nozzle tip for an image

        Given an image with the nozzle tip in view, will project
        the image down the long axis, and determine where the peak
        is. The incoming image stack should be thresholded and made 
        binary before calculating this value.

        Args:
            image (|ndarray|): The 2D image
        Returns:
            (|int|): index position of the nozzle
    """
    xs1 = np.abs(np.sum(image, axis=1))
    nidx = np.where(xs1 > 2.5 * np.mean(xs1))
    return int(nidx[0][0])

def find_nozzle_tip_by_threshold(image, threshold=20, start_index=0):
    """
    Determines the location of the nozzle tip for a single 1D array of projected intensities.

    This method searches for a significant increase in intensity, which is expected
    to occur near the nozzle tip. 
    Args:
        data (np.ndarray): The 1D array containing projected intensities.
        threshold (int, optional): The threshold for detecting the intensity jump. Defaults to 20.
        start_index (int, optional): The index to start searching from. Defaults to 900.

    Returns:
        int: The index value of the nozzle tip position.
    """
    data = np.sum(image, axis=1)

    # Find the first occurrence where the intensity exceeds the threshold
    tip_index = np.argmax(data[start_index:] > threshold) + start_index

    plt.plot(data)
    plt.vlines(tip_index, ymin=0, ymax=150, color='red')
    plt.show()

    return tip_index

def find_nozzle_tip_auto_threshold(image, start_index=500, visualize=False):
    """
    Determines the location of the nozzle tip for a single 1D array of projected intensities,
    with an automatically determined threshold.

    This method searches for a significant increase in intensity, which is expected
    near the nozzle tip. 
    Args:
        image (np.ndarray): The 2D image containing the nozzle tip region.
        start_index (int, optional): The index to start searching from. Defaults to 500.
        visualize (bool, optional): If True, visualize the plot. Defaults to False.

    Returns:
        int: The index value of the nozzle tip position.
    """

    data = np.sum(image, axis=1)

    # Split the data into two sections
    first_section = data[:start_index]
    last_section = data[start_index:]

    # Filter out zeros and calculate median of each section
    median_first = np.mean(first_section[first_section > 0]) if np.any(first_section > 0) else 0
    median_last = np.mean(last_section[last_section > 0]) if np.any(last_section > 0) else 0

    # Determine threshold based on the difference
    threshold = median_first + (median_last - median_first) * 0.25

    # Find indices where the intensity exceeds the threshold
    tip_indices = np.where(data[start_index:] > threshold)[0] + start_index

    # Select the second occurrence if it exists
    tip_index = tip_indices[1] if len(tip_indices) > 1 else tip_indices[0] if len(tip_indices) > 0 else len(data)

    if visualize:
        plt.plot(data)
        plt.vlines(tip_index, ymin=0, ymax=np.max(data), color='red')
        plt.title(f"first: {median_first:0.2f}, last: {median_last:0.2f}, ntip_idx: {tip_index}")
        plt.show()

    return tip_index



def remove_nozzle_tip(stack, ntip_idx):
    r""" Will remove the nozzle tip from a stack of images

        Args:
            stack (|ndarray|): The 3D image stack
            ntip_idx (|list|): A list of nozzle tip index positions

        Returns:
            (|ndarray|): The segmented image stack
    """
    mi, ma = np.min(ntip_idx), np.max(ntip_idx)
    if mi == ma:
        stack = stack[:,:int(mi),:]
    else:
        for i, idx in enumerate(ntip_idx):
            stack[i, int(idx):, :] = 0
        stack = stack[:,:int(ma),:]
    return stack

def find_longest_nonzero_subset(data):
    r"""Given a list of numbers, will determine the 
        longest set of nonzero numbers
    
        Args:
            data (|list|): A list of numbers
        Returns:
            (|list|): The subset of data with the longest stretch of nonzero values
    """
    max_subset = []
    current_max_subset = []
    for i, n in enumerate(data):
        if n > 0:
            current_max_subset.append(i)
            if len(current_max_subset) > len(max_subset):
                max_subset = current_max_subset
        else:
            current_max_subset = []
    return max_subset


def find_jet_breakup_by_longest_nonzero(data):
    """
    Args:
        data (numpy.ndarray): a single 2D cleaned-up image.

    Returns:
        int: An index position corresponding to the jet breakup region. Will
        return None if there is an error.
    """
    x = np.sum(data, axis=1)
    try:
        xs = find_longest_nonzero_subset(x)
        jr_idx = int(xs[0])
    except IndexError:
        jr_idx = -1
    return jr_idx

def find_jet_breakup_by_max_row_diff(image):
    """Find the jet breakup region in the given binary image using a fast, low-memory approach.

    Args:
        image (numpy.ndarray): The binary jet image to analyze.

    Returns:
        int: The row index of the jet breakup region.
    """
    row_sums = np.sum(image, axis=0)
    nonzero_rows = np.nonzero(row_sums)[0]

    # Check if the nonzero_rows array is empty
    if nonzero_rows.size == 0:
        return -1

    # Calculate the differences between consecutive nonzero row indices
    row_diffs = np.diff(nonzero_rows)

    # Find the index of the largest difference, which corresponds to the jet breakup region
    max_diff_idx = np.argmax(row_diffs)

    # Return the row index corresponding to the jet breakup region
    return nonzero_rows[max_diff_idx]

def find_jet_breakup_by_intensity_change(data, threshold=0.1):
    """
    Args:
        data (numpy.ndarray): a single 2D cleaned-up image.
        threshold (float): the threshold for significant change, normalized to the range of intensity sums.

    Returns:
        int: An index position corresponding to the jet breakup region. Will
        return None if there is an error.
    """
    # Sum intensities in the horizontal direction
    x = np.sum(data, axis=0)
    
    # Calculate the relative change in intensity sum
    dx = np.abs(np.diff(x) / (np.ptp(x) + np.finfo(float).eps))

    # Find where the change exceeds the threshold
    change_indices = np.where(dx > threshold)[0]

    # If no indices were found, return None
    if len(change_indices) == 0:
        return -1

    # Otherwise, return the last index where a significant change was found (because jet flows from right to left)
    jr_idx = int(change_indices[-1])

    return jr_idx

def transition_region_idx(image, method:str='nonzero', idx:int=None):
    """Determine the transition region index in the provided jet image.

    This method finds the index of the transition region (jet breakup) in the image
    by using one of three methods: 'row_diff', 'nonzero', or 'provided'. If the
    'row_diff' or 'nonzero' methods are used, the appropriate find_jet_breakup_by...
    function will be called. If the 'provided' method is used, the provided index
    will be used directly.

    Args:
        image (numpy.array): The jet image to analyze.
        method (str, optional): The method to use to find the transition region index.
            Options are 'row_diff', 'nonzero', 'intensity_change', or 'provided'. Defaults to 'row_diff'.
        idx (int, optional): The index to use if the 'provided' method is selected.
            Defaults to None.

    Returns:
        int: The index of the transition region in the image.

    Raises:
        ValueError: If an incorrect method is provided or if the provided index
            is not a single integer value.

    """
    # treg_idx = 0
    if method == 'row_diff':
        treg_idx = find_jet_breakup_by_max_row_diff(image)
    elif method == 'nonzero':
        treg_idx = find_jet_breakup_by_longest_nonzero(image)
    elif method == 'intensity_change':
        treg_idx = find_jet_breakup_by_intensity_change(image)
    elif method == 'provided':
        try:
            treg_idx = int(idx)
        except:
            raise ValueError("Transition region index must be a single integer value.")
    else:
        err1 = "Incorrect breakup region finder method provided. Options are limited to"
        err2 = " 'row_diff', 'nonzero','intensity_change' and 'provided'" 
        raise ValueError(err1 + err2)
    return treg_idx


def isolate_droplet_region(im, transition_region_method='nonzero'):
    """
    Masks everything except the droplets

    Args:
        im (ndarray): The image to analyze
    Returns:
        (ndarray): The masked image of the same shape as im
    """
    idx = transition_region_idx(im, method=transition_region_method)

    # Create a mask instead of modifying im directly
    mask = np.ones_like(im, dtype=bool)  # A mask of True values

    if np.isnan(idx):
        mask[:] = False  # Set entire mask to False
    else:
        mask[:int(idx), :] = False  # Set part of the mask to False

    # Make a copy of im to avoid modifying the original
    im_c = im.copy()
    # Apply the mask to im
    im_c[mask] = 0

    return im_c

class IDJetRegions:
    r""" Segment and identify jet image regions.

        Notes:
            1. This should be independent of any image processing methods and flow conditions.
            2. This should only find the index values of the nozzle tip region and transition region. 

        Necessary config params:
            1. 'nozzle_tip_finder_method' (str)
            2. 'provided_nozzle_tip_idx' (int or None)
    """
    _ntip_idx = -1
    _treg_idx = -1
    def __init__(self, image, config):

        if config is None:
            config = dict()

        # Load and confirm the incoming config is correct
        self.config = dataio.jetregions_config(config)

        # save the incoming image as a class variable
        self._image_full = image

        # save the image shape since this will be needed a lot later on
        self._image_shape = np.shape(self._image_full)

        # determine the nozzle tip location
        ntip_method = self.config['nozzle_tip_finder_method']
        provided_ntip_idx = self.config['nozzle_tip_idx']
        self._ntip_idx = self._nozzle_tip_idx(self._image_full, method=ntip_method, idx=provided_ntip_idx)

        # determine the transition region location
        treg_method = self.config['transition_region_finder_method']
        provided_treg_idx = self.config['transition_region_idx']
        self._treg_idx = self._transition_region_idx(self._image_full, method=treg_method, idx=provided_treg_idx)

        _, _, self.jet_fit = self.hough_transform_fit()

    @property
    def nozzle_tip_idx(self):
        return self._ntip_idx

    @property
    def jet_breakup_idx(self):
        return self._treg_idx

    def _nozzle_tip_idx(self, image, method:str='column', idx:int=None):
        """Determine the nozzle tip index in the provided jet image.

        This method finds the index of the nozzle tip in the image by using one of three
        methods: 'column', 'intensity', or 'provided'. If the 'column' or 'intensity'
        methods are used, the appropriate find_nozzle_tip_by... function will be called.
        If the 'provided' method is used, the provided index will be used directly.

        Args:
            image (numpy.array): The jet image to analyze.
            method (str, optional): The method to use to find the nozzle tip index.
                Options are 'column', 'intensity', or 'provided'. Defaults to 'column'.
            idx (int, optional): The index to use if the 'provided' method is selected.
                Defaults to None.

        Returns:
            int: The index of the nozzle tip in the image.

        Raises:
            ValueError: If an incorrect method is provided or if the provided index
                is not a single integer value.

        """
        ntip_idx = 0
        if method == 'column':
            ntip_idx = find_nozzle_tip_by_column(image)
        elif method == 'intensity':
            ntip_idx = find_nozzle_tip_by_intensity(image)
        elif method == 'provided':
            try:
                ntip_idx = int(idx)
            except:
                raise ValueError("Nozzle tip index must be a single integer value.")
        else:
            err1 = "Incorrect nozzle tip finder method provided. Options are limited to"
            err2 = " 'column', 'intensity', and 'provided'" 
            raise ValueError(err1 + err2)
        return ntip_idx

    def _transition_region_idx(self, image, method:str='row_diff', idx:int=None):
        """Determine the transition region index in the provided jet image.

        This method finds the index of the transition region (jet breakup) in the image
        by using one of three methods: 'row_diff', 'nonzero', or 'provided'. If the
        'row_diff' or 'nonzero' methods are used, the appropriate find_jet_breakup_by...
        function will be called. If the 'provided' method is used, the provided index
        will be used directly.

        Args:
            image (numpy.array): The jet image to analyze.
            method (str, optional): The method to use to find the transition region index.
                Options are 'row_diff', 'nonzero', 'intensity_change', or 'provided'. Defaults to 'row_diff'.
            idx (int, optional): The index to use if the 'provided' method is selected.
                Defaults to None.

        Returns:
            int: The index of the transition region in the image.

        Raises:
            ValueError: If an incorrect method is provided or if the provided index
                is not a single integer value.

        """
        treg_idx = transition_region_idx(image, method=method, idx=idx)
        return treg_idx

    def hough_transform_fit(self, verbose=False):
        im = self._image_full.copy()
        im[self._ntip_idx:, :] = 0
        #im[:self._treg_idx, :] = 0
        bad = None, None, np.zeros(np.shape(im)[1])
        if self._ntip_idx == -1:
            if verbose:
                print("bad nozzle tip location, skipping fit routine.")
            return bad
        try:
            fit = HoughJetFit(im)
            return fit.get_fit_params()
        except TypeError:
            return bad


class HoughJetFit:
    """A class to perform a Hough Transform line fit on a given image.

    Attributes:
        image (np.array): An array representing the input image.
        xarange (np.array): A numpy array of x-values ranging from 0 to the width of the image.
        tested_angles (np.array): A numpy array of angles to test when performing the Hough Transform.
        fit (np.array): An array of fitted y values after performing the line fit.
        slope (float): The slope of the line fitted to the image data.
        y_intercept (float): The y-intercept of the line fitted to the image data.
    """

    def __init__(self, image, tested_angles=(-np.pi / 10, np.pi / 10), display=False):
        """
        Inits HoughJetFit with an image and the angles to test in the Hough Transform.

        Args:
            image (np.array): The input image to fit.
            tested_angles (tuple or list): Either a tuple of min and max values or a list of specific angles to test.
            display (bool): If True, will display figures showing each step of the process.
        """
        self.image = image
        self.display = display
        self.xarange = np.arange(np.shape(self.image)[0])

        if isinstance(tested_angles, tuple) and len(tested_angles) == 2:
            self.tested_angles = np.linspace(tested_angles[0], tested_angles[1], 1000, endpoint=False)
        elif isinstance(tested_angles, list):
            self.tested_angles = np.array(tested_angles)
        else:
            raise ValueError("tested_angles must be either a tuple of min and max values or a list of specific angles")

        self.fit = None
        self.slope = None
        self.y_intercept = None
        self.hough_jet_fit()

        if self.display:
            plt.show()

    def hough_jet_fit(self):
        """
        Performs a Hough Transform line fit on the input image.
        """
        hough, theta, dist = hough_line(self.image, theta=self.tested_angles)

        _, ang, d = hough_line_peaks(hough, theta, dist)

        if len(d) == 0:
            return None

        y = (d[0] + self.xarange * np.cos(ang[0] + np.pi / 2)) / np.sin(ang[0] + np.pi / 2)
        self.fit = y

        self.calculate_fit_params(self.xarange[-1])

        if self.display:
            fig, ax = plt.subplots(1, 2, figsize=(10, 3), tight_layout=True)

            # Display the Hough transform
            hough_im = ax[0].imshow(np.log(1 + hough),
                                     extent=[np.rad2deg(theta[0]), np.rad2deg(theta[-1]), dist[-1], dist[0]],
                                     cmap='gray', aspect='auto')
            ax[0].set_title('Hough Transform')
            ax[0].set_xlabel('Angles (degrees)')
            ax[0].set_ylabel('Distance (pixels)')

            # Find the coordinates of the peak in the Hough transform
            # We'll look for the peak (angle, distance) with the maximum votes in the Hough accumulator
            idx = np.unravel_index(np.argmax(hough), hough.shape)
            peak_angle, peak_dist = np.rad2deg(theta[idx[1]]), dist[idx[0]]

            # Highlight the peak in the Hough space
            ax[0].plot(peak_angle, peak_dist, 'ro')  # peak in Hough space
            ax[0].annotate(f'Peak: ({peak_angle:.2f}, {peak_dist:.2f})', (peak_angle, peak_dist),
                           textcoords="offset points", xytext=(-30,10), ha='center', color='red', fontsize=10)


            # Display the line detected
            ax[1].imshow(self.image.T, cmap='gray')
            ax[1].plot(self.xarange, y, color='red')
            ax[1].set_title(f'Line Fit: Slope = {self.slope:0.2f}, y-int = {self.y_intercept:0.2f}')

            plt.tight_layout()
            plt.draw()

    def calculate_fit_params(self, x1:int, x0:int=0):
        """
        Calculates the slope and y-intercept of the line fitted to the image data.

        Args:
            x1 (int): The x-value at the end of the line.
            x0 (int, optional): The x-value at the start of the line. Defaults to 0.
        """
        b0, b1 = self.fit[0], self.fit[-1]
        m = (b1 - b0) / (x1 - x0)
        self.slope = m
        self.y_intercept = b0

    def get_fit_params(self):
        """
        Returns the slope, y-intercept, and fitted y values of the line fitted to the image data.

        Returns:
            tuple: A tuple containing the slope, y-intercept, and an array of fitted y values.
        """
        return self.slope, self.y_intercept, self.fit






















