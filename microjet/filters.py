from skimage import filters
from scipy import ndimage, signal
import numpy as np
import pylab as plt
try:
    from .utils import check_shapes
except:
    from utils import check_shapes


r"""
A collection of wrappers for image filters we use for the jet analysis.

Filters came from scikit-image: https://scikit-image.org/docs/stable/api/skimage.filters.html
"""

def threshold_hysteresis(data, low, high):
    r""" Performs a threshold_hysteresis operation on a data of frames.

    Args:
        data (|ndarray|): image data.

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    return np.array([filters.apply_hysteresis_threshold(s, low, high) for s in data])

def threshold_hysteresis_clipped(data, low, high):
    r""" Performs the threshold_hysteresis method, but sets negative values to 0.

    Args:
        data (|ndarray|): image data.

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    data[data < 0] = 0
    return threshold_hysteresis(data, low, high)


def threshold_otsu(data, ):
    r"""
    performs a threshold_otsu operation on a data of frames.

    Args:
        data (|ndarray|): image data.

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_otsu(s) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_otsu_clipped(data):
    r"""Performs the threshold_otsu method, but clips values below zero
        
        Args:
            data (|ndarray|): image data.

        Returns:
            |ndarray|
    """
    data = check_shapes(data)
    data[data < 0] = 0
    return threshold_otsu(data)

def threshold_local(data, block_size:int=35):
    r"""
    Performs a threshold_local operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        block_size (int): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_local(s, block_size) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_isodata(data, nbins=256, return_all=False, hist=None):
    # performs a threshold_isodata operation on a data of frames
    r"""
    Performs a threshold_local operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        nbins (int): ??
        return_all (bool): ??
        hist (??): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_isodata(s, nbins=nbins, return_all=return_all, hist=hist) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_li(data, tolerance=None, initial_guess=None, iter_callback=None):
    r"""
    Performs a threshold_li operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        tolerance (??): ??
        initial_guess (??): ??
        iter_callback (??): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_li(s, tolerance=tolerance, initial_guess=initial_guess, iter_callback=iter_callback) for
              s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_mean(data):
    r"""
    Performs a threshold_mean operation on a data of frames.

    Args:
        data (|ndarray|): image data.

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_mean(s) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_niblack(data, window_size: int = 15):
    r"""
    Performs a threshold_niblack operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        window_size (int): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_niblack(s, window_size=window_size) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_sauvola(data, window_size: int = 15):
    r"""
    Performs a threshold_sauvola operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        window_size (int): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_niblack(s, window_size=window_size) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_triangle(data, nbins: int = 256):
    r"""
    Performs a threshold_triangle operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        nbins (int): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_triangle(s, nbins=nbins) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)


def threshold_yen(data, nbins: int = 256):
    r"""
    Performs a threshold_yen operation on a data of frames.

    Args:
        data (|ndarray|): image data.
        nbins (int): ??

    Returns:
        |ndarray|
    """
    data = check_shapes(data)
    thresh = [filters.threshold_yen(s, nbins=nbins) for s in data]
    x = [data[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def plot_all_thresholds(image, hyst_low:int=1, hyst_high=3):
    if np.ndim(image) != 2:
        raise ValueError("Image must be 2D! Ex: image shape = (1024, 512)")

    h = threshold_hysteresis(image, hyst_low,hyst_high)
    o = threshold_otsu(image)
    oc = threshold_otsu_clipped(image)
    lo = threshold_local(image)
    iso = threshold_isodata(image)
    li = threshold_li(image)
    me = threshold_mean(image)
    ni = threshold_niblack(image)
    sa = threshold_sauvola(image)
    ye = threshold_yen(image)

    fig, ax = plt.subplots(2,5,  sharey=True, sharex=True, tight_layout=True, figsize=(10,8))
    ax[0][0].imshow(h[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[0][0].set_title(f"hysteresis, \nlow={hyst_low}, high={hyst_high}")
    ax[0][1].imshow(lo[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[0][1].set_title('local')
    ax[0][2].imshow(o[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[0][2].set_title('otsu')
    ax[0][3].imshow(iso[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[0][3].set_title('isodata')
    ax[0][4].imshow(li[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[0][4].set_title('li')
    ax[1][0].imshow(me[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[1][0].set_title('mean')
    ax[1][1].imshow(ni[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[1][1].set_title('niblack')
    ax[1][2].imshow(oc[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[1][2].set_title('otsu_clipped')
    ax[1][3].imshow(sa[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[1][3].set_title('sauvola')
    ax[1][4].imshow(ye[0,:,:], cmap='gray', vmin=0, vmax=1)
    ax[1][4].set_title('yen')
    plt.show()

def gausshp(data, sigma, truncate):
    r"""Performs a Gaussian filter on an image data

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
            sigma (float): Standard deviation for Gaussian kernel
            truncate (int): Truncate the filter at this many standard deviations. Default is 4.0.

        Returns:
            Filtered image data whose shape is the same as the incoming data
    """
    # data = data.copy()
    data = check_shapes(data)
    # if np.ndim(data) == 2:
    #     data -= ndimage.gaussian_filter(data, sigma, truncate=truncate)
    # else:
    for i in range(np.shape(data)[0]):
        data[i,:,:] -= ndimage.gaussian_filter(data[i, :, :], sigma, truncate=truncate)
    data = -data
    return data

def sobel(data):
    r"""Performs a sobel filter on an image data

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)

        Returns:
            Filtered image data whose shape is the same as the incoming data
    """
    # data = data.copy()
    data = check_shapes(data)
    # if np.ndim(data) == 2:
    #     data = np.array(filters.sobel(data))
    # else:
    data = np.array([filters.sobel(s) for s in data])
    return data

def butterworth_lowpass(data, cutoff:float, fs:float, order:int):
    r"""Performs a butterworth lowpass filter on an image data

        Args:
            data (ndarray): the image data of shape (n_frames, n_fs, n_ss)
            cutoff (float): desired cutoff frequency of the filter, Hz
            fs (float): sample rate, Hz
            order (int): sin wave can be approx represented as quadratic


        Returns:
            Filtered image data whose shape is the same as the incoming data
    """
    data = check_shapes(data)
    normal_cutoff = cutoff / (0.5*fs)
    # Get the filter coefficients 
    b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    y = np.zeros_like(data)
    for i, im in enumerate(data):
        y[i,:,:] = signal.filtfilt(b, a, im, irlen='gust')
    return y

def plot_all_filters(image, vmin, vmax):
    if np.ndim(image) != 2:
        raise ValueError("Image must be 2D! Ex: image shape = (1024, 512)")

    g = gausshp(image, sigma=5, truncate=4)
    s = sobel([image])
    b = butterworth_lowpass([image], 2, 30, 2)

    fig, ax = plt.subplots(1,4, sharey=True, tight_layout=True, figsize=(20,10))
    ax[0].imshow(image, cmap="gray", vmin=vmin, vmax=vmax)
    ax[0].set_title("Original Image", fontsize=12)
    ax[1].imshow(g[0,:,:], cmap="gray", vmin=vmin, vmax=vmax)
    ax[1].set_title("gausshp, sigma=5, truncate=4", fontsize=12)
    ax[2].imshow(s[0,:,:], cmap="gray", vmin=vmin, vmax=vmax)
    ax[2].set_title("sobel", fontsize=12)
    ax[3].imshow(b[0,:,:], cmap="gray", vmin=vmin, vmax=vmax)
    ax[3].set_title("butterworth_lowpass, cutoff=2, fs=30, order=2", fontsize=12)
    plt.show()








# These won't work for us, but are available in scikit-image:

# def threshold_minimum(data, nbins=256):
#     # performs a threshold_minimum operation on a data of frames
#     thresh = [filters.threshold_minimum(s, nbins=nbins) for s in data]
#     x = [data[i] > thresh[i] for i in range(len(thresh))]
#     return np.array(x)  

# def threshold_multiotsu(data, classes:int=3, nbins:int=256):
#     # performs a threshold_minimum operation on a data of frames
#     thresh = [filters.threshold_multiotsu(s, classes=classes, nbins=nbins) for s in data]
#     x = [data[i] > thresh[i] for i in range(len(thresh))]
#     return np.array(x)
