import numpy as np

def reynolds(jet_speed, jet_diameter, rho, mu):
    return rho * jet_speed * jet_diameter / mu

def weber(jet_speed, jet_diameter, rho, sigma):
    return rho * jet_speed**2 * jet_diameter / (2 * sigma)

def capillary(jet_speed, mu, sigma):
    return jet_speed * mu / sigma

def ohnesorge(jet_diameter, rho, mu, sigma):
    return mu / np.sqrt(rho * sigma * jet_diameter)




