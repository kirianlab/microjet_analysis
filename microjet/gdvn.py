import numpy as np
import os, sys, time, warnings, logging, h5py
warnings.simplefilter("once", UserWarning)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


import matplotlib.pyplot as plt
from joblib import Parallel, delayed

from microjet import filters, regions, process, measure, fluids
from microjet import dataio, utils
from microjet.samples import sample_characteristics



# ----------------------------------------------------------
# utility methods
# ----------------------------------------------------------

def _update_metadata(metadata, log_fp):
    r"""
    Updates the metadata dictionary with new data obtained from log files.

    Parameters:
    metadata (dict): The existing metadata dictionary to be updated.
    log_fp (str): Filepath to the log file containing additional data.

    Returns:
    dict: The updated metadata dictionary.

    Notes:
    - This function utilizes the `logs_to_dict` function to extract data from log files
      and then updates the existing metadata with this new information.
    """
    data = metadata.copy()

    liq_fr = os.path.join(log_fp, "sensirion_SLI-0430_FTSTT5NA")
    lfr = dataio.get_data_from_logs(metadata=metadata, 
                                    log_filepath=liq_fr)
    if (len(lfr) == 0) or (liq_fr is None):
        # Throw a warning to indicate when there are no logs
        logger.warning(f"No liquid logs were found for this dataset. liquid_flow_mean is set to metadata['liquid_flow'] and liquid_flow_sdev is set to -1.")
        # update the metadata dictionary with the mean and sdev of the liquid flow rates for this particular movie
        data.update({'liquid_flow_mean': metadata['liquid_flow'],
                'liquid_flow_sdev': -1,
                })
    else:
        # if the logs exist, update the metadata with the corrected flow rates
        data.update({'liquid_flow_mean': np.mean(lfr),
                    'liquid_flow_sdev': np.std(lfr),
                    })

    gas_fr = os.path.join(log_fp, "bronkhorst_EL-Flow_COM4")
    gfr = dataio.get_data_from_logs(metadata=metadata, 
                                    log_filepath=gas_fr)
    if (len(gfr) == 0) or (gas_fr is None):
        # Throw a warning to indicate when there are no logs
        logger.warning(f"No gas logs were found for this dataset. gas_flow_mean is set to metadata['gas_flow'] and gas_flow_sdev is set to -1.")
        # update the metadata dictionary with the mean and sdev of the liquid flow rates for this particular movie
        data.update({'gas_flow_mean': metadata['gas_flow'],
                    'gas_flow_sdev': -1,
                    })
    else:
        # if the logs exist, update the metadata with the corrected flow rates
        data.update({'gas_flow_mean': np.mean(gfr),
                    'gas_flow_sdev': np.std(gfr),
                    })
    return data

def _filter_stack(stack, config):
    r""" Detects edges and filters background
        
        To Do:
            Add more options for background filtering, only the 'sobel' and 'gausshp' filters are available.

        Args:
            stack: The images that make up your movie. This will also work on a single image.
            filter_method (str): The filtering method of choice. 

        Returns:
            stack (ndarray): a filtered stack of the same shape as the input stack
    """

    method = config['filter_method']
    params = config['filter_parameters']

    stack = stack.copy()

    if method == 'gausshp':
        stack = filters.gausshp(stack, params['sigma'], truncate=params['truncate'])
    elif method == 'sobel':
        stack = filters.sobel(stack)
    else:
        raise KeyError(f"filter_method {method} is not recognized.")
    return np.array(stack)

def _threshold_stack(stack, config):
    r""" Will threshold an image stack using the provided method
        
        Current thresholding options are:
            'otsu', 'otsu_clipped', 'hysteresis'

        To Do:
            Add the ability to choose any thresholding method from the filters.py module.


        Args:
            stack (array): The image stack, np.ndarray or not

        Returns:
            (ndarray): The thresholded image stack
    """
    method = config['threshold_method']
    if method == 'otsu':
        thresh_stack = filters.threshold_otsu(stack)
    elif method == 'otsu_clipped':
        thresh_stack = filters.threshold_otsu_clipped()
    elif method == 'hysteresis':
        low = config['threshold_parameters']['h_low']
        high = config['threshold_parameters']['h_high']
        thresh_stack = filters.threshold_hysteresis_clipped(stack, low, high)
        thresh_stack = np.array(thresh_stack, dtype=int)
    else:
        raise ValueError(f"threshold_method {method} is not recognized.")
    return np.array(thresh_stack)

def _remove_nozzle_tip(image_stack, nozzle_tip_idx:int):
    # Make a copy the raw image stack so we don't mess with the original
    stack = image_stack.copy()
    stack[:,nozzle_tip_idx:,:] = 0
    return stack

def _get_transition_region_idx(image_stack):
    treg_idx = np.zeros(len(image_stack))
    for i, im in enumerate(image_stack):
        treg_idx[i] = regions.find_jet_breakup_by_longest_nonzero(im)
    return treg_idx


def _isolate_droplet_region(image_stack, transition_region_idx):
    r"""Masks everything except the droplets
        
        Args:
            stack (ndarray): The image stack to analyze, defaults to self.jet_region if None.
    """
    stack = image_stack.copy()
    for i, im in enumerate(stack):
        if np.isnan(transition_region_idx[i]):
            stack[i] = np.zeros_like(im)
        else:
            stack[i,int(transition_region_idx[i]):,:] = 0
    neg_drops_idx = np.where(stack < 0)
    stack[neg_drops_idx] = 0
    return stack

def hough_transform_fit(image_stack):
    fits = np.zeros((np.shape(image_stack)[:2]))
    for i, im in enumerate(image_stack):
        try:
            fits[i,:] = regions.HoughJetFit(im).get_fit_params()[2]
        except TypeError:
            fits[i,:] = np.zeros(np.shape(image_stack)[1])
    return fits

def _get_jet_deviation(jet_fits, nozzle_tip_idx):
    # Method assumes the nozzle tip index is static and predetermined
    deviations = np.zeros(len(jet_fits))
    for i, fit in enumerate(jet_fits):
        deviations[i] = measure.jet_deviation(fit, nozzle_tip_idx)
    return deviations, np.mean(deviations), np.std(deviations)

def _get_jet_length(nozzle_tip_idx:int, 
                    transition_region_idx:list, 
                    jet_deviations:list, 
                    pixel_size:float):

    # Method assumes the nozzle tip index is static and predetermined
    lengths = np.zeros(len(transition_region_idx))
    for i, treg in enumerate(transition_region_idx):
        lengths[i] = measure.jet_length(nozzle_tip_idx=nozzle_tip_idx,
                                         transition_region_idx=treg, 
                                         jet_deviation=jet_deviations[i], 
                                         pixel_size=pixel_size)
    return lengths, np.mean(lengths), np.std(lengths)

def _get_dimensionless_numbers(sample, jet_speed, jet_diameter):
    try:
        sample_dict = sample_characteristics[sample]
        weber = fluids.weber(jet_speed, jet_diameter, sample_dict['rho'], sample_dict['sigma'])
        reynolds = fluids.reynolds(jet_speed, jet_diameter, sample_dict['rho'], sample_dict['mu'])
        capillary = fluids.capillary(jet_speed, sample_dict['mu'], sample_dict['sigma'])
        ohnesorge = fluids.ohnesorge(jet_diameter, sample_dict['rho'], sample_dict['mu'], sample_dict['sigma'])
    except:
        logging.warning("Something went wrong with the dimensionless numbers calculations.")
        weber = -1
        reynolds = -1
        capillary = -1
        ohnesorge = -1
    return weber, reynolds, capillary, ohnesorge

def _get_flow_ratio(liquid_flow_rate, gas_flow_rate):
    return liquid_flow_rate / gas_flow_rate



class JetAnalysis:


    def __init__(self, image_stack, image_metadata, log_path=None, config=None):

        # =============================
        # User defined properties
        # =============================

        if config is None:
            config = dict()

        # Load and confirm the incoming config is correct
        self.config = dataio.config(config)
        self._check_config(self.config)

        # Grab the raw image stack
        self.image_stack_raw = image_stack.astype(np.float32)
        self.image_stack_raw = utils.check_shapes(data=self.image_stack_raw)  # ensure the image stack is in the correct orientation
        self.n_frames = np.shape(self.image_stack_raw)[0]
        self.resolution = np.shape(self.image_stack_raw)[1:]

        self.image_metadata = _update_metadata(image_metadata, log_path)

        # Set up class property defaults for easy dict-ing.
        self._setup_defaults()

        # Check to make sure the metadata is as expected
        self._check_metadata()

        # =============================
        # Image Processing Steps
        # =============================

        if self.stack_quality is True:
            self.image_stack = _remove_nozzle_tip(self.image_stack_raw, nozzle_tip_idx=self.config['nozzle_tip_position'])

            process.normalize_stack(self.image_stack)

            # Check if the image stack was recorded using light or dark field imaging
            # Only one frame is needed for this, but we'll check for 10 frames in case of bad frames
            if utils.determine_image_field(self.image_stack[:10]) == 'dark':
                # logging.warning("Determined lighting to be in dark mode, multiplying stack by -1.")
                self.image_stack *= -1

            # Run the stack clean-up methods
            self._stack_cleanup()

            # Filter outlier frames
            self.bad_frames = process.outlier_filter(self.image_stack)
            self.image_stack[self.bad_frames, :, :] = np.zeros(self.resolution)

            # Calculate the jet fit
            self.jet_fits = hough_transform_fit(self.image_stack)

            # Get the transition region index positions for each frame in the image stack
            self.transition_region_idx = _get_transition_region_idx(self.image_stack)
            # Check the transition indices, indicate whether this dataset has too many bad transition points.
            self._check_transition_region_quality()

        if self.stack_quality:
            # Prepare the stack for jet speed calculations
            self.droplet_region = _isolate_droplet_region(image_stack=self.image_stack, transition_region_idx=self.transition_region_idx)

            if self.config['subtract_stack_median']:
                # Subtract away the median image from the droplet stack, this will remove any outlier pixels
                self.droplet_region = process.subtract_stack_median(self.droplet_region)

        # The stack quality checks are not redundant below, each method has a way to check and update the quality metric 
        # so it needs to be called a few times
        if self.stack_quality:
            self._calculate_jet_speed()

        if self.stack_quality:
            self._calculate_all()
        else:
            logging.warning("Stack quality was set to False, meaning something went wrong. Stopping the analysis here.")

    def _setup_defaults(self):
        self.stack_filtered = None
        self.stack_thresholded = None
        self.stack_processed = None
        self._incorrect_sample = False
        self.jet_speed = -1
        self.stack_quality = True
        self.jet_diameter = -1
        self.jet_diameter_err = -1
        self.jet_deviation = -1
        self.jet_deviation_err = -1
        self.all_cross_correlation_data = None
        self.all_outer_product_data = None
        self.jet_speed_by_outer_product = -1
        self.jet_speed_by_cross_correlation = -1
        self.cross_correlation_projection = None
        self.outer_product_quality = True
        self.cross_correlation_quality = True
        self.jet_speed_units = ""
        self.jet_diameter_units = ""
        self.jet_deviation_units = ""
        self.jet_speed_comparison = True # Boolean to check whether the jet speeds match each other
        self.stability_measure_length = -1
        self.stability_measure_length_units = ""
        self.jet_stability = -1
        self.flow_ratio = -1


    # ----------------------------------------------------------
    # stack cleanup
    # ----------------------------------------------------------

    def _stack_cleanup(self):
        t = time.time()
        if self.config['parallelize']:
            split_stack = np.array_split(self.image_stack, self.config['n_cores'], axis=0)
            all_stacks = Parallel(self.config['n_cores'])(delayed(self.stack_cleanup)(split_stack[i]) for i in range(self.config['n_cores']))
            self.image_stack = np.concatenate([all_stacks[i][0] for i in range(self.config['n_cores'])])
        else:
            self.image_stack, self.stack_filtered, \
                self.stack_thresholded = self.stack_cleanup(self.image_stack) #, self.stack_processed
        print(f"Time to cleanup image stack: {time.time() - t:0.3f} s")

    def stack_cleanup(self, image_stack):
        r"""stack cleanup methods thrown into joblib
        """
        filtered = None
        thresholded = None

        stack = image_stack.copy()
        if self.config['filter']:
            stack = _filter_stack(stack, self.config)

        if self.config['threshold']:
            stack = _threshold_stack(stack, self.config)

        if self.config['processing_steps']['remove_small_objects']:
            stack = process.remove_small_objects(stack, self.config['processing_steps']['min_pixel_size'])

        if self.config['processing_steps']['binary_erosion']:
            stack = process.binary_erosion(stack, 2)
            stack = process.binary_dilation(stack, 1)

        return stack, filtered, thresholded


    # ----------------------------------------------------------
    # Checks to ensure things are happening as expected
    # ----------------------------------------------------------

    def _check_config(self, config):
        r"""
        Checks the user-provided configuration to ensure it adheres to certain rules, especially regarding parallelization.

        Parameters:
        config (dict): A dictionary containing configuration settings.

        Raises:
        ValueError: If the number of requested cores for parallelization exceeds the number of available CPU cores.

        Notes:
        - This function is particularly important when the 'parallelize' option is set to True in the config.
          It ensures that the user does not request more cores than what is available on the system.
        """
        if config['parallelize']:
            if config['n_cores'] > os.cpu_count():
                raise ValueError("Requested more cores than are available.")

    def _check_metadata(self):
        if self.image_metadata['liquid_flow_mean'] < 0:
            logger.warning("The liquid flow rate was negative, setting stack quality to False.")
            self.stack_quality = False

    def _check_speed_difference(self):
        # Calculate the percentage difference
        mean_speed = (self.jet_speed_by_cross_correlation + self.jet_speed_by_outer_product) / 2
        percent_difference = abs(self.jet_speed_by_cross_correlation - self.jet_speed_by_outer_product) / mean_speed * 100

        # Check if the difference is greater than the threshold (e.g., 5%)
        if percent_difference > 5:
            self.jet_speed_comparison = False
            warn1 = f"The speeds calculated by cross-correlation and outer product differ by {percent_difference:0.2f}% -- "
            warn2 = f"Cross Correlation: {self.jet_speed_by_cross_correlation:0.2f} -- Outer Product: {self.jet_speed_by_outer_product:0.2f}"
            logger.warning(warn1 + warn2)

    def _check_correlation_quality(self):
        # Checks if the correlation projections are bad, throws warnings if they are.
        if self.cross_correlation_quality is False:
            logger.warning(f"The 2D Cross Correlation jet speed method produced a projection with no discernible maximum!")

        if self.outer_product_quality is False:
            logger.warning(f"The Outer Product jet speed method produced a projection with no discernible maximum!")

    def _check_zero_speed(self):
        if (self.jet_speed == 0) or (self.jet_speed_by_cross_correlation == 0):
            self.stack_quality = False
            logging.warning("Jet speed is 0, something is wrong with this data set.")

    def _check_stability(self):
        if self.jet_stability > 2:
            logging.warning(f"Highly unstable jet! Is this expected?")
        if self.jet_stability < 0:
            logging.warning("Negative jet stability value found.")

    def _check_sample(self):
        if self.image_metadata['sample'] not in list(sample_characteristics.keys()):
            logging.warning("Sample not in standard dictionary, returning None for dimensionless parameters.")
            self._incorrect_sample = True

    def _check_transition_region_quality(self):
        unique, counts = np.unique(self.transition_region_idx, return_counts=True)
        if -1 in unique:
            idx = np.where(unique == -1)
            if counts[idx] > 0.02 * self.n_frames:
                logging.warning(f"This dataset has {counts[idx]} bad transition region coordinates. Consider throwing it away.")
                self.stack_quality = False

    # ----------------------------------------------------------
    # Jet Calculations
    # ----------------------------------------------------------

    def _calculate_jet_speed(self):

        # ================================
        # Jet Speed Calculations
        # ================================

        t = time.time()
        # Calculate the jet speed by the 2D cross correlation
        self.all_cross_correlation_data = measure.get_speed_by_2D_cross_correlation(self.droplet_region, 
                                                                                        self.image_metadata['pixel_size'] * 1e-6, 
                                                                                        self.image_metadata['delay'] * 1e-9)
        self.jet_speed_by_cross_correlation = self.all_cross_correlation_data['jet_speed']
        self.cross_correlation_matrix = self.all_cross_correlation_data['cross_correlation_matrix']

        # Calculate the cross correlation projection for the quality metric
        self.cross_correlation_projection = measure.get_cross_correlation_projection(self.all_cross_correlation_data['cross_correlation_matrix'], 
                                                                                        threshold=self.config['cross_correlation_projection_threshold'])
        self.cross_correlation_quality = measure.correlation_projection_quality(self.cross_correlation_projection)

        # Check if the jet speed is 0
        self._check_zero_speed()

        # Calculate the jet speed by the outer product
        self.all_outer_product_data = measure.get_speed_by_outer_product(self.droplet_region, #centroid_image,
                                                                    self.image_metadata['pixel_size'] * 1e-6,
                                                                    self.image_metadata['delay'] * 1e-9, 
                                                                    method=self.config['outer_product_method'],
                                                                    area_threshold=self.config['droplet_area_threshold'])
        self.outer_product_matrix = self.all_outer_product_data['outer_product_matrix']
        self.outer_product_projection = self.all_outer_product_data['projection']
        self.jet_speed_by_outer_product = self.all_outer_product_data['jet_speed']

        # Calculate the outer product projection for the quality metric
        self.outer_product_quality = measure.correlation_projection_quality(self.all_outer_product_data['projection'])

        # Check if the speeds are drastically different, throw a warning if they are
        self._check_speed_difference()
        # Check the quality of the correlations and throw a warning if there's an issue
        self._check_correlation_quality()

        # get a single jet speed value according to the comparison of the two methods
        self._determine_jet_speed()

        # Check if the jet speed is 0
        # redundant check, but could be useful in the event something weird happens to the jet speed
        self._check_zero_speed()

        print(f"Time to calculate jet speed: {time.time() - t:0.2f} s")
        self.jet_speed_units = "m / sec"


    def _calculate_all(self):

        # ================================
        # Jet Diameter Calculations
        # ================================

        # Calculate the jet diameter
        alpha = 1e-9 / 60  # ul/min to m^3 / sec scale factor
        self.jet_diameter = measure.jet_diameter(liquid_flow_rate=self.image_metadata['liquid_flow_mean'] * alpha, 
                                                    jet_speed=self.jet_speed, return_units='um')
        # Calculate the error in jet diameter
        if self.image_metadata['liquid_flow_sdev'] != -1:
            self.jet_diameter_err = measure.jet_diameter(liquid_flow_rate=self.image_metadata['liquid_flow_sdev'] * alpha, 
                                                            jet_speed=self.jet_speed, return_units='um')

        self.jet_diameter_units = 'um'

        # ================================
        # Flow Ratio Calculations
        # ================================

        self.flow_ratio = _get_flow_ratio(self.image_metadata['liquid_flow_mean'], self.image_metadata['gas_flow_mean'])


        # ================================
        # Jet Deviation Calculations
        # ================================

        self.jet_deviation_all, \
            self.jet_deviation, \
            self.jet_deviation_err = _get_jet_deviation(jet_fits=self.jet_fits, 
                                                            nozzle_tip_idx=self.config['nozzle_tip_position'])
        self.jet_deviation_units = 'rads'

        # ================================
        # Jet Length Calculations
        # ================================

        self.jet_length_all, \
            self.jet_length, \
            self.jet_length_err = _get_jet_length(nozzle_tip_idx=self.config['nozzle_tip_position'], 
                                                    transition_region_idx=self.transition_region_idx, 
                                                    jet_deviations=self.jet_deviation_all, 
                                                    pixel_size=self.image_metadata['pixel_size'])
        self.jet_length_units = 'um'

        # ================================
        # Jet Stability Calculations
        # ================================

        self.stability_measure_length = measure.interaction_point_from_jet_length(jet_length=self.jet_length, 
                                                        pixel_size_m=self.image_metadata['pixel_size'],  # in meters
                                                        method=self.config['interaction_point_settings']['ip_method'], 
                                                        percent_dist=self.config['interaction_point_settings']['ip_percent_distance'], 
                                                        provided_distance=self.config['interaction_point_settings']['ip_provided_distance']) # in pixel units
        self.stability_measure_length_units = self.jet_length_units

        self.jet_stability = measure.jet_stability(jet_deviations=self.jet_deviation_all,
                                                    jet_diameter=self.jet_diameter,
                                                    pixel_size_m=self.image_metadata['pixel_size'], 
                                                    nozzle_to_ip_distance=self.stability_measure_length)

        self._check_stability()


        # ================================
        # Droplet Dispersion Calculations
        # ================================

        # To Do: Implement a droplet dispersion method that uses the variance of the droplet image
        # To Do: The paper will have droplet dispersion from a per-droplet value, write this method too.

        self.droplet_dispersion = -1

        # ================================
        # Dimensionless Numbers
        # ================================

        self.weber, self.reynolds, \
            self.capillary, self.ohnesorge = _get_dimensionless_numbers(self.config['sample'], 
                                                                                self.jet_speed, 
                                                                                self.jet_diameter * 1e-6)

    def _determine_jet_speed(self):
        """
        Determines the jet speed based on cross-correlation and outer product methods.
        If both methods are in agreement (within a 2% margin), the average is taken.
        If one method fails, the other is used. If both fail, a warning is issued.
        """

        # If the cross correlation is bad, but the outer product is good
        if (self.cross_correlation_quality is False) and (self.outer_product_quality is True):
            self.jet_speed = self.jet_speed_by_outer_product

        # If the outer product is bad, but the cross correlation is good
        elif (self.outer_product_quality is False) and (self.cross_correlation_quality is True):
            self.jet_speed = self.jet_speed_by_cross_correlation

        # Both methods are good
        elif (self.outer_product_quality is True) and (self.cross_correlation_quality is True):
            # if self.jet_speed_comparison:
            #     speed_diff = abs(self.jet_speed_by_cross_correlation - self.jet_speed_by_outer_product)
            #     mean_speed = (self.jet_speed_by_cross_correlation + self.jet_speed_by_outer_product) / 2
            #     percent_difference = speed_diff / mean_speed * 100
            #     self.jet_speed = mean_speed
            # else:
            #     warn1 = "Jet speed comparison threshold not met, defaulting to "
            #     warn2 = "Cross Correlation value for jet speed."
            #     logging.warning(warn1 + warn2)
            self.jet_speed = self.jet_speed_by_cross_correlation

        # Both methods fail
        else:
            self.stack_quality = False
            self.jet_speed = -1
            warnings.warn("Both cross-correlation and outer product methods failed to produce a reliable jet speed.")


    # ----------------------------------------------------------
    # Data Saving Methods
    # ----------------------------------------------------------

    def output_dict(self):
        out = {}
        ignore = ['droplet_region', '_incorrect_sample', 'image_stack_raw', 
                    'n_cores', 'stack_processed', 'stack_filtered', 'stack_thresholded']
        to_float = ['epoch_time_start', 'epoch_time_stop']

        for attr_name, attr_value in self.__dict__.items():
            if attr_name in to_float:
                attr_value = float(attr_value)
            if attr_name not in ignore:
                if isinstance(attr_value, dict):
                    if 'jet_speed' not in attr_value:
                        # Flatten the dictionary and add to output
                        out.update(utils.flatten_dict(attr_value))
                else:
                    out[attr_name] = attr_value

        return out
