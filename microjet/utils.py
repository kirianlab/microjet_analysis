import numpy as np
import pandas as pd
import h5py
import glob
import os, sys
import json


def zoom_fixer(filepath:str, pixel_size:float):
    r"""Updates the zoom value of the h5 and text files.
    
        Sometimes we forget to update the zoom value in Odysseus. That's okay. 
        This function fixes the zoom value in the event you forget to update it 
        during data collection. 

        Note that in order for this to work, you must know the correct zoom value.
        All this function does is replace a value in an existing h5 file.

        TO DO: Edit this to accept a subset of runs, rather than the full data list.

        Args:
            filepath (str): the full path to the data, not including the h5 file
                                e.g., "/path/to/the/data/"
            pixel_size (float): the correct pixel size, used to update the metadata

    """

    all_data = sorted(glob.glob(os.path.join(filepath, "*.h5")))
    for i,im in enumerate(all_data):
        with h5py.File(im, 'r+') as f:
            x = f['metadata'][()]
            y = json.loads(x)
            y['optics']['pixelsize_um'] = pixel_size
            f['metadata'][()] = json.dumps(y, indent=4)


def check_shapes(data):
    data = np.atleast_3d(data)
    if np.shape(data)[-1] == 1:
        data = np.transpose(data, (2,0,1))
    return data


def determine_image_field(image_stack):
    """
    Determines whether an image stack is primarily bright field or dark field.

    Parameters:
    image_stack (numpy.ndarray): A stack of images, assumed to be in np.uint8 format.

    Returns:
    str: 'bright' if the image stack is bright field, 'dark' if it is dark field.

    Notes:
    - A bright field image typically has a higher average intensity, 
      whereas a dark field image has a lower average intensity.
    - The function calculates the median intensity across the entire stack 
      and classifies the stack based on this value.
    """
    # Calculate the median intensity of the entire stack
    median_intensity = np.median(image_stack)

    # Threshold to distinguish between bright and dark field
    # This threshold may need adjustment based on specific image characteristics
    threshold = 128  # Midpoint of the uint8 range (0-255)

    if median_intensity > threshold:
        return 'bright'
    else:
        return 'dark'

def pathogen(path):
    """
    Normalizes and expands a given file path.

    This function takes a file path, which may include ~ for the user's home directory, and returns the absolute normalized path.

    Args:
        path (str): A file path to normalize.

    Returns:
        str: The normalized and absolute file path.
    """
    return os.path.normpath(os.path.abspath(os.path.expanduser(path)))

def save_dict_to_hdf5(output_dict, file_path):
    with h5py.File(file_path, 'w') as hfile:
        for key, value in output_dict.items():
            if isinstance(value, dict):
                # Skip nested dictionaries or handle them as needed
                pass
            elif isinstance(value, (np.ndarray, list, tuple)):
                if isinstance(value, (np.ndarray)):
                    value = value.tolist()
                hfile.create_dataset(key, data=value, compression="gzip", compression_opts=9)
            elif np.isscalar(value):
                # Save scalars directly
                hfile.create_dataset(key, data=value)
            else:
                # Skip unsupported data types or handle them as needed
                pass

def read_dict_from_hdf5(file_path, columns=None):
    """Reads specified columns from an HDF5 file into a pandas DataFrame.

    Args:
        file_path (str): Path to the HDF5 file.
        columns (list): List of column names to load from the HDF5 file.

    Returns:
        DataFrame: A pandas DataFrame containing the data from the specified columns.
    """
    data = {}
    if columns == None:
        columns = _analysis_dataframe_columns()
    with h5py.File(file_path, 'r') as h5:
        for k, v in h5.items():
            if k in columns:
                item = v[()]
                # Check if the item is a byte string and decode it
                if isinstance(item, bytes):
                    data[k] = item.decode()
                else:
                    data[k] = item
    return data


def flatten_dict(d, parent_key='', sep='_'):
    """
    Flatten a nested dictionary, combining keys with the specified separator.
    """
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)

def _analysis_dataframe_columns():
    columns = [
            'sample', 'nozzle_id', 'filter', 'filter_method', 
            'filter_parameters_sigma', 'filter_parameters_truncate', 'threshold', 
            'threshold_method', 'threshold_parameters_h_low', 'threshold_parameters_h_high', 
            'subtract_stack_median', 'interaction_point_settings_ip_method', 
            'interaction_point_settings_ip_percent_distance', 'outer_product_method', 
            'droplet_area_threshold', 'parallelize', 'n_cores', 
            'cross_correlation_projection_threshold', 'nozzle_tip_position', 'process_stack', 
            'file_id', 'n_frames', 'year', 'date', 'time', 'epoch_time_start', 'epoch_time_stop', 
            'gas_flow', 'gas_flow_units', 'liquid_flow', 'liquid_flow2', 'liquid_flow_units', 
            'liquid_flow2_units', 'liquid_set_point', 'pixel_size', 'delay', 'frames', 
            'liquid_flow_mean', 'liquid_flow_sdev', 'gas_flow_mean', 'gas_flow_sdev', 
            'jet_speed', 'stack_quality', 'jet_diameter', 'jet_diameter_err', 'jet_deviation', 
            'jet_deviation_err', 'jet_speed_by_outer_product', 'jet_speed_by_cross_correlation', 
            'outer_product_quality', 'cross_correlation_quality', 'jet_speed_units', 
            'jet_diameter_units', 'jet_deviation_units', 'jet_speed_comparison', 
            'stability_measure_length', 'stability_measure_length_units', 'jet_stability', 
            'flow_ratio', 'jet_length', 'jet_length_err', 'jet_length_units', 'droplet_dispersion', 
            'weber', 'reynolds', 'capillary', 'ohnesorge'
        ]
    return columns

class CSVUpdater:
    def __init__(self, csv_file_path):
        self.csv_file_path = csv_file_path
        if os.path.exists(self.csv_file_path):
            self.data = pd.read_csv(self.csv_file_path)
            self.n_rows = len(self.data)
            self.columns = self.data.columns
        else:
            self.n_rows = 0
            # Define your default columns here
            self.columns = _analysis_dataframe_columns()

    def update_csv(self, data_dict):
        # Filter out non-scalar values
        filtered_dict = {k: v for k, v in data_dict.items() 
                         if np.isscalar(v) or isinstance(v, (str, bool))}

        # Ensure the dictionary keys match the CSV column headers
        aligned_dict = {col: filtered_dict.get(col, None) for col in self.columns}

        df = pd.DataFrame([aligned_dict])
        if not os.path.exists(self.csv_file_path):
            df.to_csv(self.csv_file_path, index=False, float_format='%.6f')
        else:
            df.to_csv(self.csv_file_path, mode='a', header=False, index=False, float_format='%.6f')
        
        # Update the number of rows
        self.n_rows += 1

    def file_id_exists(self, file_id):
        """Check if a file_id exists in the CSV file."""
        if self.n_rows > 0:
            existing_ids = pd.read_csv(self.csv_file_path)['file_id']
            return file_id in existing_ids.values
        return False

    @property
    def number_of_rows(self):
        return self.n_rows




