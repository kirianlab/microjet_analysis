
# To Do: Add references for each of these! The glycerol values came from Calvo's paper, but add the exact citation 

sample_characteristics = {}


# Set the water values
sample_characteristics.setdefault('water', {}).setdefault("rho", 997)
sample_characteristics.setdefault('water', {}).setdefault("rho_unit", "kg / m^3")
sample_characteristics.setdefault('water', {}).setdefault("sigma", 0.0728)
sample_characteristics.setdefault('water', {}).setdefault("sigma_unit", "N / m")
sample_characteristics.setdefault('water', {}).setdefault("mu", 0.00089)
sample_characteristics.setdefault('water', {}).setdefault("mu_unit", "Pa s")

# Set the glycerol 50% concentration values
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("rho", 1030)
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("rho_unit", "kg / m^3")
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("sigma", 0.066)
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("sigma_unit", "N / m")
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("mu", 0.00617)
sample_characteristics.setdefault('glycerol + water 50/50 v/v', {}).setdefault("mu_unit", "Pa s")

# Set the glycerol 25% concentration values
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("rho", 1078)
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("rho_unit", "kg / m^3")
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("sigma", 0.066)
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("sigma_unit", "N / m")
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("mu", 0.00276)
sample_characteristics.setdefault('glycerol + water 25/75 v/v', {}).setdefault("mu_unit", "Pa s")

# Add any other samples here using the same format. 













