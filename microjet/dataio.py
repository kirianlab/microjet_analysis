import os
import json
import configparser
import h5py
import numpy as np
import glob
import pandas as pd


def load_sahba_meta(meta_file):
    r""" Load some old-school metadata from Sahba's Odysseus package! """
    _time = ""
    _datetime = None
    gas_flow = None
    liquid_flow = None
    pixel_size = None
    delay = 550
    data = None
    if os.path.exists(meta_file):
        meta = configparser.ConfigParser()
        meta.read(meta_file)
        if 'timestamp' in meta:
            _datetime = (meta['timestamp']['year'], meta['timestamp']['date'], meta['timestamp']['time'])
        if 'fluidics' in meta:
            fluidics = meta['fluidics']
            if 'gas' in fluidics:
                g = fluidics['gas']
                if g != '':
                    q = g.split(" ")
                    gas_flow = float(q[0])
            if 'liquid' in fluidics:
                g = fluidics['liquid']
                if g != '':
                    q = g.split(' ')
                    liquid_flow = float(q[0])
        if 'optics' in meta:
            optics = meta['optics']
            if 'pixelsize_um' in optics:
                g = optics['pixelsize_um']
                if g != '':
                    pixel_size = float(g)
        data = {"year": _datetime[0], "date": _datetime[1], "time": _datetime[2], "gas_flow": gas_flow,
                "liquid_flow": liquid_flow, "pixel_size": pixel_size, "delay": delay}
    return data


def h5_metadata(filepath: str):
    """
    Open a single h5 file, check data version, return image stack and metadata.

    DEPRECIATE WARNING: 
    This method should be cleaned up and replaced with something better when ready.
    

    Args:
        filepath (str): The full path to your data, including the file extension

    Returns:
        image_stack (ndarray): The full image stack (example shape: (100, 1024, 512))
        dat (dict): The metadata
    """
    with h5py.File(filepath, "r") as h5:
        image_stack = np.array(h5["frames"], dtype=np.float32)
        image_stack = np.transpose(image_stack, (0, 2, 1))

        if 'metadata' in h5.keys():
            metadata = json.loads(h5['metadata'][()])
            dat = {
                'year': metadata.get('timestamp', {}).get('year', None),
                'date': metadata.get('timestamp', {}).get('date', None),
                'time': metadata.get('timestamp', {}).get('time', None),
                'epoch_time_start': metadata.get('fastcam', {}).get('epoch_time_start', None),
                'epoch_time_stop': metadata.get('fastcam', {}).get('epoch_time_stop', None),
                'gas_flow': metadata.get('gas', {}).get('reading', None),
                'gas_flow_units': metadata.get('gas', {}).get('units', None),
                'liquid_flow': metadata.get('liquid', {}).get('reading', None),
                'liquid_flow2': metadata.get('liquid2', {}).get('reading', None),
                'liquid_flow_units': metadata.get('liquid', {}).get('units', None),
                'liquid_flow2_units': metadata.get('liquid2', {}).get('units', None),
                'liquid_set_point': metadata.get('hplc', {}).get('flow_rate', None),
                'pixel_size': metadata.get('optics', {}).get('pixelsize_um', None),
                'delay': 550,
                'frames': metadata.get('fastcam', {}).get('frames', None),
                'resolution': (metadata.get('fastcam', {}).get('width', None), metadata.get('fastcam', {}).get('height', None)),
            }
        else:
            no_extension, file_extension = os.path.splitext(filepath)
            meta_file = no_extension + ".txt"
            print(f"WARNING: No metadata found in the h5! Gathering metadata from {meta_file}.")
            print("Are you using the newest version of Ody?")
            if not os.path.exists(meta_file):
                raise ValueError("No metadata found!")
            dat = load_sahba_meta(meta_file)

    return image_stack, dat

def open_data(filepath:str):
    # calling h5_metadata for backwards compatibility with old code. 
    image_stack, metadata = h5_metadata(filepath)
    image_stack = image_stack.astype(np.float32)
    return image_stack, metadata


def analysis_dataframe(config, save=True, savepath:str=None, verbose=True):
    r""" Converts the saved analysis into a run-wide csv file for easy viewing

        To Do: There must be a better way to handle the dictionary here.

        Args:
            config (dict): The input parameters
            save (bool): Whether to save the DataFrame as a csv file

    """

    path = config['save_path']
    if verbose:
        print(f"Making CSV... Looking for data here:", flush=True)
        print(f"\t{path}", flush=True)

    datapaths = glob.glob(os.path.join(path, "*.h5"))
    if verbose:
        print(f"Found {len(datapaths)} files to add to the CSV.", flush=True)

    dat = {'datapoint': [],
            'date': [],
            'time': [],
            'year': [],
            'delay': [],
            'pixel_size': [],
            'liquid_set_point': [],
            'liquid_flow': [],
            'liquid_flow_units': [],
            'liquid_flow_mean': [],
            'liquid_flow_sdev': [],
            'liquid_flow2': [],
            'liquid_flow2_units': [],
            'gas_flow': [],
            'gas_flow_units': [],
            'jet_speed': [],
            'jet_speed_err': [],
            'droplet_dispersion': [],
            'droplet_dispersion_err': [],
            'jet_deviation': [],
            'jet_deviation_err': [],
            'jet_diameter': [],
            'jet_diameter_err': [],
            'jet_length': [],
            'jet_length_err': [],
            'jet_stability': [],
            'angle_units': [],
            'epoch_time_start': [],
            'epoch_time_stop': [],
            'frames': [],
            'resolution': [],
            }

    for i, dp in enumerate(datapaths):
        if verbose:
            if i+1 == len(datapaths):
                print(f"Gathering data for file {i+1}/{len(datapaths)}", flush=True)
            else:
                print(f"Gathering data for file {i+1}/{len(datapaths)}", end='\r', flush=True)
        dat["datapoint"].append(dp.split(os.sep)[-1])
        f = h5py.File(dp, 'r')
        for k,v in f['metadata'].items():
            dat[k].append(v[()])
        for k,v in f['calculated_data'].items():
            dat[k].append(v[()])
        f.close()

    df = pd.DataFrame(dat)

    if save is True:
        df.to_csv(savepath, index=False)
        if verbose:
            print("Saving updated CSV file here:", flush=True)
            print(f"\t {savepath}", flush=True)
    return df

def process_config(config={}):
    config.setdefault('brightfield', False)                                  # Brightfield and darkfield imaging are both supported
    config.setdefault('filter', True)                                 # Performs a filtering step to the entire image stack
    config.setdefault('threshold', True)                              # Thresholds the image stack to remove any background
    config.setdefault('process', True)                                # Flag used to make the image stack binary (0 and 1)

    config.setdefault('filter_method', 'gausshp')                           # The filtering method: 'gausshp' and 'sobel' are the only options
    config.setdefault('filter_parameters', {}).setdefault('sigma', 5)
    config.setdefault('filter_parameters', {}).setdefault('truncate', 4)
    config.setdefault('threshold_method', 'hysteresis')                     # The thresholding method: 'otsu' and 'otsu_clipped' are the only options
    if config['brightfield'] is True:                                       # The hysteresis threshold default values, depending on bright/darkfield
        config.setdefault('threshold_parameters', {}).setdefault('h_low', 0.15)
        config.setdefault('threshold_parameters', {}).setdefault('h_high', 0.5)
    else:
        config.setdefault('threshold_parameters', {}).setdefault('h_low', 1)
        config.setdefault('threshold_parameters', {}).setdefault('h_high', 3)
    config.setdefault('processing_steps', {}).setdefault('footprint',       # The footprint used to fill holes
                                                            [[0, 0, 0],
                                                             [0, 0, 0],
                                                             [1, 1, 1],
                                                             [0, 0, 0],
                                                             [0, 0, 0]])
    config.setdefault('processing_steps', {}).setdefault('remove_small_objects', True)  # Optional flag to remove small pixels
    config.setdefault('processing_steps', {}).setdefault('min_pixel_size', 5)           # The minimum pixel size to remove
    config.setdefault('processing_steps', {}).setdefault('binary_closing', True)        # Optional binary_closing step
    config.setdefault('processing_steps', {}).setdefault('n_closing_iterations', 1)     # The number of iterations passed to the binary_closing
    config.setdefault('processing_steps', {}).setdefault('fill_holes', True)            # Optional hole-filling step
    config.setdefault('processing_steps', {}).setdefault('inflate_small_pixels', False) # Performs a binary dilation to artificially inflate small pixels after the initial cleanup
    config.setdefault('processing_steps', {}).setdefault('inflate_amount', 0)           # Number of dilate iterations
    config.setdefault('processing_steps', {}).setdefault('erode_1', False)
    config.setdefault('processing_steps', {}).setdefault('n_erosion_1_iteration', 0)
    config.setdefault('processing_steps', {}).setdefault('erode_2', False)
    config.setdefault('processing_steps', {}).setdefault('n_erosion_2_iteration', 0)
    config.setdefault('jettability', {}).setdefault('hough_threshold', 20)
    config.setdefault('jettability', {}).setdefault('min_line_length', 20)
    config.setdefault('jettability', {}).setdefault('line_gap', 5)
    config.setdefault('jettability', {}).setdefault('drop_ratio', 0.4)
    config.setdefault('jettability', {}).setdefault('distance_cutoff', 60)
    return config

def jetregions_config(config={}):
    config.setdefault('nozzle_tip_idx', 1000)                            # The user-defined nozzle tip index
    config.setdefault('nozzle_tip_finder_method', 'provided')                 # Options are 'column', 'intensity', or 'provided'
    config.setdefault('transition_region_idx', -1)                     # The user-defined nozzle tip index
    config.setdefault('transition_region_finder_method', 'nonzero')          # Options are 'row_diff', 'nonzero', 'intensity_change', 'provided'
    return config

def running_calculations_config(config={}):
    config.setdefault('nozzle_tip_idx', None)
    config.setdefault('transition_region_idx', None)
    config.setdefault('jet_fit', None)
    config.setdefault('interaction_point_method', 'percent_of_nozzle_length')
    config.setdefault('percent_dist', 0.5)
    config.setdefault('provided_distance', None)
    return config


def analysis_config(config={}):
    r""" Creates a default configuration dictionary for the gdvn analysis pipeline

        Args:
            config (dict): The user-defined config, defaults to an empty dictionary

        Returns:
            (dict): A config dictionary with any missing defaults added
    """
    config.setdefault('brightfield', True)                                  # Brightfield and darkfield imaging are both supported
    config.setdefault('filter', True)                                 # Performs a filtering step to the entire image stack
    config.setdefault('filter_method', 'gausshp')                           # The filtering method: 'gausshp' and 'sobel' are the only options
    config.setdefault('filter_parameters', {}).setdefault('sigma', 10)
    config.setdefault('filter_parameters', {}).setdefault('truncate', 4)
    config.setdefault('add_butter_filter', False)                           # Adds a butterworth lowpass filter after the filtering method chosen
    config.setdefault('threshold', True)                              # Thresholds the image stack to remove any background
    config.setdefault('threshold_method', 'hysteresis')                     # The thresholding method: 'otsu' and 'otsu_clipped' are the only options
    if config['brightfield'] is True:                                       # The hysteresis threshold default values, depending on bright/darkfield
        config.setdefault('threshold_parameters', {}).setdefault('h_low', 0.15)
        config.setdefault('threshold_parameters', {}).setdefault('h_high', 0.5)
    else:
        config.setdefault('threshold_parameters', {}).setdefault('h_low', 1)
        config.setdefault('threshold_parameters', {}).setdefault('h_high', 3)
    config.setdefault('nozzle_tip_position', -1)                            # The user-defined nozzle tip index
    config.setdefault('nozzle_tip_finder_method', 'provided')                 # Options are 'provided', 'simple', or 'shot-by-shot'
    config.setdefault('transition_region_position', -1)                     # The user-defined nozzle tip index
    config.setdefault('transition_region_finder_method', 'simple')          # Options are 'provided' or 'simple'
    config.setdefault('process', True)                                # Flag used to make the image stack binary (0 and 1)
    config.setdefault('processing_steps', {}).setdefault('footprint',       # The footprint used to fill holes
                                                            [[0, 0, 0],
                                                             [0, 0, 0],
                                                             [1, 1, 1],
                                                             [0, 0, 0],
                                                             [0, 0, 0]])
    config.setdefault('processing_steps', {}).setdefault('remove_small_objects', True)  # Optional flag to remove small pixels
    config.setdefault('processing_steps', {}).setdefault('min_pixel_size', 5)           # The minimum pixel size to remove
    config.setdefault('processing_steps', {}).setdefault('binary_closing', True)        # Optional binary_closing step
    config.setdefault('processing_steps', {}).setdefault('n_closing_iterations', 3)     # The number of iterations passed to the binary_closing
    config.setdefault('processing_steps', {}).setdefault('fill_holes', True)            # Optional hole-filling step
    config.setdefault('processing_steps', {}).setdefault('filter_outlier_frames', True) # Filters any outlier frames in a single movie
    config.setdefault('processing_steps', {}).setdefault('outlier_filter_threshold', 3) # Remove frames whose summed total pixels are beyond this threshold
    config.setdefault('processing_steps', {}).setdefault('inflate_small_pixels', False) # Performs a binary dilation to artificially inflate small pixels after the initial cleanup
    config.setdefault('processing_steps', {}).setdefault('inflate_amount', 3)           # Number of dilate iterations
    config.setdefault('processing_steps', {}).setdefault('erode_1', False)
    config.setdefault('processing_steps', {}).setdefault('n_erosion_1_iteration', 1)
    config.setdefault('processing_steps', {}).setdefault('erode_2', False)
    config.setdefault('processing_steps', {}).setdefault('n_erosion_2_iteration', 3)
    config.setdefault('label_method', 'standard')                                       # Options are 'standard' or 'watershed'
    config.setdefault('segment_droplets', True)
    config.setdefault('subtract_stack_median', True)
    config.setdefault('fit_jet', True)
    config.setdefault('find_droplet_centroids', True)
    config.setdefault('stability_from_nozzle_distance', 50)                 # distance from nozzle tip in microns, used to measure jet stability
    config.setdefault('percent_dist', 0.95)
    config.setdefault('provided_distance', None)
    config.setdefault('interaction_point_method', 'percent_of_nozzle_length')
    config.setdefault('droplet_centroiding_params', {}).setdefault('max_droplets', 20)
    config.setdefault('droplet_centroiding_params', {}).setdefault('position_vs_linefit_centroid_removal', 5)
    config.setdefault('droplet_centroiding_params', {}).setdefault('droplet_area_threshold', 40)
    config.setdefault('jet_speed_method', '2D_cross_correlation')           # Options are '2D_cross_correlation' and 'outer_product'
    config.setdefault('cross_correlation_parity_condition', 1.5)            # Decides which frame to start correlating, even or odd.
    config.setdefault('cross_correlation_parity', None)                     # None triggers the automated parity condition, 'even' or 'odd' run the cc with the given parity
    config.setdefault('region_properties', None)                            # Region properties for labeling methods
    config.setdefault('parallelize', True)
    config.setdefault('n_cores', 5)
    # config.setdefault('log_path', None)
    config.setdefault('save_stacks', {}).setdefault('raw_stack', False)
    config.setdefault('save_stacks', {}).setdefault('filtered_stack', False)
    config.setdefault('save_stacks', {}).setdefault('thresholded_stack', False)
    config.setdefault('save_stacks', {}).setdefault('processed_stack', False)
    config.setdefault('save_stacks', {}).setdefault('correlation_matrix', True)
    config.setdefault('save_stacks', {}).setdefault('stack_sdev', True)
    config.setdefault('cross_correlation_projection_threshold', 1)
    return config

def config(config={}):
    r""" Creates a default configuration dictionary for the gdvn analysis pipeline

        Args:
            config (dict): The user-defined config, defaults to an empty dictionary

        Returns:
            (dict): A config dictionary with any missing defaults added
    """
    config.setdefault('filter', True)                                 # Performs a filtering step to the entire image stack
    config.setdefault('filter_method', 'gausshp')                           # The filtering method: 'gausshp' and 'sobel' are the only options
    config.setdefault('filter_parameters', {}).setdefault('sigma', 10)
    config.setdefault('filter_parameters', {}).setdefault('truncate', 4)
    config.setdefault('threshold', True)                              # Thresholds the image stack to remove any background
    config.setdefault('threshold_method', 'hysteresis')                     # The thresholding method: 'otsu' and 'otsu_clipped' are the only options
    config.setdefault('threshold_parameters', {}).setdefault('h_low', 1)
    config.setdefault('threshold_parameters', {}).setdefault('h_high', 3)
    config.setdefault('subtract_stack_median', True)
    config.setdefault('interaction_point_settings', {}).setdefault('ip_method', 'percent_of_nozzle_length')
    config.setdefault('interaction_point_settings', {}).setdefault('ip_percent_distance', 0.95)
    config.setdefault('interaction_point_settings', {}).setdefault('ip_provided_distance', None)
    config.setdefault('outer_product_method', 'centroid')
    config.setdefault('droplet_area_threshold', 40)
    config.setdefault('parallelize', True)
    config.setdefault('n_cores', int(os.cpu_count()/2 + 1))
    config.setdefault('cross_correlation_projection_threshold', 1)
    config.setdefault('processing_steps', {}).setdefault('remove_small_objects', True)  # Optional flag to remove small pixels
    config.setdefault('processing_steps', {}).setdefault('min_pixel_size', 10)           # The minimum pixel size to remove
    config.setdefault('processing_steps', {}).setdefault('binary_erosion', False)  # Optional flag to remove small pixels
    return config

def example_data(example:str):
    r""" Loads the available example data given the example type
    
        TO DO: Add 'stack_cleanup_brightfield', and any others! 

            Current example options are:
                'stack_cleanup_darkfield', 'stack_cleanup_brightfield', 'binary'
        Args:
            example (str): The example string. Available data is:

    """
    if example == 'stack_cleanup_darkfield':
        stack, metadata = h5_metadata("../examples/example_data/example_cleanup_dark.h5")
        stack *= -1
        for i in range(np.shape(stack)[0]):
            stack[i, :, :] /= np.median(stack[i, :920, :])
    elif example == 'stack_cleanup_brightfield':
        stack, metadata = h5_metadata("../examples/example_data/example_cleanup_bright.h5")
        for i in range(np.shape(stack)[0]):
            stack[i, :, :] /= np.median(stack[i, :, :])
    elif example == 'binary':
        stack, metadata = h5_metadata("../examples/example_data/example_binary.h5")
        stack = np.swapaxes(stack, 1, 2)
    else:
        exdat = ["stack_cleanup_darkfield", "stack_cleanup_brightfield", "binary"]
        err1 = "Example not found! Available example data is: \n"
        err2 = f"\t\t {exdat}"
        raise ValueError(err1+err2)
    return stack, metadata


def rawdata_to_example(data:str, example_name:str, n_frames:int=20):
    r'''Given a raw dataset, will create a new dataset with n_frames

        Example data doesn't need to be 120+ frames unless we calculate jet speeds.
        This method creates a new dataset that is an exact copy of the incoming dataset,
        just with `n_frames` number of frames. 

        Args:
            data (str): The full path to the raw datafile
            example_name (str): Full path to where you want to store the example, including the .h5 extension
            n_frames (int): the number of frames to include, default is 20.
    '''
    f = h5py.File(data, 'r')
    g = h5py.File(example_name, 'w')
    g['frames'] = f['frames'][:n_frames,:,:]
    g['metadata'] = f['metadata'][()]
    f.close()
    g.close()
    print(f"Wrote the example dataset here: \n\t {example_name}")

def get_data_from_logs(metadata:dict, log_filepath:str, delimiter:str=" "):
    r""" Get a list of flow rates from the log files

        Given the metadata and the filepath to the logs, this method
        will search for flow rates within the epoch time range provided 
        by the metadata. 

        Args:
            metadata (dict): the metadata. Must contain 'epoch_time_end', 
                                'epoch_time_end', and 'date' in the keys. Further,
                                'date' must be in a YYYYMMDD format.
            log_filepath (str): The full path to your log files,
                                    file name should contain a date in the YYYY-MM-DD format
            delimeter (str): Log file delimeter, default is a space

        Returns:
            (ndarray): A list of liquid flow rates filtered by epoch start/stop times.
    """

    date = metadata['date']
    date_log = f"{date[:4]}-{date[4:6]}-{date[6:]}" # should output as 2023-02-09

    # Load all the log files from that date
    logs = glob.glob(os.path.join(log_filepath, f"*{date_log}*"))

    # get the epoch start and stop times from metadata
    e0, e1 = float(metadata['epoch_time_start']), float(metadata['epoch_time_stop'])

    # loop through each file in logs and find the files that correspond to the correct epoch start/stop times
    df = pd.DataFrame(columns=['epoch_time','flow_rate'])
    for i, lo in enumerate(logs):
        d = pd.read_table(lo, delimiter=' ') # load the log file
        d = d.loc[d['epoch_time'] != 'epoch_time'] # remove weird header issues
        d = d[(d['epoch_time'].astype(float) >= e0) & (d['epoch_time'].astype(float) <= e1)] # filter by epoch time

        if d.empty:
            d = pd.read_table(os.path.join(log_filepath, "data.log"), delimiter=' ') # load the log file
            d = d.loc[d['epoch_time'] != 'epoch_time'] # remove weird header issues
            d = d[(d['epoch_time'].astype(float) >= e0) & (d['epoch_time'].astype(float) <= e1)] # filter by epoch time


        if d.empty is False:
            # append correct epoch times to the new dataframe
            # needed in case the epoch times are across multiple log files
            df = pd.concat([df,d], ignore_index=True)

    return df['flow_rate'].to_numpy(dtype='float')



