import os, sys, h5py, glob, time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy import ndimage

import skimage
from skimage.filters import threshold_otsu, threshold_local
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
from skimage.segmentation import watershed
from skimage.feature import peak_local_max

try:
    from . import filters, process, dataio, regions #, viewers, dataio, analysis
except:
    import filters, process, dataio, regions #, viewers, dataio, analysis
# make one class that takes in a single frame that will output a dict with calculated params.

# save every 10th or so image to check for weird stuff. (do this later).

# in the live show, calculate the jet after processing image based on the longest nonzero values method
# i've already written. If it's below a certain user-defined threshold, assume no jet.

def rotate():
    # to do
    # Method to rotate a jet with nozzle tip in view so that it flows from right to left. 
    return

def identify_jetting(image, threshold:float=0.9):
    r""" Given a processed (binary) image, will determine if there is a jet.
    """
    jet = False
    x = np.sum(image, axis=1)
    try:
        xs = regions.find_longest_nonzero_subset(x)
        jidx = int(xs[0])
        if jidx < threshold * np.shape(image)[0]:
            jet = True
        return jet
    except IndexError:
        return jet


































