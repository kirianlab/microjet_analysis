import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os, gc
from microjet import figures 



class GDVNPlots:
    data = None
    def __init__(self, csv_file_path, output_folder, dpi=300, n_levels=500,
                    x_range=(0, 100), y_range=(0, 85)):
        self.csv_file_path = csv_file_path
        self.output_folder = output_folder
        # self.figures = {}
        self.dpi = dpi
        self.x_range = x_range
        self.y_range = y_range

    def update_data(self):
        self.data = pd.read_csv(self.csv_file_path)

    def flow_plot_jet_speed(self, z_range=(0, 140)):
        z_key = 'jet_speed'  # Column name in CSV
        fig, ax = figures.plot_flow_regions(df=self.data, z_key=z_key, dpi=self.dpi,
                                            n_neighbors=20, knn_weights='density',
                                            xr=self.x_range, yr=self.y_range, zr=z_range)
        return fig

    def flow_plot_jet_length(self, z_range=(0, 400)):
        z_key = 'jet_length'  # Column name in CSV
        fig, ax = figures.plot_flow_regions(df=self.data, z_key=z_key, dpi=self.dpi,
                                            n_neighbors=20, knn_weights='density',
                                            xr=self.x_range, yr=self.y_range, zr=z_range)
        return fig


    def flow_plot_jet_stability(self, z_range=(0, 2)):
        z_key = 'jet_stability'  # Column name in CSV
        fig, ax = figures.plot_flow_regions(df=self.data, z_key=z_key, dpi=self.dpi,
                                            n_neighbors=20, knn_weights='density',
                                            xr=self.x_range, yr=self.y_range, zr=z_range)
        return fig

    def flow_plot_jet_diameter(self, z_range=(0, 5)):
        z_key = 'jet_diameter'  # Column name in CSV
        fig, ax = figures.plot_flow_regions(df=self.data, z_key=z_key, dpi=self.dpi,
                                            n_neighbors=20, knn_weights='density',
                                            xr=self.x_range, yr=self.y_range, zr=z_range)
        return fig

    def weber_flowrate(self):
        y_key = 'weber'
        yr = (0, 1.1 * self.data['weber'].max())
        fig, ax = figures.dimensionless_numbers(df=self.data, y_key=y_key,
                                                    xr=self.x_range, yr=yr)
        return fig
    
    def reynolds_flowrate(self):
        y_key = 'reynolds'
        yr = (0, 1.1 * self.data['reynolds'].max())
        fig, ax = figures.dimensionless_numbers(df=self.data, y_key=y_key,
                                                    xr=self.x_range, yr=yr)
        return fig

    def capillary_flowrate(self):
        y_key = 'capillary'
        yr = (0, 1.1 * self.data['capillary'].max())
        fig, ax = figures.dimensionless_numbers(df=self.data, y_key=y_key,
                                                    xr=self.x_range, yr=yr)
        return fig
    
    def ohnesorge_flowrate(self):
        y_key = 'ohnesorge'
        yr = (0, 1.1 * self.data['ohnesorge'].max())
        fig, ax = figures.dimensionless_numbers(df=self.data, y_key=y_key,
                                                    xr=self.x_range, yr=yr)
        return fig

    def generate_figure(self, plot_method, *args, **kwargs):
        """ Generate and save a specific figure. """
        fig = plot_method(*args, **kwargs)
        fig.savefig(os.path.join(self.output_folder, f"{plot_method.__name__}.png"))
        plt.close(fig)  # Close the figure to free memory
        gc.collect()  # Explicitly invoke garbage collection

    def update_plots(self):
        self.update_data()
        self.generate_figure(self.flow_plot_jet_speed)
        self.generate_figure(self.flow_plot_jet_diameter)
        self.generate_figure(self.flow_plot_jet_stability)
        self.generate_figure(self.flow_plot_jet_length)
        self.generate_figure(self.weber_flowrate)
        self.generate_figure(self.reynolds_flowrate)
        self.generate_figure(self.capillary_flowrate)
        self.generate_figure(self.ohnesorge_flowrate)
        # ... other plot methods ...
        plt.close()

    # def save_figures(self):
    #     for name, fig in self.figures.items():
    #         fig.savefig(os.path.join(self.output_folder, f"{name}.png"))

    # # def update_plots(self):
    # #     self.update_data()
    # #     self.plot_key_combinations()
    # #     for fig in self.figures.values():
    # #         fig.canvas.draw()
    # #     self.save_figures()

    # def show_plots(self):
    #     for fig in self.figures.values():
    #         plt.show()











































































