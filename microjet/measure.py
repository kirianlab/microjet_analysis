import numpy as np
import warnings, logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from scipy.signal import correlate, find_peaks
from scipy.spatial import distance_matrix


from skimage import feature, transform
try:
    from . import regions
    from . import process
except:
    import regions
    import process
try:
    from .utils import check_shapes
except:
    from utils import check_shapes


def identify_jetting(image, threshold:float=0.85):
    r""" Given a processed (binary) image, will determine if there is a jet.
    """
    jet = False
    x = np.sum(image, axis=1)
    try:
        xs = regions.find_longest_nonzero_subset(x)
        jidx = int(xs[0])
        if jidx < threshold * np.shape(image)[0]:
            jet = True
        return jet, xs
    except IndexError:
        return jet, None

def identify_jetting_by_line_analysis(frame, 
                                       hough_threshold=50, 
                                       min_line_length=50, 
                                       line_gap=10):
    """
    Determine if a jet is present in the provided frame using scikit-image.

    Parameters:
    - frame: Input grayscale frame
    - hough_threshold: Threshold for Hough transform
    - min_line_length: Minimum length of the detected line to be considered as a jet
    - line_gap: Maximum gap between line segments to be treated as a single line

    Returns:
    - jet_detected: Boolean indicating if a jet is present
    """

    # Use Hough line transform to detect lines
    lines = transform.probabilistic_hough_line(frame, threshold=hough_threshold, 
                                                line_length=min_line_length, line_gap=line_gap)
    
    jet_detected = False
    if lines:
        # If any line segments are detected, a jet is present
        jet_detected = True
    
    return jet_detected, lines

def identify_jetting_by_pair_distribution(image, drop_ratio=0.5, distance_cutoff=10):
    """
    Identify jetting in an image based on the pair distribution function (PDF) of white pixel distances.

    The method computes a histogram of distances between white pixels in the image. It then checks 
    for a significant drop in the histogram values beyond a specified distance cutoff. If the drop 
    is larger than the specified ratio, the method identifies the presence of jetting.

    Parameters:
    ----------
    image : ndarray
        Input 2D binary image where white pixels (or high intensity) represent potential jetting regions.
    drop_ratio : float, optional (default=0.5)
        The ratio of post-cutoff histogram peak to initial histogram peak which, when exceeded, 
        signifies the presence of jetting. For example, a drop_ratio of 0.5 means that the method 
        will identify jetting if the histogram peak beyond the distance_cutoff is less than half 
        of the initial peak value.
    distance_cutoff : int, optional (default=10)
        The distance value below which PDF values are considered for determining the initial peak. 
        Values beyond this distance are considered for identifying the post-cutoff peak.

    Returns:
    -------
    jet_detected : bool
        Boolean indicating if a jet is detected in the image based on the PDF analysis.
    histogram_data : tuple
        A tuple containing the histogram (PDF) and bin edges used in the analysis. This can be used 
        for further analysis or for visualization purposes.

    Example:
    --------
    is_jet, hist_data = identify_jetting_by_pair_distribution(my_image)
    """
    
    # Get coordinates of white pixels
    y, x = np.where(image)
    coords = np.column_stack((x, y))
    
    # Compute distance matrix and get all unique distances
    dists = distance_matrix(coords, coords)
    unique_dists, counts = np.unique(dists, return_counts=True)
    
    # Create a histogram (which is essentially the PDF)
    histogram, bin_edges = np.histogram(unique_dists, bins=100, weights=counts)

    # Look for a drop in the histogram beyond the distance_cutoff
    initial_peak_value = np.nanmax(histogram[:distance_cutoff])
    post_cutoff_value = np.nanmin(histogram[distance_cutoff:])
    
    r = np.nan_to_num(post_cutoff_value / initial_peak_value)
    #print('I0: ', initial_peak_value, 'I1:', post_cutoff_value, 'ratio: ', r, flush=True)
    jet_detected = (r < drop_ratio) and (r > 0.0) 

    return jet_detected, (histogram, bin_edges)


def jet_length(nozzle_tip_idx:int, 
                transition_region_idx:int, 
                jet_deviation:float, 
                pixel_size:float, 
                jet_deviation_units='degrees') -> float:
    """
    Computes the physical length of the jet in meters.
    
    This function calculates the length of the jet from the nozzle tip to the transition region.
    The length is adjusted for any deviation angle of the jet to ensure an accurate representation of the physical length.

    Args:
        nozzle_tip_idx (int): Index of the pixel where the nozzle tip is located.
        transition_region_idx (int): Index of the pixel where the jet transition region begins.
        jet_deviation (float): The angle (in degrees) of jet deviation from vertical.
        pixel_size (float): The size of each pixel in the image, in meters.

    Returns:
        length (float): The physical length of the jet in meters.
    """
    dx = np.abs(nozzle_tip_idx - transition_region_idx)
    if jet_deviation_units == 'degrees':
        length = dx * np.cos(np.radians(jet_deviation)) * pixel_size
    else:
        length = dx * np.cos(jet_deviation) * pixel_size

    return length


def jet_speed(droplet_translation:int, pixel_size:float, laser_delay:float) -> float:
    """
    Calculates the speed of the jet in meters per second.

    Args:
        droplet_translation (int): The pixel distance that the droplet has moved.
        pixel_size (float): The size of each pixel in the image, in meters.
        laser_delay (float): The delay between laser pulses, in seconds.

    Returns:
        speed (float): The speed of the jet in meters per second.
    """
    return droplet_translation * pixel_size / laser_delay


def jet_diameter(liquid_flow_rate:float, jet_speed:float, return_units='um') -> float:
    """
    Calculates the diameter of the jet in meters.

    Args:
        liquid_flow_rate (float): The flow rate of the liquid in the jet, in cubic meters per second.
        jet_speed (float): The speed of the jet in meters per second.

    Returns:
        diameter (float): The diameter of the jet in meters.
    """
    if jet_speed == 0:
        return 0

    di = 2 * np.sqrt(liquid_flow_rate / (np.pi * jet_speed))
    
    if np.isnan(di) or np.isinf(di) or not np.isreal(di):
        return 0

    if return_units == 'um':
        di *= 1e6
        
    return di


def jet_deviation(jet_fit, nozzle_tip_idx:int) -> float:
    """
    Calculates the deviation of the jet from vertical, in degrees.

    This function takes in a fitted line representing the jet in the image and then calculates 
    the angle that this line makes with the vertical.

    Args:
        jet_fit (list or np.ndarray): The y-coordinates of the fitted line, representing the jet in the image.
                                     The fit should be calculated externally and passed as a parameter.
        nozzle_tip_idx (int): Index of the pixel where the nozzle tip is located.

    Returns:
        deviation (float): The deviation of the jet from vertical, in radians.
    """
    jet_fit = np.asarray(jet_fit)  # ensure jet_fit is a numpy array
    b = np.abs(jet_fit[0] - jet_fit[-1])
    return np.arctan(b/nozzle_tip_idx)


def interaction_point_from_jet_length(jet_length:float, 
                                        pixel_size_m:float, 
                                        method:str='percent_of_nozzle_length',
                                        percent_dist:float=0.5, 
                                        provided_distance:float=None) -> float:
    """
    Calculates the interaction point based on jet length.

    This function calculates the interaction point based on jet length using two methods:
    1. 'percent_of_nozzle_length': The interaction point is a percentage of the nozzle length.
    2. 'provided': The interaction point is a provided distance.

    Args:
        jet_length (float): Length of the jet (in meters).
        pixel_size (float): Size of a pixel (in meters).
        method (str, optional): Method to calculate the interaction point. Can be 'percent_of_nozzle_length' or 'provided'. Defaults to 'percent_of_nozzle_length'.
        percent_dist (float, optional): Percentage of nozzle length as interaction point. Used if method is 'percent_of_nozzle_length'. Defaults to 0.5.
        provided_distance (float, optional): Interaction point as a specific distance (in meters). Used if method is 'provided'. Defaults to None.

    Returns:
        float: Interaction point calculated in pixels.

    Raises:
        ValueError: If the input method is not one of the available methods, or if the conditions for interaction point are not parsable.
    """
    methods_available = ['percent_of_nozzle_length', 'provided']
    if method not in methods_available:
        err = f"Method not available, choose from the following: {methods_available}"
        raise ValueError(err)

    if (method == 'percent_of_nozzle_length') and isinstance(percent_dist, float):
        d = percent_dist * jet_length / pixel_size_m
    elif (method == 'provided') and isinstance(provided_distance, float):
        d = provided_distance / pixel_size_m
    else:
        raise ValueError("Cannot parse interaction point conditions")

    return d


def jet_stability(jet_deviations:list,
                    jet_diameter:float,
                    pixel_size_m:float, 
                    nozzle_to_ip_distance:float):

    dx = nozzle_to_ip_distance * np.cos(jet_deviations)
    y = dx * np.tan(jet_deviations)

    ys = np.nanstd(y)
    dj = jet_diameter / pixel_size_m
    return ys / dj


# ============================
# Cross correlation code
# ============================

def cross_correlation(a, b):
    """
    Computes the cross-correlation between two input arrays using the Fast Fourier Transform (FFT) method.

    This function computes the cross-correlation of the input arrays (a, b) using FFTs for efficiency. 
    The result is the same size as input arrays and represents the correlation between the two arrays.

    Args:
        a (np.ndarray): First input array for cross-correlation.
        b (np.ndarray): Second input array for cross-correlation.

    Returns:
        np.ndarray: Cross-correlation of a and b, computed using FFTs. The size of returned array is same as input arrays.
    """
    return np.real(np.fft.ifftn(np.fft.fftn(a)*np.conj(np.fft.fftn(b))))

def _pad(stack, ns, nf):
    """
    Pads the given image stack with zeros on all sides.

    This function is a convenience wrapper around numpy.pad to avoid wrap-around effects in subsequent processing.

    Args:
        stack (ndarray): The image stack to be padded.
        ns (int): The size of padding along the stack's vertical dimension.
        nf (int): The size of padding along the stack's horizontal dimension.

    Returns:
        ndarray: The padded image stack.
    """
    return np.pad(stack.copy(), ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))


def _find_cc_peak(cc, ns, nf):
    """
    Finds the peak of the cross-correlation matrix, representing the most common drop displacement.

    Args:
        cc (ndarray): The cross-correlation matrix.
        ns (int): Size parameter associated with the vertical dimension of `cc`.
        nf (int): Size parameter associated with the horizontal dimension of `cc`.

    Returns:
        tuple: A tuple (pf, ps) representing the x and y coordinates of the correlation peak.

    Raises:
        ValueError: If an IndexError occurs, indicating potential mismatch in expected data type (e.g., brightfield analysis on darkfield data).
    """ 
    try:
        w = np.where(cc == np.max(cc))
        pf = w[1][0] - np.floor(nf/2)
        ps = w[0][0] - np.floor(ns/2)
    except IndexError:
        e1 = "IndexError was thrown at correlation peak finder! "
        e2 = "Is your data bright or darkfield? "
        e3 = "\nThis error is typically raised when brightfield analysis is requested on darkfield data."
        raise ValueError(e1+e2+e3)
    return pf, ps

def __cc_even(stack):
    """
    Calculates the cross-correlation of alternating even-indexed frames in the stack.

    Args:
        stack (ndarray): The image stack array.

    Returns:
        ndarray: The accumulated cross-correlation matrix from even-indexed frames.
    """
    cc = 0
    nframes, ns, nf = np.shape(stack)
    # correlate with even + 1 frames
    for i in range(0, nframes, 2):
        im1 = _pad(stack[i, :, :], ns, nf)
        im2 = _pad(stack[i+1, :, :], ns, nf)
        cc += cross_correlation(im1, im2)
    return cc

def __cc_odd(stack):
    """
    Calculates the cross-correlation of alternating odd-indexed frames in the stack.

    Args:
        stack (ndarray): The image stack array.

    Returns:
        ndarray: The accumulated cross-correlation matrix from odd-indexed frames.
    """
    cc = 0 
    nframes, ns, nf = np.shape(stack)
    # correlate with odd + 1 frames
    for i in range(1, nframes-1, 2):
        im1 = _pad(stack[i, :, :], ns, nf)
        im2 = _pad(stack[i+1, :, :], ns, nf)
        cc += cross_correlation(im1, im2)
    return cc 

def __correlate(image_stack, parity):
    stack = image_stack.copy()
    ns, nf = np.shape(stack)[1:3]
    if (parity != 'even') and (parity != 'odd'):
        print("Incorrect key for parity variable. Defaulting to even.")
        parity = 'even'

    if parity == 'even':
        cc = __cc_even(stack)
    else:
        cc = __cc_odd(stack)
    cc /= np.mean(cc)

    # Auto-correlation of the mean, for background correction (approximately)
    mean = _pad(np.mean(stack, axis=0), ns, nf)
    acf = cross_correlation(mean, mean)
    acf /= np.mean(acf)

    # Approximate background correction: subtract auto-correlation of the mean from the mean of cross-correlations
    cc -= acf
    cc = np.fft.fftshift(cc)

    # Remove zero-padding
    cc = cc[int(ns/2):ns+int(ns/2), int(nf/2):nf+int(nf/2)]

    # Find the cross correlation peak
    pf, ps = _find_cc_peak(cc, ns, nf)
    return cc, pf, ps

def __redo_cc(correlation_matrix, cond:float=1.5):
    r""" Determine if the cross correlation projection is 'good' or not

        Check the cross correlation projection against its variance. If above a threshold value, 
        trigger the cross correlation to be recalculated with a different parity. 

        Args:
            cond (float): The condition to compare against, value corresponds to the variance of the projected correlation matrix
        Returns:
            The truth.

    """
    redo = False
    corr = correlation_matrix.copy()
    cidx = np.where(corr < 0)
    corr[cidx] = 0
    corr /= np.max(corr)
    po = np.sum(corr, axis=1)
    var = np.var(po)
    if var > cond:
        redo = True
    return redo

def get_stack_cross_correlation(stack, parity:str='even', parity_condition=1.5):
    r""" Calculates the cross correlation of pairs of frames.

        Args:
            stack (ndarray): The image stack array
            parity (str): 'even' or 'odd', chooses which frames to start the correlation on
        Returns:
            cc (ndarray): The cross correlation matrix
            pf (float): The x-coordinate of the correlation peak 
            ps (float): The y-coordinate of the correlation peak
    """

    cc, pf, ps = __correlate(stack, parity='even')
    if __redo_cc(cc, cond=parity_condition):
        logging.warning("Correlation matrix failed condition with even frames, trying again with the odd frames")
        cc, pf, ps = __correlate(stack, parity='odd')
        if __redo_cc(cc, cond=parity_condition):
            logging.warning('Cross Correlation condition failed on both even and odd frames.')

    return cc, pf, ps

def get_cross_correlation_projection(cross_correlation, threshold=1):
    """
    Projects the thresholded cross-correlation matrix to a 1D array by summing across one axis.

    Args:
        cross_correlation (ndarray): The cross-correlation matrix.
        threshold (float): A factor of the maximum value in `cross_correlation` used to threshold the data. Defaults to 1.
                            A value of 1 means all the data is visualized in the projection, values between 0 and 1
                            flatten data that are a threshold percentage away from the value.
                            Values greater than 1 are meaningless.

    Returns:
        ndarray: The projected 1D array from the thresholded cross-correlation matrix.
    """
    if (threshold > 1) or (threshold < 0):
        raise ValueError("threshold should be between 0 and 1.")

    threshold_value = threshold * np.max(cross_correlation)
    cc_thresholded = np.where(cross_correlation > threshold_value, cross_correlation, 0)

    # Summing along the axis with the thresholded data
    cc_proj = np.sum(cc_thresholded, axis=1)
    return cc_proj

def js_cross_correlation(pf, ps):
    """
    Calculates the displacement and angle from cross-correlation peak coordinates.

    Args:
        pf (float): The x-coordinate of the correlation peak.
        ps (float): The y-coordinate of the correlation peak.

    Returns:
        tuple: A tuple (displacement, angle) where displacement is the calculated displacement from the origin, and angle is the angle in degrees.
    """
    displacement = np.sqrt(pf**2 + ps**2)
    angle = np.arctan2(ps, pf)*180/np.pi - 90
    return displacement, angle



def get_speed_by_2D_cross_correlation(image_stack, pixelsize_m, laser_delay=550e-9, parity='even'):

    xcorr, sx, sy = get_stack_cross_correlation(stack=image_stack, parity=parity)
    s = np.sqrt(sx**2 + sy**2)
    jet_speed = _displacement_to_speed(s, pixelsize_m, laser_delay)
    dat = {'cross_correlation_matrix': xcorr,
            'sx': sx,
            'sy': sy,
            'displacement': s,
            'jet_speed': jet_speed}
    return dat




# ============================
# Outer Product code
# ============================


def _shear(matrix):
    r"""
        A special matrix shear operation for analyzing
        the jet cross correlations
    """
    arr = np.empty_like(matrix)
    fs, ss = np.shape(matrix)
    for i in range(ss):
        for j in range(fs):
            arr[i, j] = matrix[i, (j + i) % fs]
    return arr

def centroid_image(data, area_threshold=40):
    labeled = process.label_standard(data)
    centroids, areas = process.get_centroids(labeled, area_threshold=area_threshold)
    z = np.zeros(np.shape(data))
    for i, im in enumerate(centroids):
        z[i, centroids[i][:,0].astype(int), centroids[i][:,1].astype(int)] = 1
    return z

def droplet_translation_by_outer_product(outer_product_projection):
    ccmp = outer_product_projection
    if np.abs(np.min(ccmp)) > np.abs(np.max(ccmp)):
        ccmp *= -1
    return np.where(ccmp == np.max(ccmp))[0][0]

def outer_product_matrix(data, method='centroid', area_threshold=40):
    nframes, fs, ss = np.shape(data)
    cc_mat = np.zeros((fs, fs))
    if method == 'centroid':
        data = centroid_image(data=data, area_threshold=area_threshold)
    dd = np.sum(data, axis=2)

    prev = None
    for i, im in enumerate(data):
        # Add on odds, subtract on evens.  One of these two provides an estimate of background correlations
        # that have no relation to droplet translations.  I don't know which is which; deal with that later.
        if prev is not None:
            op = np.outer(dd[i], prev)
            if i % 2 == 1:
                cc_mat += op
            else:
                cc_mat -= op
        prev = dd[i]
    # projection = get_outer_product_projection(cc_mat, threshold=1)
    projection = np.mean(_shear(cc_mat), axis=0)
    if np.abs(np.min(projection)) > np.abs(np.max(projection)):
        sign = -1
    else: 
        sign = 1
    projection *= sign
    cc_mat *= sign
    return cc_mat, projection
    
def get_speed_by_outer_product(image_stack, 
                                pixelsize_m, 
                                laser_delay=550e-9, 
                                method='centroid', 
                                area_threshold=40,
                                process_image_stack=True):

    stack = image_stack.copy()
    op, proj = outer_product_matrix(stack, method=method, area_threshold=area_threshold)
    s = droplet_translation_by_outer_product(proj)
    jet_speed = _displacement_to_speed(s, pixelsize_m, laser_delay)
    dat = {'outer_product_matrix': op,
            'projection': proj,
            'displacement': s,
            'jet_speed': jet_speed}
    return dat


def _displacement_to_speed(displacement, pixelsize_m, laser_delay):
    if pixelsize_m > 1:
        warnings.warn("Pixel size is greater than 1, is your input for pixelsize_m in SI?", UserWarning)
    return displacement * pixelsize_m / laser_delay

def correlation_projection_quality(projection):

    peaks, properties = find_peaks(projection, prominence=0.9 * np.max(projection))
    if len(peaks) == 0:
        logging.warning("No peaks found in correlation projection.")
        return False  # No significant peak found

    # Check if the highest peak is significantly prominent
    max_peak_prominence = properties['prominences'][np.argmax(projection[peaks])]
    cond = bool(max_peak_prominence >= 3 * np.mean(projection))
    return cond




# class RunningCrossCorrelation:
#     """A class to compute the running cross-correlation between successive images."""
#     def __init__(self, frame_shape):
#         """Initializes the RunningCrossCorrelation with a given frame shape."""
#         self.frame_shape = frame_shape
#         self.n = 0
#         self._padded_cc = np.zeros((frame_shape[0]*2, frame_shape[1]*2))
#         self.mean = np.zeros(frame_shape)
#         self.sum = np.zeros(frame_shape)
#         self.shifted_cc = np.zeros(frame_shape)
#         self.prev_image = None  # Add this line to store the previous image

#     def _pad(self, im):
#         """
#         Pads the given image to avoid wraparound artifact of circular cross correlation.

#         Args:
#             im (np.ndarray): The image to be padded.
#         Returns:
#             np.ndarray: The padded image.
#         """
#         ns, nf = im.shape
#         return np.pad(im, ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))
    
#     def _unpad(self, im_padded):
#         """
#         Removes the padding from the image that was previously padded to avoid wraparound artifact of circular cross correlation.

#         Args:
#             im_padded (np.ndarray): The padded image.

#         Returns:
#             np.ndarray: The original, unpadded image.
#         """
#         ns, nf = im_padded.shape
#         return im_padded[int(ns/2):ns+int(ns/2), int(nf/2):nf+int(nf/2)]

#     def _cross_correlation(self, a, b):
#         """
#         Computes the cross correlation of two images.

#         Args:
#             a (np.ndarray): The first image.
#             b (np.ndarray): The second image.
#         Returns:
#             np.ndarray: The cross correlation of the input images.
#         """
#         return np.real(np.fft.ifftn(np.fft.fftn(a) * np.conj(np.fft.fftn(b))))

#     def update(self, new_image):
#         """
#         Updates the master cross-correlation with the new image.

#         Args:
#             new_image (np.ndarray): The new image for correlation calculation.
#         """
#         self.n += 1
#         self.sum += new_image
#         self.mean = self.sum / self.n

#         if self.prev_image is not None:  # Only compute cross-correlation if there is a previous image
#             im1 = self._pad(self.prev_image)  # Use previous image here
#             im2 = self._pad(new_image)
#             self._padded_cc += self._cross_correlation(im1, im2)

#             # Subtract the autocorrelation of the mean image
#             mean_padded = self._pad(self.mean)
#             acf = self._cross_correlation(mean_padded, mean_padded)
#             x = np.nanmean(acf[acf != 0])
#             cc = self._padded_cc - acf / x

#             # Shift to center
#             self.shifted_cc = np.fft.fftshift(cc)

#             # Remove padding
#             ns, nf = self.frame_shape
#             self.shifted_cc = self.shifted_cc[int(ns/2):ns+int(ns/2), int(nf/2):nf+int(nf/2)]

#         self.prev_image = new_image  # Update the previous image to the current image for the next update

#     def projection(self):
#         corr = self.shifted_cc
#         cidx = np.where(corr < 0)
#         corr[cidx] = 0

#         if np.max(corr) == 0:
#             return np.zeros(np.shape(corr)[0])

#         corr /= np.max(corr)
#         p = np.sum(corr, axis=1)
#         return p

#     def find_cc_peak(self):
#         """
#         Finds the peak of the cross-correlation image. This peak represents the most common displacement in the input images.
        
#         Returns:
#             tuple: The displacement of the peak from the center of the image.
#         """
#         if self.n < 2:
#             return 0, 0
#         else:
#             try:
#                 ns, nf = self.shifted_cc.shape
#                 w = np.where(self.shifted_cc == np.max(self.shifted_cc))
#                 pf = w[1][0] - np.floor(nf/2)
#                 ps = w[0][0] - np.floor(ns/2)
#                 return pf, ps
#             except IndexError:
#                 e1 = "IndexError was thrown at correlation peak finder! "
#                 e2 = "Is your data bright or darkfield? "
#                 e3 = "\nThis error is typically raised when brightfield analysis is requested on darkfield data."
#                 raise ValueError(e1+e2+e3)

#     def calculate_displacement(self):
#         pf, ps = self.find_cc_peak()
#         displacement = np.sqrt(pf**2 + ps**2)
#         angle = np.arctan2(ps, pf)*180/np.pi - 90
#         return displacement, angle

class RunningJetCharacteristics:
    """A class that calculates and maintains running mean and standard error for various jet characteristics,
    and computes the running jet stability based on the ratio of jet deviation to jet diameter.

    Attributes:
        config (dict): Configuration parameters for jet characteristics calculations.
        buffer_size (int): Size of the buffer to store running calculations.
        jet_deviations (np.ndarray): Buffer to store calculated jet deviations.
        jet_lengths (np.ndarray): Buffer to store calculated jet lengths.
        y_values (numpy.ndarray): A circular buffer to hold recent y_values.
        idx (int): Current index for inserting new calculation into buffers.
    """
    
    def __init__(self, config: dict, buffer_size: int = 100):
        """Initializes RunningJetCharacteristics with a config and a buffer size.

        Args:
            config (dict): Configuration parameters for jet characteristics calculations.
            buffer_size (int): Size of the buffer to store running calculations.
        """
        self.config = config
        self.buffer_size = buffer_size
        
        # Initialize buffers for each property
        self.reset_buffers()
        

    def reset_buffers(self):
        self.jet_deviations = np.zeros(self.buffer_size)
        self.jet_lengths = np.zeros(self.buffer_size)
        self.idx = 0

    def _get_jet_deviation(self) -> float:
        """Calculates jet deviation from vertical using the current config.

        Returns:
            float: Jet deviation in degrees.
        """
        return jet_deviation(self.config['jet_fit'], self.config['nozzle_tip_idx'])

    def _get_jet_length(self) -> float:
        """Calculates jet length using the current config.

        Returns:
            float: Jet length in meters.
        """
        return jet_length(self.config['nozzle_tip_idx'], self.config['transition_region_idx'], 
                          self._get_jet_deviation(), self.config['pixel_size'])

    def update_and_get_stats(self, new_config: dict) -> dict:
        """Updates buffers with new calculations and returns the current statistics.

        Args:
            new_config (dict): New configuration parameters for jet characteristics calculations.

        Returns:
            dict: A dictionary containing mean and standard deviation for each jet characteristic and stability.
        """
        self.config = new_config

        # Update buffers
        self.jet_deviations[self.idx] = self._get_jet_deviation()
        self.jet_lengths[self.idx] = self._get_jet_length()
    
        # Calculate mean and standard deviation using valid data points
        valid_deviations = self.jet_deviations[~np.isnan(self.jet_deviations)]
        valid_lengths = self.jet_lengths[~np.isnan(self.jet_lengths)]
        jet_deviation_mean, jet_deviation_std = np.mean(valid_deviations), np.std(valid_deviations)
        jet_length_mean, jet_length_std = np.mean(valid_lengths), np.std(valid_lengths)

        # Update index for next update
        self.idx = np.mod(self.idx + 1, self.buffer_size)

        out = {'jet_deviation': (jet_deviation_mean, jet_deviation_std),
                'jet_length': (jet_length_mean, jet_length_std),
                }

        return out




class AllJetCharacteristics:
    """A class that calculates and maintains running mean and standard error for various jet characteristics,
    and computes the running jet stability based on the ratio of jet deviation to jet diameter.

    Attributes:
        config (dict): Configuration parameters for jet characteristics calculations.
        buffer_size (int): Size of the buffer to store running calculations.
        jet_speeds (np.ndarray): Buffer to store calculated jet speeds.
        jet_diameters (np.ndarray): Buffer to store calculated jet diameters.
        jet_deviations (np.ndarray): Buffer to store calculated jet deviations.
        jet_lengths (np.ndarray): Buffer to store calculated jet lengths.
        y_values (numpy.ndarray): A circular buffer to hold recent y_values.
        idx (int): Current index for inserting new calculation into buffers.
    """
    
    def __init__(self, config: dict, buffer_size: int = 100):
        """Initializes RunningJetCharacteristics with a config and a buffer size.

        Args:
            config (dict): Configuration parameters for jet characteristics calculations.
            buffer_size (int): Size of the buffer to store running calculations.
        """
        # self._validate_config(config)
        self.config = config
        self.buffer_size = buffer_size
        
        # Initialize buffers for each property
        self.reset_buffers()
        

    def reset_buffers(self):
        self.jet_speeds = np.zeros(self.buffer_size)
        self.jet_diameters = np.zeros(self.buffer_size)
        self.jet_deviations = np.zeros(self.buffer_size)
        self.jet_lengths = np.zeros(self.buffer_size)
        self.y_values = np.zeros(self.buffer_size)
        self.idx = 0

    def _get_jet_speed(self) -> float:
        """Calculates jet speed using the current config.

        Returns:
            float: Jet speed in meters per second.
        """
        return jet_speed(self.config['droplet_translation'], self.config['pixel_size'], self.config['delay'])

    def _get_jet_diameter(self) -> float:
        """Calculates jet diameter using the current config.

        Returns:
            float: Jet diameter in meters.
        """
        return jet_diameter(self.config['liquid_flow'], self._get_jet_speed())

    def _get_jet_deviation(self) -> float:
        """Calculates jet deviation from vertical using the current config.

        Returns:
            float: Jet deviation in degrees.
        """
        return jet_deviation(self.config['jet_fit'], self.config['nozzle_tip_idx'])

    def _get_jet_length(self) -> float:
        """Calculates jet length using the current config.

        Returns:
            float: Jet length in meters.
        """
        return jet_length(self.config['nozzle_tip_idx'], self.config['transition_region_idx'], 
                          self._get_jet_deviation(), self.config['pixel_size'])

    def _get_stability_measure(self) -> float:
        """Calculates the stability measure using the current config.

        Returns:
            float: The calculated stability measure, or None if there's an error or insufficient data.
        """
        d = interaction_point_from_jet_length(self.jet_lengths[self.idx], self.config['pixel_size'], 
                                              self.config['interaction_point_method'],
                                              self.config['percent_dist'], self.config['provided_distance'])

        ang = self._get_jet_deviation() * np.pi/180
        dx = d * np.cos(ang)
        y = dx * np.tan(ang)
        self.y_values[self.idx] = y

        if np.count_nonzero(self.y_values) == 0:
            # Returns None if no items in the buffer.
            return None

        ys = np.nanstd(self.y_values)
        dj = self._get_jet_diameter() / self.config['pixel_size']
        s = ys / dj
        return s

    def _get_jet_stability_status(self) -> str:
        """Gets the current jet stability status based on the stability measure.

        Returns:
            str: The stability status of the jet.
        """
        stability_measure = self._get_stability_measure()

        # Return None if stability measure is None
        if stability_measure is None:
            return None

        # Define stability measure ranges
        elif stability_measure == 0:
            return "Perfectly Stable"
        elif 0 < stability_measure <= 0.25:
            return "Highly Stable"
        elif 0.25 < stability_measure <= 0.5:
            return "Moderately Stable"
        elif 0.5 < stability_measure <= 1:
            return "Marginally Stable"
        elif 1 < stability_measure <= 1.5:
            return "Unstable"
        else:  # stability_measure > 1.5
            return "Critically Unstable"

    def update_and_get_stats(self, new_config: dict) -> dict:
        """Updates buffers with new calculations and returns the current statistics.

        Args:
            new_config (dict): New configuration parameters for jet characteristics calculations.

        Returns:
            dict: A dictionary containing mean and standard deviation for each jet characteristic and stability.
        """
        self.config = new_config

        # Update buffers
        self.jet_speeds[self.idx] = self._get_jet_speed()
        self.jet_diameters[self.idx] = self._get_jet_diameter()
        self.jet_deviations[self.idx] = self._get_jet_deviation()
        self.jet_lengths[self.idx] = self._get_jet_length()
        

        # Calculate mean and standard deviation for each property
        jet_speed_mean, jet_speed_std = np.nanmean(self.jet_speeds), np.nanstd(self.jet_speeds)
        jet_diameter_mean = np.nanmean(self.jet_diameters) # For some reason, doing jet diameter in the same unpacking syntax as the others 
        jet_diameter_std = np.nanstd(self.jet_diameters)   # introduces a warning. Look into this error, this way silences the warning.
        jet_deviation_mean, jet_deviation_std = np.nanmean(self.jet_deviations), np.nanstd(self.jet_deviations)
        jet_length_mean, jet_length_std = np.nanmean(self.jet_lengths), np.nanstd(self.jet_lengths)

        # Update stability measure
        stability_numeric = self._get_stability_measure()
        stability_measure = self._get_jet_stability_status()

        # Update index for next update
        self.idx = (self.idx + 1) % self.buffer_size
        print(self.idx)

        out = {'jet_speed': (jet_speed_mean, jet_speed_std),
                'jet_diameter': (jet_diameter_mean, jet_diameter_std),
                'jet_deviation': (jet_deviation_mean, jet_deviation_std),
                'jet_length': (jet_length_mean, jet_length_std),
                'stability_measure': (stability_measure, stability_numeric)
                }

        return out



















