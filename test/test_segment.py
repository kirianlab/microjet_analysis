from microjet_analysis import segment
import numpy as np

def test_ntip_simple():
    x = np.zeros((20,10,20))
    x[:,-1, 5:11] = 1
    y = np.random.randint(0, 2, np.shape(x))
    stack = x + y
    ntip = segment.nozzle_tip_simple(x)
    assert np.mean(ntip) == 9

def test_ntip_shotbyshot():
    x = np.zeros((20,40,60))
    for i, im in enumerate(x):
        x[i,-i-1, 20:41] = 1
    y = np.random.randint(0, 2, np.shape(x))
    stack = x + y
    ntip = segment.nozzle_tip_shotbyshot(x)
    actual = np.mean([39, 38, 37, 36, 35, 
                        34, 33, 32, 31, 30, 
                        29, 28, 27, 26, 25, 
                        24, 23, 22, 21, 20])
    assert np.mean(ntip) == actual

def test_nonzero_subset():
    x = [0,0,1,0,0,1,1,0,1,1,1,1,1]
    y = segment.find_longest_nonzero_subset(x)
    assert y[0] == 8






















