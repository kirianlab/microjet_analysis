#!/bin/bash

WATCH_DIR=$1
[ "$WATCH_DIR" = "" ] && WATCH_DIR="c:\\Nanoscribe-Nozzles\\"

inotifywait --monitor --event create --event moved_to --format '%:e %f' ${WATCH_DIR} |
    while read PATH_ EVENT_ FILE_; do
        FULLPATH=${PATH_}${FILE_}
        echo "New ${EVENT_}: $FULLPATH"
        bash ./analyze_realtime.sh $FULLPATH -i
    done

