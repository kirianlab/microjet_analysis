#!/bin/bash

#-------------------------------------------------------------------
# ONLY SC30I: Find jet speed from droplet displacements
#-------------------------------------------------------------------
# 	CMD Args
# 		$1 			Nozzle type (e.g. 44_10_03_SC30I)
# 		$2,3,... 	args to pass to rick/brightfield2a.py
#-------------------------------------------------------------------

#######################################################
# Commandline Args
#######################################################

NOZZLE_TYPE=${1}_SC30I
shift
ANALYSIS_OPTIONS="$@"

echo "================================================="
echo "Analyze: " ${NOZZLE_TYPE}
echo "Options: " ${ANALYSIS_OPTIONS}
echo "================================================="

#######################################################
# Switches: What Would You Like to Do?
#######################################################

USE_ROI=true  
DO_MAIN_ANALYSIS=true
SAVE_OUTER_PRODUCTS=false

#######################################################
# Params: Specific to SC30I
#######################################################

IGNORE_THESE_NOZZLES="44_05_01_SC30I|44_06_01_SC30I|44_06_02_SC30I|44_06_03_SC30I"   # Datasets w/ very poor contrast
DAQ_DATA="./data/Nozzle_Records129.csv"

#######################################################
# Params: Input Data Paths
#######################################################

RAWDATA="../rawdata/microjets"
IGNORED_RUNS="./databases/bad_ignored_runs.csv"
ROI_DATABASE="./databases/roi_params_database.pkl"

#######################################################
# Params: Output/Resluts/Reports Paths
#######################################################

TEMPDATA="../tempdata/microjets"
FINAL_RESULTS="./results/jet_speed_${NOZZLE_TYPE}.csv"
ANALYZED_TIFS="./results/analyzed_tifs_${NOZZLE_TYPE}.txt"
RELEVANT_DATA="./results/relevant_data_${NOZZLE_TYPE}.csv"
PRE_ANALYSIS_REPORT="./databases/pre_analysis_report.txt"
ANALYSIS_REPORT="./results/analysis_report_${NOZZLE_TYPE}.txt"

#######################################################
# Pre-Analysis
#######################################################

echo "-------------------------------------------------"
echo "Pre-Analysis"
echo "-------------------------------------------------"

# bash ./fetch_data.sh ${NOZZLE_TYPE}

if [ "${USE_ROI}" = true ] ; then
	echo "Find ROIs"
	find ${RAWDATA} -iname '*.tif' | grep -Ev "${IGNORE_THESE_NOZZLES}" |grep -i ${NOZZLE_TYPE} | sort > ${ANALYZED_TIFS} 	
	python ./modules/pre_analysis.py ${ANALYZED_TIFS} ${ROI_DATABASE} ${PRE_ANALYSIS_REPORT}
fi

#######################################################
## Main Analysis
#######################################################

echo "-------------------------------------------------"
echo "Main Analysis: Droplet Translation" 
echo "-------------------------------------------------"

if [ "$DO_MAIN_ANALYSIS" = true ] ; then

	if [ "${USE_ROI}" = true ] ; then
	    echo "Use ROIs"	
	    DATA_DIR_NEW=${TEMPDATA}
	else 
		DATA_DIR_NEW=${RAWDATA}
	fi

	SEP=$(printf '=%.0s' {1..70})
	echo > ${ANALYSIS_REPORT}

	# for i in $(cat ${ANALYZED_TIFS}); do
	for i in $(find ${DATA_DIR_NEW} -iname '*.tif' | grep -Ev "${IGNORE_THESE_NOZZLES}" | grep -i ${NOZZLE_TYPE} | sort); do 	
		printf "\n${SEP}\n${i}\n${SEP}\n\n" | tee -a ${ANALYSIS_REPORT}
		python -W ignore ../rick/brightfield2a.py $i ${ANALYSIS_OPTIONS} 2>> ${ANALYSIS_REPORT}
	done
else 
	echo "Skip Main Analysis (User-Set Switch in 'analyze_sc30i.sh')" 
fi

#######################################################
## Post Analysis
#######################################################

echo "-------------------------------------------------"
echo "Post-Analysis: Droplet Translation --> Jet Speed"
echo "-------------------------------------------------"

echo "Find Analysis Results (.npz) for " ${NOZZLE_TYPE}
find ${TEMPDATA} -iname '*.tif.brightfield2.npz' | grep ${NOZZLE_TYPE} | sort > ${ANALYZED_TIFS}

echo "Make flow rate data log"
python ./modules/excel_parser_sc30i.py  ${ANALYZED_TIFS} ${DAQ_DATA} ${RELEVANT_DATA}

echo "Find Jet Speed"
python ./modules/pixel_to_speed.py  ${ANALYZED_TIFS}  ${IGNORED_RUNS} ${RELEVANT_DATA} ${FINAL_RESULTS} ${NOZZLE_TYPE}

if [ "${SAVE_OUTER_PRODUCTS}" = true ] ; then
	echo "Save Outer Products"
	python ./modules/plot_outer_prods.py  ${ANALYZED_TIFS}
else
	echo "Skip Saving Outer Products"
fi

echo "-------------------------------------------------"
echo "The End" ${NOZZLE_TYPE}
echo "-------------------------------------------------"
