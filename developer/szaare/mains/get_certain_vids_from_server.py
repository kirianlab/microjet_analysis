import os, sys
from _import_context import libs
from   libs.remoters import server_id, scpClient
from  tqdm import tqdm

#######################
## PARAMS
#######################

DATA_DIR        = "/data/rawdata/microjets"
Find_Dirs       ='find {} -type d -iname "*sc30i*" | sort -n'
Find_Tifs       ='find {} -type f -iname "*.tif*"  | sort -n'
SAVE_PATH_Tifs  = os.path.expanduser("~/Desktop/selected_relevant_tifs.txt")
SAVE_PATH_dirs  = os.path.expanduser("~/Desktop/selected_relevant_dirs.txt")

LOG_NAMES  = ["",""]

#######################
## SSH Fx
#######################

CLNT, SSH, SCP = scpClient.make_ssh_scp_clients(server_id.ID["Server"],
                                                server_id.ID["Port"],
                                                server_id.ID["User"],
                                                password = "")

def get_paths(command):

    stdin, stdout, stderr = CLNT.exec_command(command)
    stdout = [i.strip() for i in stdout.readlines()]

    return stdout

  

def scp_stuff_from_server(paths):
    
    LOCAL_DIR = os.path.join(os.path.dirname(SAVE_PATH_dirs), "new_vids")
    if not os.path.isdir(LOCAL_DIR): 
        os.makedirs(LOCAL_DIR)
    
    print("SCPing Stuff to ", LOCAL_DIR)
    print()


    # DIRS = list(set([os.path.dirname(p) for p in paths]))
    # DIRS.sort()
    LAST_DEST_DIR = None
    for p in tqdm(paths): 
        REMOTE_DIR = os.path.dirname(p)
        if os.path.isabs(REMOTE_DIR):
            REMOTE_DIR = REMOTE_DIR[1:]     # remove initial '/'
        
        DEST_DIR = os.path.join(LOCAL_DIR, REMOTE_DIR)
        
        if not os.path.isdir(DEST_DIR):
            os.makedirs(DEST_DIR)

        if LAST_DEST_DIR == DEST_DIR:
            # print("  |---- ", os.path.basename(p))
            pass
        else:
            print(DEST_DIR)
            # print("  |---- ", os.path.basename(p))
        LAST_DEST_DIR = DEST_DIR

        try: 
            SCP.get(p, DEST_DIR)
            # scpClient.scp_file_from_remote(sshClientHandle  = CLNT,
            #                                remotePath       = p,
            #                                localPath        = LOCAL_DIR)
        except Exception as e:
            print(e)
      
#######################
## SAVE Paths
#######################

def save_results(results, savePath, msg):

    with open(savePath, "w") as f:
      for i in results:
          # print("  ", i)
          f.write(i+"\n")

      print(msg, savePath)

#######################
## MAIN
#######################

def main():

    all_dirs                = get_paths(Find_Dirs.format(DATA_DIR))

    all_relevant_dirs       = []
    all_relevant_tifs       = []
    select_relevant_dirs    = []
    select_relevant_tifs    = []

    for year in range(2015,2025):
        prefix = os.path.join(DATA_DIR, str(year),"")
        for r in all_dirs:
            if r.startswith(prefix):
                all_relevant_dirs.append(r)

    for d in all_relevant_dirs: 
        tifs = get_paths(Find_Tifs.format(d))

        if tifs:
            all_relevant_tifs += tifs   
            # for t in tifs: print(os.path.basename(t), end=" ")

        if tifs and (len(tifs) >=10):
            print ("\nSearched: ", os.path.join(*d.split(os.sep)[-2:]), "    Found", len(tifs))
            select_relevant_tifs += tifs   
            select_relevant_dirs.append(d)
            # for t in tifs: print(os.path.basename(t), end=" ")


    print("\n\n")
    print("All    Relevant Tifs:", len(all_relevant_tifs))
    print("Select Relevant Tifs:", len(select_relevant_tifs))
    print()
    print("All    Relevant Dirs:", len(all_relevant_dirs))
    print("Select Relevant Dirs:", len(select_relevant_dirs))
    print("\n\n")

    save_results(select_relevant_tifs, SAVE_PATH_Tifs, "***** Saved Selected Relevant Tiff Paths at: ")
    save_results(select_relevant_dirs, SAVE_PATH_dirs, "***** Saved Selected Relevant Dirs       at: ")
    print()

    scp_stuff_from_server(paths=select_relevant_tifs )

if __name__ == '__main__':
    main()