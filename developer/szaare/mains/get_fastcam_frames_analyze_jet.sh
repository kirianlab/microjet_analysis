#!/bin/bash

#######################################################################
# Runs photronFastcam/basic_connect.py
# (which saves a stack of fastcam frames as .tif), then
# finds the most recent tif file in the specified tif directory &
# runs microjetAnalysis/development/analyzeJet.py on that tif.
#######################################################################

TIFF_DIR="${HOME}/Desktop/Fastcam_Frames/"
FASTCAM_GETTER_DIR="E:/sahba/repos/photronFastcam/"
JET_ANALYZER_DIR="E:/sahba/repos/microjetAnalysis/development/"

echo "Collecting Fastcam Frames ..."
cd $FASTCAM_GETTER_DIR
python basic_connect.py     # '$?' contains error status

if [ $? -eq 1 ]; then 
	echo 'Could not get frames from FastCam'

else
	Newest_Tiff=$(find $TIFF_DIR -type f -name "*.tif" -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")

	echo "Analyzing ${Newest_Tiff}"
	cd $JET_ANALYZER_DIR
	python analyzeJet.py $Newest_Tiff

	echo "End of Analysis for ${Newest_Tiff}"		

fi

exit 0

