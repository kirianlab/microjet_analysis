#!/bin/bash

#-------------------------------------------------------
# Replaces ImageJ for quickly viewing tif movies
#
# Input: 
#    $1:         nozzle type         e.g. 44_10_03_SC30I
#    $2, ...     space-sep run nums  e.g. 1 45 72
#-------------------------------------------------------

########################################
# Params
########################################

THIS_SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")

# DATADIR="${THIS_SCRIPT_DIR}/../rawdata/microjets"
DATADIR="${HOME}/rawdata/microjets"
POSSIBLE_TIFS="${THIS_SCRIPT_DIR}/tif_paths.txt"
SCRIPT="${THIS_SCRIPT_DIR}/modules/showme.py"

########################################
# CLI Args
########################################

FIND_NOZZLE="$1"

if [[ ! "$FIND_NOZZLE" = "-"* ]]; then 
	find ${DATADIR} -iname '*.tif' | grep -i "$FIND_NOZZLE" | sort > ${POSSIBLE_TIFS}
	shift
fi

ARGS="$@"

########################################
# Main
########################################

python ${SCRIPT} ${ARGS} --possibleTifs ${POSSIBLE_TIFS}

[ -f "$POSSIBLE_TIFS" ] && rm ${POSSIBLE_TIFS}


