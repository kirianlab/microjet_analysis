import os,sys,pickle

ROI_PARAMS_DATABASE_PATH = os.path.abspath("./roi_params_database.pkl")

with open(ROI_PARAMS_DATABASE_PATH, 'rb') as f:
    ALL_ROI_PARAMS = pickle.load(f)
    # del ALL_ROI_PARAMS["20190830\\Y_SSH_1\\002.tif"]

for k,v in ALL_ROI_PARAMS.items():
    if "20190830" in k:
        print(k)

# bad = "../rawdata/microjets/2019/"
bad = "/20190830"

for k,v in ALL_ROI_PARAMS.items():
    if k.startswith(bad):
        new_k = k.replace(bad,"20190830")
        ALL_ROI_PARAMS[new_k] = v
        del ALL_ROI_PARAMS[k]

for k,v in ALL_ROI_PARAMS.items():
    if "20190830" in k:
        print(k)

with open(ROI_PARAMS_DATABASE_PATH, "wb") as f:
    pickle.dump(ALL_ROI_PARAMS, f, pickle.HIGHEST_PROTOCOL)
