#!/bin/bash

#----------------------------------------------
# Custom Script only for 'good' SC30I data sets
#   
#     SC30I_TYPE="44_05_01 
#                 44_05_02 
#                 44_05_03 
#                 44_06_01 
#                 44_06_02 
#                 44_06_03 
#                 44_07_01 
#                 44_07_02
#                 44_07_03 
#                 44_08_01
#                 44_08_02 
#                 44_10_03"
#----------------------------------------------

########################################
# Params
########################################

FINAL_RESULTS="./results/jet_speed_all_SC30I.csv"
ANALYZED_TIFS="./results/analyzed_tifs.txt"

SC30I_TYPE="44_07_02
            44_07_03 
            44_08_01
            44_08_02 
            44_10_03"

echo "###############################"
echo "Main Analysis (Each SC30I Type)"
echo "###############################"

# for i in ${SC30I_TYPE}; do
#     bash ./analyze_sc30i.sh $i
# done 

echo "###############################"
echo "Consolidate All SC30I Results"
echo "###############################"

find ./results -type f -iname jet_speed_*_SC30I.csv | grep -Ev "_all_" | sort > "${ANALYZED_TIFS}"
python ./modules/plot_jet_speed_all_sc30i.py "${ANALYZED_TIFS}"  "${FINAL_RESULTS}" 
# rm "${ANALYZED_TIFS}"

echo "###############################"
echo "The End (All SC30I)" 
echo "###############################"
