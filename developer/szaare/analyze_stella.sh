#!/bin/bash

#-------------------------------------------------------------------
# Find jet speed from droplet displacements
#-------------------------------------------------------------------
#   CMD Args
#       $1          Nozzle type (e.g. 50um_1)
#       $2,3,...    args to pass to rick/brightfield2a.py
#-------------------------------------------------------------------

#######################################################
# Switches: What Would You Like to Do?
#######################################################

USE_ROI=true
DO_MAIN_ANALYSIS=false
DO_POST_ANALYSIS=true
ANALYZE_ONLY_PROVIDED_TIFS=false
SAVE_OUTER_PRODUCTS=false

#######################################################
# Commandline Args
#######################################################

NOZZLE_TYPE=$1 		# e.g. SC30I_10_40_1_08
shift
ANALYSIS_OPTIONS="$@"

#######################################################
# Params: Specific to Stella
#######################################################

IGNORE_THESE_NOZZLES=" "
ANALYZE_ONLY_THESE_TIFS="./analyze_only_these_tifs.txt"
# ANALYZE_ONLY_THESE_TIFS="${HOME}/Desktop/failed_tifs_stella_50um_1.txt"

#######################################################
# Params: Input Data Paths
#######################################################

RAWDATA="../rawdata/microjets"
IGNORED_RUNS="./databases/bad_ignored_runs.csv"
ROI_DATABASE="./databases/roi_params_database.pkl"

#######################################################
# Params: Output/Resluts
#######################################################

TEMPDATA="../tempdata/microjets"
FINAL_RESULTS="./results/jet_speed_${NOZZLE_TYPE}.csv"
PRE_ANALYSIS_REPORT="./databases/pre_analysis_report.txt"

#######################################################
# Params: Temp Paths 
#######################################################

ANALYZED_TIFS="./temp/analyzed_tifs_${NOZZLE_TYPE}.txt"
RELEVANT_DATA="./temp/relevant_data_${NOZZLE_TYPE}.csv"
DAQ_LOGS="./temp/daq_logs.txt"
FAILED_TIFS="./temp/failed_tifs_${NOZZLE_TYPE}.txt"
FAILED_TIFS_STDERR="./temp/failed_tifs_${NOZZLE_TYPE}_stderr.txt"


#######################################################
# Pre-Analysis
#######################################################

echo "-------------------------------------------------"
echo "Pre-Analysis"
echo "-------------------------------------------------"

# bash ./fetch_data.sh ${NOZZLE_TYPE}

if [ "${USE_ROI}" = true ] ; then
    echo "*** Find ROIs"
    find ${RAWDATA} -iname '*.tif' | grep -Ev "${IGNORE_THESE_NOZZLES}" |grep -i ${NOZZLE_TYPE} | sort > ${ANALYZED_TIFS}   
    python ./modules/pre_analysis.py ${ANALYZED_TIFS} ${ROI_DATABASE} ${PRE_ANALYSIS_REPORT}
fi

#######################################################
## Main Analysis
#######################################################

echo "-------------------------------------------------"
echo "Main Analysis: Droplet Translation" 
echo "-------------------------------------------------"

echo "Handle Analysis Params"

true > ${FAILED_TIFS}
true > ${FAILED_TIFS_STDERR}
SEP="\n"$(printf '=%.0s' {1..70})"\n"

if [ "${USE_ROI}" = true ] ; then
    echo "*** Use ROIs" 
    DATA_DIR_NEW=${TEMPDATA}
else 
    DATA_DIR_NEW=${RAWDATA}
fi
if [ "$ANALYZE_ONLY_PROVIDED_TIFS" = true ] ; then
    echo "*** Analyze Only Tifs Listed at: ${ANALYZE_ONLY_THESE_TIFS}"
    echo "*** Use Interactive Mode (-i)"
    ANALYSIS_OPTIONS="-i ${ANALYSIS_OPTIONS}"
    cat ${ANALYZE_ONLY_THESE_TIFS} > ${ANALYZED_TIFS}
else find ${DATA_DIR_NEW} -iname '*.tif' | grep -Ev "${IGNORE_THESE_NOZZLES}" | grep -i ${NOZZLE_TYPE} | sort > ${ANALYZED_TIFS}
fi

printf $SEP ; printf "%s\n" "Analyze: ${NOZZLE_TYPE}" "Options: ${ANALYSIS_OPTIONS}" ; printf $SEP

if [ "$DO_MAIN_ANALYSIS" = true ] ; then
    for i in $(cat ${ANALYZED_TIFS}); do
        printf "${SEP}${i}${SEP}\n" | tee -a ${FAILED_TIFS_STDERR}
        python -W ignore ../rick/brightfield2a.py ${ANALYSIS_OPTIONS} $i 2>> ${FAILED_TIFS_STDERR}
        if [ $? -ne 0 ] ; then
            printf ${i}"\n" &>> ${FAILED_TIFS}
        fi
    done
else echo "*** Skip Main Analysis (User-Set Switch in 'analyze_stella.sh')"
fi

#######################################################
## Post Analysis
#######################################################

if [ "$DO_POST_ANALYSIS" = false ]; then exit; fi

echo "-------------------------------------------------"
echo "Post-Analysis: Droplet Translation --> Jet Speed"
echo "-------------------------------------------------"

echo;echo "Find Analysis Results (.npz) for " ${NOZZLE_TYPE} ; echo
find ${TEMPDATA} -iname '*.tif.brightfield2.npz' | grep ${NOZZLE_TYPE} | sort > ${ANALYZED_TIFS}

echo;echo "Make flow rate data log"; echo
find ${RAWDATA} -iname '*_plot_data.txt' | sort > ${DAQ_LOGS}
python ./modules/excel_parser.py  ${ANALYZED_TIFS} ${DAQ_LOGS} ${RELEVANT_DATA}

echo;echo "Find Jet Speed"; echo
python ./modules/pixel_to_speed.py  ${ANALYZED_TIFS}  ${IGNORED_RUNS} ${RELEVANT_DATA} ${FINAL_RESULTS} ${NOZZLE_TYPE}

if [ "${SAVE_OUTER_PRODUCTS}" = true ] ; then
    echo;echo "Save Outer Products";echo
    python ./modules/plot_outer_prods.py  ${ANALYZED_TIFS}
fi

echo;echo "Cleanup"
# rm ${DAQ_LOGS}
# rm ${ANALYZED_TIFS}
# rm ${RELEVANT_DATA}

echo
echo "-------------------------------------------------"
echo "The End" ${NOZZLE_TYPE}
echo "-------------------------------------------------"
