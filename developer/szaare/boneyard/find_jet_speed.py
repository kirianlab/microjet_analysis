import  os, sys, time, collections, csv, functools, errno
from    datetime  import datetime as dt
from    tqdm      import tqdm

import  pandas as pd
import  numpy  as np
from    numba  import jit

import  scipy  as sp
from    scipy        import ndimage
from    scipy.signal import convolve2d
from    scipy.ndimage.measurements import label, center_of_mass

from    skimage import morphology as mf

import  pyqtgraph as     pg
from    pyqtgraph import QtCore, QtGui, GraphicsView

################################################################
## My Libs
################################################################
from    libs.viewers                        import analysis_panel as AnalysisPanel
from    libs.prettifiers.pretty_printers    import ppp
from    libs.viewers.roi_selectors          import draw_roi
from    libs.viewers.movie_frame_loader     import LoadMovieFrames, find_avg_image
from    libs.viewers.image_viewers          import ImageViewer2, ImageBeforeAfter, LinePlotter
from    libs.parsers.arg_parsers            import arg_parse_microjet_analysis
from    libs.parsers.args_file_readers      import read_argsFile_txt
from    libs.serializers.picklator          import save_pickle, load_pickle
from    libs.finders.file_finders           import find_files_by_extensions

###########################################################################################
## Global Plots 
###########################################################################################

# app = pg.mkQApp()

viewer, liner = None, None
# viewer = ImageViewer2()
liner  = LinePlotter()    

###########################################################################################
## Pre-Analysis 
###########################################################################################

def extract_roi(moviePath, roiParams, rotateFrame, nFrames=None):

    TAB = "    "

    mv = LoadMovieFrames(moviePath, rotateFrame)

    N = nFrames
    if (N is None) or (N > mv.total_frames): 
        N = mv.total_frames

    slicer = roiParams["roi_slices"]
    roiFrames = []

    for n in tqdm(range(N)):
        # print(3*TAB, "Frame {: >3d} of {}".format(n,N))
        f = mv.get_frame(n)[slicer]             # TODO: consider Dr. Kirian's fast frame getter
        f = ndimage.median_filter(f, size=5)    # TODO: consider removing (time-consuming)

        roiFrames.append(f)  

    np.asarray(roiFrames)  

    # TODO: here, consider flatten_illum_profile(stack=roiFrames)

    return np.asarray(roiFrames)


def get_roi_params(moviePaths, rotateFrame):
    
    ROI_KWARGS = { "roi_pos"    : None, 
                   "roi_size"   : None, 
                   "line_value" : None}  

    all_roi_params = {m:{} for m in moviePaths}

    for moviePath, roiParams in sorted(all_roi_params.items()):

        mv = LoadMovieFrames(moviePath, rotateFrame)
        f  = mv.get_frame(0)

        roi_params = draw_roi(f.copy(), ROI_KWARGS)    ## TODO: plug in AnalysisPanel.main(stack, interactive=True, title="", roi_kwargs={}), which also yields threshold so no need for BefAft LUT stuff! 
        roiParams.update(roi_params)
        del roi_params["roi_slices"]
        ROI_KWARGS.update(roi_params)

    return all_roi_params

def handle_all_args_and_rois(moviePaths, cmdline_args, nFrames =None, roiType="jetbox", rotateFrame= "flip_LeftRight", overwrite=False, saveDir="", viewer=None):

    print_header(["Making Args/Configs Files"])

    roi_paths_params = {m:{"roi_path":None, "roi_params":None} for m in moviePaths}

    COL = "  |"
    TAB = "----" 

    from ast import literal_eval

    for moviePath in moviePaths:

        YEAR, DATE, NOZZLENAME, NAME = moviePath.split(os.sep)[-4:]
        RUNNUM = os.path.splitext(NAME)[0]
        
        print(COL+TAB   , os.path.join(DATE, NOZZLENAME, NAME))

        if saveDir: roi_dir = saveDir
        else:       roi_dir = os.path.dirname(moviePath)

        for DIR in "results", "args":
            DIR = os.path.join(roi_dir, DIR)
            if not os.path.isdir(DIR):
                os.makedirs(DIR, exist_ok=True)

        timeStamp   = dt.now().strftime("_%Y%m%d_%H%M%S")
        resultsPath = os.path.join(roi_dir, "results" , RUNNUM) + ".pkl"   # timeStamp
        args_path   = os.path.join(roi_dir, "args"    , RUNNUM) + ".txt"
        roi_path    = os.path.join(roi_dir,             RUNNUM) + ".npy"

        ###############################

        if os.path.isfile(args_path):
            argsFile     = args_path
            saveArgsPath = ""

        else:
            argsFile     = cmdline_args["argsFileTemplate"]
            saveArgsPath = args_path

        ###############################

        mv_args = read_argsFile_txt(argsFile)

        roi_paths_params[moviePath]["roi_path"] = roi_path

        if not os.path.isfile(roi_path):

            try: 
                s  = mv_args["roi_slices"][1:-1].replace(" ","")
                s  = s.split("slice")[1:]
                s1 = slice(*literal_eval(s[0][:-1])) 
                s2 = slice(*literal_eval(s[1]))
                # print(s, s1, s2)
                roi_paths_params[moviePath]["roi_params"] = {"roi_slices": (s1,s2)}
                lv = mv_args["line_value"]
            
            except Exception as e:
                print(repr(e)) 
                saveArgsPath = args_path
                roi_paths_params[moviePath]["roi_params"] = None

        else:

            if saveDir and overwrite:
                saveArgsPath = args_path
                roi_paths_params[moviePath]["roi_params"] = None
                print(COL+2*TAB, "Will Overwrite Existing ROI")

            else:
                try:
                    s  = mv_args["roi_slices"][1:-1].replace(" ","")
                    lv = mv_args["line_value"]

                    roi_paths_params[moviePath]["roi_params"] = False
                    print(COL+2*TAB , "ROI Already Exists:", roi_path)

                except Exception as e:
                    print(repr(e)) 
                    saveArgsPath = args_path
                    roi_paths_params[moviePath]["roi_params"] = None


        roi_paths_params[moviePath]["mv_args"]      = mv_args
        roi_paths_params[moviePath]["args_path"]    = args_path
        roi_paths_params[moviePath]["saveArgsPath"] = saveArgsPath


    ##########################################
    ## Get roi params for all movies that either ...
    ## 1) don't have a ROI.npy AND don't have ROI params in their args/config file
    ## 2) do    have a ROI.npy BUT the user wants to overwrite

    NEED_NEW_ROI = []
    
    for moviePath, info in sorted(roi_paths_params.items()):
        if   info["roi_params"] is None  : NEED_NEW_ROI.append(moviePath)
        elif info["roi_params"] is False : pass 

    NEW_ROI_PARAMS = get_roi_params(NEED_NEW_ROI, rotateFrame)

    for moviePath in NEED_NEW_ROI:
        
        roi_params = NEW_ROI_PARAMS[moviePath]
        roi_paths_params[moviePath]["roi_params"] = roi_params

        mv_args      = roi_paths_params[moviePath]["mv_args"]

        if (not isinstance(roi_params, type(None))) and (not isinstance(roi_params, bool)):
            mv_args.update(roi_params)

        saveArgsPath = roi_paths_params[moviePath]["saveArgsPath"]
        if saveArgsPath and (cmdline_args["mode"] == "stable"):

            with open(saveArgsPath, "w") as f:
                form = "{: <20} = {}\n"
                for k,v in sorted(mv_args.items()):
                    f.write(form.format(k,v))
            
                print("\n\tSaved Analysis Args:\n\t", saveArgsPath)         

    BefAft = ImageBeforeAfter()
    # BefAft = None

    for moviePath, info in sorted(roi_paths_params.items()):
        
        roi_path   = info["roi_path"]
        roi_params = info["roi_params"]

        if not os.path.isfile(roi_path):
            
            print("\nExtracting ROI:\t", os.path.join(*moviePath.split(os.sep)[-3:]))
            
            roiFrames = extract_roi(moviePath, roi_params , rotateFrame, nFrames)
            np.save(roi_path, roiFrames)                 


        ##################################################
        
        mv_args      = info["mv_args"]
        args_path    = info["args_path"]
        saveArgsPath = info["saveArgsPath"]

        # calibFilePath        = find_matching_calib_movie(calibNumpys)
        # if calibFilePath == "": calibFilePath = None
        mv_args["calibFilePath"] = None  # or "" ? (there IS a diff)
        
        if cmdline_args["nFrames"] != 0:
            saveArgsPath = args_path
            mv_args["nFramesForAnalysis"] = cmdline_args["nFrames"]

        if (mv_args["threshold"] is None) or (mv_args["LUTLevels"] is None):
            saveArgsPath = args_path
            mv  = LoadMovieFrames(moviePath=roi_path)
            jet = mv.get_frame(0)
            jet1= mv.get_frame(1)
            if np.mean(jet) > np.mean(jet1):  # pick the darker frame (brightfield)
                jet = jet1
            # jet = ndimage.median_filter(jet, size=5)
            BefAft.set_image(jet)
            BefAft.app.exec_()

            mv_args["threshold"] = BefAft.drag.value()
            mv_args["LUTLevels"] = BefAft.hst.getLevels()

        roi_params = info["roi_params"]
        if (not isinstance(roi_params, type(None))) and (not isinstance(roi_params, bool)):
            mv_args.update(roi_params)

        #################################
        ## Save movie args (if necessary)

        if saveArgsPath and (cmdline_args["mode"] == "stable"):

            with open(saveArgsPath, "w") as f:
                form = "{: <20} = {}\n"
                for k,v in sorted(mv_args.items()):
                    f.write(form.format(k,v))
            
                print("\n\tSaved Analysis Args:\n\t", saveArgsPath) 
    
    ROIPATHS = [v["roi_path"] for v in roi_paths_params.values()]
    ROIPATHS.sort()

    return ROIPATHS


def get_all_roi(moviePaths, nFrames =None, roiType="jetbox", rotateFrame= "flip_LeftRight", overwrite=False, saveDir=""):
    
    # replaced by handle_all_args_and_rois() above

    """
    print_header(["Acquiring ROIs"])

    roi_paths_params = {}
    bad_roi_paths    = {}

    COL = "  |"
    TAB = "----" 
    from ast import literal_eval

    for moviePath in moviePaths:

        YEAR, DATE, NOZZLENAME, NAME = moviePath.split(os.sep)[-4:]
        RUNNUM = os.path.splitext(NAME)[0]
        
        print(COL+TAB   , os.path.join(DATE, NOZZLENAME, NAME))

        if saveDir: roi_dir = saveDir
        else:       roi_dir = os.path.dirname(moviePath)

        roi_path  = os.path.join(roi_dir, RUNNUM) + ".npy"
        args_path = os.path.join(roi_dir, "args", RUNNUM) + ".txt"

        # s1 = None
        
        # if os.path.exists(args_path):
        #     roi_args = read_argsFile_txt(args_path)
        #     try: 
        #         s = roi_args["roi_slices"][1:-1].replace(" ","")
        #         s = s.split("slice")[1:]
        #         s1 = slice(*literal_eval(s[0][:-1])) 
        #         s2 = slice(*literal_eval(s[1]))
        #         print(s)
        #         print(s1)
        #         print(s2)
        #     except: pass
        # if s1 is not None:
        #     bad_roi_paths[moviePath] = roi_path


        if not os.path.isfile(roi_path):
            bad_roi_paths[moviePath] = roi_path
        
        else:
            if saveDir and overwrite:
                bad_roi_paths[moviePath] = roi_path
                print(COL+2*TAB, "Will Overwrite Existing ROI")
            else:
                roi_paths_params[roi_path] = None
                print(COL+2*TAB , "ROI Already Exists:", roi_path)

    bad_roi_params = get_roi_params(list(bad_roi_paths.keys()), rotateFrame)
    
    for moviePath, roiPath in sorted(bad_roi_paths.items()):
        roiParams = bad_roi_params[moviePath]
        roi_paths_params[roiPath] = roiParams

        print("\nExtracting ROI:\t", os.path.join(*moviePath.split(os.sep)[-3:]))
        roiFrames = extract_roi(moviePath, roiParams , rotateFrame, nFrames)
        np.save(roiPath, roiFrames) 

    return roi_paths_params
    """       
    pass 

###########################################################################################
## Analysis 
###########################################################################################

def trim_image_border(image, trimPercents=[],verbose=False):
    
    tab = 4*" "
    T   = trimPercents
    im  = image

    if T:
        nx = int(T[0]*im.shape[0]/100)
        ny = int(T[1]*im.shape[1]/100)
        im = im[nx:im.shape[0]-nx , ny:im.shape[1]-ny]
        if verbose:
            print(3*tab + "*** Trimming ({},{}) percent from image borders: ({},{}) pixels from original {} pixels.".format(*T, nx,ny,im.shape))

    return im

def our_filter(image, levels, thresh, isBrightfield = False, trimPercents = [], show = False, viewer = viewer, verbose=False):

    L  = levels
    im = image.copy()

    # im = ndimage.median_filter(im, size=5)

    im[im > L[1]] = L[1]
    im[im < L[0]] = L[0]

    im = im > thresh
    im = im.astype(np.float)

    im = mf.binary_dilation(im, mf.disk(1))   
    im = mf.binary_erosion(im, mf.disk(2)) 

    im = im.astype(np.int)

    if trimPercents: 
        im = trim_image_border(image=im, trimPercents=trimPercents, verbose=verbose)

    # if isBrightfield: 
        # im *= -1   
        # im = im.astype(np.int)

    if show and (viewer is not None):
        viewer.set_image(im, pause=True)

    return im


class AnalyzeFrames():
    
    def __init__(
        self, 
        LUTLevels           = (),
        mode                = "stable",
        showPlots           = False,
        # saveArgsPath        = "",
        moviePath           = "",       # e.g. "./1_.tif",
        calibFilePath       = "",       # e.g. "./1_calib.npy",   # TODO: if provided, this single frame will be subtracted from every frame in actual movie
        # noiseBoxROI         = None,  # e.g. (13, 508, 463, 653)
        # jetBoxROI           = None,  # e.g. (13, 508, 463, 653)
        isBrightfield       = True,
        nFramesForAnalysis  = 20,
        bitRes              = 8,
        # rotateFrame         = "flip_LeftRight",
        # signifLevels        = [95],
        threshold           = False
        ):

        #################
        ## Load Movie

        # app = pg.mkQApp()
        mv = LoadMovieFrames(moviePath=moviePath)

        # if showPlots: viewer = ImageViewer2()

        #################
        ## Jet and Noise (Background) Regions
        
        # f0 = mv.get_frame(0)   # use 1st frame to determine jet/noise region

        # if not jetBoxROI: 
        #     jetBoxROI = draw_roi(f0.copy(), "Jet Box") 
        
        # if os.path.isfile(calibFilePath):
        #     calib_bkg = np.load(calibFilePath)
        #     noiseBox  = mv.rotate_frame(calib_bkg)
        #     calibBox  = noiseBox.copy()[jetBoxROI]
        calibBox = 0
        # else:
        #     if not noiseBoxROI: 
        #         noiseBoxROI = draw_roi(f0.copy(), "Noise Box") 
        #     noiseBox = f0.copy()[noiseBoxROI]


        ###################
        ## Stats Thresholds
        
        # for i, s in enumerate(signifLevels):
        #     if s > 1: signifLevels[i] = s/100.0
        # signifLevels.sort()  # in-place

        # zScores  = [round(sp.stats.norm.ppf(s),3) for s in signifLevels]  # ppf = percent point fx (Inverse of CDF: cumulative dist'n fx)
        # threshs  = [int(z*np.std(noiseBox)) for z in zScores]

        ###################
        ## Meshgrid

        resolution   = mv.resolution
        total_frames = mv.total_frames

        if  nFramesForAnalysis > total_frames: 
            nFramesForAnalysis = total_frames

        xxf, yyf = np.meshgrid(np.arange(0, resolution[0]), np.arange(0, resolution[1]), indexing='ij')    # TODO: shape trap
        xxf, yyf = xxf.astype(np.float) , yyf.astype(np.float)
        
        TRIMPERCENTS = [1,1]
        if TRIMPERCENTS:
            xxf = trim_image_border(image=xxf, trimPercents=TRIMPERCENTS)
            yyf = trim_image_border(image=yyf, trimPercents=TRIMPERCENTS, verbose=True)

        #################
        ## Data Container

        keys    = ["nGoodFrames", "nAnalyzedFrames", "nTotalFrames", "analysisRate_FPS",
                   "jetDeviationAngs_degrees", "jetDeviationAngsStdDev_jetJitter_degrees",
                   "jets1D", "jetsLineFit",
                   "frameWiseGoodnessOfFit_RMSEs_pixels", "avgOuterProduct_pixels", 
                   "avgInterframeDropletDisplacement_pixels", 
                   "fractionGoodFrames_jetStability_percent",
                   "jetDiameter_um", "jetSpeed_mps",
                   "excerpt"]

        # results = {s:dict.fromkeys(keys) for s in signifLevels}
        # results = {s:dict.fromkeys(keys) for s in [threshold]}
        results = {}

        tab = 4*" "
        # for sig, thresh in zip(signifLevels, threshs):
        # for thresh in [threshold]:
        thresh = threshold
        print("Jet Deviation (Jitter) Analysis at Threshold: {:.1f})\n".format( thresh))
        # print(tab+"Jet Deviation (Jitter) Analysis at {:.3g}% Significance (Pixel Value Threshold: {})\n".format(sig*100, thresh))

        nGoodFrames      = 0     # may not equal total num frames due to dark frames, etc.
        jetDeviationAngs = []    # in degrees (from horizontal = nozzle axis)
        RMSEs            = []    # framewise Root Mean Square of Errors (goodness of fit)
        jets1D           = []    # framewise digitized droplet locations along jet axis
        jetsLineFit      = []
        avgOuterProd     = []    # avg of frame-pair-wise outer product matrix of 'jets1D'


        ##########################################
        ## Data excerpt to save for 1st few frames 

        excerpt = {"outerProds":[], "frames": {}}    
        excerpt_range = range(3)
        for i in excerpt_range:
            excerpt["frames"][i] = dict.fromkeys(["jetBox", "jetBox_calib"])

        #########################################
        ## Main Engine
        #########################################

        t0  = time.time()
        for n in tqdm(range(0, nFramesForAnalysis)):

            # print(2*tab + 'Frame %6d of %6d ' % (n + 1, nFramesForAnalysis))
            rawIm  = mv.get_frame(n)
            # if showPlots: viewer.set_image(rawIm, pause=True)
            if n in excerpt_range: excerpt["frames"][n]["jetBox"] = rawIm.copy()

            jetBox = our_filter(rawIm, LUTLevels, thresh, isBrightfield=isBrightfield, trimPercents=TRIMPERCENTS, show=False, viewer=viewer, verbose=False)   
            
            # if showPlots: viewer.set_image(jetBox, pause=True)

            if n in excerpt_range: excerpt["frames"][n]["jetBox_calib"] = jetBox.copy()
            
            if mode == "dev":

                structure = np.ones((3, 3), dtype=np.int)               # Structure Dims must be 3. Better than default. This will consider features connected even if they touch diagonally
                labeled, ncomponents = label(jetBox == 0, structure)
                com = center_of_mass(jetBox, labels=labeled, index=np.arange(1, ncomponents, dtype=np.int))   # list of tuples (coords)
                if len(com) == 0:
                    com = ((0, 0), (1, 1))
                com = np.vstack(com)

                # TODO: clean up and speed up connected components
                # TODO: threshold on number of drops (jet or no jet?)
                # TODO: criterion on droplet ident. 
                #    num connected pixels, e.g. 5X5 or 6X6 pixels should be good
                #    or 
                #    biggest connected region is jet itself        --> jet length    
                #    Later: find size of each cocnnected region (droplet) --> gives avg droplet size, whihc we can later use as a thresh for accepting a blob as a droplet. so we may need 2 passes
                #    Now: 
                #       make a mask for bkg or record calib video
                #       for now, hard code the droplet size (pass as foced arg)
                # TODO: process connectd regions - find the one with largest aspect ratio, measure length.

                xf = com[:, 0].ravel()  # x-coords of all centers-of-mass
                yf = com[:, 1].ravel()  # y-coords of all centers-of-mass

                jetBox = labeled

            elif mode == "stable":
                
                com = None
                wJet = np.where(jetBox == 0)   # returns 2 arrs: row indices & col indices of elements satisfying a condition
            
                if len(wJet[1]) == 0:
                    msg = '---> Skipped: no pixels above threshold of %s' %(thresh)
                    print(3*tab + msg)
                    continue
                # print(xxf.shape)
                # print((wJet[0].shape))
                # print((wJet[1].shape))
                try:
                    # print("xxf    shape:", xxf.shape)
                    # print("jetBox shape:", jetBox.shape)
                    xf  = xxf[wJet]
                    yf  = yyf[wJet]     
                except IndexError as e:
                    # print("\n*** Transposing Coordinates!\n")
                    # print("xxf.T  shape:", xxf.T.shape)
                    xxf = xxf.T
                    yyf = yyf.T

                    xf  = xxf[wJet]
                    yf  = yyf[wJet]
            
            nGoodFrames += 1

            #########################
            ## least-square linear (order-1 polynom) fit to x,y positions of jet region

            slope, yint = np.polyfit(xf, yf, 1)
            yff  = slope*xf  +  yint

            ang  = np.sign(slope)*np.arctan(slope)

            line_fit_x = np.arange(0,max(jetBox.shape))   # TODO: not very reliable; assumes jetBox is longer along the jet; not unreasonable at all, but not foolproof
            line_fit_y = slope * line_fit_x + yint

            # print(line_fit_x.shape)
            # print(line_fit_y.shape)
            jetsLineFit.append(line_fit_y)

            # if showPlots: viewer.set_image(jetBox); viewer.plot_data((line_fit_x, line_fit_y), pause=True)

            ## Reference frame is JetBox[0,0]. It's possible that JetBox = Frame
            y0  = np.array([0, yint])       # (JetBox Ref Frame) pos. vector  of (extrapolated) beginning of jet 
            lit = np.vstack([xf,yf])        # (JetBox Ref Frame) pos. vectors of lit pixels
            
            ## Change Frame of Reference: JetBox --> Jet
            u1 = np.array([ np.cos(ang), np.sin(ang)])   # unit vector parallel      to jet axis
            u2 = np.array([-np.sin(ang), np.cos(ang)])   # unit vector perpendicular to jet axis

            d_lit  = lit.T - y0      # (Jet's Ref Frame)  pos. vectors of lit pixels
            lit_u1 = d_lit.dot(u1)   
            lit_u2 = d_lit.dot(u2)

            errs   = lit_u2**2
            rmse   = np.sqrt( np.mean (errs) ) # in pixel units. Same as: 'find_root_mean_square_errors(yff, yf)', where yff  = slope*xxf  +  yint
            RMSEs.append(rmse)

            ## (Jet Ref Frame) Bin data points (lit pixels) along  jet length (extrapolated)
            numBins = max(jetBox.shape)     # TODO: not very reliable; assumes jetBox is longer along the jet; not unreasonable at all, but not foolproof

            ## Hist (distr) of lit pixels along jet axis. Each histogram "signal" is a collection of lit pixels (i.e. a droplet) at that distance from nozzle tip 
            jet1D, bin_edges = np.histogram(lit_u1, bins=numBins, range=[0, numBins])
            
            if showPlots: 
                viewer.set_image(jetBox)
                viewer.plot_data((line_fit_x, line_fit_y), thickness=2, color='y',          pause=False)
                if com is None:
                    viewer.plot_data((line_fit_x,    jet1D), clear=False,                   pause=False)  # True
                else:
                    viewer.plot_data((line_fit_x, 10*jet1D), clear=False,                   pause=False)
                    viewer.plot_data(com.T, color='r', clear=False, connectPoints=False,    pause=True)  # True


            ang *= 180/np.pi               # radian --> degrees
            jetDeviationAngs.append(ang)
            jets1D.append(jet1D)
        ######################################################
        ## Find all pairwise outer product matrices of 1D jets 
        ######################################################

        def find_avg_pairwise_outer_prod_abs_diff(signals = []):

            OP     = 0
            old_op = np.multiply.outer(signals[0],signals[1])
            new_op = 0

            for i in range(1, -1 + len(signals)):

                new_op  = np.multiply.outer(signals[i],signals[i+1])
                OP     += np.abs(new_op - old_op)
                old_op  = new_op

                # if i in excerpt_range: excerpt["outerProds"].append(OP)

            avgOuterProd = OP / np.float(len(signals))

            return avgOuterProd

        def find_trim_effect(signals, trimFactors):
            all_avgOP   = []
            all_opTrans = []
            all_rowSums  = []
            all_peak_inds= []
            
            LEN = len(signals[0])

            for (Start,End) in trimFactors:

                Start   = int(Start * LEN)
                End     = int(End   * LEN)
                SIGNALS = [s[Start:End] for s in signals]

                avgOP   = find_avg_pairwise_outer_prod_abs_diff(SIGNALS)
                opTrans = matrix_transform(avgOP)
                rowSums = np.sum(opTrans, axis = 0)
                peakIndx= np.argmax(rowSums)

                all_avgOP.append(avgOP)
                all_opTrans.append(opTrans)
                all_rowSums.append(rowSums)
                all_peak_inds.append(peakIndx)

            return all_avgOP, all_opTrans, all_rowSums, all_peak_inds


        # TRIM_FACTORS = [0, 0.25,0.3, 0.5, 0.75, 0.95]
        TRIM_FACTORS = [(0.3,0.9)]
        all_avgOP, all_opTrans, all_rowSums, all_peak_inds = find_trim_effect(jets1D, TRIM_FACTORS)

        ######################################################
        tf = time.time()
        Dt = np.ceil(tf-t0)
        fps= int(np.ceil(nFramesForAnalysis / Dt))

        ######################################################
        
        if mode == "dev":
            def rebin(a, shape):
                sh = shape[0], a.shape[0] // shape[0], shape[1], a.shape[1] // shape[1]
                return a.reshape(sh).mean(-1).mean(1)

            viewer3.set_image(rebin(avgOuterProd, np.array(avgOuterProd.shape)/2))
            viewer2.set_image(convolve2d(avgOuterProd, np.ones((5, 5))))
        

        ###########################################################
        ## Find droplet displacelacement (in pixels) from avg outer product 
        ###########################################################

        def make_plots(plots, titles=[]):

            num     = len(plots)
            viewers = {}
            PAUSE   = False

            for i, plaut in enumerate(plots):
                title = ""
                if i < len(titles):
                    title = titles[i]

                v = ImageViewer2(title, logLUT = True)
                
                if i == num - 1: 
                    PAUSE = True
                v.set_image(plaut, pause=PAUSE)
                viewers[i] = v

            return viewers

        def make_line_plots(plots, titles=[], clear=False):
            

            num     = len(plots)
            PAUSE   = False
            
            for i, plaut in enumerate(plots):
                title = ""
                if i < len(titles):
                    title = str(titles[i])
                
                if i == num - 1: 
                    PAUSE = True
                liner.add_plot(plaut, title, pause=PAUSE, clear=clear)

            return liner


        if True:

            # viewers = make_plots(all_avgOP)
            # viewers = make_plots(all_opTrans)

            # v3 = ImageViewer2()
            # v4 = ImageViewer2()

            # v3.set_image(op_trans)
            # v4.set_image(op_trans2)
            TITLES = ["Transformed Outer Product Row Sums Using Top {} of 1D Jet. Peak Index = {}".format(i,j) for i,j in zip(TRIM_FACTORS, all_peak_inds)]
            r = make_line_plots(all_rowSums, TITLES, clear = True)
            # liner  = LinePlotter()  
            # liner.add_plot(rowSums,  "Row Sums: Avg Outer Prod")
            # liner.add_plot(rowSums2, "Row Sums: Avg Outer Prod (Median Clipped)")
            # liner.add_plot(rowSums3, "Row Sums: Avg Outer Prod (Truncated 1D Jets)")
            # liner.add_plot(rowSums4, "Row Sums: Avg Outer Prod (Truncated 1D Jets) (Median Clipped)", pause=True)

        #########################
        rowSums     = all_rowSums[0]
        peak_index  = all_peak_inds[0]

        possible_dx = np.abs(np.array([0 , np.max(rowSums.shape)]) - peak_index)  # possible droplet displacelacements (num pixels btw peak & each end 1D image axis). prevents errors due to axes/shape swaps 
        dx_pixels   = np.min(possible_dx)   # actual droplet displacement (in pixels)

        ###########################################################
        ## Stats
        ###########################################################


        angs  = np.asarray(jetDeviationAngs)   # degrees

        df    = pd.DataFrame.from_dict({"angs":angs})
        stats = df.describe().to_dict()["angs"]
        stats = {k:float("%.4f"%v) for k, v in stats.items()} 

        frac_good_frames      = int(100 * nGoodFrames/nFramesForAnalysis)
        
        jet_deviation_degrees = float("%.2f"%stats["mean"])
        jet_jitter_degrees    = float("%.2f"%stats["std"])

        ###########################################################
        ## Make data set for this signif level
        ###########################################################

        results["threshold"]                                 = float("{:.1f}".format(thresh))
        results["moviePath"]                                 = moviePath
        results["nGoodFrames"]                               = nGoodFrames
        results["nAnalyzedFrames"]                           = nFramesForAnalysis
        results["nTotalFrames"]                              = total_frames
        results["analysisRate_FPS"]                          = fps
        results["jetDeviationAngs_degrees"]                  = np.asarray(jetDeviationAngs)
        results["jetDeviationAngsStdDev_jetJitter_degrees"]  = jet_jitter_degrees
        results["jets1D"]                                    = jets1D
        results["jetsLineFit"]                               = jetsLineFit
        results["frameWiseGoodnessOfFit_RMSEs_pixels"]       = np.asarray(RMSEs)
        results["avgOuterProduct_pixels"]                    = avgOuterProd
        results["avgInterframeDropletDisplacement_pixels"]   = dx_pixels
        results["fractionGoodFrames_jetStability_percent"]   = frac_good_frames
        results["excerpt"]                                   = excerpt

        self.results = results 

#############################
def stdout_data(dataDict): 

    ## stdout format

    tab    = 4*" "
    form   = 3*tab + "{: <15} | {: <5} |{: ^9}| {}\n"
    header = form.format("Quantity", "Value", "Unit", "Description")

    def summarize(field, value, unit, description):
        row = form.format(field, value, unit, description)
        summary+=row + "\n"
        print(row)

    summary = ""

    data= dataDict

    # sig       = 100*sig
    fps       = data["analysisRate_FPS"]
    rmse      = int(np.median(data["frameWiseGoodnessOfFit_RMSEs_pixels"]))
    pixSize   = "%.1f"%data["pixelSize_um"]
    liqFlow   = "%.1f"%float(data.get("LiqVolFlowRate_uLPERmin",0))
    gasFlow   = "%.1f"%float(data.get("HeliumMassFlowRate_mgPERmin",0))
    stability = data["fractionGoodFrames_jetStability_percent"] 
    jitter    = data["jetDeviationAngsStdDev_jetJitter_degrees"]
    angle     = "%.1f"%np.median(data["jetDeviationAngs_degrees"])
    diameter  = int(data["jetDiameter_um"])
    speed     = int(data["jetSpeed_mps"])

    nGoodFrames      = data["nGoodFrames"]
    nAnalyzedFrames  = data["nAnalyzedFrames"]
    analysisTime_sec = "{:.1f}".format(nAnalyzedFrames/fps)

    # summary += form.format("Confidence"      , sig       , "percent" , "Statistical Significance")
    summary += form.format("Efficiency"      , fps       , "Hz"     , "Analyzed {} frames in {} seconds".format(nAnalyzedFrames, analysisTime_sec))
    summary += form.format("RMS Error"       , rmse      , "um"     , "Goodness of Fit")
    summary += form.format("Pixel Size"      , pixSize   , "um"     , "Magnification dependent")
    summary += form.format("Liq Flow Rate"   , liqFlow   , "uL/min" , "0 iff unknown")
    summary += form.format("Gas Flow Rate"   , gasFlow   , "mg/min" , "0 iff unknown")
    summary += form.format("Jet Stability"   , stability , "%"      , "{}/{} frames had 'good' jet.".format(nGoodFrames, nAnalyzedFrames))
    summary += form.format("Jet Jitter"      , jitter    , "deg"    , "Sporadic variation in jet axis.")
    summary += form.format("Jet Angle"       , angle     , "deg"    , "From nozzle axis")
    summary += form.format("Jet Diameter"    , diameter  , "um"     , "0 iff liq flow rate is unknown")
    summary += form.format("Jet Speed"       , speed     , "m/s"    , "Based on droplet displacement")
    

    print("\n"+tab+"Results\n")
    print(header + 2*tab + "="*70)
    print(summary)
    # print(2*tab + "="*70 + "\n")

    return summary


@jit(nopython=True)
def matrix_transform(matrix):   
    arr = np.empty_like(matrix)
    fast_scan, slow_scan = matrix.shape

    for i in range(slow_scan):
        for j in range(fast_scan):
            arr[i, j] = matrix[i,(j+i) % fast_scan]
    
    return arr


def find_jet_speed_diameter(displacement_pixels, pixelSize_um, period_ns, liqFlowRate_uLPERmin):

    speed_mps  = int(0)
    diameter_um= int(0)

    if not displacement_pixels:
        return speed_mps, diameter_um

    ############################################################ 
    ## convert (droplet displacement) from pixel units to microns    
    ############################################################ 
    """
    Model formula: 
        pixel_size = 2.0809 * zoom^(-1.001)  ,  where  R^2 = 0.9994
    """
    # if fastCamZoom == 0:
    #     raise ValueError("FastCam zoom cannot be zero!")

    # validZooms = np.arange(1, 7.5, step = 0.5)
    # assert fastCamZoom in validZooms, "Invalid FastCam Zoom. Choices: " + validZooms

    # pixelSize_um = 2.0809/(fastCamZoom**(1.001))  # pixel size (um) for nativar 12x lens


    ############################################################
    ## find jet speed
    ############################################################
    """
    'period_ns' is delay (nano-sec) 
         between 2 pulses in a burst from the function generator
    """

    dx  = displacement_pixels  
    dx *= pixelSize_um          # pixels  --> micrometers
    dt  = period_ns / 1000      # nanosec --> microseconds

    try:    speed_mps = dx/dt                # meters per second 
    except: speed_mps = int(0)

    ############################################################
    ## Find jet diameter from jet speed
    ############################################################

    """
    r  = s * sqrt(Q/v)

        where ...

        Quantity | Units    | Description    | value
        ---------|----------|----------------|-----
            r    | um       | jet  radius    | 
            Q    | uL/min   | liq  flow rate | 
            v    | m /s     | jet  speed     |
            s    | dim-less | unit convertor | sqrt(50/(3*pi)) = 2.3033 

        
        e.g. a nozzle jetting at 1 m/2 at 1 uL/min liq flow rate 
             will have a jet radius of 2.3 um
    """

    s = np.sqrt(50/(3*np.pi))
    Q = liqFlowRate_uLPERmin
    v = speed_mps
    r = int(0)

    if (Q != 0) and (v != 0): 
        r = s * np.sqrt(Q/v)

    diameter_um = 2*r

    return speed_mps, diameter_um


###########################################################################################

def handle_all_calib_movies(target_dir, extensions):

    ## See if movie dir contains calib_files.tiff: movie w/ just background, ie. no nozzle/jet
    ##      For every existing calib file, find the avg background and save as same_name.npy

    # calibTiffs = find_files_by_extensions(target_dir = target_dir, extensions = extensions)
    # if calibTiffs:
    #     for calibTiff in calibTiffs:
    #         name, ext = os.path.splitext(calibTiff)
    #         calib_npy = name + ".npy"
    #         if not os.path.isfile(calib_npy):
    #             find_avg_image(calibTiff, startFrameNum = 0, nFrames = 10 , saveResult = True)

    # calibNumpys = find_files_by_extensions(target_dir=target_dir,  extensions=["_calib.npy"])
    
    # return calibNumpys

    pass 

def find_matching_calib_movie(calibNpyPaths):

    # ## TODO: find closest matching calibFile for each movie based on run number or timeStamp or creation time. For now, we'll use same calibfile for every movie
    
    # try:                    calibFilePath = calibNumpys[0]
    # except IndexError:      calibFilePath = ""
    # except Exception as e:  print("*** Error While Handling Calib Movie.\n", type(e), e); sys.exit()
    
    # return calibFilePath
    
    pass    

def print_header(lines=[], symbol="*", length=0, double=False):
    
    if not length:
        length = max([len(str(i)) for i in lines])
    sep = symbol*length
    if double: sep += "\n" + sep
    
    print("\n" + sep)
    for l in lines: print(l)
    print(sep + "\n")

def print_warning(key, *terms):

    bad_msg = dict(
        speed=  "FAILED to find droplet displacement at threshold {}. Won't save results.",
        zoom =  "UNKNOWN Fastcam Zoom. Assuming {}",
        ifp  =  "UNKNOWN Burst Period. Assuming {} ns",
        liq  =  "UNKNOWN Liquid Flow.  CANNOT find jet diameter. Assuming {:.1f} uL/min",
        )

    try: 
        msg = bad_msg[key].format(*terms)
    except KeyError:
        msg = " ".join(list(terms))

    PREFIX = "\n    *** Warning: "

    print(PREFIX + msg)

def get_nozzle_database(moviePath, dbPath):

    if os.path.isfile(dbPath):

        db = load_pickle(dbPath)


def handle_movie_args_and_paths(moviePath, cmdline_args, roi_params = None, viewer=None):

    #################################
    ## Get/make movie args/results paths

    name   = os.path.splitext(os.path.basename(moviePath))[0]
    mv_dir = os.path.dirname(moviePath)

    for DIR in "results", "args":
        DIR = os.path.join(mv_dir, DIR)
        if not os.path.isdir(DIR):
            os.makedirs(DIR, exist_ok=True)

    timeStamp   = dt.now().strftime("_%Y%m%d_%H%M%S")
    resultsPath = os.path.join(mv_dir, "results" , name) + ".pkl"   # timeStamp
    argsPath    = os.path.join(mv_dir, "args"    , name) + ".txt"

    ###############################

    if os.path.isfile(argsPath):
        argsFile     = argsPath
        saveArgsPath = ""
    else:
        argsFile     = cmdline_args["argsFileTemplate"]
        saveArgsPath = argsPath

    #################################
    ## Get & update movie args

    mv_args = read_argsFile_txt(argsFile)
    # if roi_params is not None:
    #     mv_args.update(roi_params)
    #     saveArgsPath = argsPath

    # # calibFilePath        = find_matching_calib_movie(calibNumpys)
    # # if calibFilePath == "": calibFilePath = None
    # mv_args["calibFilePath"] = None  # or "" ? (there IS a diff)
    
    # if cmdline_args["nFrames"] != 0:
    #     saveArgsPath = argsPath
    #     mv_args["nFramesForAnalysis"] = cmdline_args["nFrames"]

    # if (mv_args["threshold"] is None) or (mv_args["LUTLevels"] is None):
    #     saveArgsPath = argsPath
    #     mv  = LoadMovieFrames(moviePath=moviePath)
    #     jet = mv.get_frame(0)
    #     jet1= mv.get_frame(1)
    #     if np.mean(jet) > np.mean(jet1):  # pick the darker frame (brightfield)
    #         jet = jet1
    #     # jet = ndimage.median_filter(jet, size=5)
    #     viewer.set_image(jet)
    #     viewer.app.exec_()

    #     mv_args["threshold"] = viewer.drag.value()
    #     mv_args["LUTLevels"] = viewer.hst.getLevels()

    #################################
    ## Save movie args (if necessary)

    if saveArgsPath and (cmdline_args["mode"] == "stable"):

        with open(saveArgsPath, "w") as f:
            form = "{: <20} = {}\n"
            for k,v in sorted(mv_args.items()):
                f.write(form.format(k,v))
        
            print("\n\tSaved Analysis Args:\n\t", saveArgsPath) 

    #################################

    print_header(lines = ["Analyzing: " + moviePath , 
                          "Args File: " + argsFile], symbol="-")


    return mv_args, resultsPath


def handle_commandline_args():

    ARGS = arg_parse_microjet_analysis() 
    PATH = ARGS["path"]

    if not os.path.exists(ARGS["dataFile"]):
        print_header(["Data Log Not Found/Provided", "CANNOT Find Jet Speed, Diameter, Length."], double=True)
    
    if ARGS["mode"] == "dev":
        print_header(["'Dev' Mode (Not Optimized)"])    

    if os.path.isfile(PATH):
        ARGS["movies"] = [PATH]
        PARENTDIR      = os.path.dirname(ARGS["path"])

    elif os.path.isdir(PATH):
        ARGS["movies"] = find_files_by_extensions(target_dir = PATH, recursive = False, extensions=[".tif", ".tiff"], ignores=[] )
        PARENTDIR      = PATH
        
    
    YEAR, DATE, NOZZLENAME  = PARENTDIR.split(os.sep)[-3:]

    ARGS["parentdir"]       = PARENTDIR
    ARGS["year"]            = YEAR
    ARGS["date"]            = DATE
    ARGS["nozzlename"]      = NOZZLENAME.replace("_","")

    ARGS["repodir"]         = os.path.abspath(os.path.dirname(__file__))
    ARGS["tempdatadir"]     = os.path.join(ARGS["repodir"], "tempdata", "microjets", YEAR, DATE, NOZZLENAME)      # for ROI.npy    (local; git-ignored)
    ARGS["configdir"]       = os.path.join(ARGS["repodir"], "config" )                                            # for ROI coords (git-controlled)
    
    ARGS["argsFileTemplate"]= os.path.join(ARGS["configdir"],  "argsFileTemplate.txt")

    # ppp(ARGS, exit=True)
    return ARGS

def pre_process_movies(moviePaths, cmdline_args):

    # calibPaths = handle_all_calib_movies(target_dir=ARGS["parentdir"], extensions=["_calib.tiff", "_calib.tif"])
    
    tempdatadir = cmdline_args["tempdatadir"]
    
    if not os.path.isdir(tempdatadir):
        os.makedirs(tempdatadir, exist_ok=True)
    
    ROIPATHS = handle_all_args_and_rois(
                moviePaths  = moviePaths, 
                cmdline_args= cmdline_args, 
                nFrames     = 500,  # None = all frames! 
                roiType     = "jetbox", 
                rotateFrame = "flip_LeftRight", 
                overwrite   = False,
                saveDir     = tempdatadir)
    """
    roi_paths_params = get_all_roi(
            moviePaths  = moviePaths, 
            nFrames     = None,  # None = all frames! 
            roiType     = "jetbox", 
            rotateFrame = "flip_LeftRight", 
            overwrite   = False,
            saveDir     = tempdatadir) 

    BefAft = ImageBeforeAfter()
    # BefAft = None
    
    for jetboxPath, roiParams in sorted(roi_paths_params.items()):
        handle_movie_args_and_paths(jetboxPath, cmdline_args, roiParams, viewer = BefAft)    

    ROIPATHS = list(roi_paths_params.keys())
    ROIPATHS.sort()
    """

    return ROIPATHS

def get_run_observations(nozzle, date, runNum, dframe):
    try:
        DF = dframe
        df = DF[DF.index.get_level_values("nozzle") == str(nozzle)]
        df = df[df.index.get_level_values("date")   == int(date)]
        df = df[df.index.get_level_values("run")    == runNum]
    
        data = df[["gas", "liq", "zoom", "pixelSize"]].values[0]
        data = [float(i) for i in data]

    except Exception as e:
        print(repr(e))
        data = [0,0,0,0]

    gasFlow, liqFlow, zoom, pixelSize = data
    period = 0

    if zoom == 0:
        zoom = 3.5
        print_warning("zoom", zoom)

    if period == 0:
        period = 550
        print_warning("ifp", period)
    
    if liqFlow == 0:
        liqFlow = 20
        print_warning("liq",liqFlow)  

    pixelSz= float("{:.3f}".format(2.0809/(zoom**(1.001))))

    obs = dict(
        zoom    = zoom,
        period  = period,
        liqFlow = liqFlow,
        gasFlow = gasFlow,
        pixelSz = pixelSz)

    return obs 

def post_process(all_results={"moviePath":"results"}, experimentalDataPath="", saveResults=False):

    ## Find Jet Speed/Diam 
    

    try:
        df = pd.read_csv(experimentalDataPath)
        df = df.set_index(["nozzle","date", "run"])

    except:
        df = ""
    
    results_types  = [dict, str]    
    msg_bad_results= "*** Skipped. TypeError: Movie results are not of type {} for movie".format(repr(results_types))     

    # old_date   = None
    # old_nozzle = None
    # old_runnum = None

    ALL_RESULTS = {}

    for moviePath, results in all_results.items():

        if isinstance(results, str) : 
            resultsPath = results
            results     = load_pickle(results)
        elif isinstance(results, dict): 
            resultsPath = ""
        else:
            print(msg_bad_results + moviePath)
            continue

        DATE, NOZZLENAME, RUNNUM = moviePath.split(os.sep)[-3:]

        NOZZLENAME = NOZZLENAME.replace("_","")
        RUNNUM     = int(os.path.splitext(RUNNUM)[0])

        OBS = get_run_observations(NOZZLENAME, DATE, RUNNUM, dframe = df)

        speed, diam = find_jet_speed_diameter(
                            results["avgInterframeDropletDisplacement_pixels"], 
                            OBS["pixelSz"], OBS["period"], OBS["liqFlow"])
        
        if not speed: 
            print_warning("speed", thresh)
            continue     

        results["jetSpeed_mps"]   = speed
        results["jetDiameter_um"] = diam

        results["pixelSize_um"]                = OBS["pixelSz"]
        results["HeliumMassFlowRate_mgPERmin"] = OBS["gasFlow"]
        results["LiqVolFlowRate_uLPERmin"]     = OBS["liqFlow"]
        results["FastcamZoom"]                 = OBS["zoom"]
        results["FastcamIFP_ns"]               = OBS["period"]

        results["frameWiseGoodnessOfFit_RMSEs_pixels"] *= OBS["pixelSz"]
            
        stdout_data(results)

        if saveResults and resultsPath:

            save_pickle(results, resultsPath)   
            ALL_RESULTS[moviePath] = resultsPath
            print("\n    Saved Post-Processing Results: \n\t\t", resultsPath)
        
        else:
            ALL_RESULTS[moviePath] = results
            print_warning(None, "Did NOT save post-processing results to disk. Consider passing --save.")

        ALL_RESULTS[moviePath] = results

    return ALL_RESULTS


def main_analysis(moviePaths, cmdline_args):

    ALL_RESULTS = {}
    bad_movies  = set()

    for moviePath in moviePaths:

        ###########################
        ## Analyze
        ########################### 

        mv_args, resultsPath = handle_movie_args_and_paths(moviePath, cmdline_args)    

        MV_ARGS = mv_args.copy()
        for k in "roi_pos", "roi_size", "roi_slices", "line_value":
            if k in MV_ARGS: del MV_ARGS[k]

        Run = AnalyzeFrames(moviePath = moviePath,
                            mode      = cmdline_args["mode"],  
                            showPlots = cmdline_args["show"],  
                            **MV_ARGS)
        
        RESULTS = Run.results
        if not RESULTS["avgInterframeDropletDisplacement_pixels"]:
            print_warning("speed", MV_ARGS["threshold"])
            bad_movies.add(moviePath)
            continue

        ###########################
        ## Save
        ###########################        
    
        if cmdline_args["save"]:        # and  (cmdline_args["mode"] == "stable"):
            save_pickle(RESULTS, resultsPath)   
            ALL_RESULTS[moviePath] = resultsPath
            print("\n    Saved Initial Analysis Results: \n\t\t", resultsPath)
        else:
            ALL_RESULTS[moviePath] = RESULTS
            print_warning(None, "Did NOT save analysis results to disk. Consider passing --save.")
          
    if bad_movies:
        print_header(["BAD MOVIES (could not find droplet displacement)"])
        for mv in bad_movies:
            print("--->", os.path.basename(mv))
    else:
        print_header(["No 'Bad' Movies"])

    return ALL_RESULTS 


def main_pre_process():

    print_header(["Pre-Processing"])

    CMDL_ARGS   = handle_commandline_args()
    ROIPATHS    = pre_process_movies(CMDL_ARGS["movies"], CMDL_ARGS)    

    return ROIPATHS, CMDL_ARGS

def main_whole_process(): 

    ROIPATHS, CMDL_ARGS = main_pre_process()
    ALL_RESULTS = main_analysis(ROIPATHS, CMDL_ARGS)
    ALL_RESULTS = post_process(ALL_RESULTS, CMDL_ARGS["dataFile"], CMDL_ARGS["save"])

    # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        
    #     APP = QtGui.QApplication.instance()
    #     if APP is not None: 
    #         APP.exec_() 

    print_header(["THE END!"])

    return True

if __name__ == "__main__":
    
    # boom = main_pre_process(); pg.exit()
    boom = main_whole_process(); pg.exit()


   
