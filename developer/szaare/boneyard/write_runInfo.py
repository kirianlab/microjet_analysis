import os 

with open("./observations.txt" , "r") as f:
    lines= [i.strip() for i in f.readlines()]
    rows = [i.split(",") for i in lines]

# TDOO: import the following header from the original logger (in microjetDAQ repo?) b/c they must match!

header = ["HeliumMassFlowRate_mgPERmin","LiqVolFlowRate_uLPERmin",
          "EpochTime_sec","HumanTime",
          "JetStability","JetJitter","JetDeviation",
          "Bubbles",
          "Lens","Zoom","PixelSize_um","FastcamIFP_ns",
          "Medium","Comments"]

header = ",".join(header)+"\n"

for r in rows[1:]:
    name, ext = os.path.splitext(os.path.basename(r[0]))
    runInfoPath = name + "_observations.txt"
    with open(runInfoPath, "w+") as f:
        line = ",".join(r[1:])+"\n"
        f.writelines([header, line])