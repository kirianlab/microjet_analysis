
import os, sys, functools
import numpy as np 

import matplotlib
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

import pyqtgraph as pg
from pyqtgraph import QtGui, QtCore

from scipy.ndimage import gaussian_filter
from scipy.interpolate import griddata

from find_jet_speed import ImageViewer2, load_pickle


########################################################################
## Params
########################################################################

dataPicklePath = os.path.expanduser(
    "/home/szaare/Desktop/movies/stella/old_faithful_substack_1-100_analysisResults_20190408_161241.pkl")

usePyQtGraph  = True
useMatPlotLib = False   # TODO: figure out later


########################################################################
## Matplotlib params
########################################################################

params = {'font.size'     : 20,
          'figure.figsize':(15.0, 25.0),
          # 'figure.facecolor':'k',
          'figure.titleweight':'bold',
          'axes.facecolor':'k',
          'axes.titleweight':'bold',
          'grid.color':'w',
          'grid.linewidth':0.5,
          'legend.facecolor':'w',
          'lines.linewidth': 10.,
          'lines.markersize': 15,
          'image.cmap': 'Greys',}
matplotlib.rcParams.update(params)
# for k in matplotlib.rcParams.keys():
#   if "map" in k: print(k)
# sys.exit()

########################################################################
## Load pickle & get data w/ highest stats confidence
########################################################################

results = load_pickle(dataPicklePath)
sigs    = list(results.keys())
SIG     = np.max(sigs)
data    = results[SIG]          

excerpt         = data["excerpt"]
outerProds      = excerpt["outerProds"]
frames          = excerpt["frames"]

########################################################################
## Organize data
########################################################################

jetBoxes        = []
jetBoxes_calib  = []
jets1D          = []
jetsLineFit     = []

for n,frame in frames.items():
    jetBoxes.append(frame["jetBox"])
    jetBoxes_calib.append(frame["jetBox_calib"])
    jets1D.append(data["jets1D"][n]) 
    jetsLineFit.append(data["jetsLineFit"][n])

X = np.arange(0, len(jets1D[0]))   # x values for jet1D and lineFit plots (both same 'length' as a jetBox)

########################################################################
## Make images from data
########################################################################

## Jet boxes

jetBoxes_pair       = np.concatenate(jetBoxes[:2],       axis = 1)
jetBoxes_pair_calib = np.concatenate(jetBoxes_calib[:2], axis = 1)

jet1D_jetBox = np.concatenate([
    np.ones_like(jetBoxes_calib[0]),
    jetBoxes_calib[0]], axis = 1)            

center = int(np.min(jetBoxes_pair.shape)/2)
y_offset1 = center - 15
y_offset2 = center + 10

## Outer product 

sx, sy =  jetBoxes[0].shape
prod   = outerProds[0].copy()

prod[0:sx,0:sy] = 10+ jetBoxes[0] 
prod[0:sy,0:sx] = 10+ jetBoxes[1].T 

# prod[prod < np.median(prod)] = 0
# prod[prod > np.median(prod)] = 1
np.fill_diagonal(prod, np.max(prod))

####################
## Plot w/ Pyqtgraph
####################

if usePyQtGraph:

    PAUSE  = True
    viewer = ImageViewer2()

    viewer.set_image(jetBoxes_pair,       pause=PAUSE)
    viewer.set_image(jetBoxes_pair_calib, pause=PAUSE)

    viewer.set_image(jetBoxes_calib[0]); viewer.plot_data((X, jetsLineFit[0]),  pause=PAUSE)
    viewer.set_image(jetBoxes_calib[0]); viewer.plot_data((X, jets1D[0]),       pause=PAUSE)
    viewer.set_image(jetBoxes_pair);     viewer.plot_data((X, 1*(jets1D[0]) + y_offset1)); viewer.plot_data((X, jets1D[1]+ y_offset2), pause=PAUSE, clear=False, color='g')
    viewer.set_image(prod, pause=True, autoLevels=True, clear=True)

####################
## Plot w/ Matplotlib
####################

if useMatPlotLib: 
    # TODO: figure out later
    fig, ax = plt.subplots(6,1, sharex='all', sharey='none')
    ax[0].imshow(jetBoxes_pair.T)
    ax[1].imshow(jetBoxes_pair_calib.T)
    ax[2].imshow(jetBoxes_calib[0].T)
    ax[3].imshow(jetBoxes_calib[0].T)
    ax[4].imshow(jetBoxes_pair.T)
    ax[5].imshow(prod.T)
    plt.show()

if __name__ == '__main__':

    pg.exit()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        
        APP = QtGui.QApplication.instance()
        if APP is not None: 
            APP.exec_() 