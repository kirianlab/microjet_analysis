import sys, os
import pandas as pd
import numpy  as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm

pd.set_option('display.max_columns', 100)

def get_processed_movies(listPath):
    
    print("Getting list of processed .npz movies")

    with open(listPath, 'r') as f:
        paths = [os.path.abspath(l.strip()) for l in f.readlines()]
        get_run_num = lambda x: int(x.split('_')[2])
        
        ## sort paths by run num
        paths   = sorted(paths, key=get_run_num)
        runNums = [get_run_num(f) for f in paths]

    return paths, runNums


def get_droplet_displacements(paths):

    print("Getting droplet displacements from each processed .npz movie")
    print("Drop Translation (pixels)\t", "Movie")

    disps = []
    for p in paths:

        dat = np.load(p)
        t = np.round(dat['droplet_translation'],2)
        disps.append(t)
        # print(t, "\t", os.path.basename(p))

    return np.round(np.array(disps),1)


def get_flow_data(logPath, runNums):

    print("Getting flow rates, pixel size, etc. for each movie.")
    
    desired_cols = ["RunVidNum","HeliumMassFlowRate_mgPERmin", "LiqVolFlowRate_uLPERmin", "Zoom", "PixelSize_um", "FastcamIFP_ns"]
    renamed_cols = ['run', 'gas', 'liq', 'zoom', 'pixelsize', 'ifp']
    colname_map  = dict(zip(desired_cols, renamed_cols))

    df = pd.read_csv(logPath)    
    df = df.loc[df['RunVidNum'].isin(runNums), desired_cols]
    df.rename(columns=colname_map, inplace=True)

    return df


def plot_speed_v_gas(dFrame, title=""):

    fig_title       = "Jet Speed for Austin's Data (2019/05/01).\n(Point Labels Are Run Numbers) (Helium Gas Flow is 10 mg/min for all points) "
    gas_ax_label    = 'Helium Gas Flow Rate (mg/min)\n'
    liq_ax_label    = 'Liq Flow Rate (uL/min)\n'
    speed_ax_label  = "Jet Speed (m/s)"

    # unique_liq = np.unique(dFrame['liq'])
    
    ax = sns.scatterplot( x="liq", y="jet_speed", size="gas", data=dFrame, sizes=(100,500), legend=False)    #  style='liq', hue='liq', ,legend='full', palette=sns.color_palette("dark",len(unique_liq))
    ax.grid(True)

    ## Add run nums to each point
    for row in range(0,dFrame.shape[0]):
         X = dFrame.liq[row]
         Y = dFrame.jet_speed[row] + 0.05
         L = dFrame.run[row]
         ax.text(X, Y, L, horizontalalignment='left', size=12, color='black', weight='light')

    # plt.legend(loc='upper left', title='Water Vol Flow \nRate (uL/min)')
    plt.title(fig_title)
    plt.xlabel(liq_ax_label)
    plt.ylabel(speed_ax_label)
    plt.show()  


def main(npz_paths, data_log_path):

    paths, runNums  = get_processed_movies(npz_paths)
    displacements   = get_droplet_displacements(paths)
    # displacements = np.random.randint(100,size=len(runNums))  # for debugging

    df              = get_flow_data(data_log_path, runNums)
    find_jet_speed  = lambda row : round(1000*row['droplet_translation']*row['pixelsize']/row['ifp'], 1)

    df['droplet_translation']   = pd.Series(displacements , index=df.index)
    df['jet_speed']             = df.apply(find_jet_speed , axis=1)
    df['movie']                 = pd.Series([os.path.basename(p)[:20] for p in paths], index=df.index)
    

    df.to_csv('./austin_results_jet_speed',index=False)
    plot_speed_v_gas(df)
    print(df)


if __name__ == '__main__':

    print(len(sys.argv))
    npz_paths, data_log_path = sys.argv[1:] 
    main(npz_paths, data_log_path)
