import sys
import pyqtgraph as pg

import pyqtgraph as      pg
from   pyqtgraph import  GraphicsView
from   pyqtgraph.Qt import QtCore, QtGui

from    skimage import morphology as mf

import  numpy  as np
from    numba  import jit

import  scipy  as sp
from    scipy.ndimage.measurements import label, center_of_mass
from    scipy.signal import convolve2d


from libs.rand_data_makers import make_random_frame_stack
from    libs.viewers.image_viewers      import ImageViewer2

frameStack_npy = os.path.expanduser("~/Desktop/movies/stella/old_faithful/old_faithful_substack_1-5.npy")
resolution = (1024,1024)
#########################
viewer = ImageViewer2()

xxf, yyf = np.meshgrid(np.arange(0, resolution[1]), np.arange(0, resolution[0]), indexing='ij')    # TODO: shape trap
xxf, yyf = xxf.astype(np.float) , yyf.astype(np.float)

with np.open(frameStack_npy) as data:
    jetBoxes = data[:]


# jetBox = 100*make_random_frame_stack((1,5,5))[0]
# jetBox.astype(np.int)

for jetBox in jetBoxes:

    jetBox = jetBox - np.median(jetBox)
    jetBox *=  -1
    jetBox = mf.opening(jetBox, mf.disk(2))

    thresh = np.median(jetBox)
    jetBox[jetBox < thresh] = 0 
    jetBox[jetBox > thresh] = 2**8

    structure = np.ones((3, 3), dtype=np.int)
    labeled, ncomponents = label(jetBox > 0, structure)
    com = center_of_mass(jetBox, labels=labeled, index=np.arange(1, ncomponents, dtype=np.int))
    if len(com) == 0:
        com = ((0, 0), (1, 1))
    com = np.vstack(com)

    wJet = np.where(jetBox > 0)

    # xf  = xxf[wJet]
    # yf  = yyf[wJet]

    xf = com[:, 0].ravel()
    yf = com[:, 1].ravel()

if __name__ == '__main__':
    
    viewer.set_image(jetBox)

    print("1")
    # pg.exit()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        
        APP = QtGui.QApplication.instance()
        print("2")

        if APP is not None: 
            APP.exec_() 
            print("3")

    # pg.exit()
    print("4")
