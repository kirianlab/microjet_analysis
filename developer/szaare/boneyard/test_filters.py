import os, sys, imageio

import  numpy  as np
from    numba  import jit
import  scipy  as sp
from   scipy import ndimage

import pyqtgraph as      pg
from   pyqtgraph import  GraphicsView
from   pyqtgraph.Qt import QtCore, QtGui

from    scipy.ndimage.measurements import label, center_of_mass


from    libs.viewers.image_viewers      import ImageViewer2

viewer1 = ImageViewer2()
viewer2 = ImageViewer2()


###########################################################
## Image
###########################################################

this_script_dir = os.path.dirname(os.path.abspath(__file__))
SAMPLE_JET_PATH = os.path.join(this_script_dir, "libs","viewers", "sample_jet.png")

###########################################################

def get_png(path):

	im = imageio.imread(path)
	im = im[50:120,800:,0].T    		# RGB --> grayscale
	im = im.astype(np.int)

	return im

def remove_speckle(image):

	img_open   = ndimage.binary_opening(image)
	img_erode  = ndimage.binary_erosion(image)
	img_recon  = ndimage.binary_propagation(img_erode, mask=image)	

	return img_recon

def find_edges_sobel(image):
	border = np.min(image)
	sx = ndimage.sobel(image, axis =0, mode="constant", cval=border)
	sy = ndimage.sobel(image, axis =1, mode="constant", cval=border)
	h  = np.hypot(sx,sy) 

	return h, sx, sy

def filter_gaussian(image):
	sz = np.sqrt(image.shape)
	sigmas = 1/(4.0*sz)
	return ndimage.gaussian_filter(image, sigma=(2,2))

def thresh_percentile(image, percentiles = [5,15], verbose = True):

	im    = image.copy()    
	p1,p2 = np.min(percentiles), np.max(percentiles)
	
	t1 = np.percentile(image, p1, axis=None, interpolation='nearest')
	t2 = np.percentile(image, p2, axis=None, interpolation='nearest')
	
	MIN , MAX = np.min(image), np.max(image)
	im[image  < t1] = MIN
	im[image  > t2] = MAX


	if verbose:
		pretty = "{: >10} th percentile: {: >4} --> {}"
		print("\nPercentile Thresholding:\n")
		print(pretty.format(0,   MIN, ""))
		print(pretty.format(p1,  t1 , MIN))
		print(pretty.format(p2,  t2 , MAX))
		print(pretty.format(100, MAX, ""))
		print()

	return im

def make_plots(plots):

	num     = len(plots)
	viewers = {}
	PAUSE   = False

	for i, (title, plaut) in enumerate(plots):
		v = ImageViewer2(title, logLUT = True)
		
		if i == num - 1: 
			PAUSE = True
		v.set_image(plaut, pause=PAUSE)
		viewers[i] = v

	return viewers


###########################################################

jet = get_png(SAMPLE_JET_PATH)

# jet *=-1

med 	  = ndimage.median_filter(jet, size=5) 	
# med_prc = ndimage.percentile_filter(med, percentile=40, size=20)  # good but not enough
med_percs = thresh_percentile(med, percentiles = [5,15]) 			# seems like the best
# med_percs_sob, _,_ = find_edges_sobel(med_percs)						# cute & effective but resource-intensive and not very useful
# gauss = filter_gaussian(sob) 										# not a major improvement

im = med > 50

# structure = np.ones((3, 3), dtype=np.int)       	# Structure Dims must be 3. Better than default. This will consider features connected even if they touch diagonally
# labeled, ncomponents = label(jet>80, structure)

make_plots([
	("original", jet), 
	("median", med), 
	# ("median then percentile 40",    med_prc), 
	("median then percentiles 5-15", med_percs), 
	("median then percentiles 5-15 then clip", im), 
	])
pg.exit()
###########################################################
if __name__ == '__main__':

    # pg.exit()

    ## Start Qt event loop unless running in interactive mode or using pyside.
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        
        APP = QtGui.QApplication.instance()
        if APP is not None: 
            APP.exec_() 

    pg.exit()  # MUST! Avoids many pyqt(graph) seg. faults and bugs. ref: http://www.pyqtgraph.org/documentation/functions.html#pyqtgraph.exit
