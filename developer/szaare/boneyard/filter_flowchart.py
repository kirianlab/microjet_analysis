import  sys
import  numpy       as np
import  pyqtgraph   as pg
import  pyqtgraph.metaarray as metaarray

import  pyqtgraph.flowchart.library as fclib   # to register custom Node sublcass (e.g. fclib.registerNodeType(CustomNodeTemplate, [('Category', 'Sub-Category')]))
from    pyqtgraph.flowchart import Flowchart, Node
from    pyqtgraph.Qt        import QtGui, QtCore

###############################
## GLOBAL FX
###############################

def make_fake_noisy_signal():

    data  = np.random.normal(size=1000)
    data += np.sin(np.linspace(0, 15*(2*np.pi), len(data)))

    data[400:500] += 1

    data  = metaarray.MetaArray(data, info=[{'name'   : 'Time', 
                                             'values' : np.linspace(0, len(data), len(data))
                                             },{}])

    return data

def make_fake_noisy_image():

    data  = np.random.normal(size=(100,100))
    data  = 25 * pg.gaussianFilter(data, (5,5))
    data += np.random.normal(size=(100,100))
    
    data[40:60, 40:60] += 15.0
    data[30:50, 30:50] += 15.0
    
    #data += np.sin(np.linspace(0, 100, 1000))
    #data = metaarray.MetaArray(data, info=[{'name': 'Time', 'values': np.linspace(0, 1.0, len(data))}, {}])

    return data

def make_random_image(shape, sigma=3):

    imageData = np.random.normal(size=shape)
    imageData[10:20, 30:40] += 2.
    if sigma > 0:
        imageData = pg.gaussianFilter(imageData, sigma)
    imageData += 0.1* np.random.normal(size=shape)

    return imageData


###############################
## GLOBAL FX
###############################

class CustomNodeTemplate(Node):

    """CustomNodeTemplate: short description

    This description will appear in the 
    flowchart design window when the user
    selects a node of this type.
    """

    nodeName = 'CustomNodeTemplate'     # will also be shown to user

    def __init__(self, name):           # every node must have a unique name 

        Node.__init__(self,
                      name,
                      terminals={'inputTerminal_1' : {'io': 'in' },
                                 'inputTerminal_2' : {'io': 'in' },
                                 'outputTerminal'  : {'io': 'out'}})     # required dict of I/O terminals
                                            
    def process(self, **kwds):  # required method
        # required args  : {"input_terminals" : "stuff"}
        # required return: {"output_terminals": "results"} 

        return {'outputTerminal': "result"}

    def ctrlWidget(self):       # optional method. 
        # must return QWidget (of QWidgets). 
        # displayed in flowchart control panel
        # Often, form-like structure: param_name <--> spinbox, checkbox, etc
        return "someQWidget"


class GaussianFilterNode(Node):
    nodeName = "GaussianFilterNode"

    def __init__(self, name):           # every node must have a unique name 

        Node.__init__(self,
                      name,
                      terminals={'In'  : {'io': 'in' },
                                 'Out' : {'io': 'out'}})     # required dict of I/O terminals
                                            
    def process(self, **kwds):  # required method
        # required args  : {"input_terminals" : "stuff"}
        # required return: {"output_terminals": "results"} 

        return {'outputTerminal': "result"}

    def ctrlWidget(self):       # optional method. 
        # must return QWidget (of QWidgets). 
        # displayed in flowchart control panel
        # Often, form-like structure: param_name <--> spinbox, checkbox, etc
        return "someQWidget"




class AnalysisFlowchart():

    def __init__(self, debug = False, data = None, kind = "1d"):

        app = QtGui.QApplication([])

        ###########
        ## Validate
        ###########

        kind_choices = ["1d", "2d"]
        assert kind in kind_choices, "'kind' arg can only be one of "+ str(kind_choices)

        is_1D = (kind == "1d")

        if data is None:
            if is_1D: data = make_fake_noisy_signal()
            else    : data = make_fake_noisy_image()

        if is_1D: 
            chart_key = "PlotWidget"
            p1        = pg.PlotWidget()
            p2        = pg.PlotWidget()
        else: 
            chart_key = "ImageView"
            p1        = pg.ImageView()
            p2        = pg.ImageView()

        ############
        ## Flowchart (Empty) (Defines Terminals)
        ############

        fc = Flowchart(terminals={
            'dataIn'  : {'io': 'in'},
            'dataOut' : {'io': 'out'}})

        fc.setInput(dataIn=data)

        ##########
        ## GUI 
        ##########

        ## Create main window with grid layout
        win = QtGui.QMainWindow()
        cw  = QtGui.QWidget()

        win.setWindowTitle('Jet Analysis Filter Pipeline')
        win.setCentralWidget(cw)

        ## Layout
        layout = QtGui.QGridLayout()
        cw.setLayout(layout)
        layout.addWidget(p1, 0, 1)
        layout.addWidget(p2, 1, 1)

        layout.addWidget(fc.widget(), 0, 0, 2, 1)  # control panel

        win.show()

        ############
        ## Attrs
        ############

        self.app    = app
        self.win    = win
        self.cw     = cw 
        self.fc     = fc 
        self.layout = layout
        self.update = pg.QtGui.QApplication.processEvents
        self.p1     = p1
        self.p2     = p2
        self.chart_key=chart_key

        ############
        ## Process
        ############

        self.make_node_plots()
        self.fc.connectTerminals(self.fc['dataIn'], self.p1_node['In'])  

    def make_node_plots(self):

        fc     = self.fc
        k      = self.chart_key
        p1, p2 = self.p1, self.p2

        p1_node = fc.createNode(k, pos=(0  , -150))
        p2_node = fc.createNode(k, pos=(150, -150))

        p1_node.setPlot(p1)
        p2_node.setPlot(p2)

        plotList = {'Top Plot'   : p1, 
                    'Bottom Plot': p2}

        p1_node.setPlotList(plotList)
        p2_node.setPlotList(plotList)       

        self.p1_node = p1_node
        self.p2_node = p2_node 

    def make_node(self, kind, defaults={"ctrl":"default"}): 
        
        node = self.fc.createNode(kind, pos=(0, 0))

        for c,d in defaults.items():
            node.ctrls[c].setValue(d)
        
        return node 

    def connect_nodes(self, n1, n2):

        self.fc.connectTerminals(n1["Out"], n2["In"])

    def connect_remaining_nodes(self): pass



class GaussianFilter(AnalysisFlowchart):
    
    def __init__(self):

        super().__init__(debug = False, kind='1d')

        self.node = self.make_node("GaussianFilter", defaults={"sigma":5})

        self.connect_remaining_nodes()
        self.update()

    def connect_remaining_nodes(self):

        fc   = self.fc
        node = self.node
        p1_node, p2_node = self.p1_node, self.p2_node

        fc.connectTerminals(fc['dataIn'], node['In'])

        fc.connectTerminals(node['Out'], p2_node['In'])
        fc.connectTerminals(node['Out'], fc['dataOut'])       

##########################################################################

if __name__ == '__main__':
    
    Gauss = GaussianFilter()

    # pg.exit()

    ## Start Qt event loop unless running in interactive mode or using pyside.
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        
        APP = QtGui.QApplication.instance()
        if APP is not None: 
            APP.exec_() 

    pg.exit()  # MUST! Avoids many pyqt(graph) seg. faults and bugs. ref: http://www.pyqtgraph.org/documentation/functions.html#pyqtgraph.exit
