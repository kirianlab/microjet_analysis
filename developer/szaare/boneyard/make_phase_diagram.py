import os, sys
import numpy as np 

import seaborn as sns
import pandas as pd 

import matplotlib
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D

from scipy.ndimage import gaussian_filter
from scipy.interpolate import griddata

from    libs.serializers.picklator          import save_pickle, load_pickle
from    libs.finders.file_finders           import find_files_by_extensions
########################################################################
##  PICKLE Params
########################################################################
NOZZLE_DATASET = "2017/20171228/44_10_03_SC30I"

repoDir                   = os.path.abspath(os.path.dirname(__file__))
analysisResultsPicklesDir = os.path.join(repoDir, "tempdata/microjets/", NOZZLE_DATASET, "results")
phaseDiagDataSavePath     = os.path.join(analysisResultsPicklesDir, "phase_diag_data.pkl")

########################################################################
##  PLOT Params
########################################################################

fig_title    = '{} Phase Diagram\nfor 3D Printed Nozzle\n'
gas_ax_label = 'Helium Gas Flow Rate (mg/min)\n'
liq_ax_label = 'Water Flow Rate (uL/min)\n'


params = {'font.size'           : 20,
          'font.family'         : "monospace",
          'font.serif'          : "Verdana",
          'font.variant'        : "small-caps",
          'figure.figsize'      : (15.0, 25.0),
          # 'figure.facecolor'  : 'k',
          # 'figure.titlesize'  : 50,
          'figure.titleweight'  : 'black',
          'axes.facecolor'      : 'white',
          'axes.titleweight'    : 'heavy',
          'axes.labelweight'    : 'bold',
          'axes.labelsize'      : 20,
          'grid.color'          : 'black',
          'grid.linewidth'      : 0.5,
          'legend.facecolor'    : 'w',
          'lines.linewidth'     : 10.,
          'lines.markersize'    : 15,
          'image.cmap'          : 'hot',
          # 'text.usetex'       : True,
          }

matplotlib.rcParams.update(params)
# for k in matplotlib.rcParams.keys():
#   if "weight" in k: print(k)
# sys.exit()

########################################################################
## Gather Data
########################################################################



def get_save_all_data(saveResults = False):
    
    data = []

    for pik in find_files_by_extensions(target_dir = analysisResultsPicklesDir, extensions=[".pkl"]):
        try:
            v = load_pickle(pik) 

            movie    = os.path.basename(v["moviePath"])
            gasflow  = float(v["HeliumMassFlowRate_mgPERmin"])
            liqflow  = float(v["LiqVolFlowRate_uLPERmin"])
            stability= float(v["fractionGoodFrames_jetStability_percent"])
            jitter   = float(v["jetDeviationAngsStdDev_jetJitter_degrees"])
            diameter = float(v["jetDiameter_um"])
            speed    = float(v["jetSpeed_mps"])

        except (KeyError,TypeError,AttributeError) as e:
            print(e, pik)
            continue

        entry = (gasflow, liqflow, stability, jitter, diameter, speed, movie)
        data.append(entry)
        print(entry)

    df = pd.DataFrame(data, columns=['gas', 'liq', 'stability', 'jitter', 'diam', 'speed', 'movie'])

    if saveResults:
        save_pickle(df, phaseDiagDataSavePath)
    

    return df, phaseDiagDataSavePath


def load_all_data():

    df, _ = get_save_all_data(saveResults = True)

    # all_data = dict(
    #   gas  = np.asarray([int(i[0]) for i in data]),
    #   liq  = np.asarray([int(i[1]) for i in data]),
    #   diam = np.asarray([int(i[4]) for i in data]),
    #   speed= np.asarray([int(i[5]) for i in data]),
    #   )
    
     
    # print("ASDASDSADADSADDSA")
    return df

def make_plot_arrays(allData = None):

    if allData is None:
        allData = load_all_data()

    shape = (50,25)     # max gas, max liq range for plot
    SPEED = np.zeros(shape = shape)
    DIAM  = np.zeros(shape = shape)

    gas, liq, speed, diam = [allData[i] for i in ['gas', 'liq', 'speed', 'diam']]
    
    for g,l,x in zip(gas,liq,speed):
        SPEED[g,l] = x

    for g,l,x in zip(gas,liq,diam):
        DIAM[g,l] = x

    return SPEED, DIAM

# print(DIAM[DIAM>0]); sys.exit()

########################################################################
## PLOT
########################################################################

def make_contour(DATA,title):

    COORDS=np.vstack([gas,liq]).transpose()
    
    N = 100
    x = np.linspace(0., shape[0], N)
    y = np.linspace(0., shape[1], N)
    X, Y = np.meshgrid(x, y)
    coords = np.array([X.flatten(), Y.flatten() ]).transpose()

    Z_cubic = griddata(COORDS, DATA, coords, method = "cubic").reshape([N, N])

    # plt.clf()
    konf = plt.contourf(X, Y, Z_cubic, 10 )  # cmap = plt.cm.hot
    
    cbar = plt.colorbar(konf, format="%d", shrink=0.7, aspect=10)  # ticks=np.arange(cbar_min, cbar_max+cbar_step, cbar_step) 
    cbar.ax.set_ylabel("\n"+title, weight="bold")
    cbar.ax.tick_params(labelsize=14) 
    
    plt.contour(X, Y, Z_cubic, 10, colors = "k", linewidths=1)
    plt.plot(gas, liq, "og", label = "Empirical Data")

    plt.legend()
    plt.title(fig_title.format(title))
    plt.xlabel(gas_ax_label)
    plt.ylabel(liq_ax_label)
    plt.grid()
    # plt.savefig('interpolated.png', dpi=100)
    plt.show()  

def make_plot(DATA, title):
    # plt.clf()
    im   = plt.imshow(DATA.T, origin="lower", interpolation="catrom")  # cmap=plt.cm.hot
    
    cbar = plt.colorbar(im, format="%d", shrink=0.7, aspect=10)  # ticks=np.arange(cbar_min, cbar_max+cbar_step, cbar_step) 
    cbar.ax.set_ylabel("\n"+title, weight="bold")
    cbar.ax.tick_params(labelsize=14) 

    plt.title(fig_title.format(title))
    plt.xlabel(gas_ax_label)
    plt.ylabel(liq_ax_label)
    plt.grid()
    plt.show()

def make_speed_gasflow_plot(dFrame, title=""):
    
    # plt.clf()
    # colors = {'D':'red', 'E':'blue', 'F':'green', 'G':'black'}
    # ccc = np.random.rand(3,1)
    unique_liq = np.unique(dFrame['liq'])


    # fig, ax = plt.subplots()
    # ax.scatter(dFrame['gas'], dFrame['speed'], c=dFrame['liq'], s = 200, label="")
    
    # ax = sns.lmplot( x="gas", y="speed", data=dFrame, fit_reg=False, hue='liq', legend=True)
    ax = sns.scatterplot( x="gas", y="speed", data=dFrame, hue='liq', size="liq", sizes=(400,401),legend='full', palette=sns.color_palette("dark",len(unique_liq)))    #  style='liq'
    # ax.legend()
    ax.grid(True)

    for line in range(0,dFrame.shape[0]):
         X = dFrame.gas[line]+0.5
         Y = dFrame.speed[line]
         L = os.path.splitext(dFrame.movie[line])[0]
         ax.text(X, Y, L, horizontalalignment='left', size=8, color='black', weight='light')

    plt.legend(loc='upper left', title='Water Vol Flow \nRate (uL/min)')
    plt.title(title+"\n")
    plt.xlabel(gas_ax_label)
    plt.ylabel("Jet Speed (m/s)")
    plt.show()  


def make_all_plots(dFrame):
    print(NOZZLE_DATASET[5:])

    # make_contour(speed, "Jet Speed (m/s)")
    # make_plot(SPEED, "Jet Speed (m/s)")
    make_speed_gasflow_plot(dFrame=dFrame,  title = NOZZLE_DATASET[5:])
    # make_contour(diam, "Jet Diameter (micron)")
    # make_plot(DIAM, "Jet Diameter (micron)")

def main():
    df = load_all_data()
    make_all_plots(df)  

if __name__ == '__main__':
    main()


"""
    ## Make the plot
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_trisurf(gas, liq, speed, cmap=plt.cm.viridis, linewidth=0.2)
    # plt.show()

    ## to Add a color bar which maps values to colors.
    # surf=ax.plot_trisurf(gas, liq, speed, cmap=plt.cm.viridis, linewidth=0.2)
    # surf=ax.plot_trisurf(gas, liq, speed, cmap=plt.cm.jet, linewidth=0.2)
    # fig.colorbar( surf, shrink=0.5, aspect=5)
    # plt.show()
     
    ## Rotate it
    # ax.view_init(45, 45)
    # plt.show()

    # f, (ax1, ax2, ax3, ax4) = plt.subplots(2,2, sharex="none", sharey='none')
    # ax1.imshow(aa)
    # ax2.imshow(bb)
    # ax3.imshow(op_fill)
    # ax1.set_aspect('auto')
    # ax2.set_aspect('auto')
    # ax3.set_aspect('auto')
    # plt.show()
"""