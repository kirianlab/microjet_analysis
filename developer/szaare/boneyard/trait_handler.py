import os, sys

from traits.api     import HasTraits                # "Type" def    (extends any python obj attrs w/ utilities: standardize (initialize, validate, defer), notify, visualize, document)
from traits.api     import Str, Float, Range, Bool  # Validation    (also: Array, Enum, Range, Event, Dict, List, Color, Set, Expression, Code, Callable, Type, Tuple, etc.)
from traits.api     import Instance, DelegatesTo    # Deferral
from traits.api     import Event
from traitsui.api   import View

from scipy.ndimage  import gaussian_filter, median_filter, percentile_filter, sobel
from scipy.ndimage  import convolve1d, correlate1d
from scipy          import misc

IMAGE_PATH = os.path.expanduser("~/Desktop/traits/old_faithful_original_1.png")

image = misc.imread(IMAGE_PATH)

class Analysis(HasTraits):

    ## Class Attrs
    ##      * 'self' is defined only in __init__ and onward
    ##      * Don't use self here before __init__
    ##      * A class does not even officially need an __init__

    analysis_traits = ( 'movie_path',
                        'gaussian_sigma',
                        'gamma_range',
                        'useFilter_gauss')

    traits_view     = View(*analysis_traits,
                           title     = 'Analysis Params',
                           resizable = True)   

    movie_path      = Str(desc = "Movie Path!!!")
    gaussian_sigma  = Float(0)
    gamma_range     = Range(0,2.)
    useFilter_gauss = Bool(False)

    def __init__(self, 
                 movie_path      = None,
                 gaussian_sigma  = None,
                 gamma           = None,
                 useFilter_gauss = None): 

        if movie_path       is not None: self.movie_path     = movie_path
        if gaussian_sigma   is not None: self.gaussian_sigma = gaussian_sigma
        if gamma            is not None: self.gamma_range    = gamma
        if useFilter_gauss  is not None: self.useFilter_gauss= useFilter_gauss

    def __repr__(self):

        temp = "{: >15} : {: <11}"
        for k,v in self.__dict__.items():
            print(temp.format(k,v))
    

class AnalysisState(HasTraits):
    """Keeps track of Analysis State"""

    ## Here, we have Class, Deferred, State, and Control Attrs!

    ## Class    Attrs
    gamma_min   = 0.5
    gamma_max   = 1.5

    ## Deferred Attrs
    analysis    = Instance(Analysis, ())
    gamma       = DelegatesTo('analysis')

    ## State    Attrs
    gamma_range = Range('gamma', 12)
    print(gamma_range)
    # gamma_range = 19

    # def __repr__(self):
        # print(type(self.gamma_range))
        # print(self.gamma_range)
        # temp = "{: >15} : {: <11}"
        # for k,v in self.__dict__.items():
        #     print(temp.format(k,v))
############################################################
def main_analysis():

    analysis = Analysis(movie_path      = IMAGE_PATH,
                        gaussian_sigma  = 3.5,
                        gamma           = 1.3,
                        useFilter_gauss = True)
    # analysis.edit_traits()
    # analysis.configure_traits()

    return analysis

def main_analysis_state():

    analysis = main_analysis()
    state    = AnalysisState(analysis=analysis)
    state.__repr__()
    return state

if __name__ == '__main__':
    
    # analysis = main_analysis()
    state    = main_analysis_state()


