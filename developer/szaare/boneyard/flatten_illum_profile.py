import sys, os
import numpy as np
import pyqtgraph as pg
from skimage import io
from numba import jit


from libs.viewers import analysis_panel as AnalysisPanel

"""
    Process: for a given movie path
    1. flatten illumination profile
        1.1.    read movie
        1.2.    extract even/odd frames separately
        1.3.    for each pixel, bkg value is its value
                'most of the time', i.e. n-th percentile 
                value across all frames in 
                even/odd substacks separately
        1.4.    'properly' divide each pixel by its bkg value
        1.5.    subtract all-pixel median from each pixel

"""

#########################################################################

PERC_BKG    = 80
PERC_THRESH = 99.5
INTERACTIVE = False

##########################################################################
## Viewer / Probe
##########################################################################

class ImWin(pg.ImageView):
    def __init__(self, *args, **kargs):
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(800, 600)
        if 'title' in kargs:
            self.win.setWindowTitle(kargs['title'])
            del kargs['title']
        pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
        self.setImage(*args, **kargs)
        self.ui.histogram.plot.setLogMode(False, True)
        self.win.setCentralWidget(self)
        for m in ['resize']:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient('flame')       # bipolar
        self.win.show()

def probe(*images, exit=False):

    app = pg.mkQApp()
    print("\n>>>>> Debug Probe <<<<<\n")
    print("Interactive:", INTERACTIVE)
    
    imwins = {}
    
    for i,image in enumerate(images):
        print("Image Shape:", image.shape)
        # image = image[:100,:,100:800]
        # print(image)
        imwins[i] = ImWin(image)
        # pg.QtGui.QApplication.instance().exec_()
    
    app.exit(app.exec_())
    if exit: pg.exit()

    if len(imwins) == 1: imwins = list(imwins.values())[0]
    
    return imwins 

##########################################################################
## Analysis
##########################################################################

@jit(nopython=True)
def shear(matrix):
    arr = np.empty_like(matrix)
    fast_scan, slow_scan = matrix.shape
    for i in range(slow_scan):
        for j in range(fast_scan):
            arr[i, j] = matrix[i, (j + i) % fast_scan]
    return arr


def get_stack_roi(stack):       ## NOT in use; replaced by AnalysisPanel.main()
    """
    im      = ImWin(stack, title='Choose ROI and threshold')
    thresh  = np.percentile(stack.ravel(), PERC_THRESH)
    roi     = pg.ROI(pos=(0, stack.shape[2]*1/4), size=(stack.shape[1]*3/4, stack.shape[2]*3/4))

    im.setLevels(thresh, max(thresh*1.1, im.getLevels()[1]))
    roi.addScaleHandle([1, 1], [0, 0])
    im.getView().addItem(roi)

    if INTERACTIVE: pg.QtGui.QApplication.instance().exec_()

    thresh = im.getLevels()[0]
    roi_slices , roi_transform = roi.getArraySlice( stack[1,:,:], 
                                                    im.getImageItem(), 
                                                    axes = (0,1), 
                                                    returnSlice = True)
    (rs, cs) = roi_slices
    c = (rs.start, rs.stop, cs.start, cs.stop)
    # c = (max(int(roi.pos()[0]), 0), min(int(roi.pos()[0]+roi.size()[0]), stack.shape[1]-1),
    #      max(int(roi.pos()[1]), 0), min(int(roi.pos()[1]+roi.size()[1]), stack.shape[2]-1))

    print("Bkg percentile:", PERC_BKG)
    print("ROI thresh    :", thresh)
    print("ROI coords    :", c)

    return im, thresh, c
    """
    pass


def flatten_illum_profile(stack, axis, Percentile):

    evens   = stack[0::2, :, :]
    odds    = stack[1::2, :, :]

    back0   = np.percentile(evens, Percentile, axis=axis)
    back1   = np.percentile(odds , Percentile, axis=axis)

    stack[0::2, :, :] = np.divide(evens, back0, out=np.zeros_like(evens), where=back0 != 0)
    stack[1::2, :, :] = np.divide(odds , back1, out=np.zeros_like(odds) , where=back1 != 0)

    stack = -(stack - np.median(stack.ravel()))

    return stack

##########################################################################

def main(tifPath):

    title = os.path.join(*tifPath.split(os.sep)[-3:])
    print(title)

    stack = AnalysisPanel.get_stack(moviepath   = tifPath, 
                                    rotateFrame = "flip_LeftRight", 
                                    Transpose   = (0, 2, 1), 
                                    nFrames     = 20,
                                    frameSlicer = None) # (slice(476, 560, None),slice(62, 989, None))

    stack = flatten_illum_profile(stack=stack, axis=0, Percentile=PERC_BKG)
    nframes, nrows, ncols = stack.shape
    
    ROI, roi_params = AnalysisPanel.main(stack=stack, interactive=INTERACTIVE, title=title)
    roi_slices      = roi_params['roi_slices'] # (slice(231, 922, None), slice(21, 63, None)) # 
    roi_thresh      = roi_params['lut_thresh']

    ## Jet Mask
    x, y    = np.meshgrid(np.arange(nrows), np.arange(ncols), indexing='ij')
    ccmat   = np.zeros((nrows, nrows))
    mask    = np.zeros((nrows, ncols))
    mask[roi_slices] = 1
    prev1D  = 0 

    for i in range(nframes):
        
        wjet        = np.where((stack[i, :, :]*mask).flat > roi_thresh)[0]
        xj, yj      = x.flat[wjet], y.flat[wjet]
        slope, yint = np.polyfit(xj, yj, 1)
        
        xf  = np.arange(nrows)
        yf  = yint + slope*xf
        
        ang = np.sign(slope) * np.arctan(slope)
        
        u1  = np.array([ np.cos(ang), np.sin(ang)])
        u2  = np.array([-np.sin(ang), np.cos(ang)])
        
        v   = np.vstack([xj, yj - yint]).T
        v1  = v.dot(u1)
        v2  = v.dot(u2)
        
        jet1D, bin_edges = np.histogram(v1, bins=nrows, range=[0, nrows])
        
        if i > 0:
            op = np.outer(jet1D, prev1D)
            if i % 2 == 1: ccmat += op
            else:          ccmat -= op

        prev1D = jet1D

    # prb = probe(ccmat, exit=False)
    shearmat = shear(ccmat)
    #shearmat = np.cumsum(shearmat, axis=0)/np.outer(np.arange(1,shearmat.shape[0]+1), np.ones(shearmat.shape[1]))
    #shearmat = np.abs(shearmat)

    ROI_shear, roi_params_shear = AnalysisPanel.main(np.expand_dims(shearmat,axis=0), interactive = INTERACTIVE, title="Stella")

    ccproj  = np.mean(shearmat[roi_params_shear["roi_slices"]], axis=0)
    ccproj *= np.sign(np.max(ccproj)+np.min(ccproj))
    wpeak   = np.where(ccproj == np.max(ccproj))[0]
    print(wpeak)

    np.savez(   save_file, 
                roi_slices  = roi_params['roi_slices'], 
                roi_thresh  = roi_params['lut_thresh'], 
                ccmat       = ccmat, 
                matproj     = ccproj, 
                bkg_perctl  = PERC_BKG, 
                wpeak       = wpeak,
                shearMatrix_roi_slices = roi_params_shear['roi_slices'], 
                )
    print(save_file)

    return ROI


if __name__ == "__main__":
        
    app = pg.mkQApp()

    if 'interactive' in sys.argv: INTERACTIVE = True
    if len(sys.argv) == 1: 
        print("\n","*"*20, "\n")
        print("Required: tifpath; Optional: 'interactive'\n")
        pg.exit() 

    #file_name  = [s for s in sys.argv if 'rawdata' in s][0] 
    file_name   = os.path.abspath('rawdata/microjets/2017/20170906/44_08_01_SC30I/0026.tif')
    # file_name = os.path.expanduser('~/Desktop/work/movies/stella/old_faithful_substack_1-100.tif')
    save_file   = file_name.replace('rawdata', 'tempdata').replace('.tif', '.tif.brightfield_v01.npz')

    ROI = main(tifPath=file_name)
    pg.exit()   

