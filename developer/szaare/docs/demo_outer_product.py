import numpy as np 
import matplotlib.pyplot as plt 

n = 70
m = 3
nDrops = int(0.1*n)
dropShift = int(0.1*n)
a = np.zeros(n)
b = np.zeros(n)

aa = np.zeros((m,n))
bb = np.zeros((m,n))

# droplet_pos = np.array(np.random.choice(int(0.8*n), nDrops, replace=False))
# droplet_pos.sort()
droplet_pos = 0.01 * n * np.array([10, 20, 24, 45, 60,64, 80,82])
droplet_pos = droplet_pos.astype(np.int)

a[droplet_pos]   = int(n/2)
b[droplet_pos+dropShift] = int(n/2)

aa[int(m/2),:] = a
bb[int(m/2),:] = b

op = np.multiply.outer(a,b)
op_fill = op
np.fill_diagonal(op_fill, -int(n/2)**2)


f, (ax1, ax2, ax3) = plt.subplots(3,1, sharex="col", sharey='none')
ax1.imshow(aa)
ax2.imshow(bb)
ax3.imshow(op_fill)
ax1.set_aspect('auto')
ax2.set_aspect('auto')
ax3.set_aspect('auto')
plt.show()	
