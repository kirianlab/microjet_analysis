#!/bin/bash

########################################
# Customized only for Austin's data set
########################################

DATADIR="../rawdata/microjets"
RELEVANT_DATA_PATH="./relevant_data.txt"
RELEVANT_TIFS_PATH="./relevant_tifs.txt"
ANALYZED_DATA_PATH="./list.txt"

########################################
# Get Movies/Logs from xfel1 server 
########################################

# scp -r raklab@xfel1:/data/rawdata/microjets/2019/20190501/Austin  <destination>
# scp -r raklab@xfel1:/data/rawdata/microjets/2019/logs             <destination>

########################################
# Data Log: Get header and relevant rows
########################################

# head -n 1 $(find ${DATADIR}/2019/logs/ -type f -iname 20190501*_plot_data.txt | head -n 1) > ${RELEVANT_DATA_PATH}
# find ${DATADIR}/2019/logs -type f -iname 20190501*_plot_data.txt -exec grep -ih "sahba*" {} \; >> ${RELEVANT_DATA_PATH}

########################################
# Get run nums from file names. 
# Just a template for my learning 
########################################

# for i in $(find ${DATADIR}/2019/20190501/Austin/ -type f -iname *.tif -exec basename {} \;); do 
#     echo $i | cut -d'_' -f3 | cut -f 1 -d '.';
# done  | sort -n

########################################
# Main Analysis: Get Droplet Translation
########################################

# for i in $(cat ${RELEVANT_TIFS_PATH}); do 
#     moviepath="${DATADIR}/$i"
#     echo $moviepath
#     python -W ignore ../rick/brightfield2a.py $1 --thresh_percentile=99.2 --nozzle_pos=2000 --drop_pos=2000 $moviepath
# done


########################################
# Post Analysis: Get Jet Speed
########################################

find ../tempdata/ -iname '*.npz' | grep oil | sort > ${ANALYZED_DATA_PATH}
python ../pixels_to_speed.py "${ANALYZED_DATA_PATH}" "${RELEVANT_DATA_PATH}"
