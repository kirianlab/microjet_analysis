#!/bin/bash

#-------------------------------------------------------------------
# Prototype for real-time microjet analysis 
#-------------------------------------------------------------------
# Watches a dir for new fastcam tif movies 
# 	CMD Args
# 		$1 			tif path
# 		$2,3,... 	args to pass to rick/brightfield2a.py
#-------------------------------------------------------------------


#######################################################
# Commandline Args
#######################################################

TIF_PATH=$1
shift
ANALYSIS_OPTIONS="$@"

#######################################################
# Params
#######################################################

RAWDATA="../rawdata/microjets"
TEMPDATA="../tempdata/microjets"
FINAL_RESULTS="./results/jet_speed_${NOZZLE_TYPE}.csv"
PRE_ANALYSIS_REPORT="./databases/pre_analysis_report.txt"

#######################################################
# Params: Temp Paths 
#######################################################

ANALYZED_TIFS="./temp/analyzed_tifs_${NOZZLE_TYPE}.txt"
RELEVANT_DATA="./temp/relevant_data_${NOZZLE_TYPE}.csv"
DAQ_LOGS="./temp/daq_logs.txt"
FAILED_TIFS="./temp/failed_tifs_${NOZZLE_TYPE}.txt"
FAILED_TIFS_STDERR="./temp/failed_tifs_${NOZZLE_TYPE}_stderr.txt"


#######################################################
## Main Analysis
#######################################################

echo "-------------------------------------------------"
echo "Main Analysis: Droplet Translation" 
echo "-------------------------------------------------"

echo "Handle Analysis Params"

true > ${FAILED_TIFS}
true > ${FAILED_TIFS_STDERR}
SEP="\n"$(printf '=%.0s' {1..70})"\n"

printf $SEP ; printf "%s\n" "Analyze: ${TIF_PATH}" "Options: ${ANALYSIS_OPTIONS}" ; printf $SEP
printf "${SEP}${i}${SEP}\n" | tee -a ${FAILED_TIFS_STDERR}

python -W ignore ../rick/brightfield2a.py ${ANALYSIS_OPTIONS} ${TIF_PATH} 2>> ${FAILED_TIFS_STDERR}

if [ $? -ne 0 ] ; then printf ${i}"\n" &>> ${FAILED_TIFS}; fi

#######################################################
## Post Analysis
#######################################################
exit

## TODO: start here

echo "-------------------------------------------------"
echo "Post-Analysis: Droplet Translation --> Jet Speed"
echo "-------------------------------------------------"

echo "Find Analysis Results (.npz) for " ${TIF_PATH}
find ${TEMPDATA} -iname '*.tif.brightfield2.npz' | grep ${TIF_PATH} | sort > ${ANALYZED_TIFS}

echo "Make flow rate data log"
find ${RAWDATA} -iname '*_plot_data.txt' | sort > ${DAQ_LOGS}
python ./modules/excel_parser.py  ${ANALYZED_TIFS} ${DAQ_LOGS} ${RELEVANT_DATA}

echo "Find Jet Speed"
python ./modules/pixel_to_speed.py  ${ANALYZED_TIFS}  ${IGNORED_RUNS} ${RELEVANT_DATA} ${FINAL_RESULTS} ${TIF_PATH}

if [ "${SAVE_OUTER_PRODUCTS}" = true ] ; then
    echo "Save Outer Products"
    python ./modules/plot_outer_prods.py  ${ANALYZED_TIFS}
fi

echo "Cleanup"
rm ${DAQ_LOGS}
rm ${ANALYZED_TIFS}

echo "-------------------------------------------------"
echo "The End" ${TIF_PATH}
echo "-------------------------------------------------"
