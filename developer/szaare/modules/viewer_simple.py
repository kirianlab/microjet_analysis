import pyqtgraph as pg

class ImWin(pg.ImageView):
    def __init__(self, *args, **kargs):
        self.app = pg.mkQApp()
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(1000, 800)
        if 'title' in kargs:
            self.win.setWindowTitle(kargs['title'])
            del kargs['title']
        pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
        self.setImage(*args, **kargs)
        self.win.setCentralWidget(self)
        for m in ['resize']:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient('flame')
        self.win.show()
    def add_plot(self, *args, **kwargs):
        return self.getView().plot(*args, **kwargs)
    def add_line(self, *args, **kwargs):
        p = kwargs['position']
        del kwargs['position']
        line = pg.InfiniteLine(*args, **kwargs)
        line.setPos([p, p])
        self.getView().addItem(line)
        return line
    def keyPressEvent(self, ev):
        if ev.key() == 32:  # spacebar
            self.app.quit()
        if ev.key() == 81:  # q button
            print('\nQuit\n')
            sys.exit()
    def set_mask(self, mask, color=None):
        d = mask
        if color is None:
            color = (255, 255, 255, 20)
        mask_rgba = np.zeros((d.shape[0], d.shape[1], 4))
        r = np.zeros_like(d)
        r[d == 0] = color[0]
        g = np.zeros_like(d)
        g[d == 0] = color[1]
        b = np.zeros_like(d)
        b[d == 0] = color[2]
        t = np.zeros_like(d)
        t[d == 0] = color[3]
        mask_rgba[:, :, 0] = r
        mask_rgba[:, :, 1] = g
        mask_rgba[:, :, 2] = b
        mask_rgba[:, :, 3] = t
        im = pg.ImageItem(mask_rgba)
        self.getView().addItem(im)