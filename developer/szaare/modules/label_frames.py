"""
This script will 
    open a tif file whose path is sys.argv[1]
    user can keyboard numbers (0-9) to categorize frames

Click "save": the script will 
    extract and save each frame category separately
    save an array of frame labels 
"""


###########################################################
import sys, os
import numpy as np
from skimage import io
import pyqtgraph as pg
import pyqtgraph.exporters
import argparse 
from PIL import Image
import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


###########################################################
## Params
###########################################################

SAVE_DIR = os.path.expanduser("~/Desktop/labeled_frames")
NFRAMES  = 1000


###########################################################

def save_labeled_frames(labels, stack, savepath):        
    for L in np.unique(labels):     

        savepath_L = savepath + "__%d.tif"%L        
        frame_nums = np.where(labels==L)[0]
        pg.image(stack[frame_nums], title="%d"%int(L))

        try:
            tif_stack = [Image.fromarray(f, mode="I") for f in stack[frame_nums]]     # F = float32, I = int32, L= 8 bit int black/white
            tif_stack[0].save(savepath_L, save_all=True, append_images=tif_stack[1:])          
            status = "Success"

        except Exception as e:
            status = "Failure " + str(type(e))
        
        finally:
            print("  ",  status, os.path.basename(savepath_L))
    # return status
            
class ImWin(pg.ImageView):

    def __init__(self, *args, **kargs):
        self.app = pg.mkQApp()
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(1000, 800)
        if 'title' in kargs:
            self.win.setWindowTitle(kargs['title'])
            del kargs['title']
        pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
        self.setImage(*args, **kargs)
        self.win.setCentralWidget(self)
        for m in ['resize']:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient('flame')
        self.win.show()

        save_btn = pg.QtGui.QPushButton("Save")
        save_btn.clicked.connect(self.save_frames)
        self.win.centralWidget().layout().addWidget(save_btn)

        self.frame_labels = None
        if len(self.image.shape) == 3:
            self.frame_labels = np.zeros(self.image.shape[0],dtype=int)

    def keyPressEvent(self, ev):
        super().keyPressEvent(ev)

        index  = self.currentIndex
        numpad = ev.key()-48    # maps to number pad

        if numpad in range(10) and (self.frame_labels is not None):
            self.frame_labels[index] = numpad
            print("label: {} , frame: {:3}".format(numpad, index))

    def save_frames(self):
        # labels = self.frame_labels
        # if (labels is not None) and (1 < len(np.unique(labels))):
        #     save_labeled_frames(labels = labels, stack = self.image)
        pass

def get_stack(tifpath, nframes):

    short_tif_path = os.path.join(*tifpath.split(os.sep)[-6:])
    print(short_tif_path)

    roi_path = tifpath.replace("rawdata","tempdata")
    if os.path.isfile(roi_path): tifpath = roi_path

    try: im = Image.open(tifpath)
    except Exception as e:
        print("  *** %s: %s"%(e, tifpath))

    sx,sy   = im.size
    n_frames= min(nframes, im.n_frames) 
    stack   = []   

    for n in range(n_frames):
        
        im.seek(n)
        frame  = np.array(im.getdata()).reshape((sy,sx)).astype(np.single)
        frame *= -1
        frame -= np.median(frame)
        stack.append(frame)

    stack = np.transpose(stack, (0,2,1))
    
    return stack

def get_labels(stack):

    # app = pg.mkQApp()
    win = ImWin(stack)
    win.app.exec_()     # pg.QtGui.QApplication.instance().exec_()

    labels = win.frame_labels

    return labels

def save_labels(labels, tifpath):

    if (labels is not None) and (1 < len(np.unique(labels))):

        labels      = labels.astype(np.int)
        rel_dir     = os.path.dirname(tifpath).split(os.sep)[-3:]
        rel_dir     = os.path.join(*rel_dir)   
        savedir     = os.path.expanduser(os.path.join(SAVE_DIR, rel_dir))
        filename    = os.path.basename(tifpath).split(".")[0]
        savepath    = os.path.join(savedir, filename)
        labels_path = os.path.join(savedir, "frame_labels.txt")

        os.makedirs(savedir, exist_ok=True)
        print("\nResults:\n")

        with open(labels_path, "a+") as f: 
            f.write(os.path.basename(tifpath) + "\n")
            f.write(",".join([str(L) for L in labels]) + "\n")
       
            for L in np.unique(labels):     
                Ls = np.zeros_like(labels, dtype=np.int)
                Ls[labels==L] = int(L)
                f.write(",".join([str(L) for L in Ls]) + "\n")

    return savepath
    
if __name__ == '__main__':
                  
    rawdata = "/home/szaare/mine/work/repos/microjet-analysis/rawdata/microjets"
    austin_runs_labels = {
        "/2019/20190501/Austin/oil18_buff03_39_C001H001S0001.tif" : np.array([2,3,3,1,1,2,2,3,3,4,4,3,3,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3]),
        "/2019/20190501/Austin/oil19_buff02_43_C001H001S0001.tif" : np.array([3,3,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,1,1,2,2,3,3,1,1,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,3,3,3,3,3,3,1,1,2,2,3,3,2,2,3,3,4,4,2,2,3,3,4,4,2,2,3,3,4,4,3]),
        "/2019/20190501/Austin/oil20_buff01_47_C001H001S0001.tif" : np.array([2,4,4,2,2,3,3,2,2,3,3,4,4,2,2,3,3,2,2,3,3,4,4,2,2,3,3,1,1,2,2,4,4,2,2,3,3,1,1,2,2,4,4,2,2,3,3,3,3,2,2,3,3,2,2,3,3,4,4,2,2,3,3,2,2,3,3,4,4,2,2,3,3,1,1,2,2,4,4,2,2,3,3,1,1,2,2,4,4,2,2,3,3,1,1,2,2,4,4,2]),
        }
       
    for tif_path,labels in austin_runs_labels.items():
        tif_path = rawdata + tif_path
        # tif_path = sys.argv[1]
        stack    = get_stack(tif_path, NFRAMES)
        # labels   = get_labels(stack)
        savepath = save_labels(labels, tif_path)
        stack    = np.transpose(stack, (0,2,1)).astype(np.int32)  # int32 in prep for PIL.Image.fromarray(mode="I")
        
        save_labeled_frames(labels=labels, stack=stack, savepath=savepath)
    pg.QtGui.QApplication.instance().exec_()
    pg.exit()