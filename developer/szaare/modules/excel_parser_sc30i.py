
import  os, sys, time, collections
from datetime import datetime
import numpy as np
import pandas as pd

########################################################################
## DISCLAIMER
########################################################################

# This script is only for SC30I analysis.
# It converts 'old style' Excel data log (not colelcted by Sahba) to a Pandas dataframe.
# Given the specificity of this task, this script must use global variables for col names.

########################################################################
## CLI Args
########################################################################

tif_paths_file      = sys.argv[1]  # sc30i_analysis/data/relevant_tifs.csv
nozzle_records_path = sys.argv[2]  # sc30i_analysis/data/Nozzle_Records129.csv
save_path           = sys.argv[3]  # os.path.expanduser("~/Desktop/sc30i_relevant_data.xlsx")
    
########################################################################

def convert_timestamp(timestamp, old_format ="%Y%m%d" , new_format = "%m/%d/%Y"):

    old = datetime.strptime(timestamp, old_format)
    new = datetime.strftime(old,       new_format)

    return new  


def sc30i_excel_to_pd_dataframe():

    print("Cleaning up ", nozzle_records_path,"\n")

    RELEVANT_EXCEL_COLS = [ "Folder (Nozzle) Name", "Date", "Run", 
                            "He flow rate (mg/min)", "H2O volume flow rate (uL/min)", 
                            "Fastcam Zoom","Size of 1 Pixel (um)", "Sample"]
    RELEVANT_TIFF_COLS  = [ "nozzle", "date", "run", "gas", "liq", "zoom", "pixelSize", "sample"]
    COL_MAP             = dict(zip(RELEVANT_EXCEL_COLS, RELEVANT_TIFF_COLS))
    
    df = pd.read_csv(nozzle_records_path)
    df = df[RELEVANT_EXCEL_COLS]
    df.rename(columns = COL_MAP, inplace = True)
    df["nozzle"].replace("_","", inplace=True, regex=True)
    
    print("\tFix NaN")

    df.dropna(subset=["nozzle", "date",  "run", "gas", "zoom"]  , inplace=True)     # discard rows with NaN in important cols
    df.drop(df[df['nozzle'].map(len) > 13].index                , inplace=True)     # discard rows whose nozzle name is longer  than 15 chars!
    df.drop(df[df['nozzle'].map(len) <  8].index                , inplace=True)     # discard rows whose nozzle name is shorter than  8 chars!
   
    # fixed   = "........ Fixed   : {: >10}"
    # deleted = "........ Deleted : {: >10}"

    print("\tFix Run Nums")
    for r in df["run"].unique():
        try:    df["run"].replace(r, int(r), inplace=True)
        except: df.drop(df[df['run'] == r].index, inplace=True)

    print("\tFix Dates")
    for r in df["date"].unique():
        try:    df["date"].replace(r, convert_timestamp(str(r),"%m/%d/%Y", "%Y%m%d"), inplace=True)
        except: df.drop(df[df['date'] == r].index, inplace=True)
    
    print("\tFix Liq Flows ")
    for r in df["liq"].unique():
        try:    df["liq"].replace(r, float(r), inplace=True)
        except: df.drop(df[df['liq'] == r].index, inplace=True)

    # df = df.set_index(["nozzle","date", "run"])    

    return df

############################################################################
## MAIN
############################################################################

df     = sc30i_excel_to_pd_dataframe()
sub_df = pd.DataFrame(columns=df.columns.tolist())

with open(tif_paths_file, "r") as f:
    tif_paths = [l.strip() for l in f.readlines()]

for p in tif_paths:
    p = p.replace("_roi", "")
    p = p.replace(".npy.brightfield2.npz","")
    p = p.replace(".tif.brightfield2.npz","")

    year, date, nozzle, run = p.split(os.sep)[-4:]
    nozzlename  = nozzle.replace("_","")
    runnum      = int(run.replace("imp","").replace("_",""))
    selection   = df.loc[ (df['nozzle']   == nozzlename)  & 
                          (df['date']     == date)        &
                          (df['run']      == runnum)]

    sub_df = sub_df.append(selection, sort=False)

sub_df = sub_df.set_index(["nozzle"])
sub_df.dropna(subset=["gas"], inplace=True)
sub_df.to_csv(save_path)

print()
print("\tInput  Log: ", nozzle_records_path)
print("\tOutput Log: ", save_path)



