
"""
    Find plot_data.txt DAQ logs collected on the same date
    as a list of paths to tif movies 
"""

import  os, sys, time, collections
from datetime import datetime
import numpy as np
import pandas as pd

########################################################################
## DISCLAIMER
########################################################################

# This script is only for SC30I analysis.
# It converts 'old style' Excel data log (not colelcted by Sahba) to a Pandas dataframe.
# Given the specificity of this task, this script must use global variables for col names.

########################################################################
## CLI Args
########################################################################

tif_paths_file      = sys.argv[1]  # txt file w/ paths to all *.npz.brightfield2a.py analyzed tifs
daq_logs_paths_file = sys.argv[2]  # txt file w/ paths to all DAQ's *_plot_data.txt logs ever! 
save_path           = sys.argv[3]  # txt file w/ paths to RELEVANET DAQ's *_plot_data.txt logs 

############################################################################
## MAIN
############################################################################

with open(tif_paths_file, "r") as f:
    tif_paths = [os.path.normpath(l.strip()) for l in f.readlines()]

with open(daq_logs_paths_file, "r") as f:
    daq_logs_paths = [os.path.normpath(l.strip()) for l in f.readlines()]

print("  Find unique tif dates")
unique_tif_dates = set(p.split(os.path.sep)[-3] for p in tif_paths)

## Keep only those DAQ plot_data.txt logs collected on dates with a tif movie of interest
print("  Find relevant daq plot data logs")
relevant_daq_logs = set(p for p in daq_logs_paths if os.path.basename(p)[:8] in unique_tif_dates)
relevant_daq_logs = sorted(list(relevant_daq_logs))
for p in relevant_daq_logs: print("    ", p)

DAQ_COLS = [ "NozzleID", "date", "RunVidNum", "HeliumMassFlowRate_mgPERmin", "LiqVolFlowRate_uLPERmin", "Zoom", "PixelSize_um", 'FastcamIFP_ns', 'Medium', 'JetStability', 'JetJitter', 'JetDeviation', 'Bubbles', 'Comments']
COL_MAP  = ['nozzle', 'date', 'run', 'gas', 'liq', 'zoom', 'pixelSize', 'ifp', 'medium', 'jetstability', 'jetjitter', 'jetdeviation', 'bubbles', 'comments']
COLNAME_MAP       = dict(zip(DAQ_COLS, COL_MAP))

## Combine all relevant DAQ plot_data.txt logs into one dataframe
df = pd.read_csv(relevant_daq_logs[0])[DAQ_COLS]
for p in relevant_daq_logs[1:]:
    df = df.append(pd.read_csv(p)[DAQ_COLS],sort=False)

df.rename(columns = COLNAME_MAP, inplace = True)
df["nozzle"].replace("_","", inplace=True, regex=True)
df = df.set_index(["nozzle"])
df.to_csv(save_path)

print("  Input  Log: ", daq_logs_paths_file)
print("  Output Log: ", save_path)



