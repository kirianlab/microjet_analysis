import sys, os, datetime
import pandas as pd
import numpy  as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyqtgraph as pg

##############################################
## CMD ARGS
##############################################

nozzle_type_results_path, save_results_path = sys.argv[1:]

##############################################
## PARAMS
##############################################

sns.set(rc={'figure.figsize':(8,12)})   # US letter size

fig_title       = "Jet Speed (m/s) vs. Helium Gas Mass Flow Rate (mg/min)\n"
legend_title    = 'Water Vol Flow \nRate (uL/min)'
gas_ax_label    = 'Helium Gas Mass Flow Rate (mg/min)\n'
liq_ax_label    = 'Water Volume Flow Rate (uL/min)\n'
speed_ax_label  = "Jet Speed (m/s)"

single_flow_plot_styles = dict(
    hue     = 'nozzle', 
    palette = None,
    size    = "nozzle" , 
    sizes   = (200,201), 
    style   = 'nozzle',
    legend  = 'full')

multi_flow_plot_styles = dict(
    hue     = 'liq', 
    palette = None,
    size    = "nozzle" , 
    sizes   = (200,201), 
    style   = 'nozzle',
    legend  = 'full')

legend_styles=dict(
    loc           = 'upper left', 
    title         = legend_title, 
    scatterpoints = 1, 
    numpoints     = 1)


##############################################
## Make dirs
##############################################

print("Make Results Directory")

datestamp= datetime.datetime.today().strftime('%Y%m%d')
savedir  = "{}_report_all_SC30I_analysis".format(datestamp)
savedir  = os.path.join("~/Desktop", savedir)
savedir  = os.path.expanduser(savedir)

os.makedirs(savedir, exist_ok=True)

##############################################
## Plot Functions
##############################################


def scatter_plot_jet_speed(df, save_path="", plot_styles={},  legend_styles={}, fig_styles={}, axes_limits={}, add_run_nums=False, interactive=False):

    ax   = sns.scatterplot(data = df, x = "gas" , y = "jet_speed", **plot_styles) 

    xlim = axes_limits.get("xlim", (-1 + np.min(df['gas'])       , 5 + np.max(df['gas'])))
    ylim = axes_limits.get("ylim", (-5 + np.min(df['jet_speed']) , 5 + np.max(df['jet_speed'])))
    
    ax.set(xlim= xlim, ylim=ylim)
    ax.grid(True)

    if add_run_nums:
        for row in df.index:
            X = df.gas[row] + 0.5
            Y = df.jet_speed[row] 
            L = df.run[row]
            ax.text(X, Y, L, horizontalalignment='left', size=12, color='black', weight='normal')

    # ax.get_figure().tight_layout(pad=3) #  plt.tight_layout(pad=3)
    # legend_handles, legend_labels = ax.get_legend_handles_labels()

    plt.legend(**legend_styles)
    plt.title(fig_title + fig_styles.get("title",""))
    plt.xlabel(gas_ax_label)
    plt.ylabel(speed_ax_label)
    if interactive: plt.show()   

    if save_path: 
        ax.get_figure().savefig(save_path, format='png', dpi=300)
        print("\tSaved", os.path.basename(save_path))
    plt.close()


def plot_main(DF, suffix=""):

    print("Jet Speed Plot")

    ##############################################
    print("  All Liq Flows")
    ##############################################

    filename    = "all_SC30I{}.png".format(suffix)
    savepath    = os.path.join(savedir, filename)
    figstyles   = {"title": "All SC30I Nozzles"}
    plotstyles  = multi_flow_plot_styles.copy()
    plotstyles['palette'] = sns.color_palette("bright" , len(np.unique(DF['liq'])))

    scatter_plot_jet_speed(DF, save_path=savepath, plot_styles=plotstyles, legend_styles=legend_styles, fig_styles=figstyles)

    ##############################################
    print("  Each Liq Flow")
    ##############################################

    for flow in np.unique(DF['liq']):        
        sub_df      = DF.loc[(DF['liq'] == flow)]
        filename    = "all_SC30I_{}uL-min{}.png".format(str(int(flow)).rjust(2,"0"),suffix) 
        savepath    = os.path.join(savedir, filename)
        figstyles   = {"title": "All SC30I Nozzles\n" + "Water Flow Rate: {} uL/min".format(int(flow))}
        plotstyles  = single_flow_plot_styles.copy()
        plotstyles['palette'] = sns.color_palette("bright" , len(np.unique(sub_df['nozzle'])))
        
        scatter_plot_jet_speed(sub_df, save_path=savepath, plot_styles=plotstyles, legend_styles=legend_styles, fig_styles=figstyles)

    ##############################################
    print("  Each Nozzle Type")
    ##############################################

    ## Match x/y-axis ranges w/ those of "all sc30i" plots
    axeslimits = dict(
        xlim   = (-1 + np.min(DF['gas'])       , 5 + np.max(DF['gas'])),
        ylim   = (-5 + np.min(DF['jet_speed']) , 5 + np.max(DF['jet_speed'])))

    for nozzle in np.unique(DF['nozzle']):        
        sub_df      = DF.loc[(DF['nozzle'] == nozzle)]
        filename    = "all_{}{}.png".format(nozzle, suffix) 
        savepath    = os.path.join(savedir, filename)
        figstyles   = {"title": "Nozzle Subset: " + nozzle}
        plotstyles  = multi_flow_plot_styles.copy()

        plotstyles["size"]    = "liq"
        plotstyles['palette'] = sns.color_palette("bright" , len(np.unique(sub_df['liq'])))
        
        scatter_plot_jet_speed(sub_df, save_path=savepath, plot_styles=plotstyles, legend_styles=legend_styles, fig_styles=figstyles, axes_limits=axeslimits)


##############################################
## Get Data
##############################################

print("Get nozzle-wise results")

with open(nozzle_type_results_path, 'r') as f:
    noz_results_paths = [os.path.abspath(l.strip()) for l in f.readlines()]
    
df = pd.read_csv(noz_results_paths[0])    
for p in noz_results_paths[1:]:
    df = df.append(pd.read_csv(p),sort=False)

df.sort_values(by=["nozzle","date","run"], axis="index", inplace=True)

print("Handle ignored runs")
sifted_df  = df.loc[df["status"] != "ignore"]

##############################################
## Plot
##############################################

print("Save Jet Speed Plots (Jet Speed v Gas Flow)\n")

plot_main(df)

if not sifted_df.empty:
    plot_main(sifted_df, suffix="_sifted")
##############################################
## Save 
##############################################
print("Save Data")

df.to_csv(save_results_path, index=False)
df.to_csv(os.path.join(savedir, os.path.basename(save_results_path)), index=False)

print("\tSaved CSV  at", os.path.dirname(os.path.abspath(save_results_path)))
print("\tSaved Figs at", savedir)

pg.exit()
