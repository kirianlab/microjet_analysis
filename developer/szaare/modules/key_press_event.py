import sys
import pyqtgraph as pg

class KeyPressWindow(pg.GraphicsWindow):
    sigKeyPress = pg.Qt.QtCore.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def keyPressEvent(self, ev):
        self.scene().keyPressEvent(ev)
        self.sigKeyPress.emit(ev)


def keyPressed(evt):
    print("Key pressed minus 48 (maps to number pad)", evt.key()-48)


app = pg.mkQApp()
win = KeyPressWindow()
win.sigKeyPress.connect(keyPressed)
pl = win.addPlot()
pl.plot([x*x for x in range(-10,11)])


sys.exit(app.exec_())
# pg.QtGui.QApplication.instance().exec_()