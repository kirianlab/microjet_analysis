import os, sys,time, pickle, datetime
import numpy as np
import pyqtgraph as pg
# from skimage import io
from PIL import Image
from tqdm import tqdm


from _import_context import libs
from libs.viewers.roi_selectors import draw_roi

###########################################################
## Params
###########################################################

MOVIE_PATHS_FILE_PATH    = os.path.abspath(sys.argv[1])      # e.g. "../results/list.txt"
ROI_PARAMS_DATABASE_PATH = os.path.abspath(sys.argv[2]) 
PRE_ANALYSIS_REPORT_PATH = os.path.abspath(sys.argv[3])
PRE_ANALYSIS_REPORT_COLS = [    "timestamp",
                                "nozzle",
                                "fps",
                                "num_movies",
                                "num_frames",
                                "duration_seconds"]

###########################################################
## Get list of movies and roi database
###########################################################

try: 
    with open(ROI_PARAMS_DATABASE_PATH, 'rb') as f:
        ALL_ROI_PARAMS = pickle.load(f)
except: ALL_ROI_PARAMS = {}

with open(MOVIE_PATHS_FILE_PATH, 'r') as f:
    moviepaths = [i.strip() for i in f.readlines()]

###########################################################
## If necessary, get roi params and/or extract and save roi
###########################################################

last_roi_params = { "roi_pos"    : None, 
                    "roi_size"   : None, 
                    "line_value" : None}

##################
## If unavailable, Get ROI params (pos, size, slices, nozzle tip location)
##################

for mv in moviepaths:

    mv_key = os.path.join(*mv.split("/")[-3:]).replace("\\","/")

    if mv_key not in ALL_ROI_PARAMS:
        print(mv_key)
        app = pg.mkQApp()
        im = Image.open(mv)
        im.seek(0)
        im  = np.array(im.getdata()).reshape(im.size[1],im.size[0]).astype(np.single)
        roi_params = draw_roi(im, last_roi_params, line_angle=0) 
        ALL_ROI_PARAMS[mv_key] = roi_params.copy()
        del roi_params["roi_slices"]
        last_roi_params.update(roi_params)
        with open(ROI_PARAMS_DATABASE_PATH, "wb") as f:
            pickle.dump(ALL_ROI_PARAMS, f, pickle.HIGHEST_PROTOCOL)

t0 = int(time.time())
tot_frames = 0
tot_movies = 0

##################
## If unavailable, extract and save ROI 
##################

for mv in moviepaths:

    mv_key      = os.path.join(*mv.split("/")[-3:]).replace("\\","/")
    mv_roi_path = mv.replace('rawdata', 'tempdata')    # .replace('.tif', '_roi.npy')

    # print("\t", mv_roi_path)
    
    if  not os.path.isfile(mv_roi_path):
        im          = Image.open(mv)
        n_frames    = min(300, im.n_frames)  # 'too many' frames may cause numpy MemoryError in main analysis
        tot_frames += n_frames
        tot_movies += 1

        roi_pos     = ALL_ROI_PARAMS[mv_key]["roi_pos"] 
        roi_size    = ALL_ROI_PARAMS[mv_key]["roi_size"]

        # roi_pos   = ((roi_pos[1],roi_pos[0]))
        # roi_size  = ((roi_size[1],roi_size[0]))

        crop_coords = ( roi_pos[1], 
                        roi_pos[0], 
                        roi_pos[1] + roi_size[1], 
                        roi_pos[0] + roi_size[0])

        print("\n\tROI Pos    : {}\n\tROI Size   : {}\n\tROI Coords : {}\n".format(roi_pos, roi_size, crop_coords))

        # npy_stack = np.empty(shape=(n_frames, *roi_size))
        tif_stack   = []        
        
        for n in tqdm(range(n_frames)):
            
            im.seek(n)
            frame = im.crop(crop_coords).getdata()
            frame_size_before = frame.size
            frame = np.array(frame).reshape(roi_size).astype(np.single)
            frame_size_after = frame.size               
            
            # frame = np.transpose(frame, (1, 0))
            # npy_stack[n] = frame
            tif_stack.append(Image.fromarray(frame, mode="I"))  # I = int32
            
            if n == 0: 
                print("\n\tPIL.Image  frame.size  (before reshape):", frame_size_before)
                print("\n\tnp.ndarray frame.shape (after  reshape):", frame_size_after)
                pg.image(frame)
                pg.QtGui.QApplication.processEvents()
                pg.QtGui.QApplication.instance().exec_()  
                # input("Press Enter to continue...") 

        os.makedirs(os.path.dirname(mv_roi_path), exist_ok=True)
        
        ##############
        ## Save as NPY
        ##############
        
        # np.save(mv_roi_path, npy_stack)

        ##############
        ## Save as TIF
        ##############
        
        try:
            tif_stack[0].save(mv_roi_path, save_all=True, append_images=tif_stack[1:])          
            status = "Success"

        except Exception as e:
            status = "Failure " + str(type(e))
        finally:
            print("  "    ,  status)
            print("   ---", "ROI:",os.path.relpath(mv_roi_path))
                    


###########################################################
## Log performance
###########################################################

if tot_movies > 0:
    
    tf           = int(time.time())
    Dt           = int(1+tf-t0)
    fps          = int(tot_frames/Dt)
    timestamp    = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    nozzle_type  = moviepaths[0].split("/")[-2]
    report_entry = "{},{},{},{},{}\n".format(timestamp, nozzle_type, fps, tot_movies, tot_frames, Dt)

    if not os.path.isfile(PRE_ANALYSIS_REPORT_PATH):
        with open(PRE_ANALYSIS_REPORT_PATH, "w") as f:
            f.write(",".join(PRE_ANALYSIS_REPORT_COLS))

    with open(PRE_ANALYSIS_REPORT_PATH, "a+") as f:
        f.write(report_entry)

pg.exit()
