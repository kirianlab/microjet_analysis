import os, sys
import numpy as np
from PIL import Image

MOVIE_PATHS_FILE_PATH = os.path.abspath(sys.argv[1])      # e.g. "./results/list.txt"

with open(MOVIE_PATHS_FILE_PATH, 'r') as f:
    moviepaths = [i.strip() for i in f.readlines()]

for tif_path in moviepaths:
    npy_path  = tif_path.replace("rawdata","tempdata").replace(".tif", "_roi.npy")
    save_path = npy_path.replace("_roi.npy", ".tif")

    if os.path.isfile(npy_path) and (not os.path.isfile(save_path)):
        
        try:
            frames= np.load(npy_path).astype(np.int32)   # don't use np.single here
            stack = []                                                                                      
            for f in frames:
                stack.append(Image.fromarray(f, mode="I"))  # I = int32
            stack[0].save(save_path, save_all=True, append_images=stack[1:])          
            status = "Success"

        except Exception as e:
            status = "Failure " + str(type(e))
        finally:
            print("  "   ,  status)
            print("  ---", "NPY:",os.path.relpath(npy_path))
            print("  ---", "TIF:",os.path.relpath(save_path))
            
""" PIL.Image  
    .fromarray 
        mode
            1    (1-bit pixels, black and white, stored with one pixel per byte)
            L    (8-bit pixels, black and white)
            P    (8-bit pixels, mapped to any other mode using a color palette)
            I    int32
            F    float32
    .save 
        compression
            tiff_deflate    don't use. slower. larger file size. worse quality
"""