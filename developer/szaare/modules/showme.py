"""
Replaces ImageJ for quickly viewing tif movies
    
    Run with -h flag or see argparser below for details                
"""

import sys, os
import numpy as np
from skimage import io
import pyqtgraph as pg
import pyqtgraph.exporters
import argparse 
from PIL import Image
import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

#################################################################
## Params
#################################################################

SAVE_DIR = os.path.expanduser("~/Desktop/sample_frames")

#################################################################

parser = argparse.ArgumentParser()

parser.add_argument('--possibleTifs',  type=str, default="")
parser.add_argument('--file'        ,  type=str, default="")
parser.add_argument('--range'       ,  nargs=2  , type=int)
parser.add_argument('--runs'        ,  nargs='*', type=int)
parser.add_argument('--nframes'     ,  nargs=1  , type=int, default=50)
parser.add_argument('--all'         ,  action='store_true')
parser.add_argument('--save'        ,  action='store_true')
parser.add_argument('--show'        ,  action='store_true')

args = parser.parse_args()

#################################################################

def find_run_num(tifpath):
    dir_, base = os.path.split(tifpath)
    base = os.path.splitext(base)[0]    # remove extension
    for c in [" ","-", ".", ",", "__"]:
        base = base.replace(c, "_")
    run = -1
    for part in base.split("_"):
        try: run = int(part)
        except ValueError: continue
        except Exception as e: print(repr(e))
        else: break
    return run


def show_me(tif_path, save=False, show=True, nframes=10):

    p = tif_path
    short_p = os.path.join(*p.split(os.sep)[-6:])
    print(short_p)
    
    roi_path = p.replace("rawdata","tempdata")
    if os.path.isfile(roi_path): p = roi_path

    try: im = Image.open(p)
    except Exception as e:
        print("  *** %s: %s"%(e.strerror, p))
        return 

    sx,sy   = im.size
    n_frames= min(nframes, im.n_frames) 
    stack   = []   

    for n in range(n_frames):
        
        im.seek(n)
        frame  = np.array(im.getdata()).reshape((sy,sx)).astype(np.single)
        frame *= -1
        frame -= np.median(frame)
        stack.append(frame)

    stack = np.transpose(stack, (0,2,1))
    # win = pg.image(stack, title=short_p, levels=(np.min(stack.ravel()), np.mean(stack.ravel())))
    win = pg.image(stack, title=short_p, levels=(np.mean(stack.ravel()), 40))
    win.setPredefinedGradient("yellowy")   # "thermal", "flame", "yellowy", "bipolar", "spectrum", "cyclic", "greyclip", "grey"
    # save_btn = pg.QtGui.QPushButton("Save")
    # win.win.centralWidget().layout().addWidget(save_btn, -1,0)
    if save:
        filename  = os.path.basename(tif_path).split(".")[0] + ".png"
        rel_dir   = os.path.dirname(tif_path).split(os.sep)[-3:]
        rel_dir   = os.path.join(*rel_dir)   
        savedir1  = os.path.expanduser(os.path.join(SAVE_DIR, rel_dir))
        savepath1 = os.path.join(savedir1, filename)

        os.makedirs(savedir1, exist_ok=True)
        exp = pg.exporters.ImageExporter(win.getView())
        exp.export(savepath1)
        print("  ** saved:", savepath1)
    if not show:
        # win.win.hide()
        win.win.close()

#################################################################

if   args.file          : pathsFile = args.file
elif args.possibleTifs  : pathsFile = args.possibleTifs

try: 
    paths = []
    with open(pathsFile, 'r') as f:
        paths = [os.path.expanduser(os.path.join("~",l.strip())) for l in f.readlines()]
    if not paths: raise

except Exception as e:
    print(repr(e))
    print("\t", os.path.abspath(pathsFile))
    parser.print_help()

if args.all or args.file:
    show_these = paths
elif args.range:
    run_nums   = range(args.range[0], args.range[1]+1)
    show_these = [p for p in paths if find_run_num(p) in run_nums]
else:
    run_nums   = args.runs
    show_these = [p for p in paths if find_run_num(p) in run_nums] 

if args.save: 
    args.show = False
elif not (args.save or args.show): 
    args.show = True

#################################################################

if __name__ == '__main__':

    for n,p in enumerate(sorted(show_these, key=find_run_num)):
        show_me(p, save=args.save, show=args.show, nframes=args.nframes)
        n += 1
        if args.show:
            if n%5 == 0 or n == len(show_these):
                pg.QtGui.QApplication.processEvents()
                pg.QtGui.QApplication.instance().exec_()   

    pg.exit()