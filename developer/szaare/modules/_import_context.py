import os, sys

try: 
	project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", ".."))
	sys.path.insert(0, project_dir)

	import libs

	sys.path.pop(0)
	
except Exception as e: 
	print(type(e), e)

