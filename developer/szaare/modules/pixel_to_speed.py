import sys, os, datetime
import pandas as pd
import numpy  as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyqtgraph as pg

##############################################
## CMD ARGS
##############################################

npz_paths_file, ignored_runs_file, data_log_path, save_results_path, nozzle_type = sys.argv[1:]

###############################################
## CMD ARGS Summary
###############################################

print("\n\t Commandline Args Summary\n")

print("\n\t\t \
.npz paths file    : {} \n\t\t \
ignored runs file  : {} \n\t\t \
data log path      : {} \n\t\t \
save results path  : {} \n\t\t \
nozzle type        : {} \n\n".format(npz_paths_file, ignored_runs_file, data_log_path, save_results_path, nozzle_type))


##############################################
## PARAMS
##############################################

sns.set(rc={'figure.figsize':(8.27,11.7)})   # US letter size

fig_title       = "\nJet Speed (m/s) vs. Helium Gas Mass Flow Rate (mg/min).\n(Point Labels Are Run Numbers)"
legend_title    = 'Water Vol Flow \nRate (uL/min)'
gas_ax_label    = 'Helium Gas Mass Flow Rate (mg/min)\n'
liq_ax_label    = 'Water Volume Flow Rate (uL/min)\n'
speed_ax_label  = "Jet Speed (m/s)"

single_flow_plot_styles = dict(
    hue     = 'liq_round',   # 'run' to get rainbow for hysteresis
    palette = None,
    size    = "liq_round" , 
    sizes   = (300,301), 
    # style = 'liq_round',
    legend  = 'full')

multi_flow_plot_styles = dict(
    hue     = 'liq_round', 
    palette = None,
    size    = "liq_round" , 
    sizes   = (100,300), 
    # style = 'liq_round',
    legend  = 'full')

legend_styles=dict(
    loc           = 'upper left', 
    title         = legend_title, 
    scatterpoints = 1, 
    numpoints     = 1)

point_label_styles= dict(
    horizontalalignment = 'center',
    verticalalignment   = 'center', 
    size                = 12, 
    # color             = 'black', 
    weight              = 'normal') 

##############################################
## Make dirs
##############################################

print("\t Making Results Directory")

datestamp= datetime.datetime.today().strftime('%Y%m%d')
savedir  = "{}_report_all_{}_analysis".format(datestamp, nozzle_type)
savedir  = os.path.join("~/Desktop", savedir)
savedir  = os.path.expanduser(savedir)

os.makedirs(savedir, exist_ok=True)

##############################################
## Plot Functions
##############################################


def scatter_plot_jet_speed(df, save_path="", plot_styles={},  legend_styles={}, point_label_styles = point_label_styles, add_run_nums=False, add_actual_liq_flows=False, interactive=False):

    ax = sns.scatterplot(data = df, x = "gas" , y = "jet_speed", **plot_styles) 
    ax.grid(True)
    ax.set(xlim=(-1 + np.min(df['gas'])       , 5 + np.max(df['gas'])),
           ylim=(-5 + np.min(df['jet_speed']) , 5 + np.max(df['jet_speed'])))

    if add_run_nums:
        for row in df.index:
            X = df.gas[row] 
            Y = df.jet_speed[row] + 1.0
            L = "R:%s"%df.run[row]
            ax.text(X, Y, L, color = 'black', **point_label_styles)

    if add_actual_liq_flows:
        for row in df.index:
            X = df.gas[row] 
            Y = df.jet_speed[row] - 1.0
            L = "L:%.1f"%df.liq[row]
            ax.text(X, Y, L, color = 'red'  , **point_label_styles)
    # ax.get_figure().tight_layout(pad=3) #  plt.tight_layout(pad=3)
    # legend_handles, legend_labels = ax.get_legend_handles_labels()

    plt.legend(**legend_styles)
    plt.title(nozzle_type +  fig_title)
    plt.xlabel(gas_ax_label)
    plt.ylabel(speed_ax_label)
    if interactive: plt.show()   

    if save_path: 
        ax.get_figure().savefig(save_path, format='png', dpi=300)
        print("\t Saved", os.path.basename(save_path))
    plt.close()


def plot_main(DF, suffix=""):

    print("\t Jet Speed Plot")
    print("\t    All Liq Flows")

    filename    = "{}_all{}.png".format(nozzle_type,suffix)
    savepath    = os.path.join(savedir, filename)
    plotstyles  = multi_flow_plot_styles.copy()

    plotstyles['palette'] = sns.color_palette("bright" , len(np.unique(DF['liq_round'])))

    scatter_plot_jet_speed(DF, save_path=savepath, plot_styles=plotstyles, legend_styles=legend_styles)

    print("\t   Each Liq Flow")

    for flow in np.unique(DF['liq_round']):
        
        sub_df      = DF.loc[(DF['liq_round'] == flow)]
        filename    = "{}_{}uL-min{}.png".format(nozzle_type, str(int(flow)).rjust(2,"0"),suffix) 
        savepath    = os.path.join(savedir, filename)
        plotstyles  = single_flow_plot_styles.copy()
        legendstyles= legend_styles.copy()

        if plotstyles["hue"] == "run":
            plotstyles['palette']  = sns.color_palette("rainbow", len(sub_df.index))
        elif plotstyles["hue"] == "liq_round":
            plotstyles['palette']  = sns.color_palette("bright", 1)

        # legendstyles["labels"] = ['liq_round', str(flow)]
        
        scatter_plot_jet_speed(sub_df, save_path=savepath, plot_styles=plotstyles, add_run_nums=True, add_actual_liq_flows=True, legend_styles=legendstyles)


##############################################
## Get Data
##############################################
print("\n\t Get processed .npz paths")

with open(npz_paths_file, 'r') as f:
    npz_paths = [os.path.abspath(os.path.normpath(l.strip())) for l in f.readlines()]

print("\n\t Get flow rates, pixel size, etc. for each movie.")
    
desired_cols = ["nozzle", "date","run","gas", "liq",'zoom', 'pixelSize', 'ifp']
new_columns  = ["droplet_translation", "jet_speed", "path", "liq_round"]

df           = pd.read_csv(data_log_path)    
if "ifp" not in df.columns: df['ifp'] = 550
sub_df       = pd.DataFrame(columns=df.columns.tolist() + new_columns)

df["nozzle"] = df["nozzle"].astype(str)
df["date"]   = df["date"].astype(str)
df["run"]    = df["run"].astype(str)

for p in npz_paths:

    data = np.load(p)

    p = p.replace("_roi", "")
    p = p.replace(".npy.brightfield2.npz","")
    p = p.replace(".tif.brightfield2.npz","")

    year, date, nozzle, run = [str(i) for i in p.split(os.sep)[-4:]]
    selection   = df.loc[ (df['nozzle']   == nozzle.replace("_",""))  & 
                          (df['date']     == date)        &
                          (df['run']      == str(int(run))), desired_cols].copy()

    selection['droplet_translation'] = np.round(data['droplet_translation'],2)
    selection["path"] = os.path.join(year,date,nozzle,run) + ".tif"

    sub_df = sub_df.append(selection, sort=False)

    """ Pandas Recipes: 

        Conditional Assignment
        ======================

        sub_df.loc[ (sub_df['liq']    <  sift_rules["min_liq_flow" ]) |
                    (sub_df['run'].isin( sift_rules["ignore_runs"  ])),   
            "status"] = "ignored"  

        Renaming Cols 
        =============
        
        desired_cols = ["RunVidNum","HeliumMassFlowRate_mgPERmin", "LiqVolFlowRate_uLPERmin", "Zoom", "PixelSize_um", "FastcamIFP_ns"]
        renamed_cols = ['run', 'gas', 'liq', 'zoom', 'pixelsize', 'ifp']
        colname_map  = dict(zip(desired_cols, renamed_cols))
        df.rename(columns=colname_map, inplace=True)
    """

##############################################
## Jet Speed 
##############################################

print("\n\t Convert: Pixels --> Jet Speed\n")

df = sub_df
find_jet_speed  = lambda row : round(1000*row['droplet_translation']*row['pixelSize']/row['ifp'], 1)
round_liq_flow  = lambda liq : 5 * round(float(liq)/5)

for c in df.columns: print(c)
df['jet_speed'] = df.apply(find_jet_speed , axis=1)
df['liq_round'] = df['liq'].apply(round_liq_flow)

##############################################
## Handle Ignored Runs
##############################################

print("\n\t Flag ignored runs\n")

ignores_df = pd.read_csv(ignored_runs_file)
df         = pd.merge(df, ignores_df, how="left", on=["path"])
sifted_df  = df.loc[df["status"] != "ignore"]

print("\n\t   Ignored runs\n")
for index,row in df.loc[df["status"] == "ignore"].iterrows():
    print("\t", row["run"],"  ", row["path"])

##############################################
## Plot
##############################################

print("\n\t Save Jet Speed Plots (Jet Speed v Gas Flow)\n")

plot_main(df)
if not sifted_df.empty:
    plot_main(sifted_df, suffix="_sifted")

##############################################
## Save 
##############################################

print("\n\t Save Data\n")

df.drop(columns=["liq_round"], inplace=True)
df.to_csv(save_results_path, index=False)
df.to_csv(os.path.join(savedir, os.path.basename(save_results_path)), index=False)

print("\n\t\t Saved CSV  at", os.path.dirname(os.path.abspath(save_results_path)))
print("\n\t\t Saved Figs at", savedir)

pg.exit()
