import sys, os, datetime
import pandas as pd
import numpy  as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pyqtgraph as pg
import pyqtgraph.exporters
import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

###########################################################################

NPZ_PATH_FILE = os.path.abspath(sys.argv[1])
SAVE_DIR      = os.path.expanduser("~/Desktop/outerprods")
USE_PYPLOT    = False
INTERACTIVE   = False
WIN           = None

with open(NPZ_PATH_FILE, 'r') as f:      # e.g. "./results/list.txt"
    npz_paths = [i.strip() for i in f.readlines()]

###########################################################################

class ImWin(pg.ImageView):
    ''' Helper class for displaying images. '''

    def __init__(self, *args, **kargs):
        pg.mkQApp()
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(1000, 1000)
        if 'title' in kargs:
            self.win.setWindowTitle(kargs['title'])
            del kargs['title']
        pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
        # self.setImage(*args, **kargs)
        self.win.setCentralWidget(self)
        for m in ['resize']:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient('flame')
        self.win.show()

    def add_plot(self, *args, **kwargs):
        return self.getView().plot(*args, **kwargs)

###########################################################################

if USE_PYPLOT:

    print("Using Pyplot")
    fig, WIN = plt.subplots()

    def show_it(matrix, xs, ys, win, interactive=False):
        plt.clf()
        win.imshow(ccmat, cmap='seismic')
        win.plot(xs, ys, linewidth=3, color='red' )
        if interactive: plt.show()
        
    def save_it(win, savepath, interactive=False):
        win.get_figure().savefig(savepath, format='png', dpi=300)
        if interactive: plt.close()

else: 

    print("Using Pyqtgraph")
    WIN = ImWin()

    def show_it(matrix, xs, ys, win, interactive=False):
        win.getView().clearPlots()
        win.setImage(ccmat)
        win.setLevels(0, 0.5*win.getLevels()[1])
        win.add_plot(xs, ys, pen='g')
        win.getView().autoRange()
        win.win.hide()            
        if interactive: 
            # win.win.show()
            # win.win.showMaximized() 
            pg.QtGui.QApplication.processEvents()
            pg.QtGui.QApplication.instance().exec_()

    def save_it(win, savepath, interactive=False):
        exp = pg.exporters.ImageExporter(win.getView())
        # exp.parameters()['width']  = win.getView().width()
        exp.export(savepath)
        if interactive: win.close()

###########################################################################

for npz_path in npz_paths:
    
    # print("="*40); print(npz_path); print("="*40)

    data = np.load(npz_path)
    
    for k in ['matproj', 'cc_matrix_projection']:
        if k in data:
            ccproj= data[k]
            break
    for k in ['ccmat', 'cc_matrix']:
        if k in data:
            ccmat = data[k]
            break    

    try:    
        xs = -50 -100*ccproj/np.max(ccproj)
        ys = np.arange(len(ccproj))
    except: pass 
    
    try: show_it(ccmat, xs, ys, WIN, INTERACTIVE)
    except: 
        print("  ---- Failed to Save", filename)
        continue

    if SAVE_DIR:

        filename  = os.path.basename(npz_path).split(".")[0].replace("_roi", "") + ".png"
        rel_dir   = os.path.dirname(npz_path).split(os.sep)[-3:]
        rel_dir   = os.path.join(*rel_dir)   
        
        savedir1  = os.path.expanduser(os.path.join(SAVE_DIR, rel_dir))
        savedir2  = os.path.abspath(os.path.dirname(npz_path))

        savepath1 = os.path.join(savedir1, filename)
        savepath2 = os.path.join(savedir2, filename)

        os.makedirs(savedir1, exist_ok=True)

        save_it(WIN, savepath1)
        save_it(WIN, savepath2)

        print("  ---- Saved", filename)

        # print("**** Saved Outer Product Matrix and Projection at\n\t", 
        #       os.path.dirname(savepath1), "\n\t", 
        #       os.path.dirname(savepath2))
        
pg.exit()
###########################################################################
