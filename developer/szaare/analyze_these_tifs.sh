#!/bin/bash

#-------------------------------------------------------------------
# Simplest Script
#	Run rick/brightfield2a.py 
# 	on a text file containing tif paths
#-------------------------------------------------------------------
# 	CMD Args
# 		$1 			path to text file containing tif paths to analyze 
# 		$2,3,... 	args to pass to brightfield2a.py
#-------------------------------------------------------------------

#######################################################
# Switches: What Would You Like to Do?
#######################################################

USE_ROI=false

#######################################################
# Commandline Args
#######################################################

TIF_PATHS_FILE="$1"
shift
ANALYSIS_OPTIONS="$@"

#######################################################
# Params: Input/Output Data Paths
#######################################################

RAWDATA="../rawdata/microjets"
TEMPDATA="../tempdata/microjets"

#######################################################
# Params: Temp Paths 
#######################################################

ANALYZED_TIFS="./temp/analyzed_tifs.txt"
FAILED_TIFS="./temp/failed_tifs.txt"
FAILED_TIFS_STDERR="./temp/failed_tifs_stderr.txt"

#######################################################

SEP="\n"$(printf '=%.0s' {1..70})"\n"
printf $SEP ; printf "%s\n" "Analyze: ${TIF_PATHS_FILE}" "Options: ${ANALYSIS_OPTIONS}" ; printf $SEP
true > ${ANALYZED_TIFS}
true > ${FAILED_TIFS}
true > ${FAILED_TIFS_STDERR}
printf $SEP ; echo "Main Analysis: Droplet Translation" ; printf $SEP 
# echo "*** Find ROIs Only if They Exist. Else, Use Original Tif."
for i in $(cat ${TIF_PATHS_FILE}); do
	i="${i#"/"}"
	i="${i#"rawdata/microjets/"}"
	TIF="${RAWDATA}/${i}"
	ROI="${TEMPDATA}/${i}"
	[ -f "$ROI" ] && [ "$USE_ROI" = true ] && TIF="$ROI"
	printf ${TIF}"\n" >> ${ANALYZED_TIFS}
    printf "${SEP}${TIF}${SEP}\n" | tee -a ${FAILED_TIFS_STDERR}
    python -W ignore ./brightfield2a.py ${ANALYSIS_OPTIONS} "${TIF}" 2>> ${FAILED_TIFS_STDERR}
    [ $? -ne 0 ] && printf ${TIF}"\n" &>> ${FAILED_TIFS} 
done

printf $SEP 
echo "The End" 
printf "%s\n" \
"Analyze: ${TIF_PATHS_FILE}" \
"Options: ${ANALYSIS_OPTIONS}" \
"TIFs   : ${ANALYZED_TIFS}" \
"FAILED : ${FAILED_TIFS}" \
"STDERR : ${FAILED_TIFS_STDERR}"
printf $SEP
