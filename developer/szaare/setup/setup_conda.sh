#!/bin/bash

echo "Windows: Run this from an Anaconda Prompt or Git Bash (not Cygwin!)"
echo "Linux  : Run this!"

conda create -n py35 python=3.5
 
source activate py35	# or conda activate

conda install -c anaconda pyqt=4.11 
conda install -c anaconda pillow 
conda install -c conda-forge scikit-image

conda install numpy scipy pandas matplotlib pyqtgraph seaborn tqdm tabulate
