#!/bin/bash

echo "
*** If on Windows, run this in Cygwin (not Git Bash)
"

######################################

SYNC_EXT_tempdata="*.npz"
SYNC_EXT_rawdata="*_plot_data.txt"

REMOTE="szaare@phase1.la.asu.edu:/home/szaare"
REMOTE_tempdata="${REMOTE}/tempdata/"
REMOTE_rawdata="${REMOTE}/rawdata/"

LOCAL_tempdata="../../tempdata/"
LOCAL_rawdata="../../rawdata/"


#####################################

echo "

##############################
TEMPDATA
##############################

    RSYNC  
        ext     ${SYNC_EXT_tempdata} 
        from    ${REMOTE_tempdata}
        to      ${LOCAL_tempdata}

------------------------------

"

rsync   -avr --stats --progress             \
        --include="*/"                      \
        --include="${SYNC_EXT_tempdata}"    \
        --exclude="*"                       \
        "${REMOTE_tempdata}"                \
        "${LOCAL_tempdata}"

echo "

##############################
RAWDATA
##############################

    RSYNC 
        ext     ${SYNC_EXT_rawdata} 
        from    ${REMOTE_rawdata}
        to      ${LOCAL_rawdata}    

------------------------------

"

rsync   -avr --stats --progress             \
        --include="*/"                      \
        --include="${SYNC_EXT_rawdata}"     \
        --exclude="*"                       \
        "${REMOTE_rawdata}"                 \
        "${LOCAL_rawdata}"