import time
import sys
import glob
import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.cm as cm

def compile_data(file_list, ignore_list):
    r"""
    Compile data in the provided file_list (ignoring those in the ignore_list) and put them into
    a dictionary with the keys:
    - liquid_flow
    - gas_flow
    - jet_speed
    - jet_diameter
    - etc (more to be added, e.g. a stability metric is presently lacking).
    """
    liq = []
    gas = []
    speed = []
    diam = []
    for f in file_list:
        if f in ignore_list:
            continue
        h5 = h5py.File(f, 'r')
        if 'jet_analysis' not in list(h5.keys()):  # This means that the analysis failed
            continue
        else:
            g = h5['/jet_analysis/liquid_flow'][()]  # WTF... why do we need the [()] here?
            l = h5['/jet_analysis/gas_flow'][()]
            s = h5['/jet_analysis/jet_speed'][()]
            d = h5['/jet_analysis/jet_diameter'][()]
            liq.append(g)
            gas.append(l)
            speed.append(s)
            diam.append(d)
            print('%s liq=%4.1f gas=%4.1f speed=%4.1f' % (f, g, l, s))
        h5.close()
    data = {
        'liquid_flow': np.array(liq),
        'gas_flow': np.array(gas),
        'jet_speed': np.array(speed),
        'jet_diameter': np.array(diam)
    }
    return data


# def update_contour_plot(fig_obj, axis_obj, data, config):
#     r"""Update given `contour_plot_object`.
#
#         Only updates the plotted data points and (if applicable) the axis ranges,
#             which is much faster than recreating the entire plot.
#
#         Requires `data` to have same shape as the data used to create the original plot.
#     """
#
#     axis_obj.set_data(np.array(data[config['x_key']]), np.array(data[config['y_key']]))
#
#     axis_obj.set(xlim=config["x_range"], ylim=config["y_range"])
#
#     #TODO: consider updating 'contour' & 'contourf' objects, esp. their `levels`
#
#     fig_obj.canvas.draw()
#     fig_obj.canvas.flush_events()


def make_contours(data, config, fig=None, axis=None):
    r""" Make a contour plot.  The data dictionary should come from compile_data(). """

    # Configurations
    xr = config['x_range']
    yr = config['y_range']
    zr = config['z_range']

    # Make the contour plot
    x = np.array(data[config['x_key']])
    y = np.array(data[config['y_key']])
    z = np.array(data[config['z_key']])

    # Make a contour plot of irregularly spaced data coordinate via interpolation on a grid.
    n_levels = int((zr[1]-zr[0])/config['level_step'])
    levels = np.linspace(zr[0], zr[1], int(n_levels+1))

    # Create grid values first.
    xi = np.linspace(0, np.max(x), 200)
    yi = np.linspace(0, np.max(y), 200)
    Xi, Yi = np.meshgrid(xi, yi)

    # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
    interpolator = tri.LinearTriInterpolator(tri.Triangulation(x, y), z)
    zi = interpolator(Xi, Yi)

    # plot --------------------------------------------------------------------

    fig, axis = plt.subplots(nrows=1)

    contour  = axis.contour(xi, yi, zi, levels=levels, linewidths=0.3, colors='k')
    contourf = axis.contourf(xi, yi, zi, levels=levels, cmap="RdBu_r")

    fig.colorbar(contourf, ax=axis, label=config['z_label'])

    cmap = cm.get_cmap('RdBu_r')
    if config['marker_size_is_jet_diameter']:
        s = data['jet_diameter']*4
    else:
        s = 10
    for i in range(len(x)):
        axis.scatter(x[i], y[i], s[i], marker='o', color=cmap((z[i]-zr[0])/(zr[1]-zr[0])),
                     edgecolors=(0, 0, 0), linewidths=0.3)
    axis.set(xlim=xr, ylim=yr)

    plt.xlabel(config['x_label'])
    plt.ylabel(config['y_label'])

    title = config["title"]
    plt.title(title)
    if title == '':
        print('You should provide a title.  Otherwise, the figure will not be saved.')
        plt.savefig('figures/'+title+'.jpg')

    # plt.show()

    return fig, axis, contour, contourf


def get_data_and_config(dataset=6):

    mask = []
    if dataset == 1:
        result_files = sorted(glob.glob('tempdata/2021/20210722/mh_cut1_01/*_analysis.h5'))
        mask = ['tempdata/2021/20210722/mh_cut1_01/070_analysis.h5',
                'tempdata/2021/20210722/mh_cut1_01/008_analysis.h5',
                'tempdata/2021/20210722/mh_cut1_01/013_analysis.h5',
                'tempdata/2021/20210722/mh_cut1_01/010_analysis.h5',
                'tempdata/2021/20210722/mh_cut1_01/019_analysis.h5',
                'tempdata/2021/20210722/mh_cut1_01/012_analysis.h5']
        title = 'mh_cut1_01'
    elif dataset == 2:
        result_files = sorted(glob.glob('tempdata/2021/20210724/mh_coarse_01/*_analysis.h5'))
        mask = ['tempdata/2021/20210724/mh_coarse_01/002_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/003_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/004_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/005_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/006_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/007_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/054_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/043_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/056_analysis.h5',
                'tempdata/2021/20210724/mh_coarse_01/055_analysis.h5']
        title = 'mh_coarse_01'
    elif dataset == 3:
        result_files = sorted(glob.glob('tempdata/2021/20210724/mh_cut1_01/*_analysis.h5'))
        mask = ['tempdata/2021/20210724/mh_cut_01/015_analysis.h5']
        title = 'mh_cut1_01_d2'
    elif dataset == 4:
        result_files = sorted(glob.glob('tempdata/2021/20210724/mh_cpf_01/*_analysis.h5'))
        mask = ['tempdata/2021/20210724/mh_cpf_01/015_analysis.h5',
                'tempdata/2021/20210724/mh_cpf_01/010_analysis.h5']
        title = 'mh_cpf_01'
    elif dataset == 5:
        result_files = sorted(glob.glob('tempdata/2021/20210727/mh_cut2_01/*_analysis.h5'))
        mask = ['tempdata/2021/20210727/mh_cut2_01/033_analysis.h5']
        title = 'mh_cut2_01'
    elif dataset == 6:
        result_files = sorted(glob.glob('tempdata/2021/20210729/mh_coarse_02/*_analysis.h5'))
        mask = ['tempdata/2021/20210729/mh_coarse_02/062_analysis.h5',
                'tempdata/2021/20210729/mh_coarse_02/132_analysis.h5',
                'tempdata/2021/20210729/mh_coarse_02/054_analysis.h5',
                'tempdata/2021/20210729/mh_coarse_02/057_analysis.h5']
        title = 'mh_coarse_02'
    elif dataset == 7:
        result_files = sorted(glob.glob('tempdata/2021/20210729/mh_cpf_02/*_analysis.h5'))
        mask = ['tempdata/2021/20210729/mh_cpf_02/030_analysis.h5',
                'tempdata/2021/20210729/mh_cpf_02/029_analysis.h5',
                'tempdata/2021/20210729/mh_cpf_02/007_analysis.h5']
        title = 'mh_cpf_02'
    else:
        print('dataset not recognized')
        sys.exit()

    # compile data ------------------------------------------------------------

    data = compile_data(result_files, mask)

    # plot configs ------------------------------------------------------------

    config = {
        'title': title,  # Title of the plot
        'x_key': 'liquid_flow',  # What hdf5 key to use for x coordinate
        'x_label': 'Liquid flow (ul/min)',  # What to label the x coordinate
        'x_range': (0, 60),  # Ranges (use None if not sure)
        'y_key': 'gas_flow',
        'y_label': 'Gas flow (mg/min)',
        'y_range': (0, 85),
        'z_key': 'jet_speed',
        'z_label': 'Jet speed (m/s)',
        'z_range': (10, 120),
        'level_step': 5,  # One contour per step
        'marker_size_is_jet_diameter': True
    }

    return data, config


if __name__ == '__main__':

    DATASET = 6
    PLOT_UPDATE_PERIOD = 5    # seconds

    # initial contour plot setup ----------------------------------------------
    # plt.ion()
    data, config = get_data_and_config(dataset=DATASET)
    fig, axis, contour, contourf = make_contours(data, config)
    # fig.canvas.draw()
    # fig.canvas.flush_events()
    plt.show()
    # regularly update plot ---------------------------------------------------

    while True:
        # data, config = get_data_and_config(dataset=DATASET)
        # update_contour_plot(fig, axis, data, config)
        # plt.pause(PLOT_UPDATE_PERIOD)
        time.sleep(PLOT_UPDATE_PERIOD)
