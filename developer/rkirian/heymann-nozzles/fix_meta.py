import sys
import glob
import configparser
import datetime
import pandas
import numpy as np
liqlogfiles = sorted(glob.glob('rawdata/logs/20210722*sensirion*'))
gaslogfiles = sorted(glob.glob('rawdata/logs/20210722*Bronkhors*'))
metafiles = sorted(glob.glob('rawdata/2021/20210722/mh_cut1_01/*.txt'))
liqdf = pandas.concat(pandas.read_csv(f) for f in liqlogfiles)
gasdf = pandas.concat(pandas.read_csv(f) for f in gaslogfiles)
liqdf.rename(columns=lambda x: x.strip(), inplace=True)
gasdf.rename(columns=lambda x: x.strip(), inplace=True)
liq_reading = liqdf['reading'].values
liq_time = liqdf['time'].values
gas_reading = gasdf['reading'].values
gas_time = gasdf['time'].values
for f in metafiles:
    print(f)
    c = configparser.ConfigParser()
    c.read(f)
    year = c['timestamp']['year']
    date = c['timestamp']['date']
    time = c['timestamp']['time']
    year = int(year)
    month = int(date[4:6])
    day = int(date[6:8])
    hour = int(time[0:2])
    minute = int(time[2:4])
    second = int(time[4:6])
    dt = datetime.datetime(year, month, day, hour, minute, second)
    t = dt.timestamp()
    liq_interp = np.interp(t, liq_time, liq_reading)
    gas_interp = np.interp(t, gas_time, gas_reading)
    c['fluidics']['gas'] = '%.3f mg/min' % gas_interp
    c['fluidics']['liquid'] = '%.3f microliter/minute' % liq_interp
    c['optics']['pixelsize_um'] = '0.8476'
    with open(f, 'w') as configfile:
        c.write(configfile)


# fudge = np.loadtxt('fudge.csv', delimiter=',')
# run = fudge[:, 0]
# pix = fudge[:, 1]
# gas = fudge[:, 2]
# liq = fudge[:, 3]
# for r in run:
#     f = files[i]
#     f = '%03d.txt' % r
#     c = configparser.ConfigParser()
#     c.read(f)
#     p = pix[i]
#     g = gas[i]
#     l = liq[i]
#     c['fluidics']['gas'] = '%g mg/min' % g
#     c['fluidics']['liquid'] = '%g microliter/minute' % l
#     c['optics']['pixelsize_um'] = '%g' % p
#     with open(f, 'w') as configfile:
#         c.write(configfile)
#
#
