#!/bin/bash

# What to move off of xfel1 (from within microjets)
dir=$1
if [ "$dir" = '' ]; then
   dir='2021/20210803'
fi

# from xfel1 to analysis laptop
SOURCEDIR="raklab@10.206.48.2:/data/rawdata/microjets/./$dir"
DESTDIR="./rawdata"

while [ true ]; do
    rsync -aRv --stats --progress $SOURCEDIR $DESTDIR
    echo "Waiting 5 seconds..."
    sleep 5
done
