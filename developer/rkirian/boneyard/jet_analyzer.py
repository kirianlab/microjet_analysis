#!/bioxfel/user/rkirian/local/miniconda/bin/python
import sys
import os
import argparse
import glob
from time import time, sleep
import datetime
import numpy as np
from skimage import io
from numba import jit
from scipy.signal import savgol_filter, medfilt
import configparser
from skimage.filters import gaussian
import h5py
#import pandas as pd
# from libtiff import TIFF


def write(msg):
    sys.stdout.write(msg)
    sys.stdout.flush()

REJECT = False

#===========================================================
# Load special ini file created by Sahba's software.  It is a txt file that
# has the same name as the hdf5 file (but with ".txt" extension instead of ".h5")
#============================================================
def load_sahba_meta(meta_file):
    year = ""
    date = ""
    _time = ""
    gas_flow = None
    liquid_flow = None
    if os.path.exists(meta_file):
        write('Loading meta file %s\n' % meta_file)
        meta = configparser.ConfigParser()
        meta.read(meta_file)
        if 'timestamp' in meta:
            year = meta['timestamp']['year']
            date = meta['timestamp']['date']
            _time = meta['timestamp']['time']
        if 'fluidics' in meta:
            fluidics = meta['fluidics']
            if 'gas' in fluidics:
                g = fluidics['gas']
                if g != '':
                    gas_flow = float(g)
            if 'liquid' in fluidics:
                g = fluidics['liquid']
                if g != '':
                    liquid_flow = float(g)
    return year, date, _time, gas_flow, liquid_flow

#======================================================================
# Load log files with flow rates written by sahba's software.  These seem to 
# go into a common logs directory.  Not sure if this is stable, but it's 
# hard-coded here.  There is no error checking, so you might need to fix
# this to deal with missing log files.
#======================================================================
def load_sahba_logs(log_files_regex):
    file_names = glob.glob(log_files_regex)
    if len(file_names) == 0:
        print("Cannot find the log file %s" % (file_name,))
        return None, None, None, None, None
    epoch_sec = []
    datetime = []
    gas_flow = []
    liquid_flow = []
    pixel_size = []
    delay = []
    for file_name in file_names:
        with open(file_name) as f:
            for line in f:
                if line[0] == 'E':
                    continue
                s = line.split(',')
                epoch_sec.append(float(s[0].strip()))
                datetime.append(s[1].strip())
                gas_flow.append(float(s[2].strip()))
                liquid_flow.append(float(s[3].strip()))
                pixel_size.append(float(s[7].strip()))
                delay.append(float(s[8].strip()))
    epoch_sec = np.array(epoch_sec)
    gas_flow = np.array(gas_flow)
    liquid_flow = np.array(liquid_flow)
    pixel_size = np.array(pixel_size)
    delay = np.array(delay)
    return epoch_sec, gas_flow, liquid_flow, pixel_size, delay

#=============================================
# This seems to be the fastest way to load thus far (doesn't work for 12-bit tiff...)
#===========================================
# def load_tiff(file_name):
#     tif = TIFF.open(file_name, mode='r')
#     nimage = 0
#     while (tif.SetDirectory(nimage)):
#         nimage += 1
#     tif.SetDirectory(0)
#     ex = tif.read_image()
#     a = np.empty((nimage, ex.shape[0], ex.shape[1]))
#     a[0, :, :] = ex
#     for i in range(nimage):
#         tif.SetDirectory(i)
#         a[i, :, :] = tif.read_image()
#     return a

#==============================================================================
# A special matrix shear operation for analyzing the jet cross correlations
#==============================================================================
@jit(nopython=True)
def shear(matrix):
    arr = np.empty_like(matrix)
    fast_scan, slow_scan = matrix.shape
    for i in range(slow_scan):
        for j in range(fast_scan):
            arr[i, j] = matrix[i, (j + i) % fast_scan]
    return arr

#=============================================================
# A special function for finding droplet thresholds
# *  The basic idea is that we do a brute-force scan through threshold levels.  As we raise the level, we expect a
#    rather sudden drop in the number of pixels above threshold when we rise above the background.  We find this point,
#    and the rate at which the drop happens, but taking the derivative of the counts above threshold as a function of
#    the threshold level.
#=============================================================
def get_thresh(image, n_fwhm=3.5, mask=None):
    mimage = image.copy()
    if mask is None:
        mask = np.ones_like(mimage)
    mimage = mimage.flat[mask.flat != 0]    # Right away, we throw out all masked pixels.
    im_min = np.percentile(mimage, 1)       # Start the threshold scan at the 1% level of image values
    im_max = np.percentile(mimage, 99.999)  # End the scan at the 99.999% level
    thresholds = im_min + (im_max - im_min)*np.arange(0, 1, 0.001)
    counts = np.zeros_like(thresholds)
    for i in range(thresholds.shape[0]):    # Brute force scan of threshold value
        counts[i] = np.sum(mimage > thresholds[i])
    counts /= mimage.size                   # Turn counts into a fraction of pixels above threshold
    cnt_grad = savgol_filter(np.gradient(counts), 51, 3)  # Smooth the function (else our derivative is wild)
    cnt_grad = - cnt_grad / np.max(-cnt_grad)  # Normalize gradient, and invert.
    w = np.where(cnt_grad.flat > 0.5*np.max(cnt_grad.flat))[0]
    fwhm = len(w)*(thresholds[1] - thresholds[0])
    best_threshold = np.median(thresholds[w]) + n_fwhm * fwhm
    return best_threshold, cnt_grad, thresholds, counts, mask

#=============================================================
# Special function for fitting lines to the jet
#=============================================================
def analyze_jet(image, thresh, x, y):
    fail = True
    wjet = np.where((image).flat > thresh)[0]
    if len(wjet) == 0:
        return None, None, None, None, None, None, None, None, None, fail
    fail = False
    xj, yj = x.flat[wjet], y.flat[wjet]
    slope, yint = np.polyfit(xj, yj, 1)  # Unweighted linear fit to positions of all pixels above threshold
    xf = np.arange(image.shape[0])
    yf = yint + slope*xf
    ang = np.sign(slope) * np.arctan(slope)  # What's the sign for?  I can't recall
    u1 = np.array([np.cos(ang),  np.sin(ang)])  # Unit vector along jet direction
    u2 = np.array([-np.sin(ang), np.cos(ang)])  # Unit vector orthogonal to jet direction
    v = np.vstack([xj, yj - yint]).T
    v1 = v.dot(u1)  # Coordinates along jet direction
    v2 = v.dot(u2)  # Coordinates orthogonal to jet direction
    nbins = image.shape[0]
    p1, bin_edges = np.histogram(v1, bins=nbins, range=[0, nbins])  # Projection along jet direction
    p2, bin_edges = np.histogram(v2, bins=nbins, range=[0, nbins])  # Projection orthogonal jet direction
    return slope, yint, v1, v2, p1, p2, xf, yf, ang, fail


def main_analysis(config):

    #==========================================
    # Argument parsing
    #===========================================
    # Defaults (None => attempt to determine parameter algorithmically)
    is_darkfield = config['darkfield']
    file_name = config['file']
    thresh_sigma = config['thresh_sigma']
    display = config['display']
    interactive = config['interactive']
    reuse = config['results']
    delay = None
    pixel_size = None # exception here~
    gas_flow = None
    liquid_flow = None
    logs = None
    meta = None
    back_even = None
    back_odd = None
    save_results = True
    jet_crop = None
    nozzle_pos = None
    drop_pos = None
    droplet_threshold = None
    if interactive:
        display = True
    print('File name: %s' % (file_name,))
    file_no_extension, file_extension = os.path.splitext(file_name)
    directory, _ = os.path.split(file_name)
    if directory.count('rawdata') != 1:
        print('Data path must have exactly one "rawdata" sub-directory')
        sys.exit()
    save_file = file_name.replace('rawdata', 'tempdata') + '.jet_analyzer.npz'
    if os.path.exists(save_file) and not config['overwrite']:
        print('The results file already exists: %s' % (save_file,))
        print('Use the --overwrite flag if you want to clobber the results')
        print('Exiting')
        sys.exit(0)
    # Load up previous results on request (try to keep this simple...)
    if reuse:
        if os.path.isfile(save_file):
            print('Loading previous results...')
            try:
                dat = np.load(save_file)
                jet_crop = dat['jet_crop']
                back_even = dat['back_even']
                back_odd = dat['back_odd']
                droplet_threshold = dat['droplet_threshold']
            except KeyError:
                print('Cant load previous results... old format?')
                reuse = False
    nozzle_pos = config['nozzle_pos']
    drop_pos = config['drop_pos']
    # Manually set some parameters (override saved values, if we requested to recall them)
    if config['droplet_roi'] is not None:
        jet_crop = tuple([int(n) for n in config['droplet_roi'].replace(':', ',').split(',')])
    if config['nozzle_pos'] is not None:
        nozzle_pos = config['nozzle_pos']
    if config['drop_pos'] is not None:
        drop_pos = config['drop_pos']
    if config['droplet_threshold'] is not None:
        droplet_threshold = config['droplet_threshold']
    if config['thresh_sigma'] is not None:
        thresh_sigma = config['thresh_sigma']
    if display:
        # pyqtgraph causes failure on Agave under some circumstances, so we need to remove all traces of it
        # when we run in a non-interactive setting.
        print('Preparing for GUI displays...')
        import pyqtgraph as pg
        class ImWin(pg.ImageView):
            reject = False
            def __init__(self, *args, **kargs):
                self.app = pg.mkQApp()
                self.win = pg.QtGui.QMainWindow()
                self.win.resize(1000, 800)
                if 'title' in kargs:
                    self.win.setWindowTitle(kargs['title'])
                    del kargs['title']
                pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
                self.setImage(*args, **kargs)
                self.win.setCentralWidget(self)
                for m in ['resize']:
                    setattr(self, m, getattr(self.win, m))
                self.setPredefinedGradient('flame')
                self.win.show()
            def add_plot(self, *args, **kwargs):
                return self.getView().plot(*args, **kwargs)
            def add_line(self, *args, **kwargs):
                p = kwargs['position']
                del kwargs['position']
                line = pg.InfiniteLine(*args, **kwargs)
                line.setPos([p, p])
                self.getView().addItem(line)
                return line
            def keyPressEvent(self, ev):
                # print(ev.key())
                if ev.key() == 82:
                    print('Reject triggered')
                    self.reject = True
                    self.app.quit()
                if ev.key() == 32:  # spacebar
                    self.app.quit()
                if ev.key() == 81:  # q button
                    print('\nQuit\n')
                    sys.exit()
            def set_mask(self, mask, color=None):
                d = mask
                if color is None:
                    color = (255, 255, 255, 20)
                mask_rgba = np.zeros((d.shape[0], d.shape[1], 4))
                r = np.zeros_like(d)
                r[d == 0] = color[0]
                g = np.zeros_like(d)
                g[d == 0] = color[1]
                b = np.zeros_like(d)
                b[d == 0] = color[2]
                t = np.zeros_like(d)
                t[d == 0] = color[3]
                mask_rgba[:, :, 0] = r
                mask_rgba[:, :, 1] = g
                mask_rgba[:, :, 2] = b
                mask_rgba[:, :, 3] = t
                im = pg.ImageItem(mask_rgba)
                self.getView().addItem(im)

    if display or interactive:
        print('\n' + '='*78 + '\n')
        print('NOTE:\n'
                '  * Press spacebar to close windows and continue analysis.\n'''
                '  * Press "q" to quit analysis (results will not be saved).\n')
        print('='*78 + '\n')

    #===================================================
    # Load up the data, prepare for saving results
    #===================================================
    write('Loading file... \n')
    t = time()
    if file_extension == '.tif':
        stack = io.imread(file_name).astype(np.single)
        stack_transpose = (0, 2, 1)
        stack = np.transpose(stack, stack_transpose)
    elif file_extension == '.h5':
        f = h5py.File(file_name, 'r')
        stack = np.array(f['frames'], dtype=np.double)
        stack_transpose = (0, 2, 1)
        stack = np.transpose(stack, stack_transpose)
        # If it's an hdf5 then perhaps Sahba's software created an ini file with flow rates...
        meta_file = file_no_extension + '.txt'
        if os.path.exists(meta_file):
            year, date, _time, gas_flow, liquid_flow = load_sahba_meta(meta_file)
            print(int(year), int(date[4:6]), int(date[6:8]), int(_time[0:2]), int(_time[2:4]), int(_time[4:6]))
            epoch_sec = datetime.datetime(int(year), int(date[4:6]), int(date[6:8]), int(_time[0:2]), int(_time[2:4]), int(_time[4:6])).timestamp()
            print(date, _time, epoch_sec)
            if gas_flow is None or liquid_flow is None:
                write('Cannot find fluidics data\n')
            log_directory = '/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/logs'
            write('Looking for log files in %s\n' % log_directory)
            log_regex = log_directory+'/'+date+'_*_all_data.txt'
            glb = glob.glob(log_regex)
            if len(glb) > 0:
                epoch_sec_all, gas_flow_all, liquid_flow_all, pixel_size, delay = load_sahba_logs(log_regex) # Kosta
                w = np.where(epoch_sec_all == epoch_sec)
                if len(w[0]) > 0:
                    gas_flow = np.mean(gas_flow_all[w])
                    liquid_flow = np.mean(liquid_flow_all[w])
                    delay = np.mean(delay[w])
                    pixel_size = np.mean(pixel_size[w])
        else:
            write('No meta file %s\n' % meta_file)
    else:
        print('File extension %s recognized.' % file_extension)
        sys.exit()
    dt = time() - t
    sys.stdout.write('%5g seconds (%5g per frame)\n' % (dt, dt/stack.shape[0]))
    # The following transpose is silly.  Why did I do this?

    # TODO: we should check here if the images is a darkfield or brightfield since each of these requires different
    #       image corrections.  One possible algorithm: make a cropped pixel-wise-maximum image, which should light up the
    #       jet/drops, and then check if the distribution is skewed toward lower or higher values.  Skewed lower => bright

    #===================================================
    # We read in all data at once because there does not seem to be
    # good ways to read in sub-blocks with python tif utilities.
    # Note that tiffs never exceed ~2Gb by definition.
    #===================================================
    n_frames = stack.shape[0]
    nx = stack.shape[1]
    ny = stack.shape[2]
    print('Found %d images of shape %d x %d' % stack.shape)
    # These are for fitting lines to the jet
    x, y = np.meshgrid(np.arange(nx), np.arange(ny), indexing='ij')

    #==================================================
    # Hold on to the std deviation of the original data stack.  It is useful for estimating jet length and jet position
    # *  Note that we only use even or odd frames because of the dual laser pulses; we don't want the variation
    #    in laser intensity to contribute to this standard deviation
    #==================================================
    stack0_std = np.std(stack[::2, :, :] / np.mean(stack[::2, :, :]), axis=0)
    stack0_std -= np.median(stack0_std)
    stack0_std /= np.max(np.abs(stack0_std))  # stupid scikit image: input must have values between -1 and 1.  WHY??

    #==========================================
    # Estimate the incident brightfield illumination profile
    # *  Treat evens and odds separately since they are different laser flashes and may have different pulse energies.
    # *  We take the illumination profile to be the 80th percentile of intensities.  This should be reasonable in the
    #    region where there are droplets, which are likely present roughly 50% of the time.  This is not a good estimate for
    #    regions where there a jet or anything else that is static.
    #==========================================
    write('Normalizing brightfield... ')
    t = time()
    brightfield_percentage = 80
    if back_even is None:
        back_even = np.percentile(stack[0:min(200, n_frames):2, :, :], brightfield_percentage, axis=0)
        back_odd = np.percentile(stack[1:min(200, n_frames):2, :, :], brightfield_percentage, axis=0)
    stack[0::2, :, :] = np.divide(stack[0::2, :, :], back_even, out=np.zeros_like(stack[0::2, :, :]), where=back_even != 0)
    stack[1::2, :, :] = np.divide(stack[1::2, :, :], back_odd, out=np.zeros_like(stack[1::2, :, :]), where=back_odd != 0)
    stack_median = np.median(stack[0:min(10, n_frames), :, :].ravel())
    stack -= stack_median
    if not is_darkfield:  # TODO: proper darkfield correction switch
        stack *= -1
    sys.stdout.write('%5g seconds\n' % (time()-t,))

    #============================================
    # Roughly estimate the jet location
    # *  Look at droplet variance in half the image.  Fit a line.
    #============================================
    write('Finding jet position... ')
    stack0_std_p0 = np.sum(stack0_std, axis=0)
    stack0_std_p0 -= np.median(stack0_std_p0)
    jet_pos = (np.where(stack0_std_p0 == np.max(stack0_std_p0))[0])[0]
    jet_fwhm = len(np.where(stack0_std_p0 > np.max(stack0_std_p0)/2.0)[0])
    write('%5g seconds\n' % (time()-t,))
    stack0_std_mask = np.ones_like(stack0_std)
    stack0_std_mask[int(nx/2):, :] = 0
    stack0_std_mask[:, 0:50] = 0
    stack0_std_mask[:, ny-50:ny] = 0
    sigthresh, _, _, _, stack0_std_mask = get_thresh(stack0_std, n_fwhm=3, mask=stack0_std_mask)
    slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = analyze_jet(stack0_std*stack0_std_mask, sigthresh, x, y)
    jet_width = len(p2[p2 > np.max(p2)/2])*2
    jet_pos = int(yint + slope*nx/2)
    jetx = np.array([0, nx])
    jety = yint + slope*jetx
    jetmask = np.ones_like(stack0_std)  # This mask is a narrow strip of ones that blocks out the jet
    jetmask[y -  yint - slope*x + 2*jet_width < 0] = 0
    jetmask[y -  yint - slope*x - 2*jet_width > 0] = 0
    # Now isolate the region with the jet and repeat the above to improve our estimate of the jet region.
    slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = analyze_jet(stack0_std*stack0_std_mask*jetmask, sigthresh, x, y)
    jet_width = len(p2[p2 > np.max(p2)/2])*2
    jet_pos = int(yint + slope*nx/2)
    jetx = np.array([0, nx])
    jety = yint + slope*jetx
    jetmask = np.ones_like(stack0_std)
    jetmask[y -  yint - slope*x + 2*jet_width < 0] = 0
    jetmask[y -  yint - slope*x - 2*jet_width > 0] = 0
    print('jet_width =', jet_width)

    #===========================================
    # Attempt to find the nozzle tip location
    # *  Empirically, I notice the standard deviation of the brightfield-corrected image is high where the nozzle is (why?).
    #===========================================
    write('Finding nozzle tip... ')
    t = time()
    jetmask2 = np.ones_like(stack0_std)  # This mask is two narrow strips of ones that are above and below the jet
    jetmask2[y -  yint - slope*x + 6*jet_width < 0] = 0
    jetmask2[y -  yint - slope*x - 6*jet_width > 0] = 0
    jetmask2 *= jetmask
    stack_std = np.std(stack[0:min(500, n_frames), :, :], axis=0)
    m = stack_std*jetmask2
    stack_std_p1 = (np.sum(m, axis=1).T - np.median(m, axis=1)).T
    stack_std_p1 -= np.median(stack_std_p1)
    stack_std_p1 = savgol_filter(stack_std_p1, 11, 3)
    stack_std_g1 = np.gradient(stack_std_p1)
    if nozzle_pos is None:
        g = stack_std_g1.copy()
        g[0:int(g.size/2)] = 0
        nozzle_pos = (np.where(g == np.max(g))[0])[0]
    nozzle_pos = min(nozzle_pos, nx)
    if nozzle_pos < nx/2:
        nozzle_pos = nx
    write('%5g seconds\n' % (time()-t,))

    #============================================
    # Attempt to find jet/drip transition location
    # *  Look for the decrease in the droplet variance along the length of the jet.
    #============================================
    write('Finding jet/drip transition... ')
    m = stack0_std*jetmask
    stack0_std_p1 = np.sum((m.T - np.median(m, axis=1)).T, axis=1)
    stack0_std_p1 = medfilt(stack0_std_p1, 101)
    stack0_std_p1 = savgol_filter(stack0_std_p1, 101, 3)
    stack0_std_g1 = np.gradient(stack0_std_p1)
    if drop_pos is None:
        a = stack0_std_g1[50:nozzle_pos-50]
        drop_pos = (np.where(a == np.min(a))[0])[0] + 50
    drop_pos = min(drop_pos, nx)
    write('%5g seconds\n' % (time()-t,))

    #=============================================
    # Configure droplet crop region
    #==============================================
    if jet_crop is None:
        wid = max(jet_width*2 + np.abs(jety[0]-jety[1]), jet_width*10)
        pos = (0, jet_pos - wid/2)
        size = (drop_pos, wid)
        jet_crop = (max(int(pos[0]), 0), min(int(pos[0] + size[0]), nx - 1),
                    max(int(pos[1]), 0), min(int(pos[1] + size[1]), ny - 1))
    else:
        pos = (jet_crop[0], jet_crop[2])
        size = (jet_crop[1]-jet_crop[0], jet_crop[3]-jet_crop[2])

    #============================================
    # Attempt to guess the droplet threshold.
    # *  As we increase the threshold, the is a sudden drop in the number of pixels above threshold once we rise above the
    #    background level.  We try to identify the jump by taking the derivative of the number of lit pixels as a function
    #    of the threshold level.
    #============================================
    write('Estimating droplet threshold... ')
    im = stack[0:min(10, stack.shape[0]), jet_crop[0]:jet_crop[1], jet_crop[2]:jet_crop[3]]
    droplet_threshold, cnt_grad, thrsh, cnts, mask = get_thresh(im, thresh_sigma)
    # Make sure there is at least half a single row of lit pixels
    crpim = stack[:, jet_crop[0]:jet_crop[1], jet_crop[2]:jet_crop[3]]
    frac = 100*(1-crpim.shape[1]/float(crpim.size))
    upper = np.percentile(crpim, frac)
    if droplet_threshold is None:
        droplet_threshold = min(droplet_threshold, upper)
    write('%5g seconds\n' % (time()-t,))

    if display:
        imwin_thresh = ImWin(stack, title="Choose threshold %s" % file_name, levels=(droplet_threshold, droplet_threshold * 1.1))
        roi = pg.ROI(pos=pos, size=size)
        roi.addScaleHandle([1, 1], [0, 0])
        roi.addScaleHandle([1, 0], [1, 0])
        roi.addScaleHandle([1, 0], [0, 1])
        roi.addScaleHandle([0, 1], [1, 0])
        imwin_thresh.getView().addItem(roi)
        pltmag = 0.1 * min(nx, ny)
        im = stack0_std
        imwin_stack0_std = ImWin(im, title='Stack Standard Deviation %s' % file_name, levels=(-0.015, 0.3))
        imwin_stack0_std.add_plot(jetx, jety+jet_width, pen='y')
        imwin_stack0_std.add_plot(jetx, jety-jet_width, pen='y')
        drop_line = imwin_stack0_std.add_line(position=drop_pos, movable=True, pen='r')
        imwin_stack0_std.add_plot(-pltmag * stack0_std_g1/ np.max(stack0_std_g1), pen='r')
        imwin_stack0_std.add_plot(-pltmag * stack_std_g1 / np.max(stack_std_g1), pen='g')
        nozzle_line = imwin_stack0_std.add_line(position=nozzle_pos, movable=True, pen='g')
        # imwin_stack0_std.add_plot(-pltmag*stack0_std_p0/np.max(stack0_std_p0), np.arange(len(stack0_std_p0)), pen='y')
        # jet_line = imwin_stack0_std.add_line(position=jet_pos, movable=True, angle=0, pen='y')
        # imwin_stack0_std.getView().addItem(roi)
        # p = pg.plot(thrsh, cnts, title='Lit pixels above threshold')
        # p.setLabel('left', 'Number of pixels above threshold')
        # p.setLabel('bottom', 'Threshold value')
        # p.plot(thrsh, cnt_grad, pen='r')
        # p.plot([droplet_threshold]*2, [0, np.max(cnt_grad)], pen='r')
        pg.QtGui.QApplication.processEvents()
        if interactive:
            print('')
            print("This is INTERACTIVE MODE.  Here's how to interact with the two windows that are open:")
            print('    * Drag vertical green line to the nozzle tip.')
            print('    * Drag vertical red line to the jet breakup point.')
            print("    * Modify the rectangular (ROI) so that it enclouses the droplet region.")
            print("    * Press "r" to abort this dataset but continue to the next.")
            pg.QtGui.QApplication.instance().exec_()
            if imwin_thresh.reject or imwin_stack0_std.reject:
                pg.QtGui.QApplication.instance().closeAllWindows()
                print('Dataset rejected.  Moving to next dataset.')
                return None
        nozzle_pos = nozzle_line.getPos()[0]
        drop_pos = drop_line.getPos()[0]
        # jet_pos = jet_line.getPos()[0]
        droplet_threshold = imwin_thresh.getLevels()[0]
        jet_crop = (max(int(roi.pos()[0]), 0), min(int(roi.pos()[0] + roi.size()[0]), nx - 1),
                max(int(roi.pos()[1]), 0), min(int(roi.pos()[1]+roi.size()[1]), ny-1))

    print('nozzle_pos =', nozzle_pos)
    print('jet_pos =', jet_pos)
    print('drop_pos = ', drop_pos)
    print('droplet_threshold =', droplet_threshold)
    print('jet_crop = %d:%d,%d:%d' % jet_crop)

    # Mask the region outside of the ROI, for jet line fitting
    mask = np.zeros((nx, ny))
    mask[jet_crop[0]:jet_crop[1], jet_crop[2]:jet_crop[3]] = 1
    # Also mask any pixels that are consistently above threshold (say, 80% of the time)
    above = np.sum(stack > droplet_threshold, axis=0)/float(stack.shape[0])
    mask[above > 0.80] = 0
    # pg.image(mask, title='mask')

    # Prepare for viewing the line fits
    if display:
        imwin_jetline = ImWin(np.zeros((nx, ny)), title='Jet line fit %s' % file_name)
        imwin_jetline.set_mask(mask)
        imwin_jetline.getView().addItem(pg.ROI(pos=(jet_crop[0], jet_crop[2]),
                                           size=(jet_crop[1] - jet_crop[0], jet_crop[3] - jet_crop[2])))
        line_plt = imwin_jetline.getView().plot()
        imwin_jetline.setLevels(0, max(droplet_threshold * 2, imwin_jetline.getLevels()[1]))

    # Here comes the creation of the average outer product
    cc_matrix = np.zeros((nx, nx))
    prev_jet1d = None
    jet_angles = np.zeros(n_frames)
    jet_widths = np.zeros(n_frames)
    pmsg = ''
    for i in range(n_frames):
        image = stack[i, :, :]
        slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = analyze_jet(image * mask, droplet_threshold, x, y)
        if fail:
            msg = 'Line fit failed on frame %d' % (i,)
            sys.stdout.write('\b' * len(pmsg) + msg)
            pmsg = msg
            prev_jet1d = None  # If we're going to throw out frames, we need to account for that below.
            continue
        jet1d = p1
        jet_angles[i] = ang
        jet_widths[i] = np.sqrt(np.mean(v2**2))
        if i > 0:
            if prev_jet1d is not None:
                op = np.outer(jet1d, prev_jet1d)
                # Add on odds, subtract on evens.  One of these two provides an estimate of background correlations
                # that have no relation to droplet translations.  I don't know which is which; deal with that later.
                if i % 2 == 1:
                    cc_matrix += op
                else:
                    cc_matrix -= op
            if (i % 10 == 0) and display:
                line_plt.setData(xf, yf, pen='g')
                dispim = np.zeros_like(image)
                dispim[image > droplet_threshold] = 1
                imwin_jetline.setImage(dispim, autoLevels=False)
                pg.QtGui.QApplication.processEvents()
        prev_jet1d = jet1d
        msg = '%6.1f' % (np.std(v2),)
        sys.stdout.write('\b' * len(pmsg) + msg)
        pmsg = msg
    sys.stdout.write('\b' * len(pmsg))

    if display:  # Always hold open the final correlation matrix.  Press any key to kill all windows.
        pg.QtGui.QApplication.processEvents()

    # Now identify the offset of the off-diagonal correlation peak
    cc_matrix_mask = np.zeros_like(cc_matrix)
    cc_matrix_mask[jet_crop[0]:jet_crop[1], jet_crop[0]:jet_crop[1]] = 1
    cc_matrix *= cc_matrix_mask
    cc_matrix_projection = np.mean(shear(cc_matrix), axis=0)
    # We don't know if the first or second frame is the one that is delayed in time.  Flip the sign if necessary.
    sgn = np.sign(np.max(cc_matrix_projection) + np.min(cc_matrix_projection))
    cc_matrix_projection *= sgn
    cc_matrix *= sgn
    # This step gets rid of some systematic column- and row-wise correlations
    med0 = np.median(cc_matrix[jet_crop[0]:jet_crop[1], :], axis=0)
    med1 = np.median(cc_matrix[:, jet_crop[0]:jet_crop[1]], axis=1)
    cc_matrix -= med0
    cc_matrix = (cc_matrix.T - med1).T
    cc_matrix *= cc_matrix_mask
    cc_matrix_projection = np.mean(shear(cc_matrix), axis=0)

    #=================================================================
    # Try to find the peak in the cross correlation matrix
    #  * Needs proper centroiding
    #  * Even better, we should fit a curved line to get acceleration
    #=================================================================
    w = (np.where(cc_matrix_projection == np.max(cc_matrix_projection))[0])[0]
    ccm = cc_matrix
    n = ccm.shape[0]
    ccp = cc_matrix_projection
    rollby = np.int(n/2-w)
    ccpr = np.roll(ccp, rollby)


    slc = np.s_[max(0, w-5):min(w + 5, len(cc_matrix_projection))]
    droplet_translation = np.sum(np.arange(len(cc_matrix_projection))[slc] * cc_matrix_projection[slc]) / np.sum(cc_matrix_projection[slc])
    print('Droplet translation: %d (%5.1f)' % (w, droplet_translation))
    if display:
        imwin_ccmat = ImWin(ccm, title='Correlation matrix %s' % file_name)
        imwin_ccmat.setLevels(0, imwin_ccmat.getLevels()[1])
        imwin_ccmat.add_plot(-100 * ccp / np.max(ccp) - 50, np.arange(n), pen='g')
        imwin_ccmat.add_plot(np.arange(n), -100 * ccpr/np.max(ccpr)+50+n, pen='g')
        # imwin_ccmat.add_plot(np.arange(n), -100 * med1/np.max(med1) - 50, pen='r')
        # imwin_ccmat.add_plot(-100 * med0/np.max(med0) + 50 + n, np.arange(n), pen='r')
        imwin_ccmat.add_plot(np.arange(n-w), t+np.arange(n-w), pen='y')
        imwin_ccmat.add_plot(np.arange(n), np.arange(n), pen='w')
        pg.QtGui.QApplication.processEvents()
        pg.QtGui.QApplication.instance().exec_()
        if interactive:
            print('')
            print('Check that there is a clear streak in the correlation matrix.')
            print('')


    # def get_liquid_flow(log_file, desired_time):
    #     log_data = load(log_file)
    #     time = log_data[0, :]
    #     flow = log_data[1, :]
    #     flow = smooth(flow, filter_width=~1 second)
    #     return np.interp1d(flow, time, desired_time)  # wrong arguments...

    # Separate script that calls this one.  The "Crawler"
    # def crawler(directory):
    #     files = glob.glob(directory + '*.h5')
    #     wait ( 1 second)
    #



    # lflow = get_liquid_flow()
    # gflow = get...

    # delay = args.delay
    # pixel_size = args.pixel_size


    # Save our results for post-processing?
    if save_results:
        os.makedirs(os.path.dirname(save_file), exist_ok=True)
        np.savez(save_file,
                 file_name=file_name,
                 jet_crop=jet_crop,
                 droplet_threshold=droplet_threshold,
                 cc_matrix=cc_matrix,
                 cc_matrix_projection=cc_matrix_projection,
                 droplet_translation=droplet_translation,
                 back_even=back_even,
                 back_odd=back_odd,
                 stack_transpose=stack_transpose,
                 gas_flow=gas_flow,
                 liquid_flow=liquid_flow,
                 pixel_size=pixel_size,
                 delay=delay)

    print("\n***Saved at:", save_file, "\n")
    print('='*78)
    print('\n')


    print(gas_flow, liquid_flow, pixel_size, delay)
    print('jet speed:', droplet_translation*(pixel_size*1e-6)/(delay*1e-9))

    if display:
        try:
            pg.mkQApp().closeAllWindows()  # exit()
        except:
            pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Process liquid jet data.  Assumes two-flash pairs of sequential frames')
    parser.add_argument('files', nargs='+', help='File names or regular expressions for several files')
    parser.add_argument('-i', '--interactive', action='store_true',
                        help='Interactive mode.  Allows selection of ROIs and thresholds.')
    parser.add_argument('-d', '--display', action='store_true', help='Display images, even if not in interactive mode.')
    parser.add_argument('-r', '--results', action='store_true',
                        help='Reuse previous results for jet crop, threshold, etc.')
    parser.add_argument('-c', '--droplet_roi', type=str, default=None,
                        help='Choose droplet roi: must take form a:b,c:d or a,b,c,d')
    parser.add_argument('-o', '--overwrite', action='store_true', help='Overwrite if file exists, else do nothing')
    parser.add_argument('--logs_directory', type=str,
                        default='/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/logs',
                        help='Location of Sahba log files with flow rates, pixel size, laser delay')
    parser.add_argument('--require_logs', action='store_true',
                        help='Do not proceed without flow rates, pixel size, delay')
    parser.add_argument('--nozzle_pos', type=int, default=None, help='Choose where the nozzle tip is located')
    parser.add_argument('--drop_pos', type=int, default=None, help='Choose where the jet/drop transition is located')
    parser.add_argument('--droplet_threshold', type=float, default=None, help='Choose thresholding value')
    parser.add_argument('--thresh_sigma', type=float, default=3,
                        help='Choose thresholding sigma (as explained in code...)')
    parser.add_argument('--darkfield', type=bool, default=False, help='Attempt to analyze darkfield movie')
    args = parser.parse_args()


    files = []
    for a in args.files:
        file_path = glob.glob(a) # Put loop around this so it's const. checking, make 'crawler mode'
        for c in file_path:
            print(c)
            files.append(c)
    og_files = len(files)

    if og_files == 0:
        print('No files found.  Exiting.')
        sys.exit()

    for f in files:
        config = dict()
        config['file'] = f
        config['interactive'] = args.interactive
        config['display'] = args.display
        config['results'] = args.results
        config['droplet_roi'] = args.droplet_roi
        config['logs_directory'] = args.logs_directory
        config['require_logs'] = args.require_logs
        config['drop_pos'] = args.drop_pos
        config['nozzle_pos'] = args.nozzle_pos
        config['droplet_threshold'] = args.droplet_threshold
        config['thresh_sigma'] = args.thresh_sigma
        config['darkfield'] = args.darkfield
        config['overwrite'] = args.overwrite


        main_analysis(config)

    crawler = True
    while crawler:
        updated_files = []
        for a in args.files:
            file_path = glob.glob(a) # Put loop around this so it's const. checking, make 'crawler mode'
            for c in file_path:
                print(c)
                updated_files.append(c)
        new_files = len(updated_files) - og_files
        if new_files != 0:
            newest_files = updated_files[-2:]
            for f in newest_files:
                config = dict()
                config['file'] = f
                config['interactive'] = args.interactive
                config['display'] = args.display
                config['results'] = args.results
                config['droplet_roi'] = args.droplet_roi
                config['logs_directory'] = args.logs_directory
                config['require_logs'] = args.require_logs
                config['drop_pos'] = args.drop_pos
                config['nozzle_pos'] = args.nozzle_pos
                config['droplet_threshold'] = args.droplet_threshold
                config['thresh_sigma'] = args.thresh_sigma
                config['darkfield'] = args.darkfield
                config['overwrite'] = args.overwrite

                main_analysis(config)