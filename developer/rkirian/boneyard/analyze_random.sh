#!/bin/bash

# Ignore the following; they have very poor contrast
ignorethese='44_05_01_SC30I|44_06_01_SC30I|44_06_02_SC30I|44_06_03_SC30I'

cmd="python -W ignore brightfield2a.py $1 $2 $3 $4 $5 $6 $7 $8 $(find ../rawdata/microjets -name '*.tif' | grep -i sc30i | grep -Ev $ignorethese | sort -R | head -n 1)"
echo ''
echo $cmd
$cmd
