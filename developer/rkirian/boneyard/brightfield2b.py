import sys, os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

f = open('list.txt')

a = []
for line in f:
    dat = np.load(line.strip())
    t = dat['droplet_translation']
    a.append(t)
    print(t, line)
a = np.array(a)
n = len(a)
colors = cm.rainbow(np.linspace(0, 1, n))
plt.scatter(np.arange(1,n+1), a, color=colors)
plt.title(line.split('/')[-2])
plt.xlabel('file number')
plt.ylabel('displacement (pixels)')
plt.show()
