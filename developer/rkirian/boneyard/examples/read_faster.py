from PIL import Image
from libtiff import TIFF, TIFFfile
import numpy as np
from time import time


file_name = '../../rawdata/microjets/2018/20181109/old_faithful_.tif'

# First trial: just open the file one by one and read the whole image into memory:
if 0:
    t = time()
    im = Image.open(file_name)
    n_frames = min(100, im.n_frames)
    h, w = im.size
    for i in range(n_frames):
        im.seek(i)
        dat = np.array(im.getdata()).reshape((h, w))  # TODO: possibly h and w are reversed (testing on 1024x1024 image...
    print("Read the whole frame: %.1f ms per image (%d images total)" % ((time()-t)/n_frames*1e3, n_frames))

# Second trial: try to crop before reading.  Does this only read in the cropped region?
t = time()
im = Image.open(file_name)
n_frames = min(100, im.n_frames)
h, w = im.size
h = int(np.ceil(h / 10))  # We will read 1/10th of the region, for a total of 1/100th of the total image.  Therefore I
w = int(np.ceil(w / 10))  # expect a ~100-fold speedup if we only read 1/100th of the data into memory.  Let's see...
for i in range(n_frames):
    im.seek(i)
    # Note that we use im.crop before reading the frame:
    dat = np.array(im.crop((0, 0, h, w)).getdata()).reshape((h, w))
assert np.max(np.abs(dat.shape - np.array((h, w)))) == 0  # Check that we really get images of the right shape
assert np.max(dat) > 0  # Check that we actually read in some data
print("Read cropped frame: %.1f ms per image (%d images total)" % ((time()-t)/n_frames*1e3, n_frames))

# Now try libtiff
t = time()
tiff = TIFFfile(file_name)
samples, sample_names = tiff.get_samples()
outList = []
n_frames = 0
for sample in samples:
    outList.append(np.copy(sample)[...,np.newaxis])
    n_frames += 1
out = np.concatenate(outList,axis=-1)
print("libtiff whole frame: %.1f ms per image (%d images total)" % ((time()-t)/n_frames*1e3, n_frames))

# Output on Rick's macpro
# Read the whole frame: 128.7 ms per image (100 images total)
# Read cropped frame: 4.5 ms per image (100 images total)
