#!/bin/bash

unison-2.48 -auto -ignore='Name {*.swp,*.log,*.so,build,*.pyc,*cache*,results}' $1 \
       -servercmd /home/rkirian/work/local/bin/unison \
       /home/rkirian/work/projects/nozzles/sahba-software/microjet-analysis/ \
       ssh://agave//bioxfel/user/rkirian/microjet-analysis
