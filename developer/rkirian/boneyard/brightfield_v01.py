import sys
import numpy as np
import pyqtgraph as pg
from skimage import io
from numba import jit


@jit(nopython=True)
def shear(matrix):
    arr = np.empty_like(matrix)
    fast_scan, slow_scan = matrix.shape
    for i in range(slow_scan):
        for j in range(fast_scan):
            arr[i, j] = matrix[i, (j + i) % fast_scan]
    return arr

class ImWin(pg.ImageView):
    def __init__(self, *args, **kargs):
        pg.mkQApp()
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(800, 600)
        if 'title' in kargs:
            self.win.setWindowTitle(kargs['title'])
            del kargs['title']
        pg.ImageView.__init__(self, self.win, view=pg.PlotItem())
        self.setImage(*args, **kargs)
        self.win.setCentralWidget(self)
        for m in ['resize']:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient('flame')
        self.win.show()


perc = 80
interactive = False
if 'interactive' in sys.argv: interactive = True
if len(sys.argv) == 1: sys.exit() 
file_name = [s for s in sys.argv if 'rawdata' in s][0] 
#file_name = '../rawdata/microjets/2017/20170906/44_08_01_SC30I/0026.tif'
save_file = file_name.replace('rawdata', 'tempdata').replace('.tif', '.tif.brightfield_v01.npz')
stack = io.imread(file_name).astype(np.single)
stack = np.transpose(stack, (0, 2, 1))
#stack = stack[:100,:,100:800]
back0 = np.percentile(stack[0::2, :, :], perc, axis=0)
back1 = np.percentile(stack[1::2, :, :], perc, axis=0)
stack[0::2, :, :] = np.divide(stack[0::2, :, :], back0, out=np.zeros_like(stack[0::2, :, :]), where=back0 != 0)
stack[1::2, :, :] = np.divide(stack[1::2, :, :], back1, out=np.zeros_like(stack[1::2, :, :]), where=back1 != 0)
stack = -(stack - np.median(stack.ravel()))
im = ImWin(stack, title='Choose ROI and threshold')
thresh = np.percentile(stack.ravel(), 99.5)
im.setLevels(thresh, max(thresh*1.1, im.getLevels()[1]))
roi = pg.ROI(pos=(0, stack.shape[2]*1/4), size=(stack.shape[1]*3/4, stack.shape[2]*3/4))
roi.addScaleHandle([1, 1], [0, 0])
im.getView().addItem(roi)
if interactive: pg.QtGui.QApplication.instance().exec_()
thresh = im.getLevels()[0]
c = (max(int(roi.pos()[0]), 0), min(int(roi.pos()[0]+roi.size()[0]), stack.shape[1]-1),
     max(int(roi.pos()[1]), 0), min(int(roi.pos()[1]+roi.size()[1]), stack.shape[2]-1))
print(perc, thresh, c)
mask = np.zeros(stack.shape[1:])
mask[c[0]:c[1], c[2]:c[3]] = 1
#im = ImWin(stack)
#im.getView().addItem(pg.ROI(pos=(c[0], c[2]), size=(c[1]-c[0], c[3]-c[2])))
#proj_plt = im.getView().plot()
#line_plt = im.getView().plot()
#im.setLevels(thresh, max(thresh*1.1, im.getLevels()[1]))
x, y = np.meshgrid(np.arange(stack.shape[1]), np.arange(stack.shape[2]), indexing='ij')
prev1D = 0
ccmat = np.zeros((stack.shape[1], stack.shape[1]))
#im2 = ImWin(ccmat)
for i in range(stack.shape[0]):
    wjet = np.where((stack[i, :, :]*mask).flat > thresh)[0]
    xj, yj = x.flat[wjet], y.flat[wjet]
    slope, yint = np.polyfit(xj, yj, 1)
    xf = np.arange(stack.shape[1])
    yf = yint + slope*xf
    ang = np.sign(slope) * np.arctan(slope)
    u1 = np.array([np.cos(ang),  np.sin(ang)])
    u2 = np.array([-np.sin(ang), np.cos(ang)])
    v = np.vstack([xj, yj - yint]).T
    v1 = v.dot(u1)
    v2 = v.dot(u2)
    nbins = stack.shape[1]
    jet1D, bin_edges = np.histogram(v1, bins=nbins, range=[0, nbins])
    if i > 0:
        op = np.outer(jet1D, prev1D)
        if i % 2 == 1:
            ccmat += op
        else:
            ccmat -= op
#        if i % 20 == 0:
#            im2.setImage(ccmat)
#            proj_plt.setData(bin_edges[1:], jet1D, pen='g')
#            line_plt.setData(xf, yf, pen='r')
#            im.setImage(stack[i, :, :], autoLevels=False)
#            pg.QtGui.QApplication.processEvents()
    prev1D = jet1D
shearmat = shear(ccmat)
#shearmat = np.cumsum(shearmat, axis=0)/np.outer(np.arange(1,shearmat.shape[0]+1), np.ones(shearmat.shape[1]))
#shearmat = np.abs(shearmat)
im = ImWin(shearmat)
roi = pg.ROI(pos=(0, 0), size=(stack.shape[1]*3/4, stack.shape[2]/3))
roi.addScaleHandle([1, 1], [0, 0])
im.getView().addItem(roi)
if interactive: pg.QtGui.QApplication.instance().exec_()
cc = (max(int(roi.pos()[0]), 0), min(int(roi.pos()[0]+roi.size()[0]), stack.shape[1]-1),
     max(int(roi.pos()[1]), 0), min(int(roi.pos()[1]+roi.size()[1]), stack.shape[2]-1))
print(cc)
mat = shearmat[cc[0]:cc[1], cc[2]:cc[3]]
ccproj = np.mean(mat, axis=0)
ccproj *= np.sign(np.max(ccproj)+np.min(ccproj))
ccproj_plt = pg.plot(ccproj)
wpeak = np.where(ccproj == np.max(ccproj))[0]
ccproj_plt.plot([wpeak], ccproj[wpeak], pen=None, brush='g', size=ccproj[wpeak]*0.02)
if interactive: pg.QtGui.QApplication.instance().exec_()
np.savez(save_file, c=c, thresh=thresh, ccmat=ccmat, matproj=ccproj, cc=cc, perc=perc, wpeak=wpeak)
