import sys, os
import numpy as np
import pyqtgraph as pg
import matplotlib.pyplot as plt
import matplotlib.cm as cm

f = open('list.txt')

ps = []
maxs = []
for line in f:
    dat = np.load(line.strip())
    a = dat['matproj']
    ps.append(a)
    maxs.append(np.where(a == np.max(a))[0])
maxs = np.array(maxs)
n = len(ps)
img = np.zeros((n,500))
for i in range(n):
    m = ps[i].shape[0]
    img[i,0:m] = ps[i]/m
pg.image(img)

print(maxs)
colors = cm.rainbow(np.linspace(0, 1, n))
plt.scatter(np.arange(1,n+1), maxs, color=colors)
plt.title(line.split('/')[-2])
plt.xlabel('file number')
plt.ylabel('displacement (pixels)')
plt.show()
sys.exit(pg.QtGui.QApplication.instance().exec_())
