import time
import numpy as np
import multiprocessing


"""
Try to speed up a sequential analysis job when both file reading and analysis take a comparable amount of time.

We assume that multiple reads in parallel will not be much faster than sequential reads, due to hardware restrictions.  
Therefore our simple aim is to read and analyze in parallel (one reader process, and one analyzer process).
"""


class FileReader(object):

    """
    This is the low-level file reader (e.g. a PIL Image instance)
    """

    shape = (100, 100)
    data = None

    def __init__(self):

        pass

    def read_data(self, frame_n):

        time.sleep(0.1)

        self.data = {'image': np.ones(self.shape) * frame_n, 'index': frame_n}


def do_analysis(data):

    data['image'][0, 0] = -1


if __name__ == '__main__':

    reader = FileReader()

    t0 = time.time()

    for i in range(10):
        print('Getting frame %d' % i)
        data = reader.get_data(i)
        print('Image values: %5g' % data['image'][0, 0])
        do_analysis(data)
        print('Modified image value: %5g' % data['image'][0, 0])

    print('Total time: %5g' % time.time() - t0)

    # Now we have multiple identical readers
    readers = [FileReader() for i in range(2)]
    datas = [None] * 2
    processes = [None] * 2

    # Initialize
    processes[0] = multiprocess.

    n_frames = 10
    for i in range(n_frames):
        if i+1 < n_frames:
            print('Launch process to get frame %d' % i+1)
            processes[i % 1] =
        print('Getting frame %d' % i)
        data = reader.get_data(i)
        print('Image values: %5g' % data['image'][0, 0])
        do_analysis(data)
        print('Modified image value: %5g' % data['image'][0, 0])


