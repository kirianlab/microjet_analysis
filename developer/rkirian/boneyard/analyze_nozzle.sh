#!/bin/bash

searchstring=$1
[ "$searchstring" = "" ] && searchstring=44_08_02_SC30I

for i in $(find ../rawdata/microjets -name '*.tif' | grep -i $searchstring | sort); do
python -W ignore brightfield2a.py $2 $i
done

find ../tempdata/ -iname '*.npz' | grep $searchstring | sort > list.txt
python brightfield2b.py
