#!/usr/bin/env python

import sys
import numpy as np
from PIL import Image
import pyqtgraph as pg
from time import time
from skimage import io


# This is a hack -- below are cut-n-paste from Sahba's code.  (I just couldn't understand the overall structure
# and therfore couldn't tweak it for my purposes below.)


class ImReader(object):
    def __init__(self, file_name=None):
        self.file_name = file_name
        self.im = Image.open(file_name)
        self.n_frames = self.im.n_frames
        self.shape = self.im.size
        self.crop_region = None
        self.stack = None
    def get_frame(self, frame_number):
        if self.stack is None:
            return self.get_one_frame(frame_number)
        else:
            return self.get_frame_from_stack(frame_number)
    def get_one_frame(self, frame_number):
        self.im.seek(frame_number)
        if self.crop_region is not None:
            h, w = self.crop_region[3]-self.crop_region[1], self.crop_region[2]-self.crop_region[0]
            dat = np.array(self.im.crop(self.crop_region).getdata()).reshape((h, w))
        else:
            dat = np.array(self.im.getdata()).reshape(self.im.size)
        return dat
    def get_frame_from_stack(self, frame_number):
        if self.stack is None:
            self.get_whole_stack()
        dat = self.stack[frame_number, :, :].reshape(self.shape)
        if self.crop_region is not None:
            c = self.crop_region
            dat = dat[c[1]:c[3],c[0]:c[2]]
        return dat
    def get_whole_stack(self):
        self.stack = io.imread(self.file_name)
        return self.stack

class HardFilter(object):
    def __init__(self, shape=None, high_pass=None, low_pass=None):
        self.shape = shape
        self.high_pass = high_pass
        self.low_pass = low_pass
        self.setup()
    def setup(self):
        if self.shape is None:
            return
        shape = self.shape
        x = np.arange(-np.floor(shape[0] / 2), np.ceil(shape[0] / 2)).astype(np.int)
        y = np.arange(-np.floor(shape[1] / 2), np.ceil(shape[1] / 2)).astype(np.int)
        xx, yy = np.meshgrid(x, y, indexing='ij')
        rr = np.sqrt(xx**2 + yy**2)
        rmask = np.ones_like(rr)
        if self.high_pass is not None:
            rmask[rr < self.high_pass] = 0
        else:
            rmask[rr > self.low_pass] = 0
        self.R = np.fft.ifftshift(rmask)
    def filter(self, dat):
        if np.max(np.abs(np.array(dat.shape) - np.array(self.R.shape))) != 0:
            self.shape = dat.shape
            self.setup()
        return np.real(np.fft.ifft2(np.fft.fft2(dat)*self.R))


def pick_crop(dat):
    im = pg.image(dat)
    im.setLevels(0, np.max(dat)/3.)
    im.setPredefinedGradient('flame')
    roi = pg.ROI(pos=(0, 0), size=reader.shape)
    roi.addScaleHandle([1, 1], [0, 0])
    roi.addScaleHandle([0, 0], [1, 1])
    roi.addScaleHandle([0, 1], [1, 0])
    roi.addScaleHandle([1, 0], [0, 1])
    im.getView().addItem(roi)
    pg.QtGui.QApplication.instance().exec_()
    c = np.floor(np.array((roi.pos()[0], roi.pos()[1], roi.pos()[0] + roi.size()[0],
                           roi.pos()[1] + roi.size()[1]))).astype(np.int)
    return np.array((max(c[1], 0), max(c[0], 0), min(c[3], dat.shape[1]), min(c[2], dat.shape[0])))

if len(sys.argv) == 1:
	file_name = '../rawdata/microjets/2018/20180907/281/4-01.tif'
else:
	file_name = sys.argv[1]

reader = ImReader(file_name)
stack = reader.get_whole_stack().astype(np.float64)
#stack = -(stack - np.median(stack.ravel()))
perc = 80
back0 = np.percentile(stack[0::2,:,:], perc, axis=0)
back1 = np.percentile(stack[1::2,:,:], perc, axis=0)
stack[0::2,:,:] = np.divide(stack[0::2,:,:], back0, out=np.zeros_like(stack[0::2,:,:]), where=back0!=0)
stack[1::2,:,:] = np.divide(stack[1::2,:,:], back1, out=np.zeros_like(stack[1::2,:,:]), where=back1!=0)
stack = -(stack - np.median(stack.ravel()))
#reader.stack = stack
#dat = reader.get_frame(0)
#filtwidth = 0  # do nothing -- background correction better than filtering?
#filt = HardFilter(dat.shape, high_pass=filtwidth)
# dat = filt.filter(dat)
#reader.crop_region = 
print(pick_crop(stack))
#prev_levels = (0, np.max(dat)/3.)
#for i in range(reader.n_frames):
#    print(i)
#    dat = reader.get_frame(i)
##    dat = filt.filter(dat)
#    im = pg.image(dat)
#    im.setLevels(prev_levels[0], prev_levels[1])
#    im.setPredefinedGradient('flame')
#    pg.QtGui.QApplication.instance().exec_()
#    prev_levels = im.getLevels()
#
#print(dat.shape)
#
