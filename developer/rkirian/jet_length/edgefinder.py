import numpy as np
from numpy.fft import fft, ifft, fftshift, ifftshift
import matplotlib.pyplot as plt
dat = np.load("nozzle_projection_data.npz")
for k in dat.keys():
    print(k)
sdev = dat["sdev"]
meen = dat["mean"]
n_profiles, n_pixels = sdev.shape
print("n_profiles =", n_profiles)
print("n_pixels =", n_pixels)
# plt.imshow(sdev)
# plt.show()
# We are going to take a derivative of the smoothed sdev profile
# We'll do the smoothing and derivative in one step by convolving
# with a kernel like this:
#
#                |-------|
#                |       |
# -------|       |       |------------------
#        |       |
#        |-------|
#
# We use the convolution theorem with FFT's for speed
# Here is the kernel:
kernel = np.zeros(n_pixels*2)
dn = 60  # Tuning parameter: larger value, more smoothing
kernel[-dn:] = -1./dn
kernel[:dn] = 1./dn
kernel_ffts = np.conj(fft(kernel))
indices = np.arange(n_profiles)
# For testing - shuffle the order of analysis
np.random.shuffle(indices)
# Fingers crossed
for i in list(indices):
    sdev_profile_raw = sdev[i, :][::-1]
    mean_profile = meen[i, :][::-1]
    # Normalizing the sdev by the mean doesn't help...
    # sdev_profile[w] = sdev_profile[w]/mean_profile[w]
    # First non-zero element is the "nozzle position"
    nozzle_position = n_pixels - np.sum(np.cumsum(np.abs(sdev_profile_raw)) > 0)
    sdev_profile = sdev_profile_raw.copy()
    sdev_profile[0:nozzle_position+5] = 0
    sdev_profile -= np.median(sdev_profile[nozzle_position:])
    # Because we do FFT derivative, we must zero-pad the array
    profile_padded = np.pad(sdev_profile, (0, n_pixels))
    derivative_raw = np.real(ifft(fft(profile_padded) * kernel_ffts))[:n_pixels]
    # Ignore pixels nearby the nozzle and at the tail and of the profile
    derivative = derivative_raw.copy()
    derivative[0:nozzle_position + 50] = 0  # Tuning paramter
    derivative[-100:] = 0  # Tunin parameter
    # Now find the peak of the derivative and call it the transition point
    transition_point = np.argmax(derivative)
    plt.plot(mean_profile/np.max(mean_profile), color="k", label="mean")
    plt.plot(sdev_profile/np.max(sdev_profile), color='r', label="sdev")
    plt.plot(derivative_raw/np.max(derivative_raw), color='b', label="raw derivative")
    plt.plot(derivative/np.max(derivative_raw), color='g', label="derivative accepted")
    plt.axvline(nozzle_position, linewidth=5, color='r', label="nozzle position")
    plt.axvline(transition_point, linewidth=5, color='g', label="transition point")
    plt.ylim()
    plt.legend()
    plt.show()
