import os
from glob import glob
#from PIL import Image
from skimage import io
import numpy as np
import h5py
import shutil


files = glob('rawdata/2021/20210902/290_91/*.tif')

for f in files:
    stack = io.imread(f)
    fmod = f.replace('rawdata', 'rawdata_mod').replace('tif', 'h5')
    if True: #not os.path.isfile(fmod):
        os.makedirs(os.path.dirname(fmod), exist_ok=True)
        h5 = h5py.File(fmod, 'w')
        h5['/frames'] = stack
        h5.close()
        print('Wrote', fmod)
    fmod = fmod.replace('.h5', '.txt')
    if not os.path.isfile(fmod):
        shutil.copyfile('template.txt', fmod)
        print('Wrote', fmod)