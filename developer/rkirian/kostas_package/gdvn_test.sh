#!/bin/bash
export PYTHONPATH=$PYTHONPATH:$(cd ../../.. && pwd)
python ../../../scripts/gdvn_two_flash.py ../rawdata/mh_coarse_b2_n1/* --view \
                        --savedir_replace=rawdata,tempdata \
                        --overwrite_results
