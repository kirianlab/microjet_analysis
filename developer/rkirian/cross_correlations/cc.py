import numpy as np
import h5py
import pyqtgraph as pg
from microjet_analysis.viewers import ImageView
# Get stack of binary images
s = np.array(h5py.File('temp.h5')['2D_images']['thresholded_stack'])
ns, nf = s.shape[1:]
# Remove jet? Works ok without this step, but best if it can be done.
# s[:, 820:, :] *= 0
# Pad to avoid wrap-around effects
ssum = np.pad(np.sum(s, axis=0), ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))
# Get the mean of cross-correlations
ccsum = 0
for i in range(0, s.shape[0], 2):
    print(i)
    im1 = np.pad(s[i, :, :].copy(), ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))
    im2 = np.pad(s[i+1, :, :].copy(), ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))
    ccsum += np.real(np.fft.ifftn(np.fft.fftn(im1)*np.conj(np.fft.fftn(im2))))
# Auto-correlation of the mean, for background correction (approximately)
acsum = np.real(np.fft.ifftn(np.abs(np.fft.fftn(ssum))**2))
# Approximate background correction: subtract auto-correlation of the mean from the mean of cross-correlations
cc = np.fft.fftshift(ccsum/np.sum(ccsum) - acsum/np.sum(acsum))
# Remove zero-padding
cc = cc[int(ns/2):ns+int(ns/2), int(nf/2):nf+int(nf/2)]
# Find the peak of the CC.  That's the most common drop displacement.
w = np.where(cc == np.max(cc))
pf = w[1][0] - np.floor(nf/2)
ps = w[0][0] - np.floor(ns/2)
print('displacement =', np.sqrt(pf**2 + ps**2), 'pixels')
print('angle =', np.arctan2(ps, pf)*180/np.pi - 90, 'degrees')
# Display stuff
imv = ImageView(cc, fs_lims=(-np.floor(nf/2), np.floor(nf/2-0.5)), ss_lims=(-np.floor(ns/2), np.floor(ns/2-0.5)))
imv.add_line(position=0, pen=[50, 50, 50])
imv.add_line(position=0, angle=0, pen=[50, 50, 50])
imv.add_plot([ps], [pf], symbol='o', symbolBrush=None, symbolPen='r')
imv.show()
pg.mkQApp().exec_()
# A better method would be a "masked, normalized cross correlation", which someone surely invented already.  I have
# a note on this that I can dig up.
