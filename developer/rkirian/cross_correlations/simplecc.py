import h5py
import numpy as np
import pyqtgraph as pg
from microjet_analysis.viewers import image_view
app = pg.mkQApp()
def pad_array(a):
    n, m = a.shape[-2:]
    n = int(n/2)
    m = int(m/2)
    return np.pad(a, [[n, n], [m, m]])
def cross_correlation(a, b):
    return np.real(np.fft.ifftn(np.fft.fftn(a)*np.conj(np.fft.fftn(b))))
f = h5py.File("/home/rkirian/Downloads/002.h5")
stack = np.array(f["frames"]).astype(np.float32)
# stack = stack[:, :, 0:500]
# s = 20
# n, nx, ny = stack.shape
# g = 0
# for i in range(0, n):
#     print(i)
#     a = stack[i, :, :]
#     a -= np.median(a, axis=0)
#     a = np.pad(a, [[0, s*n], [0, 0]])
#     g += np.roll(a, i*s, axis=0)
# image_view(g)
# # pg.image(stack)
# pg.mkQApp().exec_()
mean = np.mean(stack, axis=0)
mean = pad_array(mean)
acf = cross_correlation(mean, mean)
n = stack.shape[0]
# Correlate evens with even + 1 frames
cc = 0
for i in range(0, n, 2):
    print(i)
    a = pad_array(stack[i, :, :])
    b = pad_array(stack[i+1, :, :])
    cc += cross_correlation(a, b)
# Correlate odds with odd+1 frames
cc2 = 0
for i in range(1, n-1, 2):
    print(i)
    a = pad_array(stack[i, :, :])
    b = pad_array(stack[i+1, :, :])
    cc2 += cross_correlation(a, b)
cc /= np.mean(cc)
acf /= np.mean(acf)
cc2 /= np.mean(cc2)
cc -= acf  # This seems to be the best subtraction method
cc2 -= acf
cc = np.fft.fftshift(cc)
cc2 = np.fft.fftshift(cc2)
cc3 = cc - cc2  # Thought this might be clever but not as good as above
if abs(np.min(cc3)) > np.max(cc3):
    cc3 *= -1
pg.image(cc)
pg.image(cc2)
pg.image(cc3)
y = np.mean(cc, axis=0)
x = np.arange(len(y)) - len(y)/2
pg.plot(x, y)
y2 = np.mean(cc2, axis=0)
pg.plot(x, y2)
y3 = np.mean(cc3, axis=0)
pg.plot(x, y3)




pg.mkQApp().exec_()
