# First install miniconda
import sys
import os
import numpy as np  # conda install numpy
from numpy.fft import fft, ifft
from pyqtgraph import QtGui, QtCore
import pyqtgraph as pg  # conda install -c conda-forge pyqtgraph
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
from skimage.filters import threshold_otsu, threshold_local
from jet_analysis import dataio, gui


class Analyzer:
    r""" Class for analyzing sequential jet images to determine jet speed. """
    config = None
    imstack = None
    raw_stack = None
    cropped_stack = None
    projections = None
    correlations = None
    correlations_thresh = None
    hough_transform = None
    hough_angles = None
    hough_distances = None
    template = None
    correlation_idx = None
    projections_ft = None

    def __init__(self, filepath=None, config=None):
        r"""
        Arguments:
            filepath (str): Path to file to be analyzed.
            config (dict): Dictionary with analysis configurations.
        """

        self.set_config(config)
        if filepath is not None:
            self.load_file(filepath=filepath)

    def set_config(self, config):
        r""" Set the analysis configurations. """
        config.setdefault('frame_crop', [0, -1])
        config.setdefault('image_crop', [0, -1, 0, -1])
        config.setdefault('corr_thresh_percent', 95.5)
        config.setdefault('max_frames', 1000)
        config.setdefault('zoom', 3)
        config.setdefault('frame_rate', 100)
        if self.imstack is not None:
            self.imstack.n_frames_raw = min(self.imstack.n_frames_raw, config['max_frames'])
            self.imstack.set_frame_crop(config['frame_crop'])
            self.imstack.set_image_crop(config['image_crop'])
        self.config = config

    def load_file(self, filepath):
        r""" Load the file.

        Arguments:
            filepath (str): File path that is passed to ImageStack class.
        """
        config = self.config
        self.imstack = dataio.ImageStack(filepath=filepath)
        self.imstack.n_frames_raw = min(self.imstack.n_frames_raw, config['max_frames'])
        self.imstack.set_frame_crop(config['frame_crop'])
        self.imstack.set_image_crop(config['image_crop'])

    def process(self):
        r""" Do the actual image processing.  Get the jet speed. """
        config = self.config
        self.imstack.set_frame_crop(config['frame_crop'])
        self.imstack.set_image_crop(config['image_crop'])
        crop = self.imstack.get_stack(normalize=True)
        # for i in range(crop.shape[0]):
        #     crop[i, :, :] /= np.median(crop[i, :, :])
        proj = np.sum(crop, axis=1)
        proj -= np.median(proj, axis=0)
        proj = (proj.T - np.median(proj.T, axis=0)).T.copy()
        proj = proj.T.copy()
        idx = int(proj.shape[0] / 2)
        a = proj[idx, :]
        c = np.real(ifft(fft(proj-np.median(proj))*np.conj(fft(a-np.median(a)))))
        c = np.roll(c, int(len(a)/2), axis=1)
        if config['corr_thresh_percent'] is not None:
            t = np.percentile(c, config['corr_thresh_percent'])
        else:
            t = threshold_otsu(c)
        ct = c > t
        tested_angles = np.linspace(-np.pi / 10, np.pi / 10, 1000, endpoint=False)
        hough, theta, dist = hough_line(ct, theta=tested_angles)
        p = proj.copy()
        x, y = np.meshgrid(np.arange(p.shape[0]) - (p.shape[0] - 1) / 2.0,
                           np.arange(p.shape[1]) - (p.shape[1] - 1) / 2.0, indexing='ij')
        r = np.sqrt(x**2+y**2)
        m = r < np.min(p.shape) / 2
        p *= m
        pft = np.real(np.abs(np.fft.fftn(p))**2)
        # ============================================================
        self.cropped_stack = crop
        self.projections = proj
        self.correlations = c
        self.correlations_thresh = ct
        self.template = a
        self.correlation_idx = idx
        self.hough_transform = hough
        self.hough_angles = theta
        self.hough_distances = dist
        self.projections_ft = pft

        for _, angle, dist in zip(*hough_line_peaks(self.hough_transform, self.hough_angles, self.hough_distances)):
            x = np.arange(self.correlations.shape[0])
            y = (dist + x * np.cos(angle + np.pi / 2)) / np.sin(angle + np.pi / 2)
            speed_ppf = abs(1 / ((y[len(y) - 1] - y[0]) / (x[len(x) - 1] - x[0])))
            break

        return {'speed_ppf': speed_ppf, 'speed_mps': self.convertToSpeed(speed_ppf)}

    def convertToSpeed(self, speed_ppf):
        return speed_ppf*(2.0809*(self.config['zoom']**-1.001))*self.config['frame_rate']


class Main(QtGui.QMainWindow):
    r""" QMainWindow subclass.  Overrides keyPressEvent, sets window size to 2/3 of primary screen size. """
    def __init__(self, par=None, *args, **kwargs):
        r""" This should not be necessary, but is presently used to configure key-press events."""
        app = pg.mkQApp()
        super().__init__(*args, **kwargs)
        self.par = par
        s = app.primaryScreen().size()
        f = 2 / 3  # Make the main window fill this fraction of the screen
        self.setGeometry(int(s.width()*(1-f)/2), int(s.height()*(1-f)/2), int(s.width()*f), int(s.height()*f))
    def keyPressEvent(self, ev):
        r""" Overrides a built-in method. """
        if self.par is not None:
            self.par.key_pressed(ev)


class InteractiveGUI:
    r""" A GUI for running the Analyzer class.  Press "enter" to make the analysis run."""
    def __init__(self, filepath=None, config=None, analyzer=None):
        r""" 
        Arguments:
            filepath (str): Path to image data.
            config (dict): Analysis configuration dictionary.
            analyzer (Analyzer): Provide Analysis class instance directly instead of creating it internally.    
        """
        if analyzer is None:
            analyzer = Analyzer(filepath=filepath, config=config)
        self.analyzer = analyzer
        self.app = pg.mkQApp()
        self.main_window = Main(par=self)  # QtGui.QMainWindow()
        self.main_window.setWindowTitle("Sequential Jet Speed Analyzer")
        self.layout = QtGui.QHBoxLayout()
        # TODO: The line below puts a huge number of frames in memory.  We need to find a better way.
        self.imstack_view = gui.ImageView()
        self.main_widget = QtGui.QWidget()
        self.main_widget.setLayout(self.layout)
        self.layout.addWidget(self.imstack_view)
        self.grid_layout = QtGui.QGridLayout()
        self.grid_widget = QtGui.QWidget()
        self.grid_widget.setLayout(self.grid_layout)
        self.layout.addWidget(self.grid_widget)
        k = {'show_histogram': False, 'aspect_locked': False}
        self.proj_view = gui.ImageView(title='Y Projection', xlabel='Y Pixel', ylabel='Frame Number', **k)
        self.grid_layout.addWidget(self.proj_view, 0, 0)
        self.ft_view = gui.ImageView(title='Cross Correlations', xlabel='Y Offset', ylabel='Frame Offset', **k)
        self.grid_layout.addWidget(self.ft_view, 0, 1)
        self.hough_view = gui.ImageView(title='Hough Transform', xlabel='Distance (pix)', ylabel='Angle (deg)', **k)
        self.grid_layout.addWidget(self.hough_view, 1, 0)
        self.fit_view = gui.ImageView(title='Threshold', xlabel='Y Offset', ylabel='Frame Offset', **k)
        self.fit_plot = self.ft_view.add_plot(pen=pg.mkPen('r', width=5), name='fit')
        self.grid_layout.addWidget(self.fit_view, 1, 1)
        self.main_window.setCentralWidget(self.main_widget)
        self.menubar = QtGui.QMenuBar()
        self.main_window.setMenuBar(self.menubar)
        file_menu = self.menubar.addMenu('File')
        open_action = QtGui.QAction('Open file...', self.main_window)
        open_action.triggered.connect(self.open_data_file_dialog)
        file_menu.addAction(open_action)
        self.statusbar = self.main_window.statusBar()
        self.main_window.show()
        self.statusbar.showMessage('Loading raw stack...')
        self.app.processEvents()
        self.imstack_view.setImage(analyzer.imstack.get_raw_stack()) #divide_by_median=True))
        a, b, c, d = self.analyzer.imstack.get_image_crop()
        self.imstack_view.add_roi(pos=(a, c), size=(b-a, d-c), name='roi')
        self.statusbar.showMessage('Ready.  Press enter to process the data...')
    def open_data_file_dialog(self):
        r""" Select a file to open. """
        options = QtGui.QFileDialog.Options()
        f_name, f_type = QtGui.QFileDialog.getOpenFileName(self.main_window, "Load data...", os.getcwd(),
                                                           "Images (*.avi *.tif *.tiff);;Data file (*.h5)",
                                                           options=options)
        self.analyzer.load_file(f_name)
        stack = self.analyzer.imstack.get_raw_stack()
        self.imstack_view.setImage(stack)
    def process_data(self):
        r""" Process the data. Updates image crop region according to current ROI. """
        self.statusbar.showMessage('Processing the data...')
        self.app.processEvents()
        config = self.analyzer.config
        s1, s2 = self.imstack_view.get_roi_slice(name='roi')
        config['image_crop'] = [s1.start, s1.stop, s2.start, s2.stop]
        self.analyzer.set_config(config)
        results = self.analyzer.process()
        self.statusbar.showMessage('Done.  Jet speed = '+'%g'%results['speed_ppf']+
                                   ' pixels/frame, or ' +'%g'%results['speed_mps'] + ' umps.  Press enter to re-process the data...')
    def display(self):
        r""" Display analysis results. """
        ana = self.analyzer
        self.proj_view.setImage(ana.projections)
        self.ft_view.setImage(ana.correlations)
        # self.ft_view.setImage(np.log(np.fft.fftshift(ana.projections_ft)+1))
        angle_step = 0.5 * np.diff(ana.hough_angles).mean()
        d_step = 0.5 * np.diff(ana.hough_distances).mean()
        fs_lims = [np.rad2deg(ana.hough_angles[0] - angle_step), np.rad2deg(ana.hough_angles[-1] + angle_step)]
        ss_lims = [ana.hough_distances[-1] + d_step, ana.hough_distances[0] - d_step]
        self.hough_view.setImage(np.log(ana.hough_transform+1), fs_lims=fs_lims, ss_lims=ss_lims)
        self.fit_view.setImage(ana.correlations_thresh.astype(float))
        for _, angle, dist in zip(*hough_line_peaks(ana.hough_transform, ana.hough_angles, ana.hough_distances)):
            x = np.arange(ana.correlations.shape[0])
            y = (dist + x*np.cos(angle+np.pi/2))/np.sin(angle+np.pi/2)
            self.fit_plot.setData(x, y)
            break
    def start(self):
        r""" Execute the application. """
        self.app.exec_()
    def key_pressed(self, ev):
        r""" Handling of key-press events. """
        key = ev.key()
        if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
            self.process_data()
            self.display()


if __name__ == '__main__':
    thisfile = r'rawdata/PS2 fastcam/1% PEO PS2 xtals buffer C 15/pump 0.002_C001H001S0001/pump 0.002_C001H001S0001.avi'
    # if not os.path.exists(thisfile):
    #     url = r"https://drive.google.com/drive/folders/1bJrAtH4DTADhT_H96e8wUOYTjPWtRbM2?usp=sharing"
    #     # url = r"https://drive.google.com/drive/folders/1bJrAtH4DTADhT_H96e8wUOYTjPWtRbM2?usp=sharing"
    #     import gdown
    #     gdown.download_folder(url)
    config = dict(frame_crop=(400, -1), image_crop=(400, 550, 550, 600), corr_thresh_percent=99, max_frames=1000)
    interact = InteractiveGUI(filepath=thisfile, config=config)  # analyzer=ana)
    interact.start()
