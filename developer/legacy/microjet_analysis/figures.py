import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import scipy
from scipy.optimize import curve_fit, minimize
from sklearn.neighbors import NearestNeighbors
from skopt import Optimizer
from skopt.utils import use_named_args
from skopt.space import Real

class GDVNAnalyzer:
    """
        A class for analyzing data from a Gas Dynamic Virtual Nozzle (GDVN) experiment.

        Attributes:
            D (float): The nozzle orifice diameter in microns.
            _filter_threshold (float): The threshold used to filter out outliers in the data.
            df_full (pd.DataFrame): A copy of the original data frame.
            _D_meters (float): The nozzle orifice diameter in meters.
            _cmap (str): The name of the colormap used for plotting.

        Methods:
            get_stability_markers(): Updates the DataFrame with markers for the jet stability.
            get_flow_ratio(): Calculates the flow ratio and updates the DataFrame.
            get_flow_correction_v1(): Calculates the flow correction using version 1 and updates the DataFrame.
            get_flow_correction_v2(): Calculates the flow correction using version 2 and updates the DataFrame.
            update_dataframe(): Updates the DataFrame by removing outliers based on the filter threshold.

        Properties:
            convert_liquidflow: Property that returns the conversion factor to go from uL/min to m^3/s.
            convert_massflow: Property that returns the conversion factor to go from mg/min to kg/s.
            filter_threshold: Property that returns the current filter threshold used to filter out outliers.
            cmap: Property that returns the current name of the colormap used for plotting.
            dataframe: Property that returns the current DataFrame.
        
        """

    _filter_threshold = None
    _cmap = None

    def __init__(self, df, D=0, threshold:float=2, filter_by='jet_speed', colormap:str='RdBu_r'):
        """
        Initializes the GDVNAnalyzer class.

        Args:
            df (pd.DataFrame): A Pandas DataFrame containing the data from a GDVN experiment.
            D (float): The nozzle orifice diameter in microns.
            threshold (float, optional): The threshold used to filter out outliers in the data.
            filter_by (str, optional): The column to use for filtering outliers.
            colormap (str, optional): The name of the colormap used for plotting.
        """

        self.df_full = df
        self.D = D # the nozzle orifice diameter in microns
        self._D_meters = D * 1e-6
        self._cmap = colormap

        # update the dataframe to get markers for the jet stability
        self.get_stability_markers()

        # calculate the flow ratio and flow corrections, then update the DataFrame
        self.get_flow_ratio()
        self.get_flow_ratio_2()
        self.get_flow_correction_v1()
        self.get_flow_correction_v2()
        # self.get_stability()
        self.df_full['stability'] = 1 / self.df_full['jet_stability']
        self.get_reynolds()
        self.get_weber()
        self.get_capillary()


        if threshold is not None:
            self._filter_threshold = threshold

        self.update_dataframe()

    @property
    def convert_liquidflow(self):
        """
        Property that returns the conversion factor to go from uL/min to m^3/s.
        """
        return 1.6667e-11

    @property
    def convert_massflow(self):
        """
        Property that returns the conversion factor to go from mg/min to kg/s.
        """
        return 1.6667e-8

    @property
    def filter_threshold(self):
        """
        Property that returns the current filter threshold used to filter out outliers.
        """
        return self._filter_threshold

    @filter_threshold.setter
    def filter_threshold(self, threshold):
        """
        Property that sets the filter threshold used to filter out outliers.

        Args:
            threshold (float): The new filter threshold to use.
        """
        self._filter_threshold = threshold
        self.update_dataframe()

    @property
    def cmap(self):
        """
        Property that returns the current name of the colormap used for plotting.

        Returns:
            str: The name of the current colormap.
        """
        return self._cmap

    @cmap.setter
    def cmap(self, cmap:str):
        """
        Property that sets the name of the colormap used for plotting.

        Args:
            cmap (str): The new name of the colormap to use.
        """
        self._cmap = cmap

    @property
    def dataframe(self):
        """
        Property that returns the current DataFrame.

        Returns:
            pd.DataFrame: The current DataFrame.
        """
        return self.df

    def update_dataframe(self):
        """
        Updates the DataFrame by removing outliers based on the filter threshold.
        """
        out = self.remove_outliers(self.df_full['jet_speed'], self._filter_threshold)
        self.df = self.df_full.loc[out.index].reset_index(drop=True)

    def water_params(self):
        """
        Returns a dictionary of parameters for water.

        Returns:
            dict: A dictionary containing the following keys: 'rho', 'rho_units', 'mu', 'mu_units', 'sigma', and 'sigma_units'.
        """
        water = {'rho': 998, # kg/m^3 mass density at 20C
                    'rho_units': 'kg/m^3',
                    'mu': 1e-3, # kg/ms
                    'mu_units': 'kg/ms',
                    'sigma': 0.072, # N/m
                    'sigma_units': 'N/m'
                    }
        return water

    def helium_params(self):
        """
        Returns a dictionary of parameters for helium.

        Returns:
            dict: A dictionary containing the following keys: 'rho', 'rho_units', 'specific_heat_ratio', 'R', and 'R_units'.
        """
        helium = {'rho': 0.1786,
                    'rho_units': 'kg/m^3',
                    'specific_heat_ratio': 5/3, # for a diatomic ideal gas
                    'R': 2077, # specific gas constant for helium [J/(kg·K)]
                    'R_units': 'J/(kg K)'
                    }
        return helium

    def get_flow_ratio(self):
        """
        Computes the flow ratio as the liquid flow rate divided by the gas mass flow rate.
        Adds the 'flow_ratio' key to the DataFrame.
        """
        self.df_full['flow_ratio'] = self.df_full['liquid_flow'] / self.df_full['gas_flow']

    def get_flow_ratio_2(self):
        """
        Computes the flow ratio as the divided by the gas mass flow rate divided by liquid flow rate
        Adds the 'flow_ratio' key to the DataFrame.
        """
        self.df_full['flow_ratio_2'] =  self.df_full['gas_flow'] / self.df_full['liquid_flow']

    def get_flow_correction_v1(self):
        """
        Calculates the velocity given flow rates and nozzle orifice.

        Args:
            None
        
        Returns:
            None. Updates the 'flow_correction_v1' key in the DataFrame.
        """
        num = np.sqrt(self.df_full['gas_flow'])
        den = self.D * self.df_full['liquid_flow']**(1/4)
        self.df_full['flow_correction_v1'] = num / den


    def get_flow_correction_v2(self):
        """
        Calculates the velocity given flow rates and nozzle orifice.

        Args:
            None
        
        Returns:
            None. Updates the 'flow_correction_v2' key in the DataFrame.
        """
        num = np.sqrt(self.df_full['gas_flow'])
        self.df_full['flow_correction_v2'] = num / self.D

    # def get_stability(self):
    #     # To do: revisit this! 
    #     self.df_full['stability'] = np.abs(self.df_full['jet_deviation_err'] / self.df_full['jet_deviation'])


    def reynolds(self, Q, R, rho, mu):
        """
        Calculates the Reynolds number of a substance.

        Args:
            Q (float): Volumetric flow rate.
            R (float): The liquid jet diameter.
            rho (float): Density of the fluid.
            mu (float): Dynamic viscosity of the fluid.
        
        Returns:
            float: The Reynolds number.
        """
        num = rho * Q
        den = np.pi * R * mu
        return num / den

    def weber(self, Q, R, rho, sigma):
        """
        Calculates the Weber number of a substance.

        Args:
            Q (float): Volumetric flow rate.
            R (float): The liquid jet diameter.
            rho (float): Density of the fluid.
            sigma (float): Surface tension of the fluid.

        Returns:
            float: The Weber number.
        """
        num = rho * Q**2
        den = np.pi**2 * R**3 * sigma
        return num / den

    def get_capillary(self, sample='water', params=None):
        if sample == 'water':
            w = self.water_params()
        elif (params is not None) and (sample != 'water'):
            w = params
        else:
            raise ValueError("keys are wrong.")

        self.df_full['capillary'] = self.capillary(self.df_full['jet_speed'], w['mu'], w['sigma'])


    def get_weber(self, sample='water', params=None):
        if sample == 'water':
            w = self.water_params()
        elif (params is not None) and (sample != 'water'):
            w = params
        else:
            raise ValueError("keys are wrong.")

        self.df_full['weber'] = self.weber(self.df_full['liquid_flow'] * self.convert_liquidflow, 
                                                self.df_full['jet_diameter'], w['rho'], w['sigma'])


    def get_reynolds(self, sample='water', params=None):
        if sample == 'water':
            w = self.water_params()
        elif (params is not None) and (sample != 'water'):
            w = params
        else:
            raise ValueError("keys are wrong.")

        self.df_full['reynolds'] = self.reynolds(self.df_full['liquid_flow'] * self.convert_liquidflow, 
                                                    self.df_full['jet_diameter'], w['rho'], w['mu'])


    def capillary(self, v, mu, sigma):
        """
        Calculates the Capillary number of a substance.

        Args:
            v (float): Velocity of the fluid.
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.

        Returns:
            float: The Capillary number.
        """
        num = mu * v
        den = sigma * np.sqrt(2)
        return num / den


    def remove_outliers(self, data, threshold=1.5):
        """
        Removes outliers from a dataset using the IQR method.

        Args:
            data (pd.Series): A Pandas Series containing the data.
            threshold (float, optional): The IQR multiplier for outlier detection. Default is 1.5.

        Returns:
            pd.Series: A Pandas Series with outliers removed.
        """
        Q1 = data.quantile(0.25)
        Q3 = data.quantile(0.75)
        IQR = Q3 - Q1
        lower_bound = Q1 - threshold * IQR
        upper_bound = Q3 + threshold * IQR
        return data[(data >= lower_bound) & (data <= upper_bound)]


    def nearest_neighbor_filter(self, x, y, filter_by, threshold:float=10):
        """
        Filters a DataFrame using a nearest neighbor algorithm.

        Args:
            x (pd.Series): A Pandas Series containing the x-axis data.
            y (pd.Series): A Pandas Series containing the y-axis data.
            filter_by (pd.Series): A Pandas Series containing the data to filter by.
            threshold (float, optional): The radius of the nearest neighbor search. Default is 10.

        Returns:
            pd.DataFrame: A new DataFrame with outliers removed.
        """
        # Combine the x and y arrays as input for the nearest neighbor algorithm
        data = np.column_stack((x, y, filter_by))

        # Create a NearestNeighbors object with specified threshold
        nbrs = NearestNeighbors(radius=threshold, algorithm='auto').fit(data)

        # Find neighbors within the specified threshold
        indices = nbrs.radius_neighbors(data, return_distance=False)

        # Filter outliers based on whether they have at least one neighbor within the threshold
        valid_indices = [i for i, neighbors in enumerate(indices) if len(neighbors) > 1]

        # Create a new DataFrame with the filtered data
        filtered_df = self.df.iloc[valid_indices].reset_index(drop=True)

        return filtered_df


    def plot_gas_vs_velocity(self):
        """
        Creates a scatter plot of gas mass flow rate vs velocity,
        with a colorbar representing the liquid flow rate.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        fig, ax = plt.subplots()

        scatter = ax.scatter(self.df['gas_flow'], self.df['jet_speed'],
                             c=self.df['liquid_flow'], s=self.df['jet_diameter']*3e6,
                             cmap=self._cmap)
        ax.set_xlabel('Gas Mass Flow Rate [mg/min]')
        ax.set_ylabel('Velocity [m/s]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)

        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_weber_vs_reynolds(self, rho, mu, sigma):
        """
        Creates a scatter plot of Weber number vs Reynolds number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            rho (float): Density of the fluid.
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        reynolds_nums = self.reynolds(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, mu)
        weber_nums = self.weber(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, sigma)

        fig, ax = plt.subplots()

        scatter = ax.scatter(reynolds_nums, weber_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Reynolds Number')
        ax.set_ylabel('Weber Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_jet_diameter_vs_capillary(self, mu, sigma, **kwargs):
        """
        Creates a scatter plot of jet diameter vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
            **kwargs : Keyword arguments to pass to the `plt.subplots()` function.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        capillary_nums = self.capillary(self.df['jet_speed'], mu, sigma)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(self.df['jet_diameter']*1e6, capillary_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Jet Diameter [µm]')
        ax.set_ylabel('Capillary Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax


    def plot_jet_speed_vs_weber(self, rho, sigma, **kwargs):
        """
        Creates a scatter plot of jet diameter vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
            **kwargs : Keyword arguments to pass to the `plt.subplots()` function.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        weber_nums = self.weber(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, sigma)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(self.df['jet_speed'], weber_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Jet Speed [m/s]')
        ax.set_ylabel('Weber Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_jet_speed_vs_reynolds(self, rho, mu, **kwargs):
        """
        Creates a scatter plot of jet diameter vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
            **kwargs : Keyword arguments to pass to the `plt.subplots()` function.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        reynolds_nums = self.reynolds(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, mu)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(self.df['jet_speed'], reynolds_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Jet Speed [m/s]')
        ax.set_ylabel('Reynolds Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_gas_flow_vs_reynolds(self, rho, mu, **kwargs):
        """
        Creates a scatter plot of jet diameter vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
            **kwargs : Keyword arguments to pass to the `plt.subplots()` function.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        reynolds_nums = self.reynolds(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, mu)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(self.df['gas_flow'], reynolds_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Gas Flow Rate [mg/min]')
        ax.set_ylabel('Reynolds Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_gas_flow_vs_weber(self, rho, sigma, **kwargs):
        """
        Creates a scatter plot of jet diameter vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
            **kwargs : Keyword arguments to pass to the `plt.subplots()` function.

        Returns:
        fig, ax : Tuple of matplotlib Figure and Axes objects.
        """

        weber_nums = self.weber(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, sigma)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(self.df['gas_flow'], weber_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Gas Flow Rate [mg/min]')
        ax.set_ylabel('Weber Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax


    def plot_weber_vs_capillary(self, rho:float, mu:float, sigma:float, **kwargs):
        """
        Creates a scatter plot of Weber number vs Capillary number,
        with a colorbar representing the liquid flow rate and marker size being the jet diameter.

        Args:
            rho (float): Density of the fluid.
            mu (float): Dynamic viscosity of the fluid.
            sigma (float): Surface tension of the fluid.
        """

        weber_nums = self.weber(self.df['liquid_flow'] * self.convert_liquidflow, self.df['jet_diameter'], rho, sigma)
        capillary_nums = self.capillary(self.df['jet_speed'], mu, sigma)

        fig, ax = plt.subplots(**kwargs)

        scatter = ax.scatter(weber_nums, capillary_nums, c=self.df['liquid_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Weber Number')
        ax.set_ylabel('Capillary Number')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)
        ax.set_ylim(0)

        return fig, ax

    def plot_jet_diameter_vs_velocity_3d(self):
        """
        Creates a 3D scatter plot of Jet Diameter vs Velocity with the gas mass flow rate as the z-axis,
        and the liquid flow rate as the colorbar.

        """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        scatter = ax.scatter(self.df['jet_diameter']*1e6, self.df['jet_speed'], self.df['gas_flow'],
                             c=self.df['liquid_flow'], cmap=self._cmap, s=self.df['jet_diameter']*3e6)

        ax.set_xlabel('Jet Diameter [µm]')
        ax.set_ylabel('Velocity [m/s]')
        ax.set_zlabel('Gas Mass Flow Rate [mg/min]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel('Liquid Flow Rate [µL/min]')

        return fig, ax

    def calculate_discharge_coefficient(self, gas_params:dict, T1:float=293.15):
        """
        Calculates the discharge coefficient for a co-flowing jet system.

        Args:
            D_nozzle (float): Nozzle diameter, in meters.
            gas_params (dict): Dictionary of gas parameters. Must include:
                                specific_heat_ratio, rho, and R for this method.
            T1 (float): Upstream temperature of the helium gas, in Kelvin

        Returns:
            float: Discharge coefficient.
        """

        vals = ['specific_heat_ratio', 'rho', 'R']
        for v in vals:
            if v not in list(gas_params.keys()):
                raise KeyError(f"{v} key required for this calculation!")

        k = gas_params['specific_heat_ratio']
        rho_gas = gas_params['rho']
        R = gas_params['R']

        # Calculate the area of the nozzle
        A_nozzle = np.pi * (self._D_meters / 2) ** 2

        # Calculate the helium gas velocity based on mass flow rate
        m_dot_gas = self.df['gas_flow'] * self.convert_massflow
        v_gas = m_dot_gas / (rho_gas * A_nozzle)

        # Calculate the upstream pressure using the gas velocity
        P1 = 0.5 * rho_gas * v_gas ** 2

        # Calculate the critical mass flow rate (choked condition)
        m_dot_critical = A_nozzle * P1 * np.sqrt((k / (R * T1)) * (2 / (k + 1)) ** ((k + 1) / (k - 1)))

        # Calculate the discharge coefficient
        Cd = m_dot_gas / m_dot_critical

        return Cd, v_gas

    def plot_gas_velocity_vs_discharge_coefficient(self, gas_params:dict, T1:float=293.15):
        """
        Creates a scatter plot of jet velocity vs discharge coefficient,
        with a colorbar representing the gas mass flow rate and marker size being the jet diameter.

        Args:
            D_nozzle (float): Nozzle diameter, in meters.
            gas_params (dict): Dictionary of gas parameters. Must include:
                                specific_heat_ratio, rho, and R for this method.
            T1 (float): Upstream temperature of the helium gas, in Kelvin
        """

        discharge_coefficients, v_gas = self.calculate_discharge_coefficient(gas_params, T1)

        fig, ax = plt.subplots()
        scatter = ax.scatter(v_gas, discharge_coefficients, c=self.df['gas_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter'] * 3e6)

        ax.set_xlabel('Gas Jet Velocity [m/s]')
        ax.set_ylabel('Gas Discharge Coefficient')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel('Gas Mass Flow Rate [mg/min]')

        ax.grid(alpha=0.5)

        return fig, ax

    def plot_liquid_velocity_vs_discharge_coefficient(self, gas_params:dict, T1:float=293.15):
        """
        Creates a scatter plot of jet velocity vs discharge coefficient,
        with a colorbar representing the gas mass flow rate and marker size being the jet diameter.

        Args:
            D_nozzle (float): Nozzle diameter, in meters.
            gas_params (dict): Dictionary of gas parameters. Must include:
                                specific_heat_ratio, rho, and R for this method.
            T1 (float): Upstream temperature of the helium gas, in Kelvin
        """

        discharge_coefficients, v_gas = self.calculate_discharge_coefficient(gas_params, T1)

        fig, ax = plt.subplots()
        scatter = ax.scatter(self.df['jet_speed'], discharge_coefficients, c=self.df['gas_flow'],
                             cmap=self._cmap, s=self.df['jet_diameter'] * 3e6)

        ax.set_xlabel('Liquid Jet Velocity [m/s]')
        ax.set_ylabel('Gas Discharge Coefficient')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel('Gas Mass Flow Rate [mg/min]')

        ax.grid(alpha=0.5)

        return fig, ax

    def interpolate_ranges(self, ranges, data, level_step:int=5):
        r"""Prepares a basic contour plot given data

            Args:
                ranges (list or tuple): the x, y, and z ranges for plotting should be [x,y,z]
                data (list or tuple): the x, y, and z data, should be [x,y,z]
                level_step (int): the amount of countour levels between each datapoint

            Returns:
                xi (ndarray): the x grid values
                yi (ndarray): the y grid values
                zi (ndarray): the interpolated z grid values
                levels (ndarray): The number of levels used in the color gradient

        """

        # Make a contour plot of irregularly spaced data
        # coordinate via interpolation on a grid.
        n_levels = int((ranges[2][1] - ranges[2][0]) / level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(2 * n_levels + 10))

        # Create grid values first.
        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
        interpolator = mpl.tri.LinearTriInterpolator(mpl.tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels

    def get_stability_markers(self):
        """
        Calculates stability markers for the jet and updates the DataFrame with the markers.
        The markers are 'o' for stable jets, 'x' for unstable jets, and 's' for jets with marginal stability.
        """
        markers = []
        self.df_full['jet_stability'] = pd.to_numeric(self.df_full['jet_stability'], errors='coerce')
        for i, s in enumerate(self.df_full['jet_stability'].to_numpy()):
            if s > 2:
                markers.append('o')
            elif s < 0.8:
                markers.append('x')
            else:
                markers.append('D')
        self.df_full['stability_markers'] = np.array(markers)

    def plot_liquid_vs_gas(self, cbar_type:str='velocity',
                            xr=(0,40), yr=(0,60), 
                            clip_negative:bool=False, 
                            filter_outliers=True, filter_threshold:float=10):
        """
        Creates a contour plot of the liquid and gas flow rates with a color scale representing a variable related to the
        jet flow. Also overlays stability markers for each data point in the plot.

        Args:
            cbar_type (str, optional): The variable related to the jet flow that will be represented in the color scale. 
                Current options are 'velocity', 'diameter', and 'deviation'. Default is 'velocity'.
            xr (tuple, optional): The range of liquid flow rates to be plotted. Default is (0,40).
            yr (tuple, optional): The range of gas flow rates to be plotted. Default is (0,60).
            clip_negative (bool, optional): A flag to clip negative values of the variable represented in the color scale to 0.
                Default is False.
            filter_outliers (bool, optional): A flag to filter outliers based on the current filter threshold. Default is True.
            filter_threshold (float, optional): The threshold used to filter out outliers in the data. Default is 10.

        Raises:
            ValueError: If cbar_type input was incorrect. Current options are restricted to 'velocity', 'diameter', and 'deviation'.
        
        Returns:
            fig, ax (matplotlib.figure.Figure, matplotlib.axes.Axes): A tuple containing the figure and axes objects of the plot.
        """
        cbar_options = ['velocity', 'diameter', 'deviation', 'weber']
        if cbar_type == cbar_options[0]:
            key = 'jet_speed'
            cbar_title = r"Jet Speed [m/s]"
        elif cbar_type == cbar_options[1]:
            key = 'jet_diameter'
            cbar_title = r"Jet Diameter $\mu$m"
        elif cbar_type == cbar_options[2]:
            key = 'jet_deviation'
            cbar_title = r"Jet Deviation [Degree]"
        elif cbar_type == 'weber':
            key = 'weber'
            cbar_title = r"Liquid Jet Weber Number"
        elif cbar_type == 'reynolds':
            key = 'reynolds'
            cbar_title = r"Liquid Jet Reynolds Number"
        elif cbar_type == 'capillary':
            key = 'capillary'
            cbar_title = r"Liquid Jet Capillary Number"
        else:
            raise ValueError(f"cbar_type input was incorrect. Current options are restricted to {cbar_options}.")

        xf = self.df['liquid_flow']
        yf = self.df['gas_flow']
        zf = self.df[key]
        zr = (np.floor(zf.min()), np.ceil(zf.max()))


        if clip_negative:
            for i in range(len(zf)):
                if zf[i] < 0:
                    zf[i] = 0
            zidx = np.where(z != 0)
            xi, yi, zi, levels = self.interpolate_ranges(ranges=[xr, yr, zr], data=[xf[zidx], yf[zidx], zf[zidx]])
        else:
            xi, yi, zi, levels = self.interpolate_ranges(ranges=[xr, yr, zr], data=[xf, yf, zf])

        fig, ax = plt.subplots(1,1, tight_layout=True)
        cmap = mpl.cm.get_cmap('RdBu_r')
        cmapz = cmap((zf-zr[0])/(zr[1]-zr[0]))

        if not np.all((levels >= 0) & (levels <= 1)):
            levels = np.array([int(i) for i in levels])
        else:
            levels = np.array([round(i, 1) for i in levels])

        contourf = ax.contourf(xi, yi, zi, levels=np.unique(levels), cmap=cmap)
        fig.colorbar(contourf, ax=ax, label=cbar_title)
        for i, m in enumerate(self.df['stability_markers'].to_numpy()):
            d = self.df['jet_diameter'][i]*5e6
            derr = self.df['jet_deviation_err'][i] / np.abs(self.df['jet_deviation'][i])

            ax.scatter(xf[i], yf[i],
                                s = d + d*derr,
                                marker='o',
                                facecolors='none',
                                edgecolors='k',
                                linestyle='dashed',
                                linewidths=0.5
                                )

            ax.scatter(xf[i], yf[i], 
                            s=d, 
                            marker='o', 
                            color=cmapz[i],
                            edgecolors=(0, 0, 0), linewidths=0.3)

        ax.set(xlim=xr, ylim=yr)
        ax.set_xlabel(r'Liquid Flow ($\mu$l/min)')
        ax.set_ylabel('Gas Flow (mg/min)')

        return fig, ax

    def plot_flow_ratio_vs_jet_speed(self, threshold=1.5):
        """
        Creates a scatter plot of flow ratio vs jet speed, with a colorbar representing jet diameter.

        Args:
            threshold (float, optional): The IQR multiplier for outlier detection. Default is 1.5.

        Returns:
            fig, ax (tuple): The figure and axis objects of the generated plot.
        """

        fig, ax = plt.subplots()
        cmap = mpl.cm.get_cmap(self._cmap)
        for i in range(len(self.df)):
            s = self.df['stability_markers'][i]
            scatter = ax.scatter(self.df['flow_ratio'][i], self.df['jet_speed'][i],
                                 s=self.df['jet_diameter'][i] * 2e6,
                                 marker=s,
                                 cmap=cmap,
                                 c=self.df['jet_diameter'][i] * 1e6,
                                 vmin=self.df['jet_diameter'].min() * 1e6,
                                 vmax=self.df['jet_diameter'].max() * 1e6)

        ax.set_xlabel(r'Flow Ratio [Q/$\dot{m}$]')
        ax.set_ylabel('Jet Speed [m/s]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Jet Diameter [$\mu$m]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)

        return fig, ax

    def plot_flow_ratio_vs_jet_speed_2(self):
        """
        Creates a scatter plot of flow ratio vs jet speed, with a colorbar representing jet diameter.

        Args:
            None

        Returns:
            fig, ax (tuple): The figure and axis objects of the generated plot.
        """

        fig, ax = plt.subplots()
        cmap = mpl.cm.get_cmap(self._cmap)
        for i in range(len(self.df)):
            s = self.df['stability_markers'][i]
            scatter = ax.scatter(self.df['flow_ratio_2'][i], self.df['jet_speed'][i],
                                 s=self.df['jet_diameter'][i] * 2e6,
                                 marker=s,
                                 cmap=cmap,
                                 c=self.df['jet_diameter'][i] * 1e6,
                                 vmin=self.df['jet_diameter'].min() * 1e6,
                                 vmax=self.df['jet_diameter'].max() * 1e6)

        ax.set_xlabel(r'Flow Ratio [$\dot{m}$/Q]')
        ax.set_ylabel('Jet Speed [m/s]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Jet Diameter [$\mu$m]')

        ax.grid(alpha=0.5)
        ax.set_xlim(0)

        return fig, ax

    def plot_gas_vs_velocity_corrected_1(self, threshold=1.5):
        """
        Creates a scatter plot of the square root of gas flow rate divided by nozzle orifice diameter squared
        vs nozzle orifice diameter, with a colorbar representing the liquid flow rate.

        Args:
            D (float): Nozzle orifice diameter.
            threshold (float, optional): The IQR multiplier for outlier detection. Default is 1.5.

        Returns:
            fig, ax (tuple): The figure and axis objects of the generated plot.
        """

        ddf = self.df.sort_values(by='jet_speed')

        fig, ax = plt.subplots()

        scatter = ax.scatter(ddf['flow_correction_v1'], ddf['jet_speed'],
                             c=ddf['liquid_flow'],
                             s=ddf['jet_diameter'] * 2e6,
                             cmap=self._cmap)

        ax.set_xlabel(r'($Q_g$ / $D^2$)$^{1/2}$')
        ax.set_ylabel('Jet Speed [m/s]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)

        return fig, ax


    def plot_gas_vs_velocity_corrected_2(self, threshold=1.5):
        """
        Creates a scatter plot of the square root of gas flow rate divided by nozzle orifice diameter squared
        vs nozzle orifice diameter, with a colorbar representing the liquid flow rate.

        Args:
            D (float): Nozzle orifice diameter.
            threshold (float, optional): The IQR multiplier for outlier detection. Default is 1.5.

        Returns:
            fig, ax (tuple): The figure and axis objects of the generated plot.
        """

        ddf = self.df.sort_values(by='jet_speed')

        fig, ax = plt.subplots()

        scatter = ax.scatter(ddf['flow_correction_v2'], ddf['jet_speed'],
                             c=ddf['liquid_flow'],
                             s=ddf['jet_diameter'] * 2e6,
                             cmap=self._cmap)

        ax.set_xlabel(r'($Q_g$ / $D^2$)$^{1/2}$ / $Q_l^{1/4}$')
        ax.set_ylabel('Jet Speed [m/s]')

        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]')

        ax.grid(alpha=0.5)

        return fig, ax


    def plot_jet_speed_vs_stability(self):
        """
        Creates a scatter plot of jet speed vs stability, with jet diameter as the marker size and liquid flow rate as the colorbar.

        Returns:
            fig (matplotlib.figure.Figure): The matplotlib Figure object for the plot.
            ax (matplotlib.axes.Axes): The matplotlib Axes object for the plot.

        """
        fig, ax = plt.subplots()
        im = ax.scatter(self.df['jet_speed'], self.df['stability'], 
                         s=self.df['jet_diameter']*4e6, c=self.df['liquid_flow'], cmap=self._cmap)
        ax.set_xlabel('Jet Speed [m/s]')
        ax.set_ylabel('Stability')
        fig.colorbar(im, ax=ax, label='Liquid Flow Rate [μl/min]')

        ax.grid(alpha=0.5)

        return fig, ax

    def plot_gas_flow_vs_stability(self):
        """
        Creates a scatter plot of gas mass flow rate vs stability, with jet diameter as the marker size and jet speed as the colorbar.

        Returns:
            fig (matplotlib.figure.Figure): The matplotlib Figure object for the plot.
            ax (matplotlib.axes.Axes): The matplotlib Axes object for the plot.

        """
        fig, ax = plt.subplots()
        im = ax.scatter(self.df['gas_flow'], self.df['stability'], 
                         s=self.df['jet_diameter']*4e6, c=self.df['jet_speed'], cmap=self._cmap)
        ax.set_xlabel('Gas Mass Flow Rate [mg/min]')
        ax.set_ylabel('Stability')
        fig.colorbar(im, ax=ax, label='Jet Speed [m/s]')

        ax.grid(alpha=0.5)

        return fig, ax

    def plot_liquid_vs_gas_stability(self):
        """
        Creates a scatter plot of liquid flow rate vs gas mass flow rate, with jet diameter as the marker size and stability as the colorbar.

        Returns:
            fig (matplotlib.figure.Figure): The matplotlib Figure object for the plot.
            ax (matplotlib.axes.Axes): The matplotlib Axes object for the plot.

        """
        fig, ax = plt.subplots()
        im = ax.scatter(self.df['liquid_flow'], self.df['gas_flow'], 
                         s=self.df['jet_diameter']*4e6, c=self.df['stability'], vmin=0, vmax=np.max(self.df['stability']),
                        cmap=self._cmap)
        ax.set_xlabel('Liquid Flow Rate [μl/min]')
        ax.set_ylabel('Gas Mass Flow Rate [mg/min]')
        fig.colorbar(im, ax=ax, label='Stability')

        ax.grid(alpha=0.5)

        return fig, ax

    def plot_stability_vs_flow_ratio(self):
        fig, ax = plt.subplots()
        im = ax.scatter(self.df['flow_ratio'], self.df['stability'], 
                         s=self.df['jet_diameter']*4e6, c=self.df['jet_speed'], cmap=self._cmap)
        ax.set_xlabel(r'Flow Ratio [Q/$\dot{m}$]')
        ax.set_ylabel(rf'Stability [$\Delta y / d_j$]')
        fig.colorbar(im, ax=ax, label='Jet Speed [m/s]')

        ax.grid(alpha=0.5)
            
        return fig, ax

    def plot_stability_vs_flow_ratio_2(self):
        fig, ax = plt.subplots()
        im = ax.scatter(self.df['flow_ratio_2'], self.df['stability'], 
                         s=self.df['jet_diameter']*4e6, c=self.df['jet_speed'], cmap=self._cmap)
        ax.set_xlabel(r'Flow Ratio [$\dot{m}/Q$]')
        ax.set_ylabel('Stability')
        fig.colorbar(im, ax=ax, label='Jet Speed [m/s]')

        ax.grid(alpha=0.5)
            
        return fig, ax


    def dj(self, re, Q, rho, mu):
        r''' solved the reynolds number equation for the jet diameter.
        '''
        al = rho / (np.pi * mu)
        return al * Q / re

    def plot_reynolds_diameter_comparison(self, reynolds:float, gas_flow:float):

        w = self.water_params()

        # now set a gas mass flow rate and filter the dataframe to
        # values near there
        dff = self.df[(self.df['gas_flow'] >= gas_flow - 1) & (self.df['gas_flow'] <= gas_flow + 1)]

        # get the theoretical diameters based on the provided reynolds number
        liq = np.linspace(dff['liquid_flow'].min(), dff['liquid_flow'].max(), 1000)
        liqc = liq * self.convert_liquidflow
        dj_0 = self.dj(reynolds, liqc, w['rho'], w['mu']) * 1e6

        fig, ax = plt.subplots()
        ax.plot(liq, dj_0, '--k', label="Predicted")
        im = ax.scatter(dff['liquid_flow'], dff['jet_diameter']*1e6, 
                         s=dff['jet_diameter']*4e6, c=dff['jet_speed'], cmap=self._cmap)
        ax.set_xlabel("Liquid Flow Rate [μL/min]")
        ax.set_ylabel("Jet Diameter [μm]")
        fig.colorbar(im, ax=ax, label='Jet Speed [m/s]')

        ax.grid(alpha=0.5)

    def plot_reynolds_meshgrid(self, resolution=100, sample='water', params=None,
                                cbar_min:float=0, cbar_max:float=100):
        """
        Creates a meshgrid plot where x is the liquid flow rate, y is the jet diameter, 
        and the colorbar is the Reynolds number.

        Args:
            resolution (int, optional): The number of points in each dimension of the grid. Default is 100.

        Returns:
            tuple: A tuple containing the figure and axis objects (fig, ax).
        """

        if sample == 'water':
            w = self.water_params()
        elif (params is not None) and (sample != 'water'):
            w = params
        else:
            raise ValueError("keys are wrong.")

        x = np.linspace(1e-8, 40, resolution) # the liquid flow rate [ul/min]
        y = np.linspace(1e-8, 5e-6, resolution) # the jet diameter [m]
        X, Y = np.meshgrid(x, y)

        # Calculate Reynolds number for each point in the grid
        Z = self.reynolds(X * self.convert_liquidflow, Y, w['rho'], w['mu'])

        fig, ax = plt.subplots()
        c = ax.pcolormesh(X, Y*1e6, Z, cmap=self._cmap, vmin=cbar_min, vmax=cbar_max)
        im = ax.scatter(self.df['liquid_flow'], self.df['jet_diameter']*1e6, 
                        s=self.df['jet_diameter']*8e6, c=self.df['reynolds'], 
                        cmap=self._cmap)
        ax.set_xlabel('Liquid Flow Rate (uL/min)')
        ax.set_ylabel('Jet Diameter (μm)')
        fig.colorbar(c, ax=ax, label='Reynolds Number')

        return fig, ax

    def plot_reynolds_1(self, resolution=100, sample='water', params=None,
                                cbar_min:float=0, cbar_max:float=100):
        """
        Creates a meshgrid plot where x is the liquid flow rate, y is the jet diameter, 
        and the colorbar is the Reynolds number.

        Args:
            resolution (int, optional): The number of points in each dimension of the grid. Default is 100.

        Returns:
            tuple: A tuple containing the figure and axis objects (fig, ax).
        """

        if sample == 'water':
            w = self.water_params()
        elif (params is not None) and (sample != 'water'):
            w = params
        else:
            raise ValueError("keys are wrong.")

        x = np.linspace(1e-8, 40, resolution) # the liquid flow rate [ul/min]
        y = np.linspace(1e-8, 5e-6, resolution) # the jet diameter [m]
        X, Y = np.meshgrid(x, y)

        # Calculate Reynolds number for each point in the grid
        Z = self.reynolds(X * self.convert_liquidflow, Y, w['rho'], w['mu'])

        fig, ax = plt.subplots()
        c = ax.pcolormesh(X, Y*1e6, Z, cmap=self._cmap, vmin=cbar_min, vmax=cbar_max)

        im = ax.scatter(self.df['liquid_flow'], self.df['jet_diameter']*1e6, 
                        s=self.df['jet_diameter']*8e6, c=self.df['reynolds'], 
                        cmap=self._cmap, vmin=vmin, vmax=vmax)

        ax.set_xlabel('Liquid Flow Rate (uL/min)')
        ax.set_ylabel('Jet Diameter (μm)')
        fig.colorbar(c, ax=ax, label='Reynolds Number')

        return fig, ax

    def plot_speed_v_reynolds_flowratio(self, vmin:float=0, vmax:float=5):

        fig, ax = plt.subplots()

        im = ax.scatter(self.df['jet_speed'], self.df['reynolds'], 
                        s=self.df['jet_diameter']*8e6, c=self.df['flow_ratio'], 
                        cmap=self._cmap, vmin=vmin, vmax=vmax)

        ax.set_xlabel('Jet Speed [m/s]')
        ax.set_ylabel('Liquid Jet Reynolds Number')
        fig.colorbar(im, ax=ax, label=r'Flow Ratio [Q/$\dot{m}$]')
        ax.grid(alpha=0.5)

        return fig, ax

    def plot_speed_v_weber_flowratio(self, vmin:float=0, vmax:float=5):

        fig, ax = plt.subplots()

        im = ax.scatter(self.df['jet_speed'], self.df['weber'], 
                        s=self.df['jet_diameter']*8e6, c=self.df['flow_ratio'], 
                        cmap=self._cmap, vmin=vmin, vmax=vmax)

        ax.set_xlabel('Jet Speed [m/s]')
        ax.set_ylabel('Liquid Jet Weber Number')
        fig.colorbar(im, ax=ax, label=r'Flow Ratio [Q/$\dot{m}$]')
        ax.grid(alpha=0.5)

        return fig, ax


    def jet_length_predicted(self, sample='water', params=None,
                                ap=15.015, am=0.53):
        r''' plots the jet length according to calvo's prediction (2021)'''
        if (sample == 'water') and (params is None):
            w = self.water_params()
        elif params is not None:
            w = params
        else:
            raise ValueError("keys are wrong.")

        a = ap * self.df['jet_speed'] * np.sqrt(w['rho'] * self.df['jet_diameter']**3 / (2*w['sigma']))
        b = 1 + am * w['mu'] / np.sqrt(w['rho'] * w['sigma'] * self.df['jet_diameter'])

        return a * b

    def cost_function(self, params):
        ap, am = params
        pred_L = self.jet_length_predicted(ap=ap, am=am) * 1e6
        mse = np.mean((pred_L - self.df['jet_length'])**2)
        return mse

    def find_optimal_params(self):
        initial_guess = [15.015, 0.53]
        result = minimize(self.cost_function, initial_guess)
        return result.x

    def find_optimal_params_2(self, n_initial_points:int=30, base_estimator:str="GP"):
        space = [Real(1, 80, name='ap'), Real(0.001, 4, name='am')]
        optimizer = Optimizer(space, base_estimator=base_estimator, n_initial_points=n_initial_points)

        @use_named_args(space)
        def objective(**params):
            return self.cost_function(**params)

        for i in range(50):
            x = optimizer.ask()
            y = objective(x)
            optimizer.tell(x, y)

        optimal_params = optimizer.Xi[np.argmin(optimizer.yi)]
        return optimal_params

    def jet_length_pred_full(self, ap, am, dp, sample='water', params=None):
        if (sample == 'water') and (params is None):
            w = self.water_params()
        elif params is not None:
            w = params
        else:
            raise ValueError("keys are wrong.")

        num = w['sigma'] * self.df['weber']**2 * ap
        den = dp * (np.sqrt(self.df['capillary']**2 * am**2 + self.df['weber']) - self.df['capillary'] * am)
        return num / den

    def cost_function_full(self, ap, am, dp):
        pred_L = self.jet_length_pred_full(ap=ap, am=am, dp=dp) * 1e6
        mse = np.mean((pred_L - self.df['jet_length'])**2)
        return mse

    def find_optimal_params_full(self, n_initial_points:int=30, base_estimator:str="GP"):
        space = [Real(14, 16, name='ap'), Real(0.4, 0.6, name='am'), Real(703.069, 1000 * 703.069, name='dp')]
        optimizer = Optimizer(space, base_estimator=base_estimator, n_initial_points=n_initial_points)

        @use_named_args(space)
        def objective_full(**params):
            return self.cost_function_full(**params)

        for i in range(50):
            x = optimizer.ask()
            y = objective_full(x)
            optimizer.tell(x, y)

        optimal_params = optimizer.Xi[np.argmin(optimizer.yi)]
        return optimal_params


    def plot_jet_speed_calvo_compare(self, base_estimator:str='GP', n_initial_points:int=30):

        fig, ax = plt.subplots()

        ap, am, dp = self.find_optimal_params_full(base_estimator=base_estimator, n_initial_points=n_initial_points)
        pred_L = self.jet_length_pred_full(ap=ap, am=am, dp=dp) * 1e6

        ax.scatter(self.df['flow_ratio'], pred_L,
                        s=self.df['jet_diameter']*8e6, marker='x', c='b',
                        label=rf'Predicted ($α_ρ$={ap:0.2f}, $α_μ$={am:0.4f}, ΔP={dp/703.069:0.2f} PSI)')

        im = ax.scatter(self.df['flow_ratio'], self.df['jet_length'],
                        s=self.df['jet_diameter']*8e6, c='r',
                        label='Calculated')

        ax.set_xlabel(r'Flow Ratio [Q/$\dot{m}$]')
        ax.set_ylabel('Jet Length [μm]')
        ax.legend(fontsize=8)
        ax.grid(alpha=0.5)

        return fig, ax


if __name__ == '__main__':
# Load your DataFrame and create an instance of the GDVNAnalyzer class

    # filepath = "/Users/kkarpos/Desktop/NozzleData/79_water/79_water_all.csv"
    # filepath = "/Users/kkarpos/Desktop/NozzleData/20230110/79M_AA_50L_100G_analysis/79M_AA_50L_100G.csv"
    # filepath = "/Users/kkarpos/Desktop/NozzleData/20221215/79V1-DB-50G-50L-221205-02_G100_analysis/79V1-DB-50G-50L-221205-02_G100.csv"
    filepath = "/Users/kkarpos/Desktop/NozzleData2/79V1-DB-50G-50L-22126-04_water.csv"
    filepath = "/Users/kkarpos/Desktop/NozzleData2/565_63_water.csv"

    D = 75 # 75um, the gas orifice diameter

    data = pd.read_csv(filepath)
    dp = GDVNAnalyzer(data, D=D, threshold=0.75)

    water = dp.water_params()
    helium = dp.helium_params()

    # jitter = dp.df['stability']
    # dx = 50 # um
    # s = dx * np.tan(jitter) / (dp.df['jet_diameter'] * 1e6)

    # fig, ax = plt.subplots()
    # ax.scatter( dp.df['jet_diameter'] * 1e6, dx * np.tan(jitter))
    # fig1, ax1 = plt.subplots()
    # sca = ax1.scatter(dp.df['jet_diameter']*1e6, s)


    # Call the plotting methods
    # fig1, ax1 = dp.plot_weber_vs_capillary(water['rho'], water['mu'], water['sigma'])
    # fig2, ax2 = dp.plot_jet_diameter_vs_capillary(water['mu'], water['sigma'])
    # fig3, ax3 = dp.plot_weber_vs_reynolds(water['rho'], water['mu'], water['sigma'])
    # fig4, ax4 = dp.plot_gas_vs_velocity()
    # fig5, ax5 = dp.plot_jet_diameter_vs_velocity_3d()
    # fig6, ax6 = dp.plot_gas_velocity_vs_discharge_coefficient(helium)
    # fig7, ax7 = dp.plot_liquid_velocity_vs_discharge_coefficient(helium)
    fig8, ax8 = dp.plot_liquid_vs_gas()
    # fig9, ax9 = dp.plot_flow_ratio_vs_jet_speed()
    # fig10, ax10 = dp.plot_flow_ratio_vs_jet_speed_2()
    # fig11, ax11 = dp.plot_gas_vs_velocity_corrected_1()
    # fig12, ax12 = dp.plot_gas_vs_velocity_corrected_2()
    # fig13, ax13 = dp.plot_jet_speed_vs_stability()
    # fig14, ax14 = dp.plot_gas_flow_vs_stability()
    # fig15, ax15 = dp.plot_liquid_vs_gas_stability()
    # fig16, ax16 = dp.plot_stability_vs_flow_ratio()
    # fig17, ax17 = dp.plot_stability_vs_flow_ratio_2()


    # dp.plot_jet_speed_vs_weber(water['rho'], water['sigma'])
    # dp.plot_jet_speed_vs_reynolds(water['rho'], water['mu'])
    # dp.plot_gas_flow_vs_weber(water['rho'], water['sigma'])
    # dp.plot_gas_flow_vs_reynolds(water['rho'], water['mu'])

    # dp.plot_liquid_vs_gas(cbar_type='weber')
    # dp.plot_liquid_vs_gas(cbar_type='reynolds')
    # dp.plot_liquid_vs_gas(cbar_type='capillary')

    # dp.plot_reynolds_diameter_comparison(reynolds=20, gas_flow=10)
    # # dp.plot_jetdiameter_reynolds_liquidflow()

    # dp.plot_reynolds_meshgrid(resolution=100)

    # dp.plot_speed_v_reynolds_flowratio(vmin=0, vmax=2)
    # dp.plot_speed_v_weber_flowratio(vmin=0, vmax=2)

    # dp.plot_jet_speed_calvo_compare(base_estimator='GP', n_initial_points=150)

    # add a jet speed vs jet diameter (thickness/liq flow rate (~photons/sample)) maybe also add flow ratio?
    # at each datapoint on the onset, try changing the marker style to indicate the transition region

    plt.show()





















