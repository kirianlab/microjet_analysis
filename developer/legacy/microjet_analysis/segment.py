import numpy as np
import scipy
try:
    from .utils import check_shapes
except:
    from utils import check_shapes

def nozzle_tip_provided(nframes:int, nozzle_tip_index:int):
    r""" Creates a list of index whose length is the number of frames

        In some cases, the automated nozzle tip finder methods don't work.
        This method is useful when the auto nozzle tip detection does not work.

        Args:
            nframes (int): the number of frames in your stack
            nozzle_tip_index (int): The pixel index corresponding to the nozzle tip

        Returns:
            (list): a list of length nframes of the same index position
    """
    x = np.ones(nframes) * nozzle_tip_index
    return x

def nozzle_tip_simple(stack):
    r"""Determines the location of the nozzle tip for an image stack

        Given an image stack with the nozzle tip in view, will
        take the total summed image across the whole stack, project
        the image down the long axis, and determine where the peak
        is. The incoming image stack should be thresholded and made 
        binary before calculating this value.

        This function is best used when you're certain the nozzle tip
        location isn't changing per-shot. 

        Args:
            stack (|ndarray|): The 3D image stack
        Returns:
            (|ndarray|): List of index values
    """
    nidx = [find_nozzle_tip_by_column(im) for im in stack]
    # n_frames = np.shape(stack)[0]
    # xs = np.sum(stack, axis=0)
    # xs1 = np.abs(np.sum(xs, axis=1))
    # n = np.where(xs1 > 2.5 * np.mean(xs1))
    # nidx = [n[0][0] for i in range(n_frames)]
    return np.array(nidx)

def find_nozzle_tip_by_column(image):
    """
    Given a binary image, finds the nozzle tip index position.

    Args:
        image (|ndarray|): The binary image containing the nozzle tip region.

    Returns:
        int: The index position of the nozzle tip.
    """
    image = check_shapes(image)
    white_pixel_columns = np.nonzero(image)[1]

    if white_pixel_columns.size == 0:
        return -1

    # Filter out columns with very few white pixels
    unique_columns, counts = np.unique(white_pixel_columns, return_counts=True)
    filtered_columns = unique_columns[counts > np.percentile(counts, 90)]

    if filtered_columns.size == 0:
        return -1

    rightmost_white_pixel_column = np.max(filtered_columns)

    return rightmost_white_pixel_column

def nozzle_tip_shotbyshot(stack):
    r"""Determines the location of the nozzle tip for an image stack

        Given an image stack with the nozzle tip in view, will
        attempt to find the nozzle tip for each shot in the stack.

        This function is best used when the nozzle is vibrating enough
        to make the nozzle tip move per shot. 

        Args:
            stack (|ndarray|): The 3D image stack
        Returns:
            (|ndarray|): List of index values
    """
    ntip_idx = np.zeros(len(stack), dtype=int)
    for i, im in enumerate(stack):
        x = np.sum(im, axis=1)
        maxx = np.max(x)
        if maxx > np.percentile(x, 90):
            idx = np.where(x == np.max(x))[0]
            if len(idx) > 1:
                idx = idx[0]
            ntip_idx[i] = idx
        else:
            # if the previous condition failed, we assume no nozzle tip is in view
            ntip_idx[i] = np.shape(im)[0]
    return ntip_idx

def remove_nozzle_tip(stack, ntip_idx):
    r""" Will remove the nozzle tip from a stack of images

        Args:
            stack (|ndarray|): The 3D image stack
            ntip_idx (|list|): A list of nozzle tip index positions

        Returns:
            (|ndarray|): The segmented image stack
    """
    mi, ma = np.min(ntip_idx), np.max(ntip_idx)
    if mi == ma:
        stack = stack[:,:int(mi),:]
    else:
        for i, idx in enumerate(ntip_idx):
            stack[i, int(idx):, :] = 0
        stack = stack[:,:int(ma),:]
    return stack

def find_longest_nonzero_subset(data):
    r"""Given a list of numbers, will determine the 
        longest set of nonzero numbers
    
        Args:
            data (|list|): A list of numbers
        Returns:
            (|list|): The subset of data with the longest stretch of nonzero values
    """
    max_subset = []
    current_max_subset = []
    for i, n in enumerate(data):
        if n > 0:
            current_max_subset.append(i)
            if len(current_max_subset) > len(max_subset):
                max_subset = current_max_subset
        else:
            current_max_subset = []
    return max_subset


def find_transition_region(data):
    r"""Given a single cleaned up image, will find jet breakup region

        Note: Incoming data should not have the nozzle tip in view, 
                otherwise it will skew the result. Works best on 
                a single binary image.

        Args:
            data (|ndarray|): a single 2D cleaned-up image 
        Returns:
            (|int|): An index position corresponding to the jet 
                        breakup region. Will return None if there is
                        an error
    """
    x = np.sum(data, axis=1)
    try:
        xs = find_longest_nonzero_subset(x)
        jr_idx = int(xs[0])
    except IndexError:
        jr_idx = -1
    return jr_idx





























