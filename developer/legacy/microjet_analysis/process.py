import numpy as np
from scipy import ndimage
from skimage import morphology, measure

def binary_erosion(stack, iterations:int):
    r"""Wrapper for the scipy.ndimage.binary_erosion method

        Args:
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed stack whose shape is the same as the incoming stack
    """
    stack = stack.copy()
    for i in range(stack.shape[0]):
        stack[i, :, :] = ndimage.binary_erosion(stack[i, :, :], iterations=iterations)
    return stack

def binary_dilation(stack, iterations:int):
    r"""Wrapper for the scipy.ndimage.binary_dilation method

        Args:
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed stack whose shape is the same as the incoming stack
    """
    stack = stack.copy()
    for i in range(stack.shape[0]):
        stack[i, :, :] = ndimage.binary_dilation(stack[i, :, :], iterations=iterations)
    return stack

def remove_small_objects(stack, small):
    r"""Labels an image stack and removes small pixels

        Note: Incoming image stack should be thresholded so the background is all zeros

        Args:
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)
            small (int): removes pixels below this size

        Returns:
            Processed stack whose shape is the same as the incoming stack
    """
    stack = stack.copy()
    for i in range(stack.shape[0]):
        labeled = measure.label(stack[i, :, :], background=0)
        stack[i, :, :] = morphology.remove_small_objects(labeled, small)
    return stack

def binary_closing(stack, footprint=None, iterations:int=3):
    r"""Wrapper for the scipy.ndimage.binary_closing method

        Args:
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)
            footprint (ndarray): structure kernel passed to the binary closing methods
            iterations (int): the number of iterations passed to the binary erosion method

        Returns:
            Processed stack whose shape is the same as the incoming stack
    """
    stack = stack.copy()
    for i in range(stack.shape[0]):
        stack[i, :, :] = ndimage.binary_closing(stack[i, :, :], 
                                                structure=footprint, iterations=iterations)
    return stack

def binary_fill_holes(stack):
    r"""Wrapper for the scipy.ndimage.binary_fill_holes method

        Args:
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)

        Returns:
            Processed stack whose shape is the same as the incoming stack
    """
    stack = stack.copy()
    for i in range(stack.shape[0]):
        stack[i, :, :] = ndimage.binary_fill_holes(stack[i, :, :])
    return stack

def label_watershed(stack):
    r""" Uses a watershed method to do morphology labeling

        Args: 
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)

        Returns:
            The labeled image stack of the same shape as the incoming stack
    """
    stack = stack.copy()
    dist = [ndimage.distance_transform_edt(x) for x in stack]
    coords = [peak_local_max(dist[i], footprint=np.ones((3, 3)), labels=stack[i]) for i, im in enumerate(stack)]

    mask = np.zeros(np.shape(dist), dtype=bool)
    for i, c in enumerate(coords):
        mask[i, tuple(c.T)] = True

    markers = [ndimage.label(m)[0] for m in mask]
    labels = np.array([watershed(-dist[i], markers[i], mask=stack[i]) for i in range(len(stack))])
    return labels

def label_standard(stack):
    r""" Uses the standard method to do morphology labeling

        Args: 
            stack (ndarray): the image stack of shape (n_frames, n_fs, n_ss)

        Returns:
            The labeled image stack of the same shape as the incoming stack
    """
    return np.array([measure.label(sf, background=0) for sf in stack])

def subtract_stack_median(stack):
    med = np.median(stack, axis=0).astype(int)
    diff = np.subtract(stack, med)
    return diff















