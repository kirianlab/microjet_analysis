import numpy as np
from skimage import transform
import matplotlib.pyplot as plt

def hough_jet_fit(image, angles, xarange, display=False):
    r""" Calculates the Hough Transform for a single image

        Args:
            image (|ndarray|): a single image of shape (ns, nf)
            angles (list): the angles to use in the hough line peaks method
            xarange (|list|): a list of ns values to use with the hough method

        Returns:
            The
    """
    hough, theta, dist = transform.hough_line(image, theta=angles)
    _, ang, d = transform.hough_line_peaks(hough, theta, dist)
    if display:
        plt.figure()
        plt.title('Hough transform')
        plt.imshow(hough)
        plt.draw()
    if len(d) == 0:
        return None
    y = (d[0] + xarange * np.cos(ang[0] + np.pi / 2)) / np.sin(ang[0] + np.pi / 2)
    if display:
        plt.figure()
        plt.title('Line Fit')
        plt.imshow(image.T, cmap='gray')
        plt.plot(xarange, y, color='red')
        plt.show()
    return y

def fit_jet(stack):
    n_frames = np.shape(stack)[0]
    fits = np.zeros((n_frames, np.shape(stack)[1]))
    tested_angles = np.linspace(-np.pi / 10, np.pi / 10, 1000, endpoint=False)
    xarange = np.arange(np.shape(stack)[1])
    for i, im in enumerate(stack):
        fits[i] = hough_jet_fit(im, tested_angles, xarange)
    return fits

def get_fit_params(fit, x1:int, x0:int=0):
    r"""
        Returns the slope and y-intercept given a list.
        The incoming `fit` is assumed to be the data fit line,
        and assumed to be a line.

        Args: 
            fit (|list|): The fit line of your data, assumed to be 'y' 
                            in the equation y = mx + b.
            x1 (|int|): One of the x-coordinates used in the slope calculation
            x0 (|int|): The second x-coordinate in the slope calculation
        Returns:
            (|tuple|): The slope and y-intercept of the fit line
    """
    b0, b1 = fit[0], fit[-1]
    m = (b1 - b0) / (x1 - x0)
    return m, b0
































































