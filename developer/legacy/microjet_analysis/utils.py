import numpy as np
import h5py
import glob
import os
import json


def zoom_fixer(filepath:str, pixel_size:float):
    r"""Updates the zoom value of the h5 and text files.
    
        Sometimes we forget to update the zoom value in Odysseus. That's okay. 
        This function fixes the zoom value in the event you forget to update it 
        during data collection. 

        Note that in order for this to work, you must know the correct zoom value.
        All this function does is replace a value in an existing h5 file.

        TO DO: Edit this to accept a subset of runs, rather than the full data list.

        Args:
            filepath (str): the full path to the data, not including the h5 file
                                e.g., "/path/to/the/data/"
            pixel_size (float): the correct pixel size, used to update the metadata

    """

    all_data = sorted(glob.glob(os.path.join(filepath, "*.h5")))
    for i,im in enumerate(all_data):
        with h5py.File(im, 'r+') as f:
            x = f['metadata'][()]
            y = json.loads(x)
            y['optics']['pixelsize_um'] = pixel_size
            f['metadata'][()] = json.dumps(y, indent=4)


def check_shapes(data):
    data = np.atleast_3d(data)
    if np.shape(data)[-1] == 1:
        data = np.transpose(data, (2,0,1))
    return data













