import sys
import os
import glob

import numpy as np
import imageio 
import h5py
import json

from . import dataio
from matplotlib import tri


class LiveAnalysis():
    r"""A class to grab and update frames from the Kirian Lab microjet testing station"""

    
    _all_data = None  # all data in main directory
    _analyzed_data = None  # all data analyzed
    _data_to_analyze = None  # all the data to be analyzed
    _current_data = None  # single h5 to be analyzed
    _image_stack = None  # the frames from the fastcam
    _image_metadata = None  # all the metadata saved 
    _analyzed_data_save_path = None
    _csv = None # the csv file with all the compiled data


    def __init__(self, config):
        self._config = config
        self._make_analysis_dir()
        self.update_data()
        self.pathogen = lambda p: os.path.normpath(os.path.abspath(os.path.expanduser(p)))
        csv_path = self.pathogen(os.path.join(self._config['csv_data'], self._config['nozzle_id']+".csv"))
        dataio.analysis_dataframe(self._config, save=True, savepath=csv_path, verbose=True)
        # print("Saving updated CSV file here: \n\t", self._config["csv_data"], flush=True)

    @property
    def config(self):
        return self._config

    @property
    def all_data(self):
        return self._all_data

    @property
    def analyzed_data(self):
        return self._analyzed_data

    @property
    def data_to_analyze(self):
        return self._data_to_analyze

    @property
    def current_data(self):
        return self._current_data

    @property
    def image_stack(self):
        return self._image_stack

    @property
    def image_metadata(self):
        return self._image_metadata

    @property
    def analyzed_data_save_path(self):
        return self._analyzed_data_save_path

    @property
    def to_csv(self):
        return self._csv


    def _make_analysis_dir(self):
        os.makedirs(self._config['save_path'], exist_ok=True)
        os.makedirs(self._config['figure_save_path'], exist_ok=True)
        os.makedirs(self._config['gif_save_path'], exist_ok=True)

    def update_csv(self):
        csv_path = self.pathogen(os.path.join(self._config['csv_data'], self._config['nozzle_id']+".csv"))
        dataio.analysis_dataframe(self._config, save=True, savepath=csv_path, verbose=True)

    def update_data(self):
        self._all_data = sorted(glob.glob(os.path.join(self._config['data_path'], "*.h5")))
        self._analyzed_data = sorted(glob.glob(os.path.join(self._config['save_path'], "*.h5")))
        self._current_data = None

        try: # needed in case the h5 file transfer doesn't finish before attempting to analyze
            if len(self._all_data) == len(self._analyzed_data):
                pass
            else:
                if len(self._analyzed_data) == 0:
                    self._current_data = self._all_data[0]
                else:
                    adat = [a[-6:] for a in self._analyzed_data]
                    self._data_to_analyze = [d for d in self._all_data if d[-6:] not in adat]
                    self._current_data = self._data_to_analyze[0]

                self.update_csv()
                
                q = self._current_data.split('/')[-1]
                self._analyzed_data_save_path = os.path.join(self._config['save_path'], q)
                if self._current_data is None:
                    pass
                else:
                    self._image_stack, self._image_metadata = dataio.h5_metadata(self._current_data)
        except OSError:
            pass
        except AttributeError:
            pass


class PlotJetData:
    r"""A class to plot the analyzed data from the Kirian Lab microjet testing station

        Things to determine and general questions:

        Notes:
            1) This only works for a single nozzle type - any nozzle comparisons should be custom
    """

    _stack_data = None
    _nozzle_id = None

    # data lists
    _jet_speed = []
    _jet_speed_err = []
    _liquid_flow = []
    _gas_flow = []
    _jet_diameter = []
    _jet_deviation = []
    _jet_deviation_err = []
    _jet_stability = []


    def __init__(self, stack_data: dict = {}, nozzle_id: str = ""):
        self._stack_data = stack_data
        self._nozzle_id = nozzle_id

        # check if data has already been analyzed
        self._build_data_lists_on_init()

    @property
    def nozzle_id(self):
        return self._nozzle_id

    @property
    def jet_speed(self):
        return np.array(self._jet_speed)

    @property
    def jet_speed_err(self):
        return np.array(self._jet_speed_err)

    @property
    def liquid_flow(self):
        return np.array(self._liquid_flow)

    @property
    def gas_flow(self):
        return np.array(self._gas_flow)

    @property
    def jet_diameter(self):
        return np.array(self._jet_diameter)

    @property
    def jet_length(self):
        return np.array(self._jet_length)

    @property
    def jet_length_err(self):
        return np.array(self._jet_length_err)

    @property
    def jet_deviation(self):
        return np.array(self._jet_deviation)

    @property
    def jet_deviation_err(self):
        return np.array(self._jet_deviation_err)

    @property
    def jet_stability(self):
        return np.array(self._jet_stability)

    def update(self, data: dict = {}):
        r"""Updates the data lists with new data points
        """
        self._data_dict_to_lists(data)

    def _build_data_lists_on_init(self):
        r"""Reorganizes data for plotting

            Given the stack_data dictionary, will reorganize the data
            into multiple lists.
        """
        self._jet_speed = [v['jet_speed'] for k, v in self._stack_data.items()]
        # self._jet_speed_err = [v['jet_speed_err'] for k, v in self._stack_data.items()]
        self._liquid_flow = [v['liquid_flow'] for k, v in self._stack_data.items()]
        self._gas_flow = [v['gas_flow'] for k, v in self._stack_data.items()]
        self._jet_diameter = [v['jet_diameter'] for k, v in self._stack_data.items()]
        self._jet_length = [v['jet_length'] for k, v in self._stack_data.items()]
        self._jet_length_err = [v['jet_length_err'] for k, v in self._stack_data.items()]
        self._jet_deviation = [v['jet_deviation'] for k, v in self._stack_data.items()]
        self._jet_deviation_err = [v['jet_deviation_err'] for k, v in self._stack_data.items()]
        self._jet_stability = [v['jet_stability'] for k, v in self._stack_data.items()]

    def _data_dict_to_lists(self, data: dict = {}):
        r"""Updates
        """
        self._jet_speed.append(data['jet_speed'])
        # self._jet_speed_err.append(data['jet_speed_err'][()])
        self._liquid_flow.append(data['liquid_flow'])
        self._gas_flow.append(data['gas_flow'])
        self._jet_diameter.append(data['jet_diameter'])
        self._jet_length.append(data['jet_length'])
        self._jet_length_err.append(data['jet_length_err'])
        self._jet_deviation.append(data['jet_deviation'])
        self._jet_deviation_err.append(data['jet_deviation_err'])
        self._jet_stability.append(data['jet_stability'])


    def interpolate_ranges(self, ranges, data, level_step: int = 5):
        r"""Prepares a basic contour plot given data

            Args:
                ranges (list or tuple): the x, y, and z ranges for plotting should be [x,y,z]
                data (list or tuple): the x, y, and z data, should be [x,y,z]
                level_step (int): the amount of countour levels between each datapoint

            Returns:
                xi (ndarray): the x grid values
                yi (ndarray): the y grid values
                zi (ndarray): the interpolated z grid values
                levels (ndarray): The number of levels used in the color gradient

        """

        # Make a contour plot of irregularly spaced data
        # coordinate via interpolation on a grid.
        n_levels = int((ranges[2][1] - ranges[2][0]) / level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(2 * n_levels + 10))

        # Create grid values first.
        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
        interpolator = tri.LinearTriInterpolator(tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels














