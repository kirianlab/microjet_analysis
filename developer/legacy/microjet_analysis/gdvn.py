import os
import numpy as np
import pandas as pd
import pyqtgraph as pg
import pylab as plt
from pyqtgraph import QtCore
from pyqtgraph.Qt import QtWidgets as qwgt
import h5py
import scipy
from scipy import ndimage as ndi
from skimage.filters import threshold_otsu, threshold_local
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
from skimage.segmentation import watershed
from skimage.feature import peak_local_max
import skimage
import glob
from . import filters, segment, process, viewers, dataio, analysis
import time, sys
from joblib import Parallel, delayed


# to determine if there's no jet live, try calculating the 
# covariance of all x and y (maybe diagonalize after) (check wiki)
# jet is indicated by a large (or small) ratio (not close to 1).


class JetAnalysis:
    r""" A class to analyze GDVN microjet image data. 

        Works with both brightfield and darkfield data.
        Use dataio.analysis_config() to define your specific parameters. 

    """

    _stack_filtered = None
    _stack_thresholded = None

    def __init__(self, image_stack, image_metadata, config=None):

        # =============================
        # User defined properties
        # =============================

        if config is None:
            config = dict()
        
        # Load and confirm the incoming config is correct
        self.config = dataio.analysis_config(config)
        self._check_config(self.config)

        # Grab the raw image stack
        self._image_stack_raw = image_stack.astype(np.float32)
        self._check_shapes()  # ensure the image stack is in the correct orientation

        # Allow the user to input a certain number of frames for quick viewing
        if self._image_stack_raw.shape[0] > self.config['max_frames']:
            self._image_stack_raw = self._image_stack_raw[0:self.config['max_frames'], :, :]

        self._image_stack = self._image_stack_raw.copy()
        if self.config['brightfield'] is False:
            # Flip the image stack if the data is taken in the darkfield
            self._image_stack *= -1

        if config['normalize_raw']:
            # Normalize the stack so that we build a common point of reference for how "dark" a typical drop/jet is.
            if config['brightfield']:
                for i in range(self._image_stack.shape[0]):
                    self._image_stack[i, :, :] /= np.median(self._image_stack[i, :, :])
            elif (config['brightfield'] is False) and ('nozzle_tip_finder_method' == 'provided'):
                for i in range(self._image_stack.shape[0]):
                    nonz = np.nonzero(self._image_stack[i, :self.config['nozzle_tip_position'][i], :])
                    self._image_stack[i, :, :] /= np.median(self._image_stack[i, :, :][nonz])

        # Save the image metadata
        self._image_metadata = image_metadata
        self._image_metadata = self._update_metadata()

        # =============================
        # Extracted Stack Information
        # =============================

        # return the number of frames in the video
        self._n_frames = np.shape(self._image_stack_raw)[0]
        self._image_shape = np.shape(self._image_stack_raw)[1:3]  # should return (1024, 512), or whatever your resolution is



        # =============================
        # Stack Cleanup
        # =============================

        t = time.time()
        if self.config['parallelize']:
            split_stack = np.array_split(self._image_stack, self.config['n_cores'], axis=0)
            all_stacks = Parallel(self.config['n_cores'])(delayed(self.stack_cleanup)(split_stack[i]) for i in range(self.config['n_cores']))
            self._image_stack = np.concatenate([all_stacks[i][0] for i in range(self.config['n_cores'])])
            print(f"Time to cleanup image stack: {time.time() - t:0.3f} s")
            if self.config['save_stacks']['filtered_stack']:
                self._stack_filtered = np.concatenate([all_stacks[i][1]for i in range(self.config['n_cores'])])
            else:
                self._stack_filtered = None

            if self.config['save_stacks']['thresholded_stack']:
                self._stack_thresholded = np.concatenate([all_stacks[i][2]for i in range(self.config['n_cores'])])
            else:
                self._stack_thresholded = None

            if self.config['save_stacks']['processed_stack']:
                self._stack_processed = np.concatenate([all_stacks[i][3]for i in range(self.config['n_cores'])])
            else:
                self._stack_processed = None
        else:
            self._image_stack, self._stack_filtered, \
                self._stack_thresholded, self._stack_processed = self.stack_cleanup(self._image_stack)

        # filter outlier frames
        if self.config['processing_steps']['filter_outlier_frames']:
            self._image_stack = self.outlier_filter(self._image_stack, self.config['processing_steps']['outlier_filter_threshold'])
            self._n_frames = np.shape(self._image_stack)[0]

        # =============================
        # Image Segmentation
        # =============================

        # Find the nozzle tip
        if self.config['nozzle_tip_finder_method'] == 'simple':
            self._nozzle_tip_idx = self._ntip_simple(self._image_stack)
        elif self.config['nozzle_tip_finder_method'] == 'shot-by-shot':
            self._nozzle_tip_idx = self._ntip_shotbyshot(self._image_stack)
        elif self.config['nozzle_tip_finder_method'] == 'provided':
            # If the automation doesn't work, the user can input this parameter manually (in pixel index)
            self._nozzle_tip_idx = np.ones(len(self._image_stack)).astype(int) * int(self.config['nozzle_tip_position'])
        else:
            None #self._nozzle_tip_idx = np.ones(len(self._image_stack)).astype(int) * int(self.config['nozzle_tip_position'])

        # Segment the image stack to mask out the nozzle tip
        if self._nozzle_tip_idx is not None:
            self._jet_region = self._remove_nozzle_tip(self._image_stack, self._nozzle_tip_idx)
        else:
            self._jet_region = self._image_stack.copy()

        # get the first jet fit, need this to get the jet ROI
        if self.config['fit_jet']:
            # self._jet_fit = self._fit_jet(self._jet_region)
            self._jet_fit = analysis.fit_jet(self._jet_region)
        else:
            self._jet_fit = None
        # pg.image(self._jet_region)
        # pg.mkQApp().exec_()
        # plt.imshow(self._jet_fit, aspect='auto')
        # plt.show()
        # print(self._jet_fit)

        # exit()

        # Find the jet ROI for smaller arrays
        reg2 = self._get_jetregion_roi(self._image_stack)
        if len(reg2) != 0:
            self._jet_region = reg2
            # update the jet fit with the new roi
            if self.config['fit_jet']:
                # self._jet_fit = self._fit_jet(self._jet_region)
                self._jet_fit = analysis.fit_jet(self._jet_region)
            else:
                self._jet_fit = None
        # exit()





        # Segment the image stack to mask out everything but the droplets
        if self.config['segment_droplets']:
            if self.config['transition_region_finder_method'] == 'simple':
                self._transition_region_idx = self._find_transition_region()
                self._droplet_region = self._isolate_droplet_region(stack=self._jet_region, idx=self._transition_region_idx)
                neg_drops_idx = np.where(self._droplet_region < 0)
                self._droplet_region[neg_drops_idx] = 0
            elif self.config['transition_region_finder_method'] == 'provided':
                self._transition_region_idx = np.ones(len(self._image_stack)).astype(int) * int(self.config['transition_region_position'])
                self._droplet_region = self._isolate_droplet_region(stack=self._jet_region, idx=self._transition_region_idx)
        else:
            print("No droplet region requested... defaulting to full jet region.")
            self._transition_region_idx = None
            self._droplet_region = self._jet_region

        # TO DO: Add ROI regions for both jet and droplet regions! Use the ROI for the correlation
        # to minimize background correlations

        # subtract the stack median of just the droplets to remove outliers
        if self.config['subtract_stack_median']:
            self._droplet_region = process.subtract_stack_median(self._droplet_region)


        # =============================
        # Analysis Methods
        # =============================

        # define the region properties for labeling
        if self.config['region_properties'] is None:
            self._region_properties = ("label", "area", "centroid", "coords", "image", "eccentricity")
        else:
            self._region_properties = self.config['region_properties']

        # label the droplet stack
        if self.config['find_droplet_centroids']:
            self._droplet_stack_labeled = self._label_stack(self._droplet_region)
            self._droplet_centroids, self._droplet_areas = self._get_droplet_centroids(self._droplet_stack_labeled)
        else:
            self._droplet_stack_labeled = None
            self._droplet_centroids, self._droplet_areas = None, None



        # get the droplet translation
        if self.config['jet_speed_method'] == '2D_cross_correlation':
            if self.config['cross_correlation_parity'] is None:
                self._correlation_matrix, self.pf, self.ps = self._cross_correlation_calculation(self._droplet_region, parity='even')
                # check if the cross correlation passes the threshold condition, redo if it doesn't
                if self._redo_cc(cond=self.config['cross_correlation_parity_condition']):
                    print("Correlation matrix failed condition with even frames, trying again with the odd frames", flush=True)
                    self._correlation_matrix, self.pf, self.ps = self._cross_correlation_calculation(self._droplet_region, parity='odd')
                    # if self._redo_cc(cond=self.config['cross_correlation_parity_condition']):
                        # TO DO: Add a flag here for 'bad data', maybe add to a new 'quality' key to the h5
            elif (self.config['cross_correlation_parity'] == 'even') or (self.config['cross_correlation_parity'] == 'odd'):
                parity = self.config['cross_correlation_parity']
                self._correlation_matrix, self.pf, self.ps = self._cross_correlation_calculation(self._droplet_region, parity=parity)
            else:
                raise KeyError("Incorrect parity value, correct key values are 'even', 'odd', or None. ")
            self._droplet_translation, _ = self.js_cross_correlation()
            self._outer_product = None
        elif self.config['jet_speed_method'] == 'outer_product':
            self._correlation_matrix = self._outer_product_matrix()
            self._outer_product = self._outer_product_projection()
            self._droplet_translation = self._get_droplet_translaton()
            self.pf, self.ps = None, None

        # =============================
        # Useful Parameter Calculations
        # =============================

        self._jet_speed = self._get_jet_speed()
        self._jet_diameter = self._get_jet_diameter()
        self._jet_diameter_err = None
        if self._image_metadata['liquid_flow_mean'] is not None:
            self._jet_diameter_err = self._get_jet_diameter_error()

        self._jet_deviation = self._get_jet_deviation()
        self._jet_deviation_avg = np.nanmean(self._jet_deviation)
        self._jet_deviation_err = np.nanstd(self._jet_deviation)

        self._jet_length = self._get_jet_length()
        self._jet_length_avg = np.nanmean(self._jet_length)
        self._jet_length_err = np.std(self._jet_length)

        self._jet_stability = self._get_jet_stability()

        # =============================
        # Stack Stats
        # =============================

        self._stack_mean_image = self._stack_mean(self._image_stack)
        self._stack_sdev_image = self._stack_sdev(self._image_stack)
        self._stack_var_image = self._stack_var(self._image_stack)

    # =============================
    # Class Properties
    # =============================

    # ----------------------------------------------------------
    # extracted information from user-inputted image stack
    # ----------------------------------------------------------

    @property
    def image_stack_raw(self):
        r"""The image stack straight from the camera
            
            Returns:
            (ndarray): The raw image stack
            
        """
        return self._image_stack_raw

    @property
    def image_metadata(self):
        r"""A dictionary with metadata from the data collection

            Returns: 
                (dict): Dictionary containing the keys:
                            **year**, **date**, **time**, **gas_flow**,
                            **gas_flow_units**, **liquid_flow**, **liquid_flow_units**, 
                            **pixel_size**, **delay**
        """
        return self._image_metadata

    @property
    def n_frames(self):
        r"""The number of frames in the movie 

            Returns: 
                (int): The number of frames in the movie
        """

        return self._n_frames

    @property
    def image_shape(self):
        r"""The shape (resolution) of the movie. 

            Returns:
                (tuple): The image resolution as (slow-scan, fast-scan)
        """
        return self._image_shape

    # ----------------------------------------------------------
    # stack cleanup
    # ----------------------------------------------------------

    @property
    def stack_filtered(self):
        r"""The image stack after filtering out the background

            Returns:
                (ndarray): The image stack with filtering methods applied

        """
        return self._stack_filtered

    @property
    def stack_thresholded(self):
        r"""The thresholded (binary) image stack -- no other processing 
            
            Returns:
                (ndarray): The image stack with thresholding methods applied

        """
        return self._stack_thresholded

    @property
    def stack_processed(self):
        r"""Morphology operations applied to the thresholded stack to fill in pixel islands

        Returns:
                (ndarray): The image stack with processing methods applied
        """
        return self._stack_processed

    @property
    def image_stack(self):
        r""" The final image stack after all processing methods are finished"""
        return self._image_stack

    @property
    def nozzle_tip_position(self):
        r"""The index position of the nozzle tip 

            Returns:
                list (int): a list of slow-scan index positions
        """
        return self._nozzle_tip_idx

    @property
    def transition_region_position(self):
        r"""The index position of the nozzle tip 

            Returns:
                list (int): a list of slow-scan index positions
        """
        return self._transition_region_idx

    @property
    def jet_region(self):
        r"""The segmented jet region stack (jet and droplets) 

            Returns:
                (ndarray): The segmented jet region stack whose size is the same as the raw image stack
        """
        return self._jet_region

    @property
    def droplet_region(self):
        r"""The segmented droplet region stack (just droplets) 

            Returns:
                (ndarray): The segmented droplet region stack whose size is the same as the raw image stack
        """
        return self._droplet_region

    @property
    def droplet_centroids(self):
        r"""(ndarray) A stack of the calculated centroid locations"""
        return self._droplet_centroids

    @property
    def correlation_matrix(self):
        r"""A statistical measure of jet speed
            
            Returns:
                (ndarray): The correlation calculated using the outer product method or the 2D correlation matrix method
        """
        return self._correlation_matrix

    @property
    def jet_speed(self):
        return np.array(self._jet_speed)

    @property
    def jet_diameter(self):
        r"""(float) The calculated jet diameter """
        return self._jet_diameter

    @property
    def jet_diameter_err(self):
        r"""(float) The calculated jet diameter """
        return self._jet_diameter_err

    @property
    def stack_mean(self):
        r"""The mean image across an image stack (applied to the frame axis)

            Returns:
                (ndarray): The mean image stack

        """
        return self._stack_mean_image

    @property
    def stack_sdev(self):
        r"""The standard deviation image across an image stack (applied to the frame axis)

            Returns:
                (ndarray): The standard deviation of the image stack
        """
        return self._stack_sdev_image

    @property
    def stack_var(self):
        r"""The variance image across an image stack (applied to the frame axis)

            Returns:
                (ndarray): The variance of the image stack
        """
        return self._stack_var_image

    @property
    def jet_fit(self):
        r"""The calculated shot-by-shot jet fit"""
        return self._jet_fit

    @property
    def jet_deviation_avg(self):
        r"""The calculated average jet deviation angle in degrees"""
        return self._jet_deviation_avg

    @property
    def jet_deviation_std(self):
        r"""The error in the calculated jet deviation angle"""
        return self._jet_deviation_err

    @property
    def jet_length_avg(self):
        r"""The calculated average jet length in microns"""
        return self._jet_length_avg

    @property
    def jet_length_std(self):
        r"""The error in the calculated jet length"""
        return self._jet_length_err

    @property
    def jet_stability(self):
        r"""The calculated jet stability: $s = d_{jet} / D\tan\theta$"""
        return self._jet_stability

    # =============================
    # Class Methods
    # =============================

    # ----------------------------------------------------------
    # utility methods
    # ----------------------------------------------------------

    def _check_shapes(self):
        r"""A quick test to make sure the image shape lines up with the code structure

            Notes:
                Not sure why, but sometimes the image shapes read as 512 x 1024
                rather than 1024 x 512. This code is built around the latter shape...

            Returns:
                None: Updates the self._image_shape property with a corrected shape
        """
        x, y = np.shape(self._image_stack_raw)[1:]
        imst = self._image_stack_raw
        if x < y:
            print("Need to fix the shapes! Swapping axis 1 and 2.")
            imnew = np.swapaxes(imst, 1, 2)
            self._image_stack_raw = imnew
            self._image_shape = np.shape(self._image_stack_raw)[1:3]

    def _check_config(self, config):
        r"""Some checks to make sure the user abides by certain rules"""
        if config['parallelize']:
            if config['n_cores'] > os.cpu_count():
                raise ValueError("Requested more cores than are available.")

    # ----------------------------------------------------------
    # stack cleanup
    # ----------------------------------------------------------

    def _filter_stack(self, stack):
        r""" Detects edges and filters background
            
            To Do:
                Add more options for background filtering, only the 'sobel' and 'gausshp' filters are available.

            Args:
                stack: The images that make up your movie. This will also work on a single image.
                filter_method (str): The filtering method of choice. 

            Returns:
                stack (ndarray): a filtered stack of the same shape as the input stack
        """

        method = self.config['filter_method']
        params = self.config['filter_parameters']
        butter = self.config['add_butter_filter']

        stack = stack.copy()

        if method == 'gausshp':
            stack = filters.gausshp(stack, params['sigma'], truncate=params['truncate'])
        elif method == 'sobel':
            stack = filters.sobel(stack)
        else:
            raise KeyError(f"filter_method {method} is not recognized.")
        if butter:
            stack = filters.butterworth_lowpass(stack, 2, 30, 2)
        return np.array(stack)

    def _threshold_stack(self, stack):
        r""" Will threshold an image stack using the provided method
            
            Current thresholding options are:
                'otsu', 'otsu_clipped', 'hysteresis'

            To Do:
                Add the ability to choose any thresholding method from the filters.py module.


            Args:
                stack (array): The image stack, np.ndarray or not

            Returns:
                (ndarray): The thresholded image stack
        """
        method = self.config['threshold_method']
        if method == 'otsu':
            thresh_stack = filters.threshold_otsu(stack)
        elif method == 'otsu_clipped':
            thresh_stack = filters.threshold_otsu_clipped()
        elif method == 'hysteresis':
            low = self.config['threshold_parameters']['h_low']
            high = self.config['threshold_parameters']['h_high']
            thresh_stack = filters.threshold_hysteresis_clipped(stack, low, high)
            thresh_stack = np.array(thresh_stack, dtype=int)
        else:
            raise ValueError(f"threshold_method {method} is not recognized.")
        return np.array(thresh_stack)

    def _apply_processing_methods(self, stack):
        r"""Apply morphology methods to the stack, gathering a host of connected component parameters

            Args:
                stack: Your image stack, it should be filtered and thresholded before running this function
        """

        # add an analysis footprint for the morphology features
        if self.config['brightfield']:
            footprint = self.config['processing_steps']['footprint']
        else:
            footprint = None

        if self.config['processing_steps']['erode_1']:
            iter = self.config['processing_steps']['n_erosion_1_iteration']
            if iter > 0:
                stack = process.binary_erosion(stack, iterations=iter)

        # Remove all pixels that are smaller than some user-defined size (default = 5 pixels)
        if self.config['processing_steps']['remove_small_objects']:
            small = self.config['processing_steps']['min_pixel_size']
            if small == 0:
                print("remove_small_objects key and a size of zero were given. This is redundant.", flush=True)
            elif small > 0:
                stack = process.remove_small_objects(stack, small)

        if self.config['processing_steps']['binary_closing']:
            iter = self.config['processing_steps']['n_closing_iterations']
            if iter > 0:
                stack = process.binary_closing(stack, footprint=footprint, iterations=iter)

        if self.config['processing_steps']['fill_holes']:
            stack = process.binary_fill_holes(stack)

        if self.config['processing_steps']['erode_2']:
            iter = self.config['processing_steps']['n_erosion_2_iteration']
            if iter > 0:
                stack = process.binary_erosion(stack, iterations=iter)

        if self.config['processing_steps']['inflate_small_pixels']:
            iter = self.config['processing_steps']['inflate_amount']
            if iter > 0:
                stack = process.binary_dilation(stack, iterations=iter)

        return stack


    def stack_cleanup(self, stack):
        r"""stack cleanup methods thrown into joblib
        """
        filtered = None
        thresholded = None
        processed = None

        if self.config['filter']:
            stack = self._filter_stack(stack)
            if self.config['save_stacks']['filtered_stack']:
                filtered = stack.copy()

        if self.config['threshold']:
            stack = self._threshold_stack(stack)
            if self.config['save_stacks']['thresholded_stack']:
                thresholded = stack.copy()

        if self.config['process']:
            stack = self._apply_processing_methods(stack)
            if self.config['save_stacks']['processed_stack']:
                processed = stack.copy()

        return stack, filtered, thresholded, processed


    # ----------------------------------------------------------
    # image segmentation methods
    # ----------------------------------------------------------

    def _ntip_simple(self, stack):
        return segment.nozzle_tip_simple(stack)

    def _ntip_shotbyshot(self, stack):
        return segment.nozzle_tip_shotbyshot()

    def _remove_nozzle_tip(self, stack, ntip_idx=None):
        r""" Will remove the nozzle tip from a stack of images

            Args:
                stack: your image stack

            Returns:
                the cropped image stack
        """
        if stack is None:
            stack = self._image_stack.copy()
        if ntip_idx is None:
            ntip_idx = self._nozzle_tip_idx
        return segment.remove_nozzle_tip(stack, ntip_idx)

    def _find_transition_region(self, stack=None):
        r"""Given a cleaned up image stack, will find jet breakup region for each frame

            Note: Incoming data should not have the nozzle tip in view, 
                    otherwise it will skew the result. Works best on 
                    a single binary image.

            Args:
                data (|ndarray|): the cleaned-up image stack. Optional, will use the
                                    jet region property if None is passed.
            Returns:
                (|list|): List of index positions corresponding to the jet 
                            breakup region. Will return None if there is
                            an error
    """
        if stack is None:
            stack = self._jet_region
        jr_idx = np.zeros(self._n_frames)
        for i, im in enumerate(stack):
            if i in self._bad_frames:
                jr_idx[i] = None
            else:
                try:
                    jr_idx[i] = int(segment.find_transition_region(im))
                except TypeError:
                    np.append(self._bad_frames, jr_idx[i])
        return jr_idx


    def _isolate_droplet_region(self, stack, idx):
        r"""Masks everything except the droplets
            
            Args:
                stack (ndarray): The image stack to analyze, defaults to self.jet_region if None.
        """
        jrs = stack.copy()
        for i, im in enumerate(stack):
            if np.isnan(idx[i]):
                jrs[i] = np.zeros_like(im)
            elif i in self._bad_frames:
                jrs[i] = np.zeros_like(im)
            else:
                jrs[i,int(idx[i]):,:] = 0
        return jrs


    def _label_stack(self, stack):
        r""" Labels an image stack from use with the regionprops stuff

            Args:
                stack (ndarray): the cleaned up image stack

            Returns:
                (ndarray): the labeled image stack
        """
        method = self.config['label_method']
        if method == 'watershed': 
            labels = process.label_watershed(stack)
        if method == 'standard':
            labels = process.label_standard(stack)
        return labels

    def _get_fit_params(self, fit, stack):
        return analysis.get_fit_params(fit, np.shape(stack)[1])

    def _get_jetregion_roi(self, stack):
        r"""Find the jet region ROI
            
            Args: 
                stack (np.ndarray): the image stack
        """
        yidx = np.nanmax(self._nozzle_tip_idx)
        fit = self._jet_fit
        try:
            mi, ma = int(np.floor(np.nanmin(fit))), int(np.ceil(np.nanmax(fit)))
            if mi < 0:
                mi = 0
            print("_get_jetregion_roi mi ma", mi, ma)
        except ValueError:
            if np.all(np.isnan(fit)):
                raise ValueError("Calculated fit is all nan's!")
        stack = stack[:,:yidx,mi:ma]
        return stack

    def _get_droplet_centroids(self, stack):
        r""" Uses regionprops to gather centroid and area values for each droplet

            Returns:
                z (list): list of global centroid indices, i.e., [[x0,y0], [x1,x1], ..., [xn, yn]]
                a (list): list of droplet feature areas in units of pixels

        """
        z = []
        a = []
        for i, im in enumerate(stack):
            reg_table = skimage.measure.regionprops_table(stack[i], properties=self._region_properties)
            aidx = np.where(reg_table['area'] > self.config['droplet_centroiding_params']['droplet_area_threshold'])
            x = np.array(reg_table['centroid-0'][aidx])
            y = np.array(reg_table['centroid-1'][aidx])
            b = [x,y]
            z.append(b)
            a.append(reg_table['area'][aidx])
        return z, a

    def _pad(self, im):
        r'''Convenience function to clean up the numpy.pad code'''
        # ns, nf = self._image_shape
        ns, nf = self._jet_region.shape[1:3]
        # Pad to avoid wrap-around effects
        return np.pad(im.copy(), ((int(ns/2), int(ns/2)), (int(nf/2), int(nf/2))))

    def cross_correlation(self, a, b):
        return np.real(np.fft.ifftn(np.fft.fftn(a)*np.conj(np.fft.fftn(b))))

    def _find_cc_peak(self, cc):
        # Find the peak of the CC.  That's the most common drop displacement.
        try:
            ns, nf = self._jet_region.shape[1:3]
            w = np.where(cc == np.max(cc))
            pf = w[1][0] - np.floor(nf/2)
            ps = w[0][0] - np.floor(ns/2)
        except IndexError:
            e1 = "IndexError was thrown at correlation peak finder! "
            e2 = "Is your data bright or darkfield? "
            e3 = "\nThis error is typically raised when brightfield analysis is requested on darkfield data."
            raise ValueError(e1+e2+e3)
        return pf, ps

    def __cc_even(self, image):
        cc = 0
        # correlate with even + 1 frames
        for i in range(0, self._n_frames, 2):
            im1 = self._pad(image[i, :, :])
            im2 = self._pad(image[i+1, :, :])
            cc += self.cross_correlation(im1, im2)
        return cc

    def __cc_odd(self, image):
        cc = 0 
        # correlate with odd + 1 frames
        for i in range(1, self._n_frames-1, 2):
            im1 = self._pad(image[i, :, :])
            im2 = self._pad(image[i+1, :, :])
            cc += self.cross_correlation(im1, im2)
        return cc 


    def _cross_correlation_calculation(self, image, parity:str='even'):
        r""" Calculates the cross correlation of pairs of frames.

            Args:
                image (ndarray): The image array
                parity (str): 'even' or 'odd', chooses which frames to start the correlation on
            Returns:
                cc (ndarray): The cross correlation matrix
                pf (float): The x-coordinate of the correlation peak 
                ps (float): The y-coordinate of the correlation peak
        """
        # ns, nf = self._image_shape
        ns, nf = self._jet_region.shape[1:3]

        if (parity != 'even') and (parity != 'odd'):
            print("Incorrect key for parity variable. Defaulting to even.")
            parity = 'even'

        if parity == 'even':
            cc = self.__cc_even(image)
        else:
            cc = self.__cc_odd(image)
        cc /= np.mean(cc)

        # Auto-correlation of the mean, for background correction (approximately)
        mean = self._pad(np.mean(image, axis=0))
        acf = self.cross_correlation(mean, mean)
        acf /= np.mean(acf)

        # Approximate background correction: subtract auto-correlation of the mean from the mean of cross-correlations
        cc -= acf
        cc = np.fft.fftshift(cc)

        # Remove zero-padding
        cc = cc[int(ns/2):ns+int(ns/2), int(nf/2):nf+int(nf/2)]

        # Find the cross correlation peak
        pf, ps = self._find_cc_peak(cc)
        return cc, pf, ps

    def _redo_cc(self, cond:float=1.5):
        r""" Determine if the cross correlation projection is 'good' or not

            Check the cross correlation projection against its variance. If above a threshold value, 
            trigger the cross correlation to be recalculated with a different parity. 

            Args:
                cond (float): The condition to compare against, value corresponds to the variance of the projected correlation matrix
            Returns:
                The truth.

        """
        redo = False
        corr = self._correlation_matrix
        cidx = np.where(corr < 0)
        corr[cidx] = 0
        corr /= np.max(corr)
        po = np.sum(corr, axis=1)
        var = np.var(po)
        if var > cond:
            redo = True
        return redo

    def js_cross_correlation(self):
        displacement = np.sqrt(self.pf**2 + self.ps**2)
        angle = np.arctan2(self.ps, self.pf)*180/np.pi - 90
        return displacement, angle

    def _shear(self, matrix):
        r"""
            A special matrix shear operation for analyzing
            the jet cross correlations
        """
        arr = np.empty_like(matrix)
        fast_scan, slow_scan = matrix.shape
        for i in range(slow_scan):
            for j in range(fast_scan):
                arr[i, j] = matrix[i, (j + i) % fast_scan]
        return arr

    def _outer_product_projection(self):
        # dd, _ = self._get_droplet_centroids()
        dd = self._droplet_centroids
        z = np.zeros((self._n_frames, self._image_shape[0], self._image_shape[1]))
        for i, im in enumerate(dd):
            z[i, dd[i][0].astype(int), dd[i][1].astype(int)] = 1
        zs = np.sum(z, axis=2)
        return zs

    def _outer_product_matrix(self):
        cc_mat = np.zeros((self._image_shape[0], self._image_shape[0]))
        dd = self._outer_product_projection()
        prev = None
        for i, im in enumerate(dd):
            # Add on odds, subtract on evens.  One of these two provides an estimate of background correlations
            # that have no relation to droplet translations.  I don't know which is which; deal with that later.
            if prev is not None:
                op = np.outer(dd[i], prev)
                if i % 2 == 1:
                    cc_mat += op
                else:
                    cc_mat -= op
            prev = dd[i]
        self.projection = np.mean(self._shear(cc_mat), axis=0)
        if np.abs(np.min(self.projection)) > np.abs(np.max(self.projection)):
            sign = -1
        else: 
            sign = 1
        self.projection *= sign
        cc_mat *= sign
        return cc_mat

    def _get_jet_speed(self):
        speed = self._droplet_translation * self._image_metadata['pixel_size'] * 1e-6 / (
                self._image_metadata['delay'] * 1e-9)
        print(f'Jet speed = {speed:0.2f} m/s (dp = {self._droplet_translation:0.2f}, pix = {self._image_metadata["pixel_size"]:.05f}, delay = {self._image_metadata["delay"] * 1e-9})')
        return speed

    def _get_jet_diameter(self):
        alpha = 1e-9 / 60  # ul/min to m^3 / sec scale factor
        a = (self._image_metadata['liquid_flow'] * alpha) / (np.pi * self._jet_speed)  # units = m^2
        jet_diameter = 2 * np.sqrt(a)  # meters
        print(f"Jet Diameter = {jet_diameter * 1e6: 0.4f} um")
        return jet_diameter

    def _get_jet_diameter_error(self):
        alpha = 1e-9 / 60  # ul/min to m^3 / sec scale factor
        count = 0
        while count <= 10:
            try:
                a = (self._image_metadata['liquid_flow_sdev'] * alpha) / (np.pi * self._jet_speed)  # units = m^2
                jet_diameter_err = 2 * np.sqrt(a)  # meters
                return jet_diameter_err
            except TypeError:
                count += 1
                print(f'Waiting for logs to update.... {count*10}/100 seconds waited... ', end='\r', flush=True)
                self._image_metadata = self._update_metadata()
                time.sleep(10)
                if count == 10:
                    raise ValueError("Can't find the correct epoch time in the logs!", flush=True)
        

    def _get_droplet_translaton(self):
        ccmp = self.projection
        if np.abs(np.min(ccmp)) > np.abs(np.max(ccmp)):
            ccmp *= -1
        return np.where(ccmp == np.max(ccmp))[0][0]

    def _stack_stats(self, stack, stat: str = None):
        # ims = stack.copy()
        if stat == 'mean':
            return np.mean(stack, axis=0)
        elif (stat == 'std') or (stat == 'sdev') or (stat == 'standard deviation'):
            return np.std(stack, axis=0)
        elif (stat == 'variance') or (stat == 'var'):
            return np.var(stack, axis=0)
        else:
            print("No stat was given, defaulting to mean")
            return np.mean(stack, axis=0)

    def _stack_mean(self, stack):
        return self._stack_stats(stack, stat='mean')

    def _stack_sdev(self, stack):
        return self._stack_stats(stack, stat='sdev')

    def _stack_var(self, stack):
        return self._stack_stats(stack, stat='var')

    def outlier_filter(self, stack, outlier_threshold:int=3):
        s = stack.copy()
        xs = np.array([np.sum(i) for i in s])
        xm = np.array([np.mean(i) for i in xs])
        xm_idx = np.where(xm > outlier_threshold * np.median(xm))
        self._bad_frames = xm_idx[0]
        s[xm_idx] = np.zeros_like(s[0])
        return s #new

    def fullrun_raw_image(self):
        r'''Make a birds-eye-view image of the full raw image stack
            Useful for a quick view of the full image stack
        '''
        stack = self.image_stack_raw
        stack = stack[:,500:1100,:]
        s = 20
        n, nx, ny = stack.shape
        g = 0
        for i in range(0, n):
            a = stack[i, :, :].T
            a -= np.median(a, axis=0)
            a = np.pad(a, [[0, s*n], [0, 0]])
            g += np.roll(a, i*s, axis=0)
        return np.array(g)

    def _angle(self, a, b):
        r"""Returns the angle between two vectors
        
            Uses the standard definition of the dot product to find the angle between two vectors

            Args:
                a (ndarray or list): The first 1D list
                b (ndarray or list): The second 1D list
            Returns:
                The angle between a and b in degrees

        """
        return np.degrees(np.arccos(np.dot(a, b)/ (np.linalg.norm(a) * np.linalg.norm(b))))

    def _get_jet_deviation(self):
        n, ns, nf = np.shape(self._jet_region)
        b = np.array([f[0] - f[-1] for i, f in enumerate(self._jet_fit)])
        ang = np.degrees(np.arctan(b/ns))
        print(f"Average Jet Deviation = {np.nanmean(ang):0.2f} degrees")
        return ang

    def _stability_distance(self, verbose:bool=False):
        if self.config['interaction_point_method'] == 'provided':
            d = self.config['stability_from_nozzle_distance'] / self._image_metadata['pixel_size']
        elif self.config['interaction_point_method'] == 'simple':
            d = 0.5 * np.nanmean(self._jet_length) / self._image_metadata['pixel_size']
        else:
            raise KeyError("Incorrect IP method provided. Options are 'simple' or 'provided'.")
        if verbose:
            print(f"Stability measured at {d*self._image_metadata['pixel_size']:0.2f} um from nozzle tip.")
        return d

    def _interaction_region(self):
        d = self._stability_distance()
        _, ns, nf = np.shape(self._jet_region)
        ang = self._jet_deviation * np.pi / 180
        b = np.array([f[-1] for i, f in enumerate(self._jet_fit)])
        x = np.array([ns - d * np.cos(ang[i]) for i in range(len(ang))])
        y = np.array([d * np.sin(ang[i]) for i in range(len(ang))])
        return np.array(list(zip(x,y)))

    def _get_jet_length(self):
        r"""Calculate the jet length based on the nozzle tip and transition region index positions

                Returns:
                    (ndarray): A list of jet lengths, one per laser exposure
        """
        dx = np.abs(self._nozzle_tip_idx - self._transition_region_idx)
        th = self._jet_deviation
        le = np.abs(dx * np.cos(th)) * self._image_metadata['pixel_size']
        print(f"Average Jet Length = {np.nanmean(le):0.2f} um")
        return le

    def _get_jet_stability(self):
        r"""Calculate jet stability as the ratio of jet deviation to jet diameter

            Returns:
                The calculated jet stability. If s > 1, the jet jitter is less
                than the diameter of the jet. s = 1 implies the jet jitter is
                exactly the jet diameter. s < 1 means the jet is whipping. 
        """

        d = self._stability_distance(verbose=True)     # converts um to pixel using config method
        ang = self._jet_deviation * np.pi/180          # deg * rad/deg = rad
        dx = d * np.cos(ang)                           # Project the distance on to the x-axis
        y = dx * np.tan(ang)                           # pixels 
        ym, ys = np.nanmean(y), np.nanstd(y)
        try:
            mi, ma = np.nanmin(y[y > ym - 2 * ys]), np.nanmax(y[y < ym + 2 * ys])
        except ValueError:
            print("Error in calculating jet stability. Does the processed data look okay?")
            return None
        dj = self._jet_diameter / (self._image_metadata['pixel_size'] * 1e-6)
        s = dj / (ma-mi)
        if s < 0.5:
            print(f"Jet Stability: {s:0.2f} -----> Highly unstable jet!")
        elif (s > 0.5) and (s < 1):
            print(f"Jet Stability: {s:0.2f} -----> Unstable jet!")
        elif s >= 1:
            print(f"Jet Stability: {s:0.2f} -----> Stable jet!")
        else:
            print(f"Undefined jet stability...")
            return None
        return s

    # ----------------------------------------------------------
    # Output dictionaries
    # ----------------------------------------------------------

    def _calculated_data_dict(self):
        data = {"jet_length": self._jet_length_avg,
                "jet_length_err": self._jet_length_err,
                "jet_diameter": self._jet_diameter,
                "jet_diameter_err": self._jet_diameter_err,
                "jet_speed": self._jet_speed,
                "jet_speed_err": None,
                "jet_deviation": self._jet_deviation_avg,
                "jet_deviation_err": self._jet_deviation_err,
                "droplet_dispersion": None, #self._drop_dispersion_avg,
                "droplet_dispersion_err": None, #self._drop_dispersion_err,
                "jet_stability": self._jet_stability,
                "angle_units": 'degrees', #self.config['angle_units']
                }
        return data

    def _logs_to_dict(self):

        try:
            fr = dataio.get_data_from_logs(metadata=self._image_metadata, 
                                            log_filepath=self.config['log_path'])
        except:
            fr = []
            print("No logs available for this dataset.", flush=True)

            if len(fr) == 0:
                data = {'liquid_flow_mean': None,
                        'liquid_flow_sdev': None,
                        }
            else:
                data = {'liquid_flow_mean': np.mean(fr),
                        'liquid_flow_sdev': np.std(fr),
                        }


        return data


    def to_dict(self):
        r""" All the data as a dictionary

            Returns:
                data (dict): A dictionary of the calculated data and the metadata

        """
        data = self._calculated_data_dict()
        data.update(self._image_metadata)
        data.update(self._logs_to_dict())
        return data

    def _update_metadata(self):
        data = self._logs_to_dict()
        data.update(self._image_metadata)
        return data

    # ----------------------------------------------------------
    # h5 output for saving all the analyzed data
    # ----------------------------------------------------------

    def _images_dict(self):
        r""" The image stacks to save in the analysis h5 file
        """
        data = {}
        if self.config['save_stacks']['raw_stack']:
            data['raw_stack'] = self._image_stack_raw
        else:
            data['raw_stack'] = []

        if self.config['save_stacks']['filtered_stack']:
            data['filtered_stack'] = self._image_stack_raw
        else:
            data['filtered_stack'] = []

        if self.config['save_stacks']['thresholded_stack']:
            data['thresholded_stack'] = self._image_stack_raw
        else:
            data['thresholded_stack'] = []

        if self.config['save_stacks']['processed_stack']:
            data['processed_stack'] = self._image_stack_raw
        else:
            data['processed_stack'] = []

        if self.config['save_stacks']['correlation_matrix']:
            data['correlation_matrix'] = self._correlation_matrix
        else:
            data['correlation_matrix'] = []

        if self.config['save_stacks']['stack_sdev']:
            data['stack_sdev'] = self._stack_sdev_image
        else:
            data['stack_sdev'] = []

        return data

    def _h5_single_group(self, h5, data, group_name):
        r""" A method for saving a single group in an h5 file
        """
        try:
            group = h5.create_group(group_name)
        except ValueError:
            group = h5['{}'.format(group_name)]

        for k, v in data.items():
            if v is None:
                v = False
            f = group_name + '/' + f"{k}"
            h5[f] = v

    def to_h5(self, save_path: str = None, overwrite=False):
        r"""Saves all the data to an h5 file

            Args:
                save_path (str): The directory where you want to save the data as 'path/to/save/location/filename.h5'
                overwrite (bool): Whether to overwrite the data or not
        """

        if save_path is None:
            raise ValueError("save path required!")

        d = os.path.dirname(save_path)
        if d == '':
            os.makedirs(d, exist_ok=True)
        if os.path.exists(save_path):
            if overwrite:
                os.remove(save_path)
            else:
                raise ValueError('File already exists', save_path)
        try:
            h5 = h5py.File(save_path, 'a')  # FIXME: Why append to an existing file?  This could cause confusion.
        except ValueError:
            h5 = h5py.File(save_path, 'w')

        self._h5_single_group(h5=h5, data=self._image_metadata, group_name="metadata")
        self._h5_single_group(h5=h5, data=self._images_dict(), group_name="2D_images")
        self._h5_single_group(h5=h5, data=self._calculated_data_dict(), group_name="calculated_data")

        h5.close()



class GDVNView(viewers.ImageView):

    nozzle_tip_idx = None
    jet_breakup_idx = None
    drop_centroids = None
    jet_angle = None
    jetana = None
    fit_plot = None

    def __init__(self, *args, jetana=None, **kwargs):
        """ A customized subclass of the pyqtgraph ImageView class that is specialized for looking at GDVN image
        stacks.  It makes use of the output of JetAnalysis and can show centroids, line fits, etc."""
        # super().__init__(*args, **kwargs)
        viewers.ImageView.__init__(self, *args, **kwargs)
        self.app = pg.mkQApp()
        self.frame_names = kwargs.get('frame_names', None)
        self.set_jetana(jetana)
        self._frame_changed(0, None)
        self.sigTimeChanged.connect(self._frame_changed)

    def set_jetana(self, jetana):
        if jetana is None:  # Because sometimes we want to open the viewer and mouse-click to locate the file...
            return
        self.nozzle_tip_idx = jetana.nozzle_tip_position
        self.jet_breakup_idx = jetana.transition_region_position
        self.drop_centroids = jetana.droplet_centroids
        self.jet_fit = jetana.jet_fit
        # self.jet_angle = jetana.jet_deviation_list
        # self.jet_ang = None
        self.nozzle_tip_line = None
        self.jet_breakup_line = None
        self.drop_plot = None
        self.fit_plot = None
        self.jetana = jetana


    def _frame_changed(self, ind, time):
        # TODO make two versions of this, onewith jet and one with ntip
        # check setVisible(True or False)
        if self.jetana is None:
            return
        pix = self.jetana.image_metadata['pixel_size']
        if self.nozzle_tip_idx is not None:
            p = self.nozzle_tip_idx[ind]*pix
            if self.nozzle_tip_line is None:
                self.nozzle_tip_line = self.add_line(angle=90, position=p, movable=False, pen='r')
            self.nozzle_tip_line.setPos([p, p])

        if self.jet_breakup_idx is not None:
            p = self.jet_breakup_idx[ind]*pix
            if self.jet_breakup_line is None:
                self.jet_breakup_line = self.add_line(angle=90, position=p, movable=False, pen='b')
            self.jet_breakup_line.setPos([p, p])

        # if self.stability is not None:
        #     p = self.

        if self.drop_centroids is not None:
            p = self.drop_centroids[ind]
            if self.drop_plot is None:
                self.drop_plot = self.add_plot(pen=None, symbolPen='g', symbol='o', symbolBrush=None, symbolSize=10)
            self.drop_plot.setData(p[0]*pix, p[1]*pix)

        if self.jet_fit is not None:
            if np.all(np.isnan(self.jet_fit[ind])):
                pass
            else:
                p = self.jet_fit[ind]# - np.nanmin(self.jet_fit[ind])
                px = np.arange(np.shape(self.jetana.jet_region)[1])  #image_shape[0])
                if self.fit_plot is None:
                    self.fit_plot = self.add_plot(pen='c')
                self.fit_plot.setData(px*pix, p*pix)

        if self.frame_names is not None:
            self.view.setTitle(self.frame_names[ind])
        self.app.processEvents()


def gdvn_view(*args, **kwargs):
    r""" A common problem arises when one tries to create an ImageView instance: if you have not already
    created a pyqt Application instance, you will get an error.  This function will create the app first and then
    create the pyqt Application.

    In addition to the positional and keyword Args accepted by ImageView, the following Args are accepted:

    Args:
        show (bool): Set to False if you do NOT want the ImageView to be visible (i.e. ImageView.show() called).
        hold (bool): Set to False if you do NOT want the ImageView to be activated (i.e. Application.exec_() called).
    """
    hold = kwargs.pop('hold', True)
    show = kwargs.pop('show', True)
    app = pg.mkQApp()
    imv = GDVNView(*args, **kwargs)
    if show:
        imv.show()
    if hold:
        app.exec_()
    return imv


class Main(qwgt.QMainWindow):
    r""" QMainWindow subclass.  Overrides keyPressEvent, sets window size to 3/4 of primary screen size. """
    def __init__(self, par=None, *args, **kwargs):
        r""" This should not be necessary, but is presently used to configure key-press events."""
        app = pg.mkQApp()
        super().__init__(*args, **kwargs)
        self.par = par
        s = app.primaryScreen().size()
        f = 2/3  # Make the main window fill this fraction of the screen
        self.setGeometry(int(s.width()*(1-f)/2), int(s.height()*(1-f)/2), int(s.width()*f), int(s.height()*f))
    def keyPressEvent(self, ev):
        r""" Overrides a built-in method. """
        if self.par is not None:
            self.par.key_pressed(ev)


class AnalysisViewer:

    jetana = None

    def __init__(self, filepath=None, config=None):
        """ Shows JetAnalysis results: raw images, centroids, line fits, filtering, thresholding, etc."""
        self.config = config

        # Make the main GUI window
        self.app = pg.mkQApp()
        self.movie_main_window = Main(par=self)
        self.movie_main_window.setWindowTitle("Movie Viewer")

        self.ana_main_window = Main(par=self)
        self.ana_main_window.setWindowTitle("GDVN Analysis")

        # Define some GUI kwargs
        k = {'show_histogram': True, 'aspect_locked': False}

        # make the frame viewers
        self.movie_layout = qwgt.QGridLayout()
        self.movie_widget = qwgt.QWidget()
        self.movie_widget.setLayout(self.movie_layout)
        self.movie_main_widget = qwgt.QWidget()
        self.movie_main_widget.setLayout(self.movie_layout)

        self.corrview = GDVNView(title='Cross Correlation', xlabel='X', ylabel='Y', 
                                    levels=(0,1), gradient='flame', **k)
        self.corrplot = self.corrview.add_plot(symbol='o', symbolBrush=None, symbolPen='r')
        self.corrx = self.corrview.add_plot(pen=(255, 0, 0))

        self.movie_layout.addWidget(self.corrview, 0, 2, 1, 2)
        # Set the movie display panels
        self.rawview = GDVNView(title='Normalized Raw', xlabel='X (microns)', ylabel='Y (microns)', **k)
        self.movie_layout.addWidget(self.rawview, 0, 0, 1, 2)

        k['show_histogram'] = False
        self.procview = GDVNView(title='Processed', xlabel='X (microns)', ylabel='Y (microns)', **k)
        self.movie_layout.addWidget(self.procview, 1, 0, 1, 2)

        self.movie_main_window.setCentralWidget(self.movie_main_widget)
        self.menubar = qwgt.QMenuBar()
        self.movie_main_window.setMenuBar(self.menubar)
        file_menu = self.menubar.addMenu('File')
        open_action = qwgt.QAction('Open file...', self.movie_main_window)
        open_action.triggered.connect(self.open_data_file_dialog)
        file_menu.addAction(open_action)
        # Statusbar so we know what is going on
        self.statusbar = self.movie_main_window.statusBar()
        self.movie_main_window.show()
        # # Link plot axes so they rescale together:
        # linkthese = [self.rawview, self.procview]
        # for a in linkthese:
        #     for b in linkthese:
        #         if a != b:
        #             a.sigTimeChanged.connect(b.setCurrentIndex)
        #             a.view.setXLink(b.view)
        #             a.view.setYLink(b.view)
        # self.areahistview = GDVNView(title="Droplet Area Histogram", xlabel=r"Area [sq um]", ylabel="Counts", **k)
        # self.areaplot = self.areahistview.add_plot(pen=(255, 0, 0))
        # self.movie_layout.addWidget(self.areahistview, 1, 2, 1, 2)

        self.app.processEvents()
        self.set_status('Ready.')
        if filepath is not None:
            self.load_file(filepath)

    def open_data_file_dialog(self):
        r""" Select a file to open. """
        opt = qwgt.QFileDialog.Options()
        f, t = qwgt.QFileDialog.getOpenFileName(self.movie_main_window, "Load data", os.getcwd(), "Data (*.h5)", options=opt)
        self.set_status(f'Loading file {f}...')
        if f:
            self.load_file(f)

    def set_status(self, message):
        self.statusbar.showMessage(message)
        self.app.processEvents()

    def load_file(self, filepath):
        self.set_status(f'Loading data {filepath}...')
        stack, metadata = dataio.h5_metadata(filepath)
        self.set_status('Processing data...')
        t = time.time()
        self.jetana = JetAnalysis(stack, metadata, config=self.config)
        print(f"Total run time {time.time() - t:0.4f} s")
        self.set_status('Displaying results...')
        self.update_display()
        self.movie_main_window.setWindowTitle(f"GDVN Analyzer ({filepath})")
        self.ana_main_window.setWindowTitle(f"GDVN Analyzer ({filepath})")
        stat1 = f'Jet Speed: {self.jetana.jet_speed:0.2f} m/s, Jet Diameter: {self.jetana.jet_diameter * 1e6:0.4f} um, '
        stat2 = f'Average Jet Deviation: {self.jetana.jet_deviation_avg:0.2f} degrees, '
        stat3 = f'Average Jet Length: {self.jetana.jet_length_avg:0.2f} um, '
        stat4 = f"Liquid Flow: {self.jetana._image_metadata['liquid_flow']:0.2f} {self.jetana._image_metadata['liquid_flow_units']}, "
        stat5 = f"Gas Flow: {self.jetana._image_metadata['gas_flow']:0.2f} {self.jetana._image_metadata['gas_flow_units']} "
        self.set_status(stat1 + stat2 + stat3 + stat4 + stat5)


    def update_display(self):
        ja = self.jetana

        # set the movie images
        pix = ja._image_metadata['pixel_size']
        jr = ja._jet_region
        imshape_1 = ja._image_stack_raw.shape[1:]
        imkwargs = dict(fs_lims=(0, imshape_1[1]*pix), ss_lims=(0, imshape_1[0]*pix))
        self.rawview.setImage(ja._image_stack_raw, **imkwargs)
        self.rawview.setLevels(0, np.max(ja._image_stack_raw[0]))
        self.rawview._frame_changed(0, None)
        imshape_2 = np.shape(jr)[1:]
        imkwargs = dict(fs_lims=(0, imshape_2[1]*pix), ss_lims=(0, imshape_2[0]*pix))
        self.procview.set_jetana(ja)
        self.procview.setImage(ja._image_stack, **imkwargs)
        # self.procview.setImage(jr.astype(int), **imkwargs)
        # self.procview.setImage(jr, **imkwargs)
        self.procview._frame_changed(0, None)


        corr = ja._correlation_matrix
        ns, nf = np.shape(jr)[1:3]
        if ja.config['jet_speed_method'] == '2D_cross_correlation':
            self.corrplot.setData([ja.ps], [ja.pf])
            cidx = np.where(corr < 0)
            corr[cidx] = 0
            corr /= np.max(corr)
            p = np.sum(corr, axis=1)
            self.corrx.setData(np.linspace(-corr.shape[0]/2, corr.shape[0]/2, corr.shape[0]), 10*p - 5*np.max(10*p)/2)
            self.corrview.setImage(corr, fs_lims=(-np.floor(nf/2), np.floor(nf/2-0.5)), ss_lims=(-np.floor(ns/2), np.floor(ns/2-0.5)))
            self.corrview.setLevels(0, np.max(corr))
            self.corrview.show()
        elif ja.config['jet_speed_method'] == 'outer_product':
           # set the correlation matrix and projection
            p = ja.projection
            corr = ja._correlation_matrix
            corr *= np.sign(np.max(p))
            n = p.shape[0]
            p -= np.min(p)
            p *= n/np.max(p)
            self.corrx.setData(np.arange(n), p)
            self.corrview.setImage(corr)
            self.corrview.setLevels(0, 1)
            self.corrview.show()

        # # set the droplet area histogram
        # ahist, abins = ja._histogram_droplet_areas()
        # abins *= ja._image_metadata['pixel_size']
        # self.areaplot.setData(abins[:-1], ahist)
        # self.areahistview.show()

        self.app.processEvents()

    def start(self):
        r""" Execute the application. """
        self.app.exec_()

    def key_pressed(self, ev):
        r""" Handling of key-press events. """
        key = ev.key()
        if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
            self.process_data()
            self.display()























