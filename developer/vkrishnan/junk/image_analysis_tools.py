import cv2
import h5py
import numpy as np
import scipy.stats
from matplotlib import pyplot as plt

#The thresholds to which it stops have been tuned on sample files, they may need to be tuned further if errors arise.
class Analyzer():
    def __init__(self):
        None

    def _find_stack_nozzle_tip(self, stack, buf=7, init=100, isTransposed = True):
        mins = self._find_nozzle_tip(stack[0], buf, init,  isTransposed)
        for i in range(1,len(stack)):
            mins = min(mins, self._find_nozzle_tip(stack[i], buf, init,  isTransposed))
        print(mins)
        return mins

    def _find_nozzle_tip(self, img, buf=7, init=100, isTransposed = False):
        r"""Given a single image, will calculate the nozzle tip position
            Arguments:
                img (numpy array): single image
                init (int): Starting minimum index for nozzle tip. Needs to be
                    higher than 50-100 for the variance to be calculated well.
                buffer (int): small extra shift to nozzle tip location
                isTransposed(bool): If the image is transposed(ex.jetanaysis),
                    use different axis.
            Returns:
                idx(int): index of nozzle tip location - max index of image(ex. 1023) if not found
        """
        ax = 0
        if isTransposed: ax = 1;
        q = np.gradient(img, axis=(1+ax)%2)
        qs = np.mean(np.abs(q), axis=ax)
        arr = []
        for i in range(init, len(qs)):
            qn = np.square(qs[0:i] - np.mean(qs[0:i]))
            var = sum(qn) / i
            arr += [var]
        idx = np.where(arr > 5 * np.mean(arr))
        if idx[0].size == 0:
            return img.shape[(1+ax)%2] - 1
        return idx[0][0] + init - buf


    def find_boundaries(self, frame):
        # Find the boundaries of the jet
        marker1x = 0
        marker2x = frame.shape[0]

        marker1y = 0
        #Goes up till the nozzle minus some pixels
        marker2y = self._find_nozzle_tip(frame) - 10

        #c is the size of each x-threshold
        c = 10
        sampleArr = np.arange(marker1y,marker2y + c,c)
        resultArr1 = []
        resultArr2 = []
        arr1 = []
        arr2 = []
        buffer = 5
        for j in range(0, len(sampleArr)-1,1):
            m1y = sampleArr[j]
            m2y = sampleArr[j+1]
            for i in range(1, frame.shape[0], 1):
                med = np.median(frame[i, m1y:m2y])
                medOld = np.median(frame[i - 1, m1y:m2y])
                if (abs(med - medOld) / medOld > 0.07):
                    marker1x = i - 1 - buffer
                    resultArr1 += [marker1x]
                    arr1 += [sampleArr[j]]
                    break
            for i in range(frame.shape[0] - 2, marker1x, -1):
                med = np.median(frame[i, m1y:m2y])
                medOld = np.median(frame[i + 1, m1y:m2y])
                if (abs(med - medOld) / medOld > 0.07):
                    marker2x = i+1 + buffer
                    resultArr2 += [marker2x]
                    arr2 += [sampleArr[j]]
                    break
        #Now, do a linear regression
        if(arr1 == [] or arr2 == []):
            return [[0,0],[[0,0],[0,0]]]
        res1 = scipy.stats.linregress(arr1, resultArr1)
        res2 = scipy.stats.linregress(arr2, resultArr2)

        #Returns both the uper and lower percentiles, and the linear regressions
        return ([np.percentile(resultArr1, 5), np.percentile(resultArr2, 95)],[res1, res2])
    def go_through_frames(self, filepath):
        f = h5py.File(filepath, 'r')
        for i in range(len(f['frames'])):

            frame = np.array(f['frames'][i])
            nozzle_pos = self._find_nozzle_tip(frame)
            boundaries = self.find_boundaries(frame)

            #Plot nozzle boundary line
            plt.plot([nozzle_pos, nozzle_pos], [0, frame.shape[0] -1], 'k-', lw=2)

            #Plot upper and lower boundary lines
            plt.plot([0,1024],[boundaries[0][0],boundaries[0][0]], 'k-',lw = 2)
            plt.plot([0, 1024], [boundaries[0][1], boundaries[0][1]], 'k-', lw=2)

            #Plot linear regression
            data = np.arange(0, 1024, 1)
            plt.plot(data, boundaries[1][0][0] * data + boundaries[1][0][1], 'r-', lw=2)
            plt.plot(data, boundaries[1][1][0] * data + boundaries[1][1][1], 'r-', lw=2)
            plt.imshow(frame)
            plt.show(block=True)

if __name__ == '__main__':
    '''Input filepath of an h5 file, most of the methods 
    just use a frame, but the frame getter currently opens an h5 file'''

    filepath = "/Users/vivekkrishnan/PycharmProjects/KirianLab/odysseus/MachineLearning/TrainingSet/Positive/134.h5"

    ana = Analyzer()
    ana.go_through_frames(filepath)