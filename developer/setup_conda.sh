#!/bin/bash
# The script needs to start from the directory in which it is located:
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR/.." || exit
[ -f environment.yml ] || exit
# I found that conda was taking more than an hour to install everything with the above environment file.
# I tried mamba, which is orders of magnitude faster.  If mamba doesn't work for you, replace mamba
# with conda and the above should work.
# Note that I am assuming that you will make a conda environment called microjet_analysis
[ "$(which mamba)" = "" ] && conda install mamba -n base -c conda-forge
[ "$(mamba info --envs | grep microjet_analysis)" = "" ] && mamba create -n microjet_analysis
mamba env update --name microjet_analysis --file environment.yml
