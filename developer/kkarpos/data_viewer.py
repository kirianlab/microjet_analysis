import numpy as np
import matplotlib.pyplot as plt
import os, warnings, logging, glob
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from microjet import filters, regions, process, dataio, measure, gdvn, utils, figures

from PyQt5.QtWidgets import (QApplication, QMainWindow, QVBoxLayout, QHBoxLayout, QPushButton, QTableWidget, 
                             QTableWidgetItem, QWidget, QFileDialog, QSplitter, QMessageBox, QDesktopWidget,
                             QLabel, QSpacerItem, QSizePolicy, QHeaderView)
from PyQt5.QtCore import Qt, pyqtSignal, QTimer

import pyqtgraph as pg
from microjet import dataio, utils, gdvnview



def data_view(*args, **kwargs):
    r""" A common problem arises when one tries to create an ImageView instance: if you have not already
    created a pyqt Application instance, you will get an error.  This function will create the app first and then
    create the pyqt Application.

    In addition to the positional and keyword arguments accepted by ImageView, the following arguments are accepted:

    Arguments:
        show (bool): Set to False if you do NOT want the ImageView to be visible (i.e. ImageView.show() called).
        hold (bool): Set to False if you do NOT want the ImageView to be activated (i.e. Application.exec_() called).
    """
    hold = kwargs.pop('hold', True)
    show = kwargs.pop('show', True)
    app = pg.mkQApp()
    imv = ImageView(*args, **kwargs)
    if show:
        imv.show()
    if hold:
        app.exec_()
    return imv

class DataViewer(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(DataViewer, self).__init__(*args, **kwargs)
        self.initUI()
        self.currentActiveRow = None
        self.currentFolder = False


    def initUI(self):
        self.centralWidget = QWidget()
        self.setCentralWidget(self.centralWidget)

        # Label to display the current folder path
        self.currentFolderPathLabel = QLabel("Current Folder: None")
        self.currentFolderPathLabel.setAlignment(Qt.AlignCenter)
        self.currentFolderPathLabel.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)

        # Setup the splitter for table and image display
        self.mainSplitter = QSplitter(Qt.Horizontal, self.centralWidget)
        self._setup_table_ui()
        self.mainSplitter.addWidget(self.tableWidget)
        self._setup_raw_display()
        self.mainSplitter.addWidget(self.raw_displayWidget)

        # Main vertical layout
        mainVLayout = QVBoxLayout(self.centralWidget)
        mainVLayout.addWidget(self.currentFolderPathLabel)  # Add label to the layout
        mainVLayout.addWidget(self.mainSplitter)  # Add splitter to the layout

        # Set initial proportions of the split windows
        QTimer.singleShot(0, self.adjustSplitterSizes)

        # Get the screen size and set the window size
        screenSize = QDesktopWidget().availableGeometry().size()
        self.resize(screenSize.width(), screenSize.height())

        # Connect table widget item click signal to _load_stack method
        self.fileTable.itemClicked.connect(self._load_stack)
        self.fileTable.itemChanged.connect(self.onNozzleTipIndexChanged)

    def adjustSplitterSizes(self):
        totalWidth = self.centralWidget.width()
        self.mainSplitter.setSizes([totalWidth//6, 5*totalWidth//6])

    def _setup_raw_display(self):
        self.raw_displayWidget = QWidget()
        self.raw_displayLayout = QHBoxLayout(self.raw_displayWidget)
        
        # Initialize GDVNView with placeholder data
        placeholder_stack = np.zeros((10, 1024, 512))  # Example placeholder stack

        self.rawDisplay = gdvnview.gdvn_view(stack=placeholder_stack, data_dict=None, hold=False, show=False)

        # Since gdvn_view returns a GDVNView instance, we need to add it to the layout
        self.raw_displayLayout.addWidget(self.rawDisplay)

        self.rawDisplay.nozzleTipIndexChanged.connect(self.updateNozzleTipIndex)

    def updateNozzleTipIndex(self, index):
        if self.currentActiveRow is not None:
            self.fileTable.setItem(self.currentActiveRow, 1, QTableWidgetItem(str(index)))

    def _setup_proc_display(self):
        self.proc_displayWidget = QWidget()
        self.proc_displayLayout = QHBoxLayout(self.proc_displayWidget)
        self.procDisplay = pg.ImageView()
        self.proc_displayLayout.addWidget(self.procDisplay)

    def _setup_table_ui(self):
        # Vertical layout widget for file table and folder button
        self.tableWidget = QWidget()
        self.tableLayout = QVBoxLayout(self.tableWidget)
        self.fileTable = QTableWidget()
        self.fileTable.setColumnCount(2)
        self.fileTable.setHorizontalHeaderLabels(['Files', 'Nozzle Tip Index Position'])
        # Automatically resize columns to fit content
        header = self.fileTable.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        
        self.folderButton = QPushButton('Choose Folder')
        self.folderButton.clicked.connect(self.onFolderSelect)
        self.tableLayout.addWidget(self.fileTable)
        self.tableLayout.addWidget(self.folderButton)

        self.exportButton = QPushButton('Export Nozzle Tip Positions')
        self.exportButton.clicked.connect(self.exportNozzleTipPositions)
        self.tableLayout.addWidget(self.exportButton)

    def exportNozzleTipPositions(self):
        if not self.currentFolder:
            QMessageBox.warning(self, "Warning", "No folder selected.")
            return

        export_file_path = os.path.join(self.currentFolder, "nozzle_tip_positions.txt")
        with open(export_file_path, 'w') as file:
            for row in range(self.fileTable.rowCount()):
                filename_item = self.fileTable.item(row, 0)
                nozzle_tip_item = self.fileTable.item(row, 1)

                if filename_item and nozzle_tip_item and nozzle_tip_item.text():
                    file.write(f"{filename_item.text()}, {nozzle_tip_item.text()}\n")

        QMessageBox.information(self, "Success", f"Data exported to {export_file_path}")
    
    def onNozzleTipIndexChanged(self, item):
        if item.column() == 1 and not self.fileTable.signalsBlocked():  # Additional check
            try:
                new_index = int(item.text().strip())
                self.rawDisplay.set_nozzle_tip_index(new_index)
            except ValueError:
                # Reset the item text if the input is not a valid integer
                QMessageBox.warning(self, "Invalid Input", "Nozzle Tip Index must be an integer.")
                item.setText(str(self.rawDisplay.get_nozzle_tip_index()))

    def onFolderSelect(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        if folder:
            self.currentFolder = folder
            self.currentFolderPathLabel.setText(f"Current Folder: {folder}")
            self._update_table(folder)

    def _update_table(self, folder):
        # Store the data to populate in the table
        table_data = []
        nozzle_tip_positions_path = os.path.join(folder, "nozzle_tip_positions.txt")
        nozzle_tip_positions = {}
        if os.path.exists(nozzle_tip_positions_path):
            with open(nozzle_tip_positions_path, 'r') as file:
                for line in file:
                    parts = line.strip().split(', ')
                    if len(parts) == 2:
                        nozzle_tip_positions[parts[0]] = parts[1]

        files = sorted([f for f in os.listdir(folder) if f.endswith('.h5')])
        for i, file in enumerate(files):
            nozzle_tip_index = nozzle_tip_positions.get(file, "")
            table_data.append((file, nozzle_tip_index))

        # Now block signals and populate the table
        self.fileTable.blockSignals(True)

        self.fileTable.setRowCount(len(files))
        for i, (file, nozzle_tip_index) in enumerate(table_data):
            self.fileTable.setItem(i, 0, QTableWidgetItem(file))
            nozzle_tip_item = QTableWidgetItem(nozzle_tip_index)
            nozzle_tip_item.setFlags(nozzle_tip_item.flags() | Qt.ItemIsEditable)
            self.fileTable.setItem(i, 1, nozzle_tip_item)

        self.fileTable.blockSignals(False)


    def _load_stack(self, item):
        # Placeholder for loading the data
        file_path = os.path.join(self.currentFolder, item.text())
        data, metadata = dataio.open_data(file_path)

        # Update the raw display with the first frame of the stack
        if data is not None and data.ndim == 3:  # Check if data is a 3D NumPy array
            self.rawDisplay.set_image_stack(data)

            # Store the current active row index
            self.currentActiveRow = self.fileTable.indexFromItem(item).row()

            # Get the nozzle tip index from the table if available
            nozzle_tip_item = self.fileTable.item(self.currentActiveRow, 1)
            if nozzle_tip_item and nozzle_tip_item.text().isdigit():
                nozzle_tip_index = int(nozzle_tip_item.text())
                # Set the position of the movable bar in GDVNView
                self.rawDisplay.set_nozzle_tip_index(nozzle_tip_index)
            else:
                # Get the initial nozzle tip index from GDVNView
                nozzle_tip_index = self.rawDisplay.get_nozzle_tip_index()

            self.fileTable.setItem(self.currentActiveRow, 1, QTableWidgetItem(str(nozzle_tip_index)))


    def updateNozzleTipIndex(self, index):
        # Assuming the currently active file is known (you might need to store the active row index)
        active_row = self.currentActiveRow  # You need to set this attribute in your code
        self.fileTable.setItem(active_row, 1, QTableWidgetItem(str(index)))

if __name__ == "__main__":
    app = QApplication([])
    viewer = DataViewer()
    viewer.show()
    app.exec_()









































