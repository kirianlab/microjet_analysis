import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os, warnings, logging, glob, gc
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from microjet.gdvn_plots import GDVNPlots


date = "20230714"
nid = "177R_63_analysis"
basepath = "/Volumes/kkarpos_ES/PhD_Work/NozzleData/2023/"

folder_path = os.path.join(basepath, date, nid)

csv_path = os.path.join(folder_path, "data_mod.csv")
output = os.path.join(folder_path, "figures_mod")

# Check if csv_path exists, if not, copy and rename "data.csv" to "data_mod.csv"
if not os.path.exists(csv_path):
    original_csv = os.path.join(folder_path, "data.csv")
    if os.path.exists(original_csv):
        # Read the original file and write it to the new path
        with open(original_csv, 'r') as file:
            data = file.read()
        with open(csv_path, 'w') as file:
            file.write(data)
    else:
        logger.error("Original 'data.csv' file not found in the specified folder.")
        # Additional handling may be necessary here

os.makedirs(output, exist_ok=True)

gdvn_plots = GDVNPlots(csv_path, output, dpi=300)
gdvn_plots.update_plots()































