import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from pathlib import Path
import warnings
import numpy as np

def search_csv_files(basepath, ignore_folders=None):
    if ignore_folders is None:
        ignore_folders = []
    csv_files = []
    for root, dirs, files in os.walk(basepath):
        if any(ignore_folder in Path(root).parts for ignore_folder in ignore_folders):
            continue
        for file in files:
            if file.endswith(".csv"):
                csv_files.append(os.path.join(root, file))
    return csv_files

def remove_unnamed_columns(df):
    cols_to_remove = [col for col in df.columns if "Unnamed:" in col]
    return df.drop(cols_to_remove, axis=1)

def create_master_dataframe(csv_files):
    master_df = pd.DataFrame()
    for file in csv_files:
        df = pd.read_csv(file)
        nozzle_id = os.path.splitext(os.path.basename(file))[0]
        df['nozzle_id'] = nozzle_id
        df['fratio'] = df['liquid_flow'] / df['gas_flow']
        try:
            sta = pd.to_numeric(df['jet_stability'], errors='coerce')
            df['stability'] = 1 / sta
        except KeyError:
            df['stability'] = np.full(len(df), np.nan)
        master_df = pd.concat([master_df, df])
    return master_df

def plot_jet_diameter(master_df, y_label:str, upper=999, lower=0, xlim=(0,2)):
    fig, ax = plt.subplots()
    marker_styles = ['o', 'v', '^', '<', '>', 's', 'p', '*', 'h', 'H', 'D', 'd', 'X']
    unique_nozzle_ids = master_df['nozzle_id'].unique()

    for i, nozzle_id in enumerate(unique_nozzle_ids):
        data = master_df[master_df['nozzle_id'] == nozzle_id]
        data = data[(data['jet_diameter'] * 1e6 < upper) & (data['jet_diameter'] * 1e6 >= lower)]
        y = data['jet_diameter'] * 1e6
        sc = ax.scatter(data['fratio'], y,
                        s=data['jet_length'] * 0.35, c=data['jet_speed'],
                        cmap="gist_heat", norm=mcolors.Normalize(vmin=0, vmax=150),#data['jet_speed'].max()),
                        marker=marker_styles[i % len(marker_styles)],
                        edgecolors='black', linewidths=0.25)

    cbar = plt.colorbar(sc, ax=ax)
    cbar.set_label('Jet Speed [m/s]')
    ax.set_xlabel(r'Flow Ratio [Q/$\dot{{m}}$]')
    ax.set_ylabel(y_label)
    ax.grid(alpha=0.5)
    ax.set_xlim(xlim)
    return fig, ax


def plot_y_key(master_df, y_key:str, y_label:str, upper=999, lower=0, xlim=(0,2)):
    fig, ax = plt.subplots()
    marker_styles = ['o', 'v', '^', '<', '>', 's', 'p', '*', 'h', 'H', 'D', 'd', 'X']
    unique_nozzle_ids = master_df['nozzle_id'].unique()

    for i, nozzle_id in enumerate(unique_nozzle_ids):
        data = master_df[master_df['nozzle_id'] == nozzle_id]
        data = data[(data[y_key] < upper) & (data[y_key] >= lower)]
        y = data[y_key]
        sc = ax.scatter(data['fratio'], y,
                        s=data['jet_length'] * 0.35, c=data['jet_speed'],
                        cmap="gist_heat", norm=mcolors.Normalize(vmin=0, vmax=data['jet_speed'].max()),
                        marker=marker_styles[i % len(marker_styles)],
                        edgecolors='black', linewidths=0.25)

    cbar = plt.colorbar(sc, ax=ax)
    cbar.set_label('Jet Speed [m/s]')
    ax.set_xlabel(r'Flow Ratio [Q/$\dot{{m}}$]')
    ax.set_ylabel(y_label)
    ax.grid(alpha=0.5)
    ax.set_xlim(xlim)
    return fig, ax

def plot_stability_histogram_stacked(master_df, num_bins=100):
    # Create subplots
    fig, ax = plt.subplots(figsize=(12,5))
    
    # Get unique nozzle IDs
    unique_nozzles = master_df['nozzle_id'].unique()
    
    # Calculate bin edges
    bin_edges = np.linspace(0, 2, num_bins + 1)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    
    # Initialize the histogram data
    hist_data = np.zeros((len(unique_nozzles), num_bins))
    
    # Calculate histogram data for each nozzle
    for i, nozzle in enumerate(unique_nozzles):
        nozzle_data = master_df[master_df['nozzle_id'] == nozzle]['stability'].dropna()
        hist_data[i], _ = np.histogram(nozzle_data, bins=bin_edges)
    
    # Plot the stacked histogram
    for i, nozzle in enumerate(unique_nozzles):
        ax.bar(bin_centers, hist_data[i], width=(bin_edges[1] - bin_edges[0]), bottom=np.sum(hist_data[:i], axis=0), label=nozzle)
    
    # Set labels and title
    ax.set_xlabel(rf"Jet Stability [$\Delta y / d_j$]")
    ax.set_ylabel('Frequency')
    ax.set_title('Stability Histogram Across All Kirian Lab Data')
    # ax.set_xlim(0, 2)
    
    # Add legend
    ax.legend(title='Nozzle ID', loc='upper right', ncols=2, fontsize=7)
    return fig, ax

def plot_stability_histogram_simple(master_df, num_bins=50):
    fig = plt.figure()
    plt.hist(master_df['stability'].dropna(), bins=num_bins, edgecolor='black')
    plt.xlabel(rf"Jet Stability [$\Delta y / d_j$]")
    plt.ylabel('Frequency')
    plt.title('Stability Histogram Across All Kirian Lab Data')
    plt.xlim(0,2)
    return fig

def save_master_csv(master_df, output_path):
    master_df.to_csv(output_path, index=False)

def main(basepath, output_path, y_key, y_label, xlim=(0,2),
            ignore_folders=None, upper_limit=999, lower_limit=0):
    csv_files = search_csv_files(basepath, ignore_folders=ignore_folders)

    # Remove the output_path from the list of CSV files if it exists
    if output_path in csv_files:
        csv_files.remove(output_path)

    master_df = create_master_dataframe(csv_files)
    master_df = remove_unnamed_columns(master_df)
    save_master_csv(master_df, output_path)
    print(rf"Number of datapoints: {len(master_df)}")
    print(rf"Smallest Jet: {master_df[master_df['jet_diameter'] * 1e6 >= lower_limit]['jet_diameter'].min() * 1e6:0.2f}μm")
    print(rf"Number of unique nozzles: {len(np.unique(master_df['nozzle_id']))}")
    # a, b = plot_master_dataframe(master_df, y_key=y_key, y_label=y_label, upper=upper_limit, lower=lower_limit, xlim=xlim)
    # a,b = plot_jet_diameter(master_df, y_label=y_label, upper=upper_limit, lower=lower_limit, xlim=xlim)
    # c, d = plot_stability_histogram_stacked(master_df, num_bins=300)
    # e = plot_stability_histogram_simple(master_df, num_bins=1000)
    # plt.show()

if __name__ == "__main__":
    warnings.filterwarnings("ignore", category=FutureWarning)
    basepath = Path("/Users/kkarpos/Desktop/NozzleData/")
    output_path = Path("/Users/kkarpos/Desktop/master_dataset.csv")
    ignore_folders = ["ConcentrationStudy", "example_data", "untitled_folder", "79_water", "GN_glass_50_PSIBuffer", "GN_glass_50_water"]

    # y_key = 'stability'
    # y_label = rf"Jet Stability [$\Delta y / d_j$]"
    y_key = 'jet_diameter'
    y_label = "Jet Diameter [μm]"

    main(basepath, output_path, y_key=y_key, y_label=y_label,
                ignore_folders=ignore_folders, upper_limit=3, lower_limit=0.1, xlim=(-0.025, 1))












