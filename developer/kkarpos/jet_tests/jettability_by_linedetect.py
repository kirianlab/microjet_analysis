import sys
import numpy as np
from skimage import feature, transform, img_as_ubyte, io
from skimage.color import rgb2gray
from skimage.transform import probabilistic_hough_line
import matplotlib.pyplot as plt

import os
import glob
import h5py

ntip = 950
# Load the image
base_path = "/Users/kkarpos/repos/projects/odysseus/submodules/microjet_analysis/examples/example_data/036.h5"
# ims = glob.glob(os.path.join(base_path, "2023*.png"))
h5 = h5py.File(base_path, 'r')
for frame in h5['frames'][:3]:
# for image_path in ims:
    # frame = io.imread(image_path)

    # # Check if the image is RGBA (has an alpha/transparency channel)
    # if frame.shape[-1] == 4:
    #     # Remove the alpha channel
    #     frame = frame[:, :, :3]

    # # Ensure frame is in grayscale and uint8 type
    # if len(frame.shape) == 3:
    #     frame = rgb2gray(frame)
    # frame = img_as_ubyte(frame)

    frame = frame.astype(np.float32)
    frame /= np.max(frame).astype(np.float32)
    # Use Canny edge detection from skimage
    edges = feature.canny(frame[:ntip,:], sigma=3)

    # Use Probabilistic Hough Transform to detect line segments
    lines = probabilistic_hough_line(edges, threshold=10, 
                                             line_length=20, 
                                             line_gap=5)

    # Create a visualization
    fig, ax = plt.subplots(2,1, figsize=(10, 10))
    ax[0].imshow(frame, cmap='gray', vmin=0, vmax=np.percentile(frame, 95))
    ax[1].imshow(edges, cmap='gray')  # Display the edge-detected image
    ax[0].vlines(x=ntip, ymin=0, ymax=512, color='red')

    # Overlay the detected lines
    for line in lines:
        p0, p1 = line
        ax[1].plot((p0[0], p1[0]), (p0[1], p1[1]), '-r')  # Draw line segment in red


    jet_detected = False
    if lines:
        # If any line segments are detected, a jet is present
        jet_detected = True


    # Check for jet presence
    if jet_detected:
        print("A jet is detected in the image.")
    else:
        print("No jet detected in the image.")

    ax[0].set_title(f'Raw Image -- Jet Detected: {jet_detected}')
    ax[1].set_title('Edge-detected Image with Overlayed Lines')
    for i in [0,1]:
        ax[i].axis('off')
    plt.tight_layout()
    plt.draw()

plt.show()

