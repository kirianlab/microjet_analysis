import sys
import numpy as np
from skimage import feature, transform, img_as_ubyte, io
from skimage.color import rgb2gray
from skimage.transform import probabilistic_hough_line
import matplotlib.pyplot as plt
from scipy.spatial import distance_matrix
from skimage.filters import threshold_otsu

import os
import glob
import h5py

ntip = 950
threshold_fraction=0.1
distance_cutoff=10
drop_ratio=0.5
# Load the image
base_path = "/Users/kkarpos/repos/projects/odysseus/submodules/microjet_analysis/examples/example_data/036.h5"
# ims = glob.glob(os.path.join(base_path, "2023*.png"))

def bin_image_cropped(image, bin_size=10):
    """
    Bin the image by the specified bin size, cropping the image if its dimensions
    are not divisible by the bin size.
    
    Parameters:
    - image: Input grayscale or multi-channel image
    - bin_size: Size of the binning
    
    Returns:
    - binned_image: Reduced resolution image
    """
    
    # If the image has more than two dimensions, use only the first channel
    if len(image.shape) > 2:
        image = image[:, :, 0]
    
    # Crop the image to dimensions divisible by bin_size
    height_crop = image.shape[0] - (image.shape[0] % bin_size)
    width_crop = image.shape[1] - (image.shape[1] % bin_size)
    image_cropped = image[:height_crop, :width_crop]
    
    # Calculate the shape of the binned image
    binned_shape = (image_cropped.shape[0] // bin_size, image_cropped.shape[1] // bin_size)
    
    # Reshape the image to group pixels for binning
    reshaped = image_cropped.reshape(binned_shape[0], bin_size, 
                                     binned_shape[1], bin_size)
    
    # Average the pixel values within each bin
    binned_image = reshaped.mean(axis=(1, 3))
    
    return binned_image


h5 = h5py.File(base_path, 'r')
for i, frame0 in enumerate(h5['frames'][:3]):

    frame = frame0[:,:ntip].astype(np.float32)
    frame = bin_image_cropped(frame)
    print(np.shape(frame))

    # Convert the image to binary using Otsu's thresholding if not already binary
    if frame.max() > 1:
        thresh = threshold_otsu(frame)
        frame = frame > thresh
    else:
        frame = frame.astype(bool)
    
    # Get coordinates of white pixels
    y, x = np.where(frame)
    coords = np.column_stack((x, y))

    # Compute distance matrix and get all unique distances
    dists = distance_matrix(coords, coords)

    unique_dists, counts = np.unique(dists, return_counts=True)

    # Create a histogram (which is essentially the PDF)
    histogram, bin_edges = np.histogram(unique_dists, bins=100, weights=counts)

    # Look for a drop in the histogram beyond the distance_cutoff
    initial_peak_value = histogram[:distance_cutoff].max()
    post_cutoff_value = histogram[distance_cutoff:].max()
    print(initial_peak_value, post_cutoff_value)
    jet_detected = (post_cutoff_value / initial_peak_value) > drop_ratio


    fig, ax = plt.subplots(3,1, figsize=(10, 10))
    ax[0].imshow(frame0, cmap='gray', vmin=0, vmax=np.percentile(frame0, 95))
    ax[0].vlines(x=ntip, ymin=0, ymax=512, color='red')
    ax[1].imshow(frame, cmap='gray')  # Show the binary image

    # Plot the pair distribution function on the third subplot
    ax[2].bar(bin_edges[:-1], histogram, width=np.diff(bin_edges)[0])
    ax[2].set_xlim(0, distance_cutoff * 2)  # Display up to twice the distance_cutoff for clarity
    ax[2].vlines(x=distance_cutoff, ymin=0, ymax=np.max(histogram), color='red', linestyle='--')
    ax[2].set_title('Pair Distribution Function')
    ax[2].set_xlabel('Distance')
    ax[2].set_ylabel('Frequency')

    # Check for jet presence
    if jet_detected:
        print("A jet is detected in the image.")
    else:
        print("No jet detected in the image.")

    ax[0].set_title(f'Raw Image -- Jet Detected: {jet_detected}')
    ax[1].set_title('Edge-detected Image with Overlayed Lines')
    for i in [0,1]:
        ax[i].axis('off')
    plt.tight_layout()
    plt.draw()

plt.show()

