import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import tri 


class GDVNPlots:
    """
    A class to create plots based on GDVN data.

    Attributes:
        df (pd.DataFrame): A Pandas DataFrame with the GDVN data.

    """

    def __init__(self, df):
        self.df = df

    def plot_liquid_vs_gas(self, filter_outliers=True):
        """
        Creates a plot of liquid flow rate vs gas mass flow rate with the jet speed as the colorbar.
        Outliers can be filtered using the filter_outliers parameter.

        Args:
            filter_outliers (bool, optional): Whether or not to filter outliers. Defaults to True.

        Returns:
            fig (matplotlib.figure.Figure): The matplotlib Figure object for the plot.
            ax (matplotlib.axes.Axes): The matplotlib Axes object for the plot.

        """
        liquid_flow = self.df['liquid_flow']
        gas_flow = self.df['gas_flow']
        jet_speed = self.df['jet_speed']
        jet_diameter = self.df['jet_diameter']

        if filter_outliers:
            # Filter outliers based on jet speed of neighboring points
            jet_speed_mean = np.zeros(len(jet_speed))
            jet_speed_sdev = np.zeros(len(jet_speed))

            for i in range(len(jet_speed)):
                distances = np.sqrt((liquid_flow - liquid_flow[i])**2 + (gas_flow - gas_flow[i])**2)
                nearest_idx = np.argsort(distances)[:5] # Get 5 nearest points
                jet_speed_mean[i] = np.mean(jet_speed[nearest_idx])
                jet_speed_sdev[i] = np.std(jet_speed[nearest_idx])

            jet_speed_min = jet_speed_mean - 2.5*jet_speed_sdev
            jet_speed_max = jet_speed_mean + 2.5*jet_speed_sdev

            idx = (jet_speed > jet_speed_min) & (jet_speed < jet_speed_max)

            liquid_flow_filtered = liquid_flow[idx]
            gas_flow_filtered = gas_flow[idx]
            jet_speed_filtered = jet_speed[idx]
            jet_diameter_filtered = jet_diameter[idx]
        else:
            liquid_flow_filtered = liquid_flow
            gas_flow_filtered = gas_flow
            jet_speed_filtered = jet_speed
            jet_diameter_filtered = jet_diameter


        # Create a 2D grid for the interpolated jet speed gradient
        ranges = [(0, np.max(liquid_flow)), (0, np.max(gas_flow)), (np.min(jet_speed_filtered), np.max(jet_speed_filtered))]
        data = [liquid_flow_filtered, gas_flow_filtered, jet_speed_filtered]
        xi, yi, zi, levels = self.interpolate_ranges(ranges, data)

        # Create the scatter plot
        fig, ax = plt.subplots()
        im = ax.scatter(liquid_flow_filtered, gas_flow_filtered, c=jet_speed_filtered, cmap='RdBu_r', edgecolors='black', linewidths=1, s=3*jet_diameter_filtered*1e6)
        ax.set_xlabel('Liquid Flow Rate (μl/min)')
        ax.set_ylabel('Gas Mass Flow (mg/min)')
        fig.colorbar(im, ax=ax, label='Jet Speed (m/s)')

        # Add the jet speed gradient
        ax.contourf(xi, yi, zi, levels=levels, cmap='RdBu_r', alpha=0.5, extend='both')

        plt.show()

        return fig, ax

    def interpolate_ranges(self, ranges, data, level_step: int = 5):
        r"""Prepares a basic contour plot given data

            Args:
                ranges (list or tuple): the x, y, and z ranges for plotting should be [x,y,z]
                data (list or tuple): the x, y, and z data, should be [x,y,z]
                level_step (int): the amount of countour levels between each datapoint

            Returns:
                xi (ndarray): the x grid values
                yi (ndarray): the y grid values
                zi (ndarray): the interpolated z grid values
                levels (ndarray): The number of levels used in the color gradient

        """

        # Make a contour plot of irregularly spaced data
        # coordinate via interpolation on a grid.
        n_levels = int((ranges[2][1] - ranges[2][0]) / level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(2 * n_levels + 10))

        # Create grid values first.
        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
        interpolator = tri.LinearTriInterpolator(tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels



if __name__ == '__main__':

    import glob, os

    # filepath = "../developer/kkarpos/figure_tests/565_63_water.csv"
    # filepath = "../developer/kkarpos/figure_tests/79V1-DB-50G-50L-221206-04_glycerol_25.csv"
    # filepath = "../developer/kkarpos/figure_tests/79M_AA_50L_100G.csv"
    filepath = "/Users/kkarpos/Desktop/NozzleData/79_water/"

    csvs = glob.glob(os.path.join(filepath, "*.csv"))

    df = pd.read_csv(csvs[0])
    df['nozzle_id'] = csvs[0].split('/')[-1].split('_')[0]
    for i, d in enumerate(csvs):
        dm = pd.read_csv(d)
        dm['nozzle_id'] = d.split('/')[-1].split('_')[0]
        df = pd.concat([df, dm])

    df = df.reset_index(drop=True)


    dp = GDVNPlots(df=df)

    dp.plot_liquid_vs_gas()

    # dp.set_nozzle_id("")