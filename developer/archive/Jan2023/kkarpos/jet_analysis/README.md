# Quick Info

This folder contains analysis code specific to the dual-pulse jet analysis data. The analysis and FrameGetter should be generalized enough to use for any "jet" analysis, but it's meant to be used with standard GDVNs at the Kirian Lab testing station. 

## To Do


## Random Ideas


------

# Scripts Overview

* `analyze_jet.py`
    * Master script used to run the custom analysis
    * Currently has all the top-level usage examples

* `filters_playground.py`
    * Module with all the different `skimage` filters, wrappers were written for each filter so they can be used with our image stacks

* `jet_analysis_class.py`
    * Contains the main analysis class
    * Basic usage examples are shown below, each class method is (or should be) fully documented within the class

* `jetframegetter_class.py`
    * Contains a class to grab and update frames 
    * Roberto will update the documentation at some point....

* `utils.py`
    * Contains all the utility functions used in the FrameGetter and analysis classes
    * Also has methods used in the analysis, but these should be moved at some point

* `viewer.py`
    * The pyqtgraph image class is found here, it's very basic and should be updated 
    * Used mainly to view all the image stacks, see the usage in `analyze_jet.py`



-----

# Usage Examples

## Analyzing a single `.h5` image stack

The `JetAnalysis` class found in `jetanalysis_class.py` should be used on a single set of frames, it is not meant to handle data fram multiple `.h5` files. To calculate all the relevant information for a single image stack, the code should be run as follows. For all the required imports, see `analyze_jet.py`.

```python
stack, metadata = utils.h5_metadata_version("string/to/your/data.h5")

jetana = JetAnalysis(stack, metadata)

```

`JetAnalysis()` requires two parameters, the image stack and its metadata. The `h5_metadata_version()` method should be able to handle the two different metadata versions currently used, see the complete function in `utils.py`. If a different metadata version is made or used in the future, this function should be updated to include it.

The `jetana` parameter should now contain all the calculated data for the image stack. At the time of this writing, only the following information is calculated and available:

* `jetana.image_stack_raw` 
    * The raw image stack, no calculations are done to this stack
    * Returns a 3D numpy array whose shape is `(n_shots, resolution_x, resolution_y)`

* `jetana.stack_sdev`
    * The stack standard deviation
    * Returns a 2D numpy array whose shape is `(resolution_x, resolution_y)`

* `jetana.stack_filtered`
    * Each frame in the stack is filtered using the methods found in `filters_playground.py`
        * Unless the user changes the `filter_method` parameter in the `JetAnalysis()` class instantiation, the default `'sobel'` method will be used
        * !! Note !! Only the `'sobel'` method is available, other methods should be added eventually
    * Returns a 3D numpy array whose shape is `(n_shots, resolution_x, resolution_y)`

* `jetana.stack_thresholded`
    * Each frame in the stack is thresholded using the methods found in `filters_playground.py`
        * Unless the user changes the `threshold_method` parameter in the `JetAnalysis()` class instantiation, the default `'otsu'` method will be used
    * Returns a 3D numpy array whose shape is `(n_shots, resolution_x, resolution_y)`

* `jetana.stack_processed`
    * Applies morphology methods to the image stack, gathering a host of connected component parameters
    * See the `JetAnalysis.apply_processing_methods()` method for details


## Displaying Images with `pyqtgraph`

A *very* basic `pyqtgraph` display class is available in the `viewer.py` script. To use it as is, simply import the class (see `analyze_jet.py` for the imports) and run the following:

```python
im_raw = ImWin(jetana.image_stack_raw, title='Raw Image Stack')
im_sdev = ImWin(jetana.stack_sdev, title='Standard Deviation of Stack')
im_filt = ImWin(jetana.stack_filtered, title='Filtered Image Stack')
im_thresh = ImWin(jetana.stack_thresholded, title='Thresholded Image Stack')
im_processed = ImWin(jetana.stack_processed, title='Processed Image Stack')

pg.Qt.QtWidgets.QApplication.instance().exec_()

```
 

## Using the FrameGetter
















