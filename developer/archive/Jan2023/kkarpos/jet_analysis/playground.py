import os
import time
import glob
import h5py
import numpy as np
import pylab as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import pyqtgraph as pg
import skimage

import sys

from microjet_analysis import gdvn, dataio

from scipy import ndimage as ndi
from skimage.segmentation import watershed
from skimage.feature import peak_local_max
# from skimage.transform import hough_line, hough_line_peak

import imageio


# basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/microjets/2022/20221010/roberto_346_79/010.h5"
basepath = "/Users/kkarpos/Desktop/gdvn_tests/002.h5"
stack, metadata = dataio.h5_metadata_version(basepath)

config = {'max_frames': 10000,
        'threshold_method': 'otsu_clipped',
        'fill_holes': True,
        'normalize_raw': True,
        'do_filter': True,
        'filter_method': 'gausshp',
        'filter_parameters': {'sigma': 10},
        'remove_small_objects': 15,
        'binary_closing_iterations': 3,
        'label_method': 'watershed',
        'test': 'test'
        }


print("analyzing...")
jetana = gdvn.JetAnalysis(stack, metadata, config=config)
print("getting segmented images..")
jet = jetana.jet_image
drops = jetana.droplets_image


fits = jetana._get_jet_fit


# pg.image(drops)

print("houghing it up...")
# jet = jet[0]
# tested_angles = np.linspace(-np.pi / 10, np.pi / 10, 1000, endpoint=False)
# hough, theta, dist = skimage.transform.hough_line(jet, theta=tested_angles)

# _, ang, d = skimage.transform.hough_line_peaks(hough, theta, dist)
# x = np.arange(jetana.image_shape[0])
# y = (d[0] + x * np.cos(ang[0] + np.pi / 2)) / np.sin(ang[0] + np.pi / 2)


# def hough_jet_fit(jet_image):
#     tested_angles = np.linspace(-np.pi / 10, np.pi / 10, 1000, endpoint=False)
#     hough, theta, dist = skimage.transform.hough_line(jet_image, theta=tested_angles)

#     _, ang, d = skimage.transform.hough_line_peaks(hough, theta, dist)
#     x = np.arange(jetana.image_shape[0])
#     y = (d[0] + x * np.cos(ang[0] + np.pi / 2)) / np.sin(ang[0] + np.pi / 2)
#     return y


# fits = np.zeros((jetana.n_frames, len(y)))
# for i, im in enumerate(jet):
#     fits[i] = hough_jet_fit(im)


# for _, a, d in zip(*hough_line_peaks(hough, theta, dist)):
#     x = np.arange(self.correlations.shape[0])
#     y = (dist + x * np.cos(angle + np.pi / 2)) / np.sin(angle + np.pi / 2)
#     speed_ppf = abs(1 / ((y[len(y) - 1] - y[0]) / (x[len(x) - 1] - x[0])))
#     break

# r = x cos(theta) + y sin(theta)

# med = np.median(drops, axis=0).astype(int)
# diff = np.subtract(drops, med)

# dd = np.zeros(drops.shape)
# for i, d in enumerate(drops):
#     d -= med.astype(int)
#     dd[i] = d





# diff = np.subtract(float(drops), med, dtype=int)


# plt.figure()
# plt.imshow(med, cmap='binary_r')
# plt.show()


# cents = jetana.droplet_centroids





# area = []
# for i, im in enumerate(jetana._droplet_stack_labeled):
#     rt = reg_table = skimage.measure.regionprops_table(jetana._droplet_stack_labeled[i], properties=jetana._region_properties)
#     aidx = np.where(rt['area'] > 60)
#     area.append(rt['area'][aidx])

# xarea = np.concatenate(area, axis=0)
# hist, bin_edges = np.histogram(xarea, bins=1500)


# hmean = np.mean(hist[hist != 0])
# hmidx = np.where(hist > 2.8*hmean)
# bins_to_ignore = np.min(bin_edges[hmidx])



# # plt.figure()
# # plt.imshow(drops[0])
# # plt.draw()



# plt.figure()
# plt.plot(bin_edges[:-1], hist, 'b')
# plt.plot(bin_edges[hmidx],hist[hmidx], 'xr')
# plt.draw()

# plt.show()





# svar = np.var(jet, axis=2)
# svmean = np.mean(svar, axis=0)


# fig, ax = plt.subplots()
# c = ax.imshow(svar, cmap='magma', vmin=0, vmax=0.03, aspect='auto', interpolation='none')
# fig.colorbar(c, ax=ax)
# plt.show()



# d = 150* jetana._image_metadata['pixel_size'] * 1e-6
# s = jetana._jet_diameter / (d * np.tan(jetana._drop_dispersion_avg))





























