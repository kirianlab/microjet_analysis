import sys
import os
import glob
import time

import numpy as np
import matplotlib as mpl
import pylab as plt
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing


from jetframegetter import JetFrameGetter
from jetanalysis import JetAnalysis
from regionprops import JetRegionProps
from plotjetdata import PlotJetData


from viewer import ImWin
import utils
import pyqtgraph as pg

import filters_playground as filt

r"""
    This script serves as a playground and example for the JetAnalysis class. 
"""

# flip these if you want to look at image stacks with pyqtgraph
view_raw = False             # the raw images
view_sdev = False            # the stack standard deviation
view_filtered = False        # the filtered images
view_thresholded = False     # the thresholded images
view_processed = False       # the binary 'processed' images


# there's two methods below; single_movie and movie_stack
# single_movie allows you to mess with the regionprops class, see the code below for some examples
# the movie_stack is the example on looping through each h5, analyzing data, and saving results
single_movie = True 
movie_stack = False

plot_data = False                 # flip me if you want to see the default phase diagrams
save_data = False                # flip me if you actually want to save the analyzed data 

test_no_nozzle = False

# user defined config dictionary with all required parameters
# should be updated with optional parameters passed into the JetAnalysis class at some point
nozzle_id = "mh_coarse_b2_n1" #"mh_cut2_b4_n2"
# basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220609/"
basepath = "/Volumes/GoogleDrive/Shared\ drives/Kirian\ Lab/Small\ Data/mh_nozzles/20220609/mh_coarse_b2_n1/001.h5"
# basepath = 'rawdata'
# nozzle_id = ''

config = {"data_path": os.path.join(basepath, nozzle_id),
            "save_path": os.path.join(basepath, nozzle_id, "analysis_kk"),
            "reuse": True, # If False, will recalculate jet data. True pulls from cache.
            }

if save_data:
    os.makedirs(config['save_path'], exist_ok=True)

all_data = np.sort(glob.glob(os.path.join(config['data_path'], "*.h5")))
if single_movie:
    # all_data = all_data[np.random.randint(0,len(all_data))]
    all_data = all_data[0]

if movie_stack:
    # An example on how to loop through a stack of h5 files

    plots = PlotJetData(nozzle_id=nozzle_id)

    stack_data = {}
    tt = time.time()
    for i, rawdat in enumerate(all_data):
        save_here = os.path.join(config["save_path"], f"{i+1:03d}_analysis.h5")
        print(f"Working on image stack {i+1:03d}: ", end="\r")
        if config['reuse']:
            print(f"Working on image stack {i+1:03d}: Got it from cache.", end="\r")
            if os.path.exists(save_here):
                f = h5py.File(save_here, 'r')
                dat = {}
                for k,v in f.items():
                    for ki,vi in v.items():
                        dat[ki] = vi[()]
                stack_data[i] = dat
                plots.update(dat) # update the plotting class
                continue
            else:
                pass
        print("\n")


        
        t = time.time()
        stack, metadata = utils.h5_metadata_version(all_data[i])
        jetana = JetAnalysis(stack, metadata)
        dat = jetana.to_dict()
        stack_data[i] = dat
        plots.update(dat) # update the plotting class

        if save_data:
            print(f"Saving analysis file {i+1:03d}_analysis.h5 here: \n\t {save_here} \n")
            jetana.to_h5(save_path=save_here)
            print(f"Working on image stack {i+1:03d}: It took {time.time()-t:02f} seconds")

    if plot_data:
        x = plots.liquid_flow
        xr = (0,54)
        y = plots.gas_flow
        yr = (0,60)
        z = plots.jet_speed
        zr = (0,140)
        d = plots.jet_diameter * 1e6
        for i in range(len(z)):
            if (z[i] > 1.5*np.std(z) - np.mean(z)) or (z[i] < 0):
                z[i] = 0
        xi, yi, zi, levels = plots.interpolate_ranges(ranges=[xr, yr, zr], data=[x,y,z])

        fig, ax = plt.subplots(1,1, tight_layout=True)
        cmap = mpl.cm.get_cmap('RdBu_r')
        contourf = ax.contourf(xi, yi, zi, levels=levels, cmap=cmap)
        fig.colorbar(contourf, ax=ax, label="Jet Speed (m/s)")
        ax.scatter(x, y, s=4*d, marker='o', 
                        color=cmap((z-zr[0])/(zr[1]-zr[0])),
                        edgecolors=(0, 0, 0), linewidths=0.3)
        ax.set(xlim=xr, ylim=yr)
        ax.set_xlabel('Liquid Flow (ul/min)')
        ax.set_ylabel('Gas Flow (mg/min)')
        fig.suptitle(nozzle_id)

        plt.show()


    print(f"\n \t It took {time.time() - tt:02f} seconds to analyze {len(all_data)} frames")







if single_movie:
    # a playground for messing with a single movie at a time
    # feel free to edit! Just don't change the stuff in the box below

    # =============================================================
    # ====================== Don't change me ======================
    # =============================================================

    t = time.time()

    stack, metadata = utils.h5_metadata_version(all_data)

    if test_no_nozzle:
        stack = stack[:,:900,:]
        print(np.shape(stack))


    jetana = JetAnalysis(stack, metadata)#, debug=True)


    if view_raw:
        im_raw = ImWin(jetana.image_stack_raw, title='Raw Image Stack')
    if view_sdev:
        im_sdev = ImWin(jetana.stack_sdev, title='Standard Deviation of Stack')
    if view_filtered:
        im_filt = ImWin(jetana.stack_filtered, title='Filtered Image Stack')
    if view_thresholded:
        im_thresh = ImWin(jetana.stack_thresholded.astype(int), title='Thresholded Image Stack')
    if view_processed:
        im_processed = ImWin(jetana.stack_processed, title='Processed Image Stack')

    print(f"\n\n\t It took {time.time() - t} seconds to do what it do \n\n\n")

    # =============================================================
    # ====================== -^^^^^^^^^^^^^- ======================
    # =============================================================

    # JetRegionProps works with a single frame only
    # region = JetRegionProps(jetana.stack_droplets_labeled[23])

    # drop_centroids = {k:v['droplet_centroids'] for k,v in jetana._stack_regions_info.items()}
    # dd = [v[0,:,0:2] for k,v in drop_centroids.items()] # droplet_centroid idx # TODO: clean this up

    # FIXME: the droplet centroid are using the cropped image, have it return the corrected image

    m1 = ImWin(jetana.image_stack_raw)
    m1.ntip_idx = jetana.nozzle_tip_position
    m1.jet_breakup_idx = jetana.jet_region_position[:,0]
    m1.drop_centroids = jetana.droplet_centroids

    # m2 = ImWin(jetana.stack_processed)
    # m2.ntip_idx = jetana.nozzle_tip_position
    # m2.jet_breakup_idx = jetana.jet_region_position[:,0]
    # m2.drop_centroids = jetana.droplet_centroids



    pg.Qt.QtWidgets.QApplication.instance().exec_()



    # for k,v in jetana.to_dict().items():
    #     print(f"{k}:  {v}")

    if (view_raw) or (view_filtered) or (view_processed) or (view_thresholded):
        pg.Qt.QtWidgets.QApplication.instance().exec_()











# ================================================================
# uncomment this stuff to loop through multiple data files
# ================================================================
# jetana = JetFrameGetter(config)

# live_analysis = True
# idle_time = 0
# while live_analysis:
#     # for movie in jetana:
#         # start analysis function

#     jetana.update_data()
#     time.sleep(1)
#     idle_time += 1
#     if idle_time == 1800:
#         print("System idle for 30 minutes... stopping the script. ")
#         sys.exit()


