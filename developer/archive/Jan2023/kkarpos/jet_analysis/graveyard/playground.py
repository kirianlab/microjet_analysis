import sys
import os
import glob
import time

import numpy as np
import matplotlib as mpl
import pylab as plt


from microjet_analysis import gdvn, dataio

import pyqtgraph as pg

r"""
    This script serves as a playground and example for the JetAnalysis class. 
"""



nozzle_id = "mh_coarse_b2_n1" #"mh_cut2_b4_n2"
basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220609/"


config = {"data_path": os.path.join(basepath, nozzle_id)}


all_data = np.sort(glob.glob(os.path.join(config['data_path'], "*.h5")))
data = all_data[0]

stack, metadata = dataio.h5_metadata_version(data)


jetana = gdvn.JetAnalysis(stack, metadata)

cc = jetana.correlation_matrix
ccm = jetana.correlation_projection

# pg.image(cc)
# pg.Qt.QtWidgets.QApplication.instance().exec_()

# plt.plot(ccm[0])
# plt.show()


print(jetana.to_dict())


# x = jetana.droplet_centroids[0][:,0]
# y = jetana.droplet_centroids[0][:,1]
# slope, yint = np.polyfit(x,y,1)
# xf = np.arange(jetana.image_shape[0])
# yf = slope*xf + yint
# plt.figure()
# plt.imshow(jetana.droplets_image[0].T, cmap='binary') 
# plt.plot(xf, yf, '--')
# plt.plot(x,y, '.')
# plt.show()



# fig, ax = plt.subplots(dpi=150)
# ax.imshow(jetana.image_stack_raw[0].T, cmap='gray')
# ax.plot(cent[0][:,0], cent[0][:,1], '.r')
# ax.plot(0,0,'.g')
# plt.show()





