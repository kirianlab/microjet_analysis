import sys
import os
import glob
import configparser

import numpy as np
import matplotlib as mpl
import pylab as plt
import imageio 
import h5py
import json
import scipy


def validate_config(config:dict):
    r"""Makes sure your input config is correct, enforces certain keys are provided.


    Arguments:
        config (dict): A config with all the user defined parameters

    Required config keys:
        'data_path': should be the full path to the data. For example, '/data/microjets/20220615/nozzle_id/'

    FIX ME: checkout .get() method for dictionaries
    """
    c = config.copy()
    defaults = {'filter_method': 'sobel', 
                    'threshold_method':'otsu', 
                    'remove_small_object_size': 10,
                    'binary_closing_interations': 3}
    required = ["data_path"]
    a = list(c.keys())
    for k in required:
        if k not in a:
            raise KeyError("'data_path' is a required config parameter. Is it missing or mispelled?")
    for k in list(defaults.keys()):
        if k in a:
            pass
        else:
            c[k] = defaults[k]
    return c


def validate_h5_config(param_dict:str):
    x = param_dict.copy()
    defaults = {"year": None, "date": None, "time": None, "gas_flow": None, "liquid_flow": None, "pixel_size": None, "delay": None}
    for k in list(defaults.keys()):
        if k in a:
            pass
        else:
            x[k] = defaults[k]
    return x


def glob_data(config):
    r"""Gathers all the file paths with the relevant data

        Given a config with the base data path, will return a globbed list of all saved h5's.
    """ 
    config = validate_config(config)
    data_paths = glob.glob(os.path.join(config['data_path'], "*.h5"))
    return data_paths

# def _tif_metadata_version(filepath:str):
#         image_stack = io.imread(filepath).astype(np.single)
#         image_stack = np.transpose(image_stack, (0, 2, 1))
#         return image_stack

def make_analysis_folder(config):
    return

def load_sahba_meta(meta_file):
    year = ""
    date = ""
    _time = ""
    gas_flow = None
    liquid_flow = None
    pixel_size = None
    delay = 550
    if os.path.exists(meta_file):
        meta = configparser.ConfigParser()
        meta.read(meta_file)
        if 'timestamp' in meta:
            _datetime = (meta['timestamp']['year'], meta['timestamp']['date'], meta['timestamp']['time'])
        if 'fluidics' in meta:
            fluidics = meta['fluidics']
            if 'gas' in fluidics:
                g = fluidics['gas']
                if g != '':
                    q = g.split(" ")
                    gas_flow = float(q[0])
            if 'liquid' in fluidics:
                g = fluidics['liquid']
                if g != '':
                    q = g.split(' ')
                    liquid_flow = float(q[0])
        if 'optics' in meta:
            optics = meta['optics']
            if 'pixelsize_um' in optics:
                g = optics['pixelsize_um']
                if g != '':
                    pixel_size = float(g)
        data = {}
        data["year"] = _datetime[0]
        data["date"] = _datetime[1]
        data["time"] = _datetime[2]
        data["gas_flow"] = gas_flow
        data["liquid_flow"] = liquid_flow
        data["pixel_size"] = pixel_size
        data["delay"] = delay
    return data


def h5_metadata_version(filepath:str):
    r"""Open a single h5 file, check data version, return image stack and metadata"""
    with h5py.File(filepath, "r") as h5:
        image_stack = np.array(h5["frames"], dtype=np.float32)
        image_stack = np.transpose(image_stack, (0, 2, 1))
        if list(h5.keys()) == ['frames', 'metadata']:
            metadata = json.loads(h5['metadata'][()])
            dat = {'year': metadata['timestamp']['year'],
                    'date': metadata['timestamp']['date'],
                    'time': metadata['timestamp']['time'],
                    'gas_flow': metadata['gas']['reading'],
                    'gas_flow_units': metadata['gas']['units'],
                    'liquid_flow': metadata['liquid']['reading'],
                    'liquid_flow_units': metadata['liquid']['units'],
                    'pixel_size': metadata['optics']['pixelsize_um'],
                    'delay': 550
                    }
        else:
            no_extension, file_extension = os.path.splitext(filepath)
            meta_file = no_extension + ".txt"
            print(f"WARNING: No metadata found in the h5! Gathering metadata from {meta_file}.")
            print("Are you using the newest version of Ody?")
            if not os.path.exists(meta_file):
                raise ValueError("No metadata found!")
            dat = load_sahba_meta(meta_file)
    return image_stack, dat


def load_h5(filepath:str):
    r""" Loads data from 2022 versions of Odysseus

        Arguments:
            filepath (str): the full path to a specific data file
        Returns:
            image stack (np.ndarray)
            metadata (dict)

        TO DO: Add in old data versions (tif, etc).
    """
    image_stack, metadata = h5_metadata_version(filepath)
    data = {"file_name": filepath,
                "stack": image_stack}
    for k,v in metadata.items():
        data[k] = v
    # data = validate_h5_config(data)
    return data

def fit_jet(image, x, y):
    xj, yj = x.flat, y.flat
    slope, yint = np.polyfit(xj, yj, 1)
    xf = np.arange(image.shape[0])
    yf = yint + slope * xf
    ang = np.sign(slope) * np.arctan(slope)  # What's the sign for?  I can't recall
    u1 = np.array([np.cos(ang), np.sin(ang)])  # Unit vector along jet direction
    u2 = np.array([-np.sin(ang), np.cos(ang)])  # Unit vector orthogonal to jet direction
    v = np.vstack([xj, yj - yint]).T
    v1 = v.dot(u1)  # Coordinates along jet direction
    v2 = v.dot(u2)  # Coordinates orthogonal to jet direction
    nbins = image.shape[0]
    p1, bin_edges = np.histogram(v1, bins=nbins, range=[0, nbins])  # Projection along jet direction
    p2, bin_edges = np.histogram(v2, bins=nbins, range=[0, nbins])  # Projection orthogonal jet direction
    return slope, yint, v1, v2, p1, p2, xf, yf, ang



def filter_stack(stack, erode:bool=True):
    r""" Detects edges and filters background
        
        Arguments:
            stack: The images that make up your movie. This will also work on a single image.
            erode (bool): Will perform a morphological erosion step to further filter the background

        Returns:
            a filtered stack of the same shape as the input stack
    """
    stack = filters.sobel(stack)
    if erode:
        stack = morphology.erosion(stack)
    return stack


def get_stack_sdev_dualpulse(stack):
    """
    Hold on to the std deviation of the original data stack. It is useful for estimating 
    jet length and jet position
    *  Note that we only use even or odd frames because of the dual laser pulses; we 
        don't want the variation in laser intensity to contribute to this standard deviation
    """
    stack_std = np.std(stack[::2, :, :] / np.mean(stack[::2, :, :]), axis=0)
    stack_std -= np.median(stack_std)
    stack_std /= np.max(np.abs(stack_std))
    return stack_std

def _prepare_data_for_plotting(data_stack):
    speeds = [v['jet_speed'] for k,v in data_stack.items()]
    liquid_flow = [v['liquid_flow'] for k,v in data_stack.items()]
    gas_flow = [v['gas_flow'] for k,v in data_stack.items()]
    diameter = [v['jet_width']*1e6 for k,v in data_stack.items()]
    data = {
        'liquid_flow': np.array(liquid_flow),
        'gas_flow': np.array(gas_flow),
        'jet_speed': np.array(speeds),
        'jet_diameter': np.array(diameter)
            }
    return data

def make_contours(data_stack, config, save_path="figures", display_phase_diagram=False):
    r""" Make a contour plot.  The data dictionary should come from compile_data(). """

    data = _prepare_data_for_plotting(data_stack)
    title = config['title']

    # Configurations
    xr = config['x_range']
    yr = config['y_range']
    zr = config['z_range']

    # Make the contour plot
    x = np.array(data[config['x_key']])
    y = np.array(data[config['y_key']])
    z = np.array(data[config['z_key']])

    # Make a contour plot of irregularly spaced data 
    # coordinate via interpolation on a grid.
    n_levels = int((zr[1]-zr[0])/config['level_step'])
    levels = np.linspace(zr[0], zr[1], int(n_levels+1))

    # Create grid values first.
    xi = np.linspace(0, np.max(x), 200)
    yi = np.linspace(0, np.max(y), 200)
    Xi, Yi = np.meshgrid(xi, yi)

    # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
    interpolator = mpl.tri.LinearTriInterpolator(mpl.tri.Triangulation(x, y), z)
    zi = interpolator(Xi, Yi)

    # plot --------------------------------------------------------------------

    fig, axis = plt.subplots(nrows=1)

    contour  = axis.contour(xi, yi, zi, levels=levels, linewidths=0.3, colors='k')
    contourf = axis.contourf(xi, yi, zi, levels=levels, cmap="RdBu_r")

    fig.colorbar(contourf, ax=axis, label=config['z_label'])

    cmap = mpl.cm.get_cmap('RdBu_r')
    if config['marker_size_is_jet_diameter']:
        s = data['jet_diameter']*1
    else:
        s = 10
    for i in range(len(x)):
        axis.scatter(x[i], y[i], s[i], marker='o', color=cmap((z[i]-zr[0])/(zr[1]-zr[0])),
                     edgecolors=(0, 0, 0), linewidths=0.3)
    axis.set(xlim=xr, ylim=yr)

    plt.xlabel(config['x_label'])
    plt.ylabel(config['y_label'])
    plt.title(title)

    if title == '':
        print('You should provide a title.  Otherwise, the figure will not be saved.')
    else:
        print("Saving phase diagram here:")
        sp = os.path.join(save_path, title+'.jpg')
        print(sp)
        plt.savefig(sp)
    if display_phase_diagram:
        plt.show()

def phase_diagram(data_stack,
                    config=None,
                    title=None,
                    savepath=None,
                    x_range=(0, 54),
                    y_range=(0, 60),
                    z_range=(0, 120),
                    level_step=5, verbose=False,
                    display_phase_diagram=False):
    r'''
        Creates a phase diagram based on analyzed data

        Arguments:
            config (dict): a dictionary with plot config info.
                            A default is available 
            title (str): The plot title
            savepath (str): 
    '''

    if verbose:
        print("File list for plotting:")
        print(file_list)

        print("Ignore list for plotting:")
        print(ignore_list)

    # save_path = self.phase_diagram_savepath

    if config is None:
        plot_config = {
            'title': title,  # Title of the plot
            'x_key': 'liquid_flow',  # What hdf5 key to use for x coordinate
            'x_label': 'Liquid flow (ul/min)',  # What to label the x coordinate
            'x_range': x_range,  # Ranges (use None if not sure)
            'y_key': 'gas_flow',
            'y_label': 'Gas flow (mg/min)',
            'y_range': y_range,
            'z_key': 'jet_speed',
            'z_label': 'Jet speed (m/s)',
            'z_range': z_range,
            'level_step': 5,  # One contour per step
            'marker_size_is_jet_diameter': True
            }
    make_contours(data_stack, plot_config, display_phase_diagram=display_phase_diagram)






















