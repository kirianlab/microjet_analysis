import sys
import os
import glob
import time

import numpy as np
import matplotlib as mpl
import pylab as plt
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing


from jetframegetter import JetFrameGetter
from jetanalysis import JetAnalysis
from regionprops import JetRegionProps
from plotjetdata import PlotJetData


from viewer import ImWin
import utils
import pyqtgraph as pg

import filters_playground as filt

r"""
    This script serves as a playground and example for the JetAnalysis class. 
"""

# flip these if you want to look at image stacks with pyqtgraph
view_raw = False             # the raw images
view_sdev = False            # the stack standard deviation
view_filtered = False        # the filtered images
view_thresholded = False     # the thresholded images
view_processed = False       # the binary 'processed' images

nozzle_id = "mh_coarse_b2_n1"
basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220609/"

config = {"data_path": os.path.join(basepath, nozzle_id),
            "save_path": os.path.join(basepath, nozzle_id, "analysis_kk"),
            }

all_data = np.sort(glob.glob(os.path.join(config['data_path'], "*.h5")))
all_data = all_data[25]#[20] #[np.random.randint(0,len(all_data))]

t = time.time()

stack, metadata = utils.h5_metadata_version(all_data)
jetana = JetAnalysis(stack, metadata)


if view_raw:
    im_raw = ImWin(jetana.image_stack_raw, title='Raw Image Stack')
if view_sdev:
    im_sdev = ImWin(jetana.stack_sdev, title='Standard Deviation of Stack')
if view_filtered:
    im_filt = ImWin(jetana.stack_filtered, title='Filtered Image Stack')
if view_thresholded:
    im_thresh = ImWin(jetana.stack_thresholded, title='Thresholded Image Stack')
if view_processed:
    im_processed = ImWin(jetana.stack_processed, title='Processed Image Stack')

savepath = "/Users/kkarpos/repos/kkarpos/thesis/sample_delivery/pix/"

centroids =  {k:v['droplet_centroids'] for k,v in jetana._stack_regions_info.items()}
idx = 10
shift = 34
c0 = centroids[idx]
plt.figure(dpi=200)
imstack = np.concatenate([jetana.droplets_image[i].T for i in range(idx)])
plt.imshow(imstack, cmap='gray')
for i in range(idx):
    plt.plot(centroids[i][0,:,0], centroids[i][0,:,1]+shift*i, 'r.', markersize=5)
plt.xticks([])
plt.yticks([])
plt.savefig(savepath+"jetimage_dropcentroids.png", transparent=True)
# plt.show()


# im = jetana.stack_processed[0]

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.image_stack_raw[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"jetimage_raw.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.stack_thresholded[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"jetimage_thresholded.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.stack_filtered[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"jetimage_filtered.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.stack_processed[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"jetimage_processed.png")

# ======



# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.nozzle_image[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"nozzle.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.jet_image[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"jet.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.droplets_image[0], cmap='gray')
# plt.xticks([])
# plt.yticks([])
# plt.savefig(savepath+"droplets.png")

# plt.figure(dpi=200, tight_layout=True)
# plt.imshow(jetana.correlation_matrix, cmap='inferno', vmin=0, vmax=2)
# plt.xlim(50,600)
# plt.ylim(600,50)
# x = np.linspace(0,800,100)
# plt.plot(x, x, '-w', alpha=1, linewidth=0.25)
# plt.xticks([])
# plt.yticks([])
# plt.show()
# plt.savefig(savepath+"corrmat.png", transparent=True)



# JetRegionProps works with a single frame only
# region = JetRegionProps(jetana.stack_droplets_labeled[0])







if (view_raw) or (view_filtered) or (view_processed) or (view_thresholded):
    pg.Qt.QtWidgets.QApplication.instance().exec_()































