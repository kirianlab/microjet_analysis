import sys
import os
import glob

import numpy as np
import pylab as plt
import imageio 
import h5py
import json

import scipy
from scipy import stats
from skimage import filters, morphology, measure, feature
# from scipy.ndimage import binary_dilation, binary_erosion


import filters_playground as filt

import utils



class JetRegionProps():

    r"""A class to handle all the 'regions' in a single image

        Given a cleaned up image, will determine all the regions per frame and calculate a 
        bunch of information

        Notes:
            Make this class work with a single frame only
    """

    _processed_image = None
    _droplet_centroids = None
    _jet_info = None
    _region_properties = None # a tuple with properties to calculate
    _regions = None # the calculated regions


    def __init__(self, image, region_properties=None):

        # =============================
        # User defined properties
        # =============================

        # the filtered and "processed" image
        self._processed_image = image

        # decide what region properties to retrieve from skimage
        if region_properties is None:
            self._region_properties = ("label", "area", "major_axis_length", "minor_axis_length",
                                         "centroid", "local_centroid", "coords", "eccentricity", "image")
        else:
            self._region_properties = region_properties

        # get all the regions
        self._regions = self.get_regions()

    @property
    def processed_image(self):
        return self._processed_image

    @property
    def droplet_centroids(self):
        return self._droplet_centroids

    @property
    def jet_info(self):
        return self._jet_info

    @property
    def regions(self):
        return self._regions

    # =============================
    # Handling of all the regions
    # =============================

    def get_regions(self):
        r""" Given a single frame, will determine all the regions
        
            Returns:
                An skimage regionprops list
        """
        return measure.regionprops(self.processed_image)

    #Works with unfiltered image, have not tested with filtered image
    def get_nozzle_tip(self):
        f = self.processed_image
        xavg = [np.mean(f[:, i]) for i in range(f.shape[1])]
        grad = np.gradient(xavg)
        maxArr = [max(grad[0:30])]
        for i in range(30, len(grad) + 1, 30):
            maxs = max(np.abs(grad[i:i + 30]))
            if (maxs > 3 * np.max(maxArr)): return np.where(np.abs(grad) == maxs)[0][0]
            maxArr += [maxs]
        #Returns 0 if no nozzle tip found
        return 0

    def get_jet_droplet_region(self):
        frame = self.processed_image
        marker1x = 0
        marker2x = frame.shape[0]

        marker1y = 0
        # Goes up till the nozzle minus some pixels
        marker2y = self.get_nozzle_tip() - 10
        if (marker2y == -10): marker2y = frame.shape[1]
        # c is the size of each x-threshold
        c = 10
        sampleArr = np.arange(marker1y, marker2y + c, c)
        resultArr1 = []
        resultArr2 = []
        arr1 = []
        arr2 = []
        buffer = 5
        for j in range(0, len(sampleArr) - 1, 1):
            m1y = sampleArr[j]
            m2y = sampleArr[j + 1]
            for i in range(1, frame.shape[0], 1):
                med = np.median(frame[i, m1y:m2y])
                medOld = np.median(frame[i - 1, m1y:m2y])
                if (abs(med - medOld) / medOld > 0.07):
                    marker1x = i - 1 - buffer
                    resultArr1 += [marker1x]
                    arr1 += [sampleArr[j]]
                    break
            for i in range(frame.shape[0] - 2, marker1x, -1):
                med = np.median(frame[i, m1y:m2y])
                medOld = np.median(frame[i + 1, m1y:m2y])
                if (abs(med - medOld) / medOld > 0.07):
                    marker2x = i + 1 + buffer
                    resultArr2 += [marker2x]
                    arr2 += [sampleArr[j]]
                    break
        # Now, do a linear regression
        if (arr1 == [] or arr2 == []):
            return [[0, 0], [[0, 0], [0, 0]]]
        res1 = scipy.stats.linregress(arr1, resultArr1)
        res2 = scipy.stats.linregress(arr2, resultArr2)

        # Returns both the upper and lower percentiles, and the linear regressions of the upper and lower bounds
        return ([np.percentile(resultArr1, 5), np.percentile(resultArr2, 95)], [res1, res2])

    def get_jet_region(self):
        r"""Given a single frame, will return the region corresponding to the jet only
        """
        return

    def get_droplet_region(self):
        r"""Given a single frame, will return the regions corresponding to the droplets
        """
        return

    def get_droplet_centroids(self, regions):
        r"""Calculates droplet centroids for a single frame

            Arguments:
                regions: all the regions for a single frame

            Returns:
                A list of centroid coordinates for each region in the single frame

        """
        # get a list of all centroid coordinates for each region in the frame
        reg = [r.centroid for r in regions] # something like this?
        return reg

    def get_droplet_dispersion(self):
        r""" will calculate the droplet dispersion for a single frame"""
        return



    # =============================================================================
    # Things to calculate given regions -- does this belong in this class?
    # =============================================================================

    def get_jet_length(self):
        r""" Should this calculate an average jet length, or should we return a length per frame?"""
        jet_length = []
        return jet_length

    def get_jet_width(self):
        r""" Should this calculate an average jet width, or should we return a width per frame?"""
        jet_width = []
        return jet_width

    def get_droplet_dispersion(self):
        r""" Should this calculate an average droplet dispersion, or should we return it per frame?"""
        drop_dispersion = []
        return drop_dispersion

    def get_jet_deviation(self):
        r""" Where is the jet with respect to the nozzle tip (per frame or average across whole stack?)"""
        jet_deviation = []
        return jet_deviation















