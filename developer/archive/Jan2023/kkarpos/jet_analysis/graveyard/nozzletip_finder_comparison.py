import sys
import os
import glob
import time

import numpy as np
import matplotlib as mpl
import pylab as plt
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing


from jetframegetter import JetFrameGetter
from jetanalysis import JetAnalysis
from regionprops import JetRegionProps
from plotjetdata import PlotJetData


from viewer import ImWin
import utils
import pyqtgraph as pg

import filters_playground as filt

r"""
    This script serves as a playground for finding better nozzle tip finding
    methods! 
"""

nozzle_id = "mh_coarse_b2_n1" #"mh_cut2_b4_n2"
basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220609/"

config = {"data_path": os.path.join(basepath, nozzle_id),
            "save_path": os.path.join(basepath, nozzle_id, "analysis_kk"),
            }

all_data = np.sort(glob.glob(os.path.join(config['data_path'], "*.h5")))
randmovie = np.random.randint(0,len(all_data))
all_data = all_data[randmovie]


stack, metadata = utils.h5_metadata_version(all_data)
jetana = JetAnalysis(stack, metadata)


pstack = jetana.stack_processed # get the processed image stack
randframe = np.random.randint(0,len(pstack))

def _find_longest_nonzero_subset(self, im):

        max_subset = []
        current_max_subset = []
        for i, n in enumerate(im):
            if n > 0:
                current_max_subset.append(i)
                if len(current_max_subset) > len(max_subset):
                    max_subset = current_max_subset
            else:
                current_max_subset = []
        return max_subset

"""
    METHOD 1
"""

im = pstack[randframe]

im1 = ImWin(im, title=f'Video {randmovie}, Frame {randframe}')
# im1.display()

# sum across axis 1, the goal here is to see a large spike that correspondes to the jet
x = np.sum(im, axis=1)
y = np.sum(im, axis=0)
im1.add_plot(x, pen=pg.mkPen('b', width=3))


im1.display()



























