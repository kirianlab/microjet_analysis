import pyqtgraph as pg
import numpy as np


class ImWin(pg.ImageView):
    retry = False
    quit = False
    kill = False
    def __init__(self, *args, **kwargs):
        fail = True
        while fail:
            try:
                self.app = pg.mkQApp()
                self.win = pg.Qt.QtWidgets.QMainWindow()
                self.win.resize(1000, 800)
                if "title" in kwargs:
                    self.win.setWindowTitle(kwargs["title"])
                    del kwargs["title"]
                pi = pg.PlotItem()
                pg.ImageView.__init__(self, self.win, view=pi)
                # pyqtgraph does not like bool arrays and constantly prints warnings... so convert to int
                args = list(args)
                image = args.pop(0)
                if image.dtype == bool:
                    image = image.astype(int)
                self.setImage(image, *args, **kwargs)
                self.win.setCentralWidget(self)
                for m in ["resize"]:
                    setattr(self, m, getattr(self.win, m))
                self.setPredefinedGradient("flame")
                self.win.show()
                fail = False
                self.ntip_position = None
                self.ntip_idx = None
                self.jtip_position = None
                self.jet_breakup_idx = None
                self.drop_plot = None
                self.drop_centroids = None
                self.sigTimeChanged.connect(self._frame_changed)

            except RuntimeError:
                fail = True

    def _frame_changed(self, ind, time):
        # TODO make two versions of this, onewith jet and one with ntip
        # check setVisible(True or False)
        if (self.ntip_idx is None) or (self.jet_breakup_idx is None):
            return
        p = self.ntip_idx[ind]
        d = self.jet_breakup_idx[ind]
        dc = self.drop_centroids[ind]
        if self.ntip_position is None:
            self.ntip_position = self.add_line(angle=90, position=p, movable=True, pen='r')
            self.jtip_position = self.add_line(angle=90, position=d, movable=True, pen='b')
            self.drop_plot = self.add_plot(pen=None, symbolPen='g', symbol='o', symbolBrush=None, symbolSize=10)
            self.drop_plot.setData(dc[:,0],dc[:,1])
        else:
            self.ntip_position.setPos([p, p])
            self.jtip_position.setPos([d,d])
            self.drop_plot.setData(dc[:,0],dc[:,1])


    def add_plot(self, *args, **kwargs):
        return self.getView().plot(*args, **kwargs)

    def add_line(self, *args, **kwargs):
        p = kwargs["position"]
        del kwargs["position"]
        line = pg.InfiniteLine(*args, **kwargs)
        line.setPos([p, p])
        self.getView().addItem(line)
        return line

    def keyPressEvent(self, ev):
        if ev.key() == 82:  # r button
            print("'r' pushed")
            self.retry = True
            global retry
            retry = True
            self.app.quit()
        if ev.key() == 32:  # spacebar
            print("spacebar pushed")
            self.app.quit()
        if ev.key() == 81:  # q button
            print("'q' pushed")
            global quit
            quit = True
            self.quit = True
            self.app.quit()
        if ev.key() == 75:
            print("'k' pushed")
            global kill
            kill = True
            self.app.closeAllWindows()
            self.app.quit()
            self.kill = True
            del (self.app)

    def set_mask(self, mask, color=None):
        d = mask
        if color is None:
            color = (255, 255, 255, 20)
        mask_rgba = np.zeros((d.shape[0], d.shape[1], 4))
        r = np.zeros_like(d)
        r[d == 0] = color[0]
        g = np.zeros_like(d)
        g[d == 0] = color[1]
        b = np.zeros_like(d)
        b[d == 0] = color[2]
        t = np.zeros_like(d)
        t[d == 0] = color[3]
        mask_rgba[:, :, 0] = r
        mask_rgba[:, :, 1] = g
        mask_rgba[:, :, 2] = b
        mask_rgba[:, :, 3] = t
        im = pg.ImageItem(mask_rgba)
        self.getView().addItem(im)

    def display(self):
        pg.Qt.QtWidgets.QApplication.instance().exec_()


