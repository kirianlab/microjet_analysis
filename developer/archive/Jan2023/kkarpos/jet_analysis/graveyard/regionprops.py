import sys
import os
import glob

import numpy as np
import pylab as plt
import pandas as pd
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure, feature
from scipy.ndimage import binary_dilation, binary_erosion


import filters_playground as filt

import utils



class JetRegionProps():

    r"""A class to handle all the 'regions' in a single image

        Given a cleaned up image, will determine all the regions per frame and calculate a 
        bunch of information

        Notes:
            1) This class shouldn't require any camera information, it should only return 
            calculated data in terms of number of pixels. JetAnalysis should then
            convert pixel numbers to microns based on the given metadata

            2) Kosta made an executive decision... 
                incoming data should crop out the nozzle tip!


    """

    _processed_image = None
    _droplet_centroids = None
    _region_properties = None # a tuple with properties to calculate
    _regions = None # the calculated regions
    _regions_table = None

    _droplet_regions = None
    _droplet_line_fit = None

    _frame_info = None

    def __init__(self, image, region_properties=None):

        # =============================
        # User defined properties
        # =============================

        # the "processed" image
        self._processed_image = image
        self._image_shape = np.shape(self._processed_image)

        # decide what region properties to retrieve from skimage
        if region_properties is None:
            # self._region_properties = ("label", "area", "major_axis_length", "minor_axis_length",
            #                              "centroid", "coords", "eccentricity", "image")
            self._region_properties = ("label", "area", "centroid", "coords", "image")
        else:
            self._region_properties = region_properties

        # get the main regions
        self._regions_table = self._get_regions_table()

        # self._droplet_regions = self._get_droplet_regions()

        # specific region parameters
        self._droplet_centroids = self._get_droplet_centroids()
        self._droplet_line_fit = self._fit_droplets()

        # build the output dictionary
        self._frame_info = {'droplet_centroids': self._droplet_centroids,
                            'droplet_fit_line': self._droplet_line_fit,
                            }

    # =====================================
    # Set up all the module properties
    # =====================================

    @property
    def processed_image(self):
        return self._processed_image

    @property
    def droplet_centroids(self):
        return self._droplet_centroids

    @property
    def regions(self):
        return self._regions

    @property
    def regions_table(self):
        return self._regions_table

    # @property
    # def droplet_regions(self):
    #     return self._droplet_regions

    @property
    def frame_info(self):
        return self._frame_info

    @property
    def droplet_fit_line(self):
        return self._droplet_line_fit


    # =====================================
    # Handling of all the regions
    # =====================================

    def _get_regions_table(self):
        r""" Given a single frame, will determine all the regions 
                and output a Pandas DataFrame
        
            Returns:
                A Pandas DataFrame with user defined properties
        """
        reg_table = measure.regionprops_table(self.processed_image, properties=self._region_properties)
        return pd.DataFrame(reg_table)

    # def _get_droplet_regions(self):
    #     df = self.regions_table
    #     # q = df[df['label'] != df['label'][df['area'].idxmax()]]
    #     # a = q.copy()
    #     a = df.copy()
    #     print(a)
    #     a.drop(df[df['area'] < np.percentile(df['area'], 18)].index, inplace=True)
    #     return a

    def _remove_droplet_outliers(self, x, y):
        r"""Given droplet region centroid values, will remove outliers based on a z score

        """
        z = np.abs(scipy.stats.zscore(y))
        # print(np.min(z), np.max(z))
        zidx = np.where(z < 1.4)
        return x[zidx], y[zidx]

    def _fit(self, x, y):
        r"""Special function for fitting lines to the jet
            
            To Do:
                1) rename dictionary keys to something more human readable
                2) are all the outputs required?

            Returns:
                out (dict): dictionary with all calculated parameters
        """
        im_x = np.shape(self._processed_image)[0]
        slope, yint = np.polyfit(x, y, 1)  # Unweighted linear fit to positions of all pixels above threshold
        xf = np.arange(im_x)
        yf = yint + slope * xf
        ang = np.sign(slope) * np.arctan(slope)  # What's the sign for?  I can't recall
        u1 = np.array([np.cos(ang), np.sin(ang)])  # Unit vector along jet direction
        u2 = np.array([-np.sin(ang), np.cos(ang)])  # Unit vector orthogonal to jet direction
        v = np.vstack([x, y - yint]).T
        v1 = v.dot(u1)  # Coordinates along jet direction
        v2 = v.dot(u2)  # Coordinates orthogonal to jet direction
        p1, bin_edges = np.histogram(v1, bins=im_x, range=[0, im_x])  # Projection along jet direction
        p2, bin_edges = np.histogram(v2, bins=im_x, range=[0, im_x])  # Projection orthogonal jet direction
        out = {'slope': slope,
                'yint': yint,
                'v1': v1,
                'v2': v2,
                'p1': p1,
                'p2': p2,
                'xf': xf,
                'yf': yf,
                'ang': ang}
        return out

    def _fit_droplets(self):
        r"""

            Returns:
                out (dict): dictionary with all calculated parameters
        """
        x = self._droplet_centroids[0,:,0]
        y = self._droplet_centroids[0,:,1]
        out = self._fit(x, y)
        return out

    def _get_droplet_centroids(self):
        r"""Calculates droplet centroids for a single frame

            Arguments:
                regions: all the regions for a single frame

            Returns:
                A list of centroid coordinates for each region in the single frame

        """
        # get a list of all centroid coordinates for each region in the frame
        d = self.regions_table
        x = np.array(d['centroid-0'])
        y = np.array(d['centroid-1'])
        x, y = self._remove_droplet_outliers(x, y)
        return np.dstack((x,y))











