from skimage import filters
import numpy as np

r"""A place to store filters we might want to use for the jet analysis

Filters came from scikit-image, here's the link to the docs: https://scikit-image.org/docs/stable/api/skimage.filters.html

Wrappers are written below to work with our datasets. 

"""

def threshold_otsu(stack):
    # performs a threshold_otsu operation on a stack of frames
    thresh = [filters.threshold_otsu(s) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_local(stack, block_size:int=35):
    # performs a threshold_local operation on a stack of frames
    thresh = [filters.threshold_local(s, block_size) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_isodata(stack, nbins=256, return_all=False, hist=None):
    # performs a threshold_isodata operation on a stack of frames
    thresh = [filters.threshold_isodata(s, nbins=nbins, return_all=return_all, hist=hist) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_li(stack, tolerance=None, initial_guess=None, iter_callback=None):
    # performs a threshold_li operation on a stack of frames
    thresh = [filters.threshold_li(s, tolerance=tolerance, initial_guess=initial_guess, iter_callback=iter_callback) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)

def threshold_mean(stack):
    # performs a threshold_mean operation on a stack of frames
    thresh = [filters.threshold_mean(s) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)  

def threshold_niblack(stack, window_size:int=15, k:float=0.2):
    # performs a threshold_niblack operation on a stack of frames
    thresh = [filters.threshold_niblack(s, window_size=window_size) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)  

def threshold_sauvola(stack, window_size:int=15, k:float=0.2):
    # performs a threshold_sauvola operation on a stack of frames
    thresh = [filters.threshold_niblack(s, window_size=window_size) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)  

def threshold_triangle(stack, nbins:int=256):
    # performs a threshold_triangle operation on a stack of frames
    thresh = [filters.threshold_triangle(s, nbins=nbins) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)  

def threshold_yen(stack, nbins:int=256):
    # performs a threshold_mean operation on a stack of frames
    thresh = [filters.threshold_yen(s, nbins=nbins) for s in stack]
    x = [stack[i] > thresh[i] for i in range(len(thresh))]
    return np.array(x)  


# These won't work for us, but are available in scikit-image:

# def threshold_minimum(stack, nbins=256):
#     # performs a threshold_minimum operation on a stack of frames
#     thresh = [filters.threshold_minimum(s, nbins=nbins) for s in stack]
#     x = [stack[i] > thresh[i] for i in range(len(thresh))]
#     return np.array(x)  

# def threshold_multiotsu(stack, classes:int=3, nbins:int=256):
#     # performs a threshold_minimum operation on a stack of frames
#     thresh = [filters.threshold_multiotsu(s, classes=classes, nbins=nbins) for s in stack]
#     x = [stack[i] > thresh[i] for i in range(len(thresh))]
#     return np.array(x)  















