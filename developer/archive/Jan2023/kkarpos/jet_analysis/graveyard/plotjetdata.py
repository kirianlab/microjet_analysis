import sys
import os
import glob

import numpy as np
import pylab as plt
import matplotlib as mpl
import pandas as pd
import imageio 
import h5py
import json
import scipy

import utils

# This class might not need to be a class, I just wanted a way to keep things consistent

class PlotJetData():

    r"""A class to plot the analyzed data from the Kirian Lab microjet testing station

        Things to determine and general questions:

        Notes:
            1) This only works for a single nozzle type - any nozzle comparisons should be custom 
    """

    _stack_data = None
    _nozzle_id = None

    # data lists
    _jet_speed = []
    _jet_speed_err = []
    _liquid_flow = []
    _gas_flow = []
    _jet_diameter = []
    _jet_deviation = []
    _jet_deviation_err = []
    _droplet_dispersion = []
    _droplet_dispersion_err = []

    def __init__(self, stack_data:dict={}, nozzle_id:str=""):

        self._stack_data = stack_data
        self._nozzle_id = nozzle_id

        # check if data has already been analyzed
        self._build_data_lists_on_init()

    @property
    def nozzle_id(self):
        return self._nozzle_id

    @property
    def jet_speed(self):
        return np.array(self._jet_speed)

    @property
    def jet_speed_err(self):
        return np.array(self._jet_speed_err)

    @property
    def liquid_flow(self):
        return np.array(self._liquid_flow)

    @property
    def gas_flow(self):
        return np.array(self._gas_flow)

    @property
    def jet_diameter(self):
        return np.array(self._jet_diameter)

    @property
    def jet_length(self):
        return np.array(self._jet_length)

    @property
    def jet_length_err(self):
        return np.array(self._jet_length_err)

    @property
    def jet_deviation(self):
        return np.array(self._jet_deviation)

    @property
    def jet_deviation_err(self):
        return np.array(self._jet_deviation_err)

    @property
    def droplet_dispersion(self):
        return np.array(self._droplet_dispersion)

    @property
    def droplet_dispersion_err(self):
        return np.array(self._droplet_dispersion_err)


    def update(self, data:dict={}):
        r"""Updates the data lists with new data points
        """
        self._data_dict_to_lists(data)

    def _build_data_lists_on_init(self):
        r"""Reorganizes data for plotting

            Given the stack_data dictionary, will reorganize the data
            into multiple lists.
        """
        self._jet_speed = [v['jet_speed'] for k,v in self._stack_data.items()]
        self._jet_speed_err = [v['jet_speed_err'] for k,v in self._stack_data.items()]
        self._liquid_flow = [v['liquid_flow'] for k,v in self._stack_data.items()]
        self._gas_flow = [v['gas_flow'] for k,v in self._stack_data.items()]
        self._jet_diameter = [v['jet_diameter'] for k,v in self._stack_data.items()]
        self._jet_length = [v['jet_length'] for k,v in self._stack_data.items()]
        self._jet_length_err = [v['jet_length_err'] for k,v in self._stack_data.items()]
        self._jet_deviation = [v['jet_deviation'] for k,v in self._stack_data.items()]
        self._jet_deviation_err = [v['jet_deviation_err'] for k,v in self._stack_data.items()]
        self._droplet_dispersion = [v['droplet_dispersion'] for k,v in self._stack_data.items()]
        self._droplet_dispersion_err = [v['droplet_dispersion_err'] for k,v in self._stack_data.items()]

    def _data_dict_to_lists(self, data:dict={}):
        r"""Updates 
        """
        self._jet_speed.append(data['jet_speed'][()])
        self._jet_speed_err.append(data['jet_speed_err'][()])
        self._liquid_flow.append(data['liquid_flow'][()])
        self._gas_flow.append(data['gas_flow'][()])
        self._jet_diameter.append(data['jet_diameter'][()])
        self._jet_length.append(data['jet_length'][()])
        self._jet_length_err.append(data['jet_length_err'][()])
        self._jet_deviation.append(data['jet_deviation'][()])
        self._jet_deviation_err.append(data['jet_deviation_err'][()])
        self._droplet_dispersion.append(data['droplet_dispersion'][()])
        self._droplet_dispersion_err.append(data['droplet_dispersion_err'][()])




    def interpolate_ranges(self, ranges, data, level_step:int=5):
        r""" Prepares a basic contour plot given data

                Arguments:
                    ranges (list or tuple): the x, y, and z ranges for plotting
                                            should be [x,y,z] 
                    data (list or tuple): the x, y, and z data, should be [x,y,z]
                    level_step (int): the amount of countour levels between each datapoint
    
                Returns:

        """

        # Make a contour plot of irregularly spaced data 
        # coordinate via interpolation on a grid.
        n_levels = int((ranges[2][1]-ranges[2][0])/level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(n_levels+1))

        # Create grid values first.
        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
        interpolator = mpl.tri.LinearTriInterpolator(mpl.tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels



























































