import sys
import os
import glob

import numpy as np
import imageio 
import h5py
import json
import scipy

import utils


class JetFrameGetter():
    r"""A class to grab and update frames from the Kirian Lab microjet testing station"""

    
    _config = None
    _all_data = None  # all data in main directory
    _analyzed_data = None  # all data analyzed
    _data_to_analyze = None  # all the data to be analyzed
    _current_data = None  # single h5 to be analyzed
    _image_stack = None  # the frames from the fastcam
    _image_metadata = None  # all the metadata saved 
    _index = 0
    _analyzed_data_save_path = None


    def __init__(self, config):
        self._config = utils.validate_config(config)
        self._all_data = glob.glob(os.path.join(self._config['data_path'], "*.h5"))
        self._analyzed_data = glob.glob(os.path.join(self._config['data_path'], "analysis", "*.h5"))
        self._data_to_analyze = [d for d in self._all_data if d not in self._analyzed_data]
        self._current_data = self._data_to_analyze[self._index]
        self._image_stack, self._image_metadata = utils.h5_metadata_version(self._current_data)

    @property
    def config(self):
        return self._config

    @property
    def all_data(self):
        return self._all_data

    @property
    def analyzed_data(self):
        return self._analyzed_data

    @property
    def data_to_analyze(self):
        return self._data_to_analyze

    @property
    def current_data(self):
        return self._current_data

    @property
    def image_stack(self):
        return self._image_stack

    @property
    def image_metadata(self):
        return self._image_metadata

    @property
    def index(self):
        return self._index

    @property
    def analyzed_data_save_path(self):
        return self._analyzed_data_save_path

    @config.setter
    def config(self, config):
        r""" Lets you update the config live -- might be useful when combining multiple datasets"""
        self._config = utils.validate_config(config)

    # to do: make function to create necessary folders

    def update_index(self):
        self._index += 1

    def update_image(self):
        self.update_index()
        self._current_data = self._data_to_analyze[self._index]
        self._image_stack, self._image_metadata = utils.h5_metadata_version(self._current_data)
        return self._image_stack, self._image_metadata

    def update_data(self):
        self._all_data = glob.glob(os.path.join(self._config['data_path'], "*.h5"))
        self._analyzed_data = glob.glob(os.path.join(self._config['data_path'], "analysis", "*.h5"))
        self._data_to_analyze = [d for d in self._all_data if d not in self._analyzed_data]

    def __iter__(self):
        return JetAnalysisIterator(self)


class JetFrameGetterIterator:
    r""" A class for iterating through files in the analysis queue"""

    def __init__(self, jetframe):
        self.ja = jetframe
        self.count = 0

    def __next__(self):
        r""" Goes through all available files and will stop when it has gone through each file once """
        if self.count > len(self.ja.data_to_analyze):
            raise StopIteration
        self._image_stack, self._image_metadata = self.ja.update_image()
        self.count += 1
        return self._image_stack, self._image_metadata









