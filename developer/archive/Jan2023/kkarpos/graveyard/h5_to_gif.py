import h5py
import numpy as np
import imageio
import os

def get_ody_movie_frames_from_h5(h5_filepath):
    r'''
    Given the filepath to a specific h5 file written by Odysseus, 
    will output the raw movie frames as a multipl array
    '''
    h5 = h5py.File(h5_filepath, "r")
    frames = h5['frames']
    return frames


def frames_to_gif(frames, gif_filename, fps=24):
    r'''
    This function takes a set of frames and writes them as a gif
    Parameters:
        frames: a numpy array with individual frames, can be RGB
        gif_filename: a string, name of gif being written, add the full directory path here
        fps: frames per second, must be an int. Default is 24
    '''
    movie_name = os.path.join(gif_filename)
    imageio.mimwrite(movie_name, frames, fps=fps)
    print('Wrote gif here:')
    print('\t %s' % movie_name)



base_path = os.path.join(os.sep, 'mnt', 'Z', 'GradSchool', 'bioxfel', 'data', 'microjet', '2021', '20210113')
movie_path = os.path.join(base_path,"dffn_leaking_2.gif")
h5_path = os.path.join(base_path, "018.h5")

frames = get_ody_movie_frames_from_h5(h5_path)
frames_to_gif(frames, movie_path, fps=24)