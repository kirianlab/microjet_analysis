import sys
import os
import glob

import numpy as np
import pylab as plt
import pandas as pd
import pyqtgraph as pg
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure, feature

import filters_playground as filt
from regionprops import JetRegionProps

import utils


def _filter_stack(stack, filter_method:str='sobel'):
    r""" Detects edges and filters background
        
        Arguments:
            stack: The images that make up your movie. This will also work on a single image.
            erode (bool): Will perform a morphological erosion step to further filter the background

        Returns:
            a filtered stack of the same shape as the input stack
    """
    if filter_method == 'sobel':
        stack = [filters.sobel(s) for s in stack]
    else:
        raise KeyError("filter_method can only handle 'sobel' for now. Other algrothims will be added eventually.")
    return np.array(stack)

def _threshold_stack(stack, threshold_method:str='otsu'):
    r""" Will threshold an image stack using the provided method
        
        Arguments:
            stack (array): the image stack, np.ndarray or not
            threshold_method (str): the thresholding method to use
                TO DO: I don't want to waste time adding other methods in for now, 
                        update this to either take a user-defined method, or call in 
                        in a different method (with proper parameters) from the
                        filters_playground.py script. 

        Returns:
            (ndarray): thresholded image stack
    """
    if threshold_method != 'otsu':
        raise ValueError("Only 'otsu' is implemented for now.")
        # To Do: add in options to switch between other thresholding methods
    thresh_stack = filt.threshold_otsu(stack)
    return np.array(thresh_stack)

def _apply_processing_methods(stack, 
                                remove_small_objects:int=10, 
                                binary_closing_iterations:int=3,
                                footprint='default'):
    r"""Apply morphology methods to the stack, gathering a host of connected component parameters

        Arguments:
            stack: Your image stack, it should be filtered and thresholded before running this function
            remove_small_objects (int): 
    """
    if footprint == 'default':
        footprint = [[0,0,0,0],
                     [0,0,0,0],
                     [1,1,1,1],
                     [0,0,0,0],
                     [0,0,0,0]]
    # first remove all the small activated pixels
    sclean = np.array([morphology.remove_small_objects(s, remove_small_objects) for s in stack])
    # apply a binary closing method to fill in droplets
    sdil = np.array([scipy.ndimage.binary_closing(s, structure=footprint, 
                                                    iterations=binary_closing_iterations) for s in sclean])
    return sdil 

def cleanup_image(stack):
    a = _filter_stack(stack)
    b = _threshold_stack(a)
    c = _apply_processing_methods(b)
    return c

def _find_longest_nonzero_subset(im):
    max_subset = []
    current_max_subset = []
    for i, n in enumerate(im):
        if n > 0:
            current_max_subset.append(i)
            if len(current_max_subset) > len(max_subset):
                max_subset = current_max_subset
        else:
            current_max_subset = []
    return max_subset



nozzle_id = "mh_coarse_b2_n1" #"mh_cut2_b4_n2"
# basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220620/"
basepath = "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220609/"
config = {"data_path": os.path.join(basepath, nozzle_id),
            "save_path": os.path.join(basepath, nozzle_id, "analysis_kk")}

all_data = glob.glob(os.path.join(config['data_path'], "*.h5"))
data = np.sort(all_data)[2]

f = h5py.File(data, 'r')
stack = f['frames'][()]
stack = cleanup_image(stack)
imshape = np.shape(stack)
if imshape[1] < imshape[2]:
    stack = np.swapaxes(stack, 1, 2)

# ==========

im = stack[0]

# # get droplet region
# a = np.sum(im, axis=1)
# b = _find_longest_nonzero_subset(a)

# drop_jet = b[0]

# jetnoz = im[b,:]

# # get nozzle region
# c = np.sum(jetnoz, axis=1)
# cg = np.gradient(c)
# d = _find_longest_nonzero_subset(cg)

# noz = im[b[0]+d[0]:,:]
# jet = im[b[0]:b[0]+d[0],:]


# nozzle = im.copy()
# jet = im.copy()
# drops = im.copy()

# nozzle[:b[0]+d[0],:] = 0
# jet[:b[0],:] = 0
# jet[b[0]+d[0]:,:] = 0
# drops[b[0]:,:] = 0

def _get_regions_idx(im):
    # get the droplet region
    a = np.sum(im, axis=1)
    b = _find_longest_nonzero_subset(a)
    jetnoz = im[b,:]
    # get the nozzle region
    c = np.sum(jetnoz, axis=1)
    cg = np.gradient(c)
    d = _find_longest_nonzero_subset(cg)
    return b[0], d[0]

# jet_idx = np.zeros((len(stack),2), dtype=int)
# nozzle = stack.copy()
# jet = stack.copy()
# drops = stack.copy()
# for i, im in enumerate(stack):
#     b, d = _get_regions_idx(im)
#     jet_idx[i] = b, b+d
#     nozzle[i,:b+d,:] = 0
#     jet[i,:b,:] = 0
#     jet[i,b+d:,:] = 0
#     drops[i,b:,:] = 0






# pg.image(jet)
# pg.QtGui.QApplication.instance().exec_()

































































