
import pandas as pd
import h5py 
import os
import sys
import glob
import configparser


r"""
    Will compile all meta data from odysseus into a .csv. requires data path to be given.
"""

def odysseus_metadata_as_excel(path):
    r'''
    This function is specfic to the KirianLab microfluidics data. 
    This function will search through a given directory and recompile all the metadata as an excel document
    The input is the filepath to the data:
        ex: /bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2021/20210111/dffn_1/
    The output is a saved csv file in the filepath provided
    See the keys in the compiled_data variable. 

    Requirements: 
        1) import glob
        2) import pandas as pd
        3) import h5py
        4)
    '''

    compiled_data = {
        'run_number': [],
        'date': [],
        'time': [],
        'run_number': [],
        'framerate_in_Hz': [],
        'shutterspeed': [],
        'height': [],
        'width': [],
        'pixel_size_in_micrometer': [],
        'bits': [],
        'number_of_frames': [],
        'gas_flow_rate_in_mg_per_min': [],
        'liquid_flow_rate_in_microliters_per_min': []}

    meta_data = sorted(glob.glob(path + "*.txt"))


    for m in meta_data:
        meta = configparser.ConfigParser()
        meta.read(m)

        run_num = m.split("/")[-1].split(".")[0]
        compiled_data['run_number'].append(int(run_num))
        compiled_data['date'].append(int(meta['timestamp']['date']))
        compiled_data['time'].append(int(meta['timestamp']['time']))
        compiled_data['framerate_in_Hz'].append(int(meta['fastcam']['framerate_fps']))
        compiled_data['shutterspeed'].append(int(meta['fastcam']['shutter_fps']))
        compiled_data['width'].append(int(meta['fastcam']['width']))
        compiled_data['height'].append(int(meta['fastcam']['height']))
        compiled_data['pixel_size_in_micrometer'].append(int(meta['fastcam']['pixels']))
        compiled_data['bits'].append(int(meta['fastcam']['bits']))
        compiled_data['number_of_frames'].append(int(meta['fastcam']['frames']))

        gas = meta['fluidics']['gas']
        gas = gas.split(" ")
        compiled_data['gas_flow_rate_in_mg_per_min'].append(float(gas[0]))
        liq = meta['fluidics']['liquid']
        liq = liq.split(" ")
        compiled_data['liquid_flow_rate_in_microliters_per_min'].append(float(liq[0]))


    outpath = path + path.split("/")[-2] + ".csv"

    df = pd.DataFrame(compiled_data)
    df.to_csv(outpath)

    print('Wrote csv file here:')
    print('\t %s' % outpath)

if __name__ == "__main__":

    data_path = "/home/kkarpos/rawdata_temp/microjets/581_64/"

    odysseus_metadata_as_excel(data_path)