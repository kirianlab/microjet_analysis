import numpy as np
import pandas as pd
import glob

class LoadFlowData(object):
	r""" Load in data from Sahba's Athena software """	

	def __init__(self, year):
		r""" The date of the file you're interested in. Inputs should be strings. """
		self.year = year
		#self.main_path = '/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/'
		self.complete_file_path = '/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/%s/' % self.year
		#self.date = date

	def load_flow_data(self, date):
		#complete_file_path = self.main_path + self.year
		file_list = []
		for file in glob.glob(self.complete_file_path + 'logs/%s_*_plot_data.txt' % date):
			file_list.append(file)

		if len(file_list) > 1:
			print('!!!!!!!!!!!!!!!!!!!')
			print('There are multiple times available, please choose the desired time:')
			for i in file_list:
				print(i)
			user_input = input('Input file time: ' )
			chosen_file = glob.glob(self.complete_file_path + 'logs/%s_%s_plot_data.txt' % (date, user_input))
			if len(chosen_file) == 0:
				while len(chosen_file) == 0:
					print('Number of files found is:  ' + '%d' % len(chosen_file))
					print('Try again.')
					user_input = input('Input file time: ' )
					chosen_file = glob.glob(self.complete_file_path + 'logs/%s_%s_plot_data.txt' % (date, user_input))
					if len(chosen_file) == 1:
				 		print(chosen_file[0])
				 		data = pd.read_csv(chosen_file[0])
			else:
				print(chosen_file[0])
				data = pd.read_csv(chosen_file[0])
		else:
			print(file_list[0])
			data = pd.read_csv(file_list[0])

		return(data)

		
	def load_liq(self, date):
		df = self.load_flow_data(date)
		liq = df.iloc[:,4].to_numpy()
		return(liq)

	def load_gas(self, date):
		df = self.load_flow_data(date)
		gas = df.iloc[:,3].to_numpy()
		return(gas)

	def load_vid(self, date):
		df = self.load_flow_data(date)
		vid = df.iloc[:,2].to_numpy()
		return(vid)

	def load_epoc_time(self, date):
		df = self.load_flow_data(date)
		epoc = df.iloc[:,6].to_numpy()
		return(epoc)

	def load_human_time(self, date):
		df = self.load_flow_data(date)
		human = df.iloc[:,7].to_numpy()
		return(human)

	def load_chamber_medium(self, date):
		df = self.load_flow_data(date)
		medium = df.iloc[:,3].to_numpy()
		return(medium)


