import sys
import os
import argparse
import glob
from time import time, sleep
import datetime
import numpy as np
from skimage import io
from numba import jit
from scipy.signal import savgol_filter, medfilt
import configparser
from skimage.filters import gaussian
import h5py
from jet_analyzer import main_analysis as jet_analysis
from jet_analyzer import load_sahba_meta

class JetAnalysis():

    def __init__(self, datapath: str, savepath: str, logpath: str, wait_time: int):
        self.datapath = datapath
        self.savepath = savepath
        self.logpath = logpath
        self.files = self.update_files_in_datapath()
        self.meta_keys = ["year", "date", "_time", "gas_flow", "liquid_flow", "pixel_size", "delay"]
        self.wait_time = wait_time # seconds
        # self.previous_results = None

    def update_files_in_datapath(self):
        files = glob.glob(os.path.join(self.datapath, "*.h5"))
        analyzed = glob.glob(os.path.join(self.savepath, "*_analysis.h5"))
        files_names = [name.split(os.sep)[-1] for name in files]
        analyzed_names = [name.split(os.sep)[-1].replace("_analysis", "") for name in analyzed]
        new_names = [name for name in files_names if name not in analyzed_names]
        return [f for f in files for name in new_names if name in f]
        # return files, analyzed, files_names, analyzed_names, new_names

    @staticmethod
    def write_data_as_h5(data, h5_path, group_name="default", verbose=False):
        r"""
        Writes a data group to a specified h5 file.

        Arguments
        ----------
            data (dict): inputs must follow this format ("data_name", data).
                            The *args allows for multiple entries in a single group.
                            This function must be called once for each h5 group.
                                Example:
                                    write_data_as_h5(("camera_frames", frames),
                                                     ("time_stamp", time_stamp_data),
                                                       group_name="raw_frames")
                                In the example, we see that two data subgroups are made within
                                the "raw_frames" group.
            h5_path (str):
            group_name (str): Name of data group. Can be recursive,
                                Example:
                                    group_name="config/pixel_size"
                                In the example, the pixel_size group is made within the config
                                group. This allows nested groups for data to be saved into.
            verbose (bool): If True, prints group name. Else, prints nothing.
        Returns
        --------
            None: Save an h5 at desired location.
        """
        # try:
            # "Append to already made h5"
            # h5 = h5py.File(h5_path, 'a')
        # except ValueError:
        #     "Create the h5 first, then append the data"
        h5 = h5py.File(h5_path, 'w')
        try:
            "Create group if it does not exist"
            group = h5.create_group(group_name)
        except ValueError:
            "Appends to group if it already exists"
            group = h5["{}".format(group_name)]

        for k, v in data.items():
            "Save the dataset"
            group.create_dataset(k, data=v)

        h5.close()

        if verbose is True:
            print("Created the {} group!".format(group_name))
            print("Closed {}!".format(h5_path))

    def analyze_jet(self, file_path, interactive=False, display=True,
                    droplet_roi=None, results=True, overwrite=True,
                    require_logs=True, thresh_sigma=3, darkfield=False,
                    nozzle_position: int = None, droplet_position: int = None, droplet_threshold: float = None):
        r"""
        Run jet analysis on a given file.

        Arguments
        ---------
            file_path (str): path to HDF5 file to analyze
            interactive (bool): run analysis in interactive mode (default = True)
            display (bool): display analysis pipeline (default = True)
            droplet_roi=None,
            results=True,
            overwrite=True,
            logs_directory=self.logpath,
            require_logs=True,
            thresh_sigma=3,
            darkfield=False,
            nozzle_position (int): (default = None)
            droplet_position (int): (default = None)
            droplet_threshold (float): (default = None)
        Returns
        -------
            analysis (dict):
        """
        config = {"file": file_path,
                  "interactive": interactive,
                  "display": display,
                  "results": results,
                  "droplet_roi": droplet_roi,
                  "logs_directory": self.logpath,
                  "require_logs": require_logs,
                  "drop_pos": droplet_position,
                  "nozzle_pos": nozzle_position,
                  "droplet_threshold": droplet_threshold,
                  "thresh_sigma": thresh_sigma,
                  "darkfield": darkfield,
                  "overwrite": overwrite}
        analysis = jet_analysis(config)
        return analysis

    def _redo_analysis(self, analysis, f: str):
        # FIX ME: remove this, add keypress option to jet_analyzer to restart analysis
        # usr_input = input("Is the analysis good? Or do you want to move on? (yes/no)")
        if analysis is not None:
            pass
        elif analysis is None:
            analysis = self.analyze_jet(f, interactive=True)
            self._redo_analysis(analysis, f)
        else:
            self._redo_analysis(f)
        return analysis

    def start_live_analysis(self):
        # might be weird with the q & space user inputs from jet_analyzer check this later.
        if len(self.files) == 0:
            t = self.wait_time
            print(f"No new files, waiting {t} seconds.")
            sleep(t)
        else:
            for f in self.files:
                print(f"\n\n\t Analyzing {f}...\n\n")
                results_path = os.path.join(savepath, f[-6:-3] + "_analysis.h5")
                analysis = self._redo_analysis(self.analyze_jet(f), f)
                print(f"\n\t Saving analysis in {results_path}...\n\n")
                self.write_data_as_h5(data=analysis, h5_path=results_path, group_name="jet_analysis")
                meta_f = f[:-2] + "txt"
                meta_values = load_sahba_meta(meta_file=meta_f)
                meta = dict(zip(self.meta_keys, meta_values))
                self.write_data_as_h5(data=meta, h5_path=results_path, group_name="meta_data")
                print("\n\t Adding meta data to original data... \n\n")
                self.write_data_as_h5(data=meta, h5_path=f, group_name="meta_data")
                print(f"\n\n Run {f[-6:-3]} analyzed and saved! \n\n")
                # self.previous_results = results_path
        self.files = self.update_files_in_datapath()  # watch for new files
        self.start_live_analysis()


if __name__ == "__main__":
    # "/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/logs"
    base_directory = "/home/kkarpos/rawdata_temp/microjets/20210713/359_79"
    datapath = os.path.join(base_directory)
    savepath = os.path.join(base_directory, "analyzed_data")
    os.makedirs(savepath, exist_ok=True)

    logpath = ""
    wait_time = 10 # seconds
    analyzer = JetAnalysis(datapath=datapath, savepath=savepath, 
                            logpath=logpath, wait_time=wait_time)
    analyzer.start_live_analysis()