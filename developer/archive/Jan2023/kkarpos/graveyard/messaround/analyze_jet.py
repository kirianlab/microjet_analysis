import sys
import os
import argparse
import glob
from time import time, sleep
import datetime
import numpy as np
from skimage import io
from numba import jit
from scipy.signal import savgol_filter, medfilt
import configparser
from skimage.filters import gaussian
import h5py
# from jet_analyzer import main_analysis as jet_analysis
# from jet_analyzer import load_sahba_meta
import pyqtgraph as pg

class JetAnalysis():

    def __init__(self, datapath: str, savepath: str, logpath: str, wait_time: int):
        self.datapath = datapath
        self.savepath = savepath
        self.logpath = logpath
        self.files = self.update_files_in_datapath()
        self.meta_keys = ["year", "date", "_time", "gas_flow", "liquid_flow", "pixel_size", "delay"]
        self.wait_time = wait_time # seconds


    def update_files_in_datapath(self):
        g = self.datapath
        print('Searching for files in %s' % g)
        files = sorted(glob.glob(os.path.join(self.datapath, "*.h5")))
        # print(files)
        analyzed = sorted(glob.glob(os.path.join(self.savepath, "*_analysis.h5")))
        files_names = [name.split(os.sep)[-1] for name in files]
        analyzed_names = [name.split(os.sep)[-1].replace("_analysis", "") for name in analyzed]
        new_names = [name for name in files_names if name not in analyzed_names]
        return [f for f in files for name in new_names if name in f]
        # return files, analyzed, files_names, analyzed_names, new_names

    @staticmethod
    def save_data_as_h5(data, h5_path, group_name="default", verbose=False):
        r"""
        prints a data group to a specified h5 file.

        Arguments
        ----------
            data (dict): inputs must follow this format ("data_name", data).
                            The *args allows for multiple entries in a single group.
                            This function must be called once for each h5 group.
                                Example:
                                    print_data_as_h5(("camera_frames", frames),
                                                     ("time_stamp", time_stamp_data),
                                                       group_name="raw_frames")
                                In the example, we see that two data subgroups are made within
                                the "raw_frames" group.
            h5_path (str):
            group_name (str): Name of data group. Can be recursive,
                                Example:
                                    group_name="config/pixel_size"
                                In the example, the pixel_size group is made within the config
                                group. This allows nested groups for data to be saved into.
            verbose (bool): If True, prints group name. Else, prints nothing.
        Returns
        --------
            None: Save an h5 at desired location.
        """
        # try:
            # "Append to already made h5"
            # h5 = h5py.File(h5_path, 'a')
        # except ValueError:
        #     "Create the h5 first, then append the data"
        h5 = h5py.File(h5_path, 'w')
        try:
            "Create group if it does not exist"
            group = h5.create_group(group_name)
        except ValueError:
            "Appends to group if it already exists"
            group = h5["{}".format(group_name)]

        for k, v in data.items():
            "Save the dataset"
            group.create_dataset(k, data=v)

        h5.close()

        if verbose is True:
            print("Created the {} group!".format(group_name))
            print("Closed {}!".format(h5_path))

    def _load_sahba_logs(self, log_files_regex):
        file_names = glob.glob(log_files_regex)
        if len(file_names) == 0:
            print("Cannot find the log file %s" % (file_name,))
            return None, None, None, None, None
        epoch_sec = []
        datetime = []
        gas_flow = []
        liquid_flow = []
        pixel_size = []
        delay = []
        for file_name in file_names:
            with open(file_name) as f:
                for line in f:
                    if line[0] == 'E':
                        continue
                    s = line.split(',')
                    epoch_sec.append(float(s[0].strip()))
                    datetime.append(s[1].strip())
                    gas_flow.append(float(s[2].strip()))
                    liquid_flow.append(float(s[3].strip()))
                    pixel_size.append(float(s[7].strip()))
                    delay.append(float(s[8].strip()))
        epoch_sec = np.array(epoch_sec)
        gas_flow = np.array(gas_flow)
        liquid_flow = np.array(liquid_flow)
        pixel_size = np.array(pixel_size)
        delay = np.array(delay)
        return epoch_sec, gas_flow, liquid_flow, pixel_size, delay

    def _load_sahba_meta(self, meta_file):
        year = ""
        date = ""
        _time = ""
        gas_flow = None
        liquid_flow = None
        pixel_size = None
        delay = 550
        if os.path.exists(meta_file):
            meta = configparser.ConfigParser()
            meta.read(meta_file)
            if 'timestamp' in meta:
                year = meta['timestamp']['year']
                date = meta['timestamp']['date']
                _time = meta['timestamp']['time']
            if 'fluidics' in meta:
                fluidics = meta['fluidics']
                if 'gas' in fluidics:
                    g = fluidics['gas']
                    if g != '':
                        q = g.split(" ")
                        gas_flow = float(q[0])
                if 'liquid' in fluidics:
                    g = fluidics['liquid']
                    if g != '':
                        q = g.split(' ')
                        liquid_flow = float(q[0])
            if 'optics' in meta:
                optics = meta['optics']
                if 'pixelsize_um' in optics:
                    g = optics['pixelsize_um']
                    if g != '':
                        pixel_size = float(g)
        return year, date, _time, gas_flow, liquid_flow, pixel_size, delay


    def _get_thresh(self, image, n_fwhm=3.5, mask=None):
        """
        A special function for finding droplet thresholds
        *  The basic idea is that we do a brute-force scan 
           through threshold levels.  As we raise the level, 
           we expect a rather sudden drop in the number of 
           pixels above threshold when we rise above the background.
           We find this point, and the rate at which the drop 
           happens, but taking the derivative of the counts above 
           threshold as a function of the threshold level.
        """

        mimage = image.copy()
        if mask is None:
            mask = np.ones_like(mimage)
        mimage = mimage.flat[mask.flat != 0]    # Right away, we throw out all masked pixels.
        im_min = np.percentile(mimage, 1)       # Start the threshold scan at the 1% level of image values
        im_max = np.percentile(mimage, 99.999)  # End the scan at the 99.999% level
        thresholds = im_min + (im_max - im_min)*np.arange(0, 1, 0.001)
        counts = np.zeros_like(thresholds)
        for i in range(thresholds.shape[0]):    # Brute force scan of threshold value
            counts[i] = np.sum(mimage > thresholds[i])
        counts /= mimage.size                   # Turn counts into a fraction of pixels above threshold
        cnt_grad = savgol_filter(np.gradient(counts), 51, 3)  # Smooth the function (else our derivative is wild)
        cnt_grad = - cnt_grad / np.max(-cnt_grad)  # Normalize gradient, and invert.
        w = np.where(cnt_grad.flat > 0.5*np.max(cnt_grad.flat))[0]
        fwhm = len(w)*(thresholds[1] - thresholds[0])
        best_threshold = np.median(thresholds[w]) + n_fwhm * fwhm
        return best_threshold, cnt_grad, thresholds, counts, mask

    def _fit_jet(self, image, thresh, x, y):
        # Special function for fitting lines to the jet
        fail = True
        wjet = np.where((image).flat > thresh)[0]
        if len(wjet) == 0:
            return None, None, None, None, None, None, None, None, None, fail
        fail = False
        xj, yj = x.flat[wjet], y.flat[wjet]
        slope, yint = np.polyfit(xj, yj, 1)  # Unweighted linear fit to positions of all pixels above threshold
        xf = np.arange(image.shape[0])
        yf = yint + slope*xf
        ang = np.sign(slope) * np.arctan(slope)  # What's the sign for?  I can't recall
        u1 = np.array([np.cos(ang),  np.sin(ang)])  # Unit vector along jet direction
        u2 = np.array([-np.sin(ang), np.cos(ang)])  # Unit vector orthogonal to jet direction
        v = np.vstack([xj, yj - yint]).T
        v1 = v.dot(u1)  # Coordinates along jet direction
        v2 = v.dot(u2)  # Coordinates orthogonal to jet direction
        nbins = image.shape[0]
        p1, bin_edges = np.histogram(v1, bins=nbins, range=[0, nbins])  # Projection along jet direction
        p2, bin_edges = np.histogram(v2, bins=nbins, range=[0, nbins])  # Projection orthogonal jet direction
        return slope, yint, v1, v2, p1, p2, xf, yf, ang, fail


    def load_data(self, file_name):
        m_dat = {"year": [],
                 "date": [], 
                 "time": [],
                 "gas_flow": [],
                 "liquid_flow": [],
                 "pixel_size": [],
                 "delay": []}
        no_extension, file_extension = os.path.splitext(file_name)
        if file_extension == ".tif":
            stack = io.imread(file_name).astype(np.single)
            stack_transpose = (0, 2, 1)
            stack = np.transpose(stack, stack_transpose)
        elif file_extension == ".h5":
            h5 = h5py.File(file_name, "r")
            stack = np.array(h5["frames"], dtype=np.double)
            stack_transpose = (0, 2, 1)
            stack = np.transpose(stack, stack_transpose)
            """
            Odysseus saves as h5 with the flow rates 
            in a seperate txt file
            """
            meta_file = no_extension + ".txt"
            print(f"\tGathering data from the following meta file: \n\t {meta_file}")
            if os.path.exists(meta_file):
                """
                load_sahba_meta outputs in this order:
                    year, date, time, gas_flow, 
                    liquid_flow, pixel_size, delay
                """
                dat = self._load_sahba_meta(meta_file)
                print(dat)
                m_dat["year"] = dat[0]
                m_dat["date"] = dat[1]
                m_dat["time"] = dat[2]
                epoch_sec = datetime.datetime(int(dat[0]),
                                              int(dat[1][4:6]),
                                              int(dat[1][6:8]),
                                              int(dat[2][0:2]),
                                              int(dat[2][2:4]),
                                              int(dat[2][4:6]))
                if dat[3] is None or dat[4] is None:
                    print("""
                        WARNING: Cannot find fluidics data!
                        \n 
                        Accessing data from the logs""")
                    log_regex = os.path.join(self.logpath, 
                                             dat[1],
                                             "_*_all_data.txt")
                    glb = glob.glob(log_regex)
                    if len(glb) > 0:
                        slogs = self._load_sahba_logs(log_regex)
                        w = np.where(slogs[0] == epoch_sec)
                        if len(w[0]) > 0:
                            m_dat["gas_flow"] = np.mean(slogs[1][w])
                            m_dat["liquid_flow"] = np.mean(slogs[2][w])
                            m_dat["pixel_size"] = np.mean(slogs[3][w])
                            m_dat["delay"] = np.mean(slogs[4][w])
                else:
                    m_dat["gas_flow"] = dat[3]
                    m_dat["liquid_flow"] = dat[4]
                    m_dat["pixel_size"] = dat[5]
                    m_dat["delay"] = dat[6]
                    print(m_dat)
            else:
                print("WARNING: No meta file!")
        else:
            print("WARNING: File extension not recognized, quitting program.")
            sys.exit()
        return stack, stack_transpose, m_dat


    def image_analysis(self, file_name, stack, meta_data, 
                        display=True, interactive=False, is_darkfield=False,
                        thresh_sigma=3, reuse=False):
        back_even = None
        back_odd = None
        jet_crop = None
        nozzle_pos = None
        drop_pos = None
        droplet_threshold = None

        if reuse == True:
            try: 
                f = open("previous_results.txt", "r")
                prev = f.readline()
                if os.path.isfile(prev):
                    print("Loading previous results...")
                    try:
                        dat = h5py.File(prev, 'r')
                        jet_crop = dat["jet_analysis/jet_crop"]
                        droplet_threshold = dat["jet_analysis/droplet_threshold"]
                        print("Accessed previous results!")
                    except KeyError:
                        print("Can't load previous results... old format?")
                        reuse = False
            except:
                FileNotFoundError
                pass


        @jit(nopython=True)
        def _shear(matrix):
            """
            A special matrix shear operation for analyzing 
            the jet cross correlations
            """

            arr = np.empty_like(matrix)
            fast_scan, slow_scan = matrix.shape
            for i in range(slow_scan):
                for j in range(fast_scan):
                    arr[i, j] = matrix[i, (j + i) % fast_scan]
            return arr


        stack = np.array(stack)

        # gather image shapes
        n_frames = stack.shape[0]
        nx = stack.shape[1]
        ny = stack.shape[2]

        # gather line fitting parameters 
        x, y = np.meshgrid(np.arange(nx), np.arange(ny), indexing="ij")

        """
        Hold on to the std deviation of the original data stack.
        It is useful for estimating jet length and jet position
        *  Note that we only use even or odd frames because 
           of the dual laser pulses; we don't want the variation
           in laser intensity to contribute to this standard deviation
        """
        stack0_std = np.std(stack[::2, :, :] / np.mean(stack[::2, :, :]), axis=0)
        stack0_std -= np.median(stack0_std)
        stack0_std /= np.max(np.abs(stack0_std))

        """
        Estimate the incident brightfield illumination profile
        *  Treat evens and odds separately since they are 
           different laser flashes and may have different 
           pulse energies.
        *  We take the illumination profile to be the 80th 
           percentile of intensities.  This should be reasonable 
           in the region where there are droplets, which are 
           likely present roughly 50% of the time.  This is not 
           a good estimate for regions where there a jet or 
           anything else that is static.
        """
        t = time()
        brightfield_percentage = 80
        if back_even is None:
            back_even = np.percentile(stack[0:min(200, n_frames):2, :, :], brightfield_percentage, axis=0)
            back_odd = np.percentile(stack[1:min(200, n_frames):2, :, :], brightfield_percentage, axis=0)
        stack[0::2, :, :] = np.divide(stack[0::2, :, :], back_even, out=np.zeros_like(stack[0::2, :, :]), where=back_even != 0)
        stack[1::2, :, :] = np.divide(stack[1::2, :, :], back_odd, out=np.zeros_like(stack[1::2, :, :]), where=back_odd != 0)
        stack_median = np.median(stack[0:min(10, n_frames), :, :].ravel())
        stack -= stack_median
        if not is_darkfield:  # TODO: proper darkfield correction switch
            stack *= -1

        """
        Roughly estimate the jet location
        *  Look at droplet variance in half the image.  Fit a line.
        """
        stack0_std_p0 = np.sum(stack0_std, axis=0)
        stack0_std_p0 -= np.median(stack0_std_p0)
        jet_pos = (np.where(stack0_std_p0 == np.max(stack0_std_p0))[0])[0]
        jet_fwhm = len(np.where(stack0_std_p0 > np.max(stack0_std_p0)/2.0)[0])
        stack0_std_mask = np.ones_like(stack0_std)
        stack0_std_mask[int(nx/2):, :] = 0
        stack0_std_mask[:, 0:50] = 0
        stack0_std_mask[:, ny-50:ny] = 0
        sigthresh, _, _, _, stack0_std_mask = self._get_thresh(stack0_std, 
                                                               n_fwhm=3, 
                                                               mask=stack0_std_mask)
        slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = self._fit_jet(stack0_std*stack0_std_mask, 
                                                                       sigthresh, x, y)
        jet_width = len(p2[p2 > np.max(p2)/2])*2
        jet_pos = int(yint + slope*nx/2)
        jetx = np.array([0, nx])
        jety = yint + slope*jetx
        jetmask = np.ones_like(stack0_std)  # This mask is a narrow strip of ones that blocks out the jet
        jetmask[y -  yint - slope*x + 2*jet_width < 0] = 0
        jetmask[y -  yint - slope*x - 2*jet_width > 0] = 0
        # Now isolate the region with the jet and repeat the above to improve our estimate of the jet region.
        slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = self._fit_jet(stack0_std*stack0_std_mask*jetmask, 
                                                                       sigthresh, x, y)
        jet_width = len(p2[p2 > np.max(p2)/2])*2
        jet_pos = int(yint + slope*nx/2)
        jetx = np.array([0, nx])
        jety = yint + slope*jetx
        jetmask = np.ones_like(stack0_std)
        jetmask[y -  yint - slope*x + 2*jet_width < 0] = 0
        jetmask[y -  yint - slope*x - 2*jet_width > 0] = 0
        print(f"jet_width = {jet_width}") # FIX ME: Add to h5 results!

        """
        Attempt to find the nozzle tip location
        *  Empirically, I notice the standard deviation 
           of the brightfield-corrected image is high 
           where the nozzle is (why?).
        """
        t = time()
        jetmask2 = np.ones_like(stack0_std)  # This mask is two narrow strips of ones that are above and below the jet
        jetmask2[y -  yint - slope*x + 6*jet_width < 0] = 0
        jetmask2[y -  yint - slope*x - 6*jet_width > 0] = 0
        jetmask2 *= jetmask
        stack_std = np.std(stack[0:min(500, n_frames), :, :], axis=0)
        m = stack_std*jetmask2
        stack_std_p1 = (np.sum(m, axis=1).T - np.median(m, axis=1)).T
        stack_std_p1 -= np.median(stack_std_p1)
        stack_std_p1 = savgol_filter(stack_std_p1, 11, 3)
        stack_std_g1 = np.gradient(stack_std_p1)
        if nozzle_pos is None:
            g = stack_std_g1.copy()
            g[0:int(g.size/2)] = 0
            nozzle_pos = (np.where(g == np.max(g))[0])[0]
        nozzle_pos = min(nozzle_pos, nx)
        if nozzle_pos < nx/2:
            nozzle_pos = nx
        """
        Attempt to find jet/drip transition location
        *  Look for the decrease in the droplet variance 
           along the length of the jet.
        """
        m = stack0_std*jetmask
        stack0_std_p1 = np.sum((m.T - np.median(m, axis=1)).T, axis=1)
        stack0_std_p1 = medfilt(stack0_std_p1, 101)
        stack0_std_p1 = savgol_filter(stack0_std_p1, 101, 3)
        stack0_std_g1 = np.gradient(stack0_std_p1)
        if drop_pos is None:
            a = stack0_std_g1[50:nozzle_pos-50]
            drop_pos = (np.where(a == np.min(a))[0])[0] + 50
        drop_pos = min(drop_pos, nx)


        # Configure droplet crop region
        if jet_crop is None:
            wid = max(jet_width*2 + np.abs(jety[0]-jety[1]), jet_width*10)
            pos = (0, jet_pos - wid/2)
            size = (drop_pos, wid)
            jet_crop = (max(int(pos[0]), 0), min(int(pos[0] + size[0]), nx - 1),
                        max(int(pos[1]), 0), min(int(pos[1] + size[1]), ny - 1))
        else:
            pos = (jet_crop[0], jet_crop[2])
            size = (jet_crop[1]-jet_crop[0], jet_crop[3]-jet_crop[2])

        """
        Attempt to guess the droplet threshold.
        *  As we increase the threshold, the is a 
           sudden drop in the number of pixels above 
           threshold once we rise above the background 
           level.  We try to identify the jump by 
           taking the derivative of the number of lit 
           pixels as a function of the threshold level.
        """
        im = stack[0:min(10, stack.shape[0]), 
                        jet_crop[0]:jet_crop[1], 
                        jet_crop[2]:jet_crop[3]]
        droplet_threshold, cnt_grad, thrsh, cnts, mask = self._get_thresh(im, thresh_sigma)
        # Make sure there is at least half a single row of lit pixels
        crpim = stack[:, jet_crop[0]:jet_crop[1], jet_crop[2]:jet_crop[3]]
        frac = 100*(1-crpim.shape[1]/float(crpim.size))
        upper = np.percentile(crpim, frac)
        if droplet_threshold is None:
            droplet_threshold = min(droplet_threshold, upper)

        if display:
            imwin_thresh = ImWin(stack, 
                                 title="Choose threshold %s" % file_name, 
                                 levels=(droplet_threshold, droplet_threshold * 1.1))
            roi = pg.ROI(pos=pos, size=size)
            roi.addScaleHandle([1, 1], [0, 0])
            roi.addScaleHandle([1, 0], [1, 0])
            roi.addScaleHandle([1, 0], [0, 1])
            roi.addScaleHandle([0, 1], [1, 0])
            imwin_thresh.getView().addItem(roi)
            pltmag = 0.1 * min(nx, ny)
            im = stack0_std
            imwin_stack0_std = ImWin(im, title='Stack Standard Deviation %s' % file_name, levels=(-0.015, 0.3))
            imwin_stack0_std.add_plot(jetx, jety+jet_width, pen='y')
            imwin_stack0_std.add_plot(jetx, jety-jet_width, pen='y')
            drop_line = imwin_stack0_std.add_line(position=drop_pos, movable=True, pen='r')
            imwin_stack0_std.add_plot(-pltmag * stack0_std_g1/ np.max(stack0_std_g1), pen='r')
            imwin_stack0_std.add_plot(-pltmag * stack_std_g1 / np.max(stack_std_g1), pen='g')
            nozzle_line = imwin_stack0_std.add_line(position=nozzle_pos, movable=True, pen='g')
            pg.QtGui.QApplication.processEvents()
            if interactive:
                print("""
                        ========================================================================================
                        \n
                        This is INTERACTIVE MODE.  Here's how to interact with the two windows that are open:")
                        * Drag vertical green line to the nozzle tip.')
                        * Drag vertical red line to the jet breakup point.')
                        * Modify the rectangular (ROI) so that it enclouses the droplet region.")
                        * Press 'r' to abort this dataset but continue to the next.
                        \n
                        ========================================================================================
                        """)
                pg.QtGui.QApplication.instance().exec_()
                if imwin_thresh.reject or imwin_stack0_std.reject:
                    pg.QtGui.QApplication.instance().closeAllWindows()
                    print('Dataset rejected.  Moving to next dataset.')
                    return None
            nozzle_pos = nozzle_line.getPos()[0]
            drop_pos = drop_line.getPos()[0]
            # jet_pos = jet_line.getPos()[0]
            droplet_threshold = imwin_thresh.getLevels()[0]
            jet_crop = (max(int(roi.pos()[0]), 0), min(int(roi.pos()[0] + roi.size()[0]), nx - 1),
                    max(int(roi.pos()[1]), 0), min(int(roi.pos()[1]+roi.size()[1]), ny-1))

        # print(f"nozzle_pos = {nozzle_pos}")
        # print(f"jet_pos = {jet_pos}")
        # print(f"drop_pos = {drop_pos}")
        # print(f"droplet_threshold = {droplet_threshold}")
        # print(f"jet_crop = %d:%d,%d:%d" % jet_crop)


        # Mask the region outside of the ROI, for jet line fitting
        mask = np.zeros((nx, ny))
        mask[jet_crop[0]:jet_crop[1], jet_crop[2]:jet_crop[3]] = 1
        # Also mask any pixels that are consistently above threshold (say, 80% of the time)
        above = np.sum(stack > droplet_threshold, axis=0)/float(stack.shape[0])
        mask[above > 0.80] = 0
        # pg.image(mask, title='mask')

        # Prepare for viewing the line fits
        if display:
            imwin_jetline = ImWin(np.zeros((nx, ny)), title='Jet line fit %s' % file_name)
            imwin_jetline.set_mask(mask)
            imwin_jetline.getView().addItem(pg.ROI(pos=(jet_crop[0], jet_crop[2]),
                                               size=(jet_crop[1] - jet_crop[0], jet_crop[3] - jet_crop[2])))
            line_plt = imwin_jetline.getView().plot()
            imwin_jetline.setLevels(0, max(droplet_threshold * 2, imwin_jetline.getLevels()[1]))

        # Here comes the creation of the average outer product
        cc_matrix = np.zeros((nx, nx))
        prev_jet1d = None
        jet_angles = np.zeros(n_frames)
        jet_widths = np.zeros(n_frames)
        pmsg = ''
        for i in range(n_frames):
            image = stack[i, :, :]
            slope, yint, v1, v2, p1, p2, xf, yf, ang, fail = self._fit_jet(image * mask, droplet_threshold, x, y)
            if fail:
                msg = 'Line fit failed on frame %d' % (i,)
                sys.stdout.write('\b' * len(pmsg) + msg)
                pmsg = msg
                prev_jet1d = None  # If we're going to throw out frames, we need to account for that below.
                continue
            jet1d = p1
            jet_angles[i] = ang
            jet_widths[i] = np.sqrt(np.mean(v2**2))
            if i > 0:
                if prev_jet1d is not None:
                    op = np.outer(jet1d, prev_jet1d)
                    # Add on odds, subtract on evens.  One of these two provides an estimate of background correlations
                    # that have no relation to droplet translations.  I don't know which is which; deal with that later.
                    if i % 2 == 1:
                        cc_matrix += op
                    else:
                        cc_matrix -= op
                if (i % 10 == 0) and display:
                    line_plt.setData(xf, yf, pen='g')
                    dispim = np.zeros_like(image)
                    dispim[image > droplet_threshold] = 1
                    imwin_jetline.setImage(dispim, autoLevels=False)
                    pg.QtGui.QApplication.processEvents()
            prev_jet1d = jet1d
            msg = '%6.1f' % (np.std(v2),)
            sys.stdout.write('\b' * len(pmsg) + msg)
            pmsg = msg
        sys.stdout.write('\b' * len(pmsg))

        if display:  # Always hold open the final correlation matrix.  Press any key to kill all windows.
            pg.QtGui.QApplication.processEvents()

        # Now identify the offset of the off-diagonal correlation peak
        cc_matrix_mask = np.zeros_like(cc_matrix)
        cc_matrix_mask[jet_crop[0]:jet_crop[1], jet_crop[0]:jet_crop[1]] = 1
        cc_matrix *= cc_matrix_mask
        cc_matrix_projection = np.mean(_shear(cc_matrix), axis=0)
        # We don't know if the first or second frame is the one that is delayed in time.  Flip the sign if necessary.
        sgn = np.sign(np.max(cc_matrix_projection) + np.min(cc_matrix_projection))
        cc_matrix_projection *= sgn
        cc_matrix *= sgn
        # This step gets rid of some systematic column- and row-wise correlations
        med0 = np.median(cc_matrix[jet_crop[0]:jet_crop[1], :], axis=0)
        med1 = np.median(cc_matrix[:, jet_crop[0]:jet_crop[1]], axis=1)
        cc_matrix -= med0
        cc_matrix = (cc_matrix.T - med1).T
        cc_matrix *= cc_matrix_mask
        cc_matrix_projection = np.mean(_shear(cc_matrix), axis=0)

        #=================================================================
        # Try to find the peak in the cross correlation matrix
        #  * Needs proper centroiding
        #  * Even better, we should fit a curved line to get acceleration
        #=================================================================
        w = (np.where(cc_matrix_projection == np.max(cc_matrix_projection))[0])[0]
        ccm = cc_matrix
        n = ccm.shape[0]
        ccp = cc_matrix_projection
        rollby = int(n/2-w)
        ccpr = np.roll(ccp, rollby)


        slc = np.s_[max(0, w-5):min(w + 5, len(cc_matrix_projection))]
        droplet_translation = np.sum(np.arange(len(cc_matrix_projection))[slc] * cc_matrix_projection[slc]) / np.sum(cc_matrix_projection[slc])
        # print('Droplet translation: %d (%5.1f)' % (w, droplet_translation))
        if display:
            #FIXME: Add print statement that describes key presses
            imwin_ccmat = ImWin(ccm, title='Correlation matrix %s' % file_name)
            imwin_ccmat.setLevels(0, imwin_ccmat.getLevels()[1])
            imwin_ccmat.add_plot(-100 * ccp / np.max(ccp) - 50, np.arange(n), pen='g')
            imwin_ccmat.add_plot(np.arange(n), -100 * ccpr/np.max(ccpr)+50+n, pen='g')
            # imwin_ccmat.add_plot(np.arange(n), -100 * med1/np.max(med1) - 50, pen='r')
            # imwin_ccmat.add_plot(-100 * med0/np.max(med0) + 50 + n, np.arange(n), pen='r')
            imwin_ccmat.add_plot(np.arange(n-w), t+np.arange(n-w), pen='y')
            imwin_ccmat.add_plot(np.arange(n), np.arange(n), pen='w')
            pg.QtGui.QApplication.processEvents()
            pg.QtGui.QApplication.instance().exec_()
            if interactive:
                print('')
                print('Check that there is a clear streak in the correlation matrix.')
                print('')
            if imwin_ccmat.kill:
                # "q" pushed
                # 0 => throw dataset away
                print("dead.")
                sys.exit()
                return 0
            if imwin_ccmat.quit:
                # "q" pushed
                # 0 => throw dataset away
                pg.QtGui.QApplication.instance().closeAllWindows()
                print("Dataset rejected, throwing away.")
                return 0
            if imwin_ccmat.reject:
                # "r" pushed
                # 1 => restart analysis with interactive
                pg.QtGui.QApplication.instance().closeAllWindows()
                print("Dataset rejected, restarting analysis.")
                return 1

        output = {"file_name": file_name,
                  "jet_crop": jet_crop,
                  "droplet_threshold": droplet_threshold,
                  "cc_matrix": cc_matrix,
                  "cc_matrix_projection": cc_matrix_projection,
                  "droplet_translation": droplet_translation,
                  "back_even": back_even,
                  "back_odd": back_odd,
                  "jet_width": jet_width
                    }


        if display:
            try:
                pg.mkQApp().closeAllWindows()  # exit()
            except:
                pass

        print(type(output))
        return output


    def analyze_jet(self, file, h5_path, previous_results="previous_results.txt", save_data=True, interactive=False):

        if os.path.exists(previous_results):
            reuse = True
        else:
            f = open("previous_results.txt","w+")
            f.write(h5_path)
            f.close()
            reuse = False

        stack, stack_transpose, meta_data = self.load_data(file)
        image_data = self.image_analysis(file, stack, meta_data, reuse=reuse, interactive=interactive)
        if image_data == 0:
            # "q" pushed
            print(f"""\n
                    ========================================================
                    \n
                    \t\tBad data set, saving meta data only!
                    \n
                    ========================================================
                    \n
                    """)
            meta_data["data_quality"] = "failed"
            print(f"""\n
                    ========================================================
                    \n
                    \t\tDataset rejected, restarting with interactive
                    \n
                    ========================================================
                    \n
                    """)            
            self.save_data_as_h5(meta_data, h5_path, group_name="meta_data", verbose=False)

        elif image_data == 1:
            # "r" pushed
            self.analyze_jet(file, h5_path, interactive=True)

        else:
            # space bar pushed
            # merge datasets
            print(meta_data)
            # print("\n\n")
            # print(image_data["droplet_translation"])
            all_data = {**image_data, **meta_data, 
                        "jet_speed": image_data["droplet_translation"]*(meta_data["pixel_size"]*1e-6)/(meta_data["delay"]*1e-9),
                        "data_quality": "passed"}
            print(f"""\n
                    ========================================================
                    \n
                    \t\tGas flow rate = {all_data["gas_flow"]} mg/min
                    \n
                    \t\tLiquid flow rate = {all_data["liquid_flow"]} ul/min
                    \n
                    \t\tJet speed = {all_data["jet_speed"]} m/s
                    \n
                    ========================================================
                    \n
                    """)
            if save_data == True:
                print(f"\n\t Saving analysis in {h5_path}...\n\n")
                self.save_data_as_h5(all_data, h5_path, group_name="jet_analysis", verbose=False)
                f = open("previous_results.txt","w+")
                f.write(h5_path)
                f.close()
            else:
                return all_data



    def start_live_analysis(self):
        # might be weird with the q & space user inputs from jet_analyzer check this later.
        if len(self.files) == 0:
            t = self.wait_time
            print(f"No new files, waiting {t} seconds.")
            sleep(t)
        else:
            for f in self.files:
                print(f"\n\n\t Analyzing {f}...\n\n")
                # define the h5 path where we save the data
                results_path = os.path.join(savepath, f[-6:-3] + "_analysis.h5")
                # run analyze_jet on this file, save results 
                analysis = self.analyze_jet(f, results_path, save_data=True)
        self.files = self.update_files_in_datapath()  # watch for new files
        self.start_live_analysis()

#FIXME: add debug options, see reborn.utils, get_caller()




class ImWin(pg.ImageView):
    reject = False
    quit = False
    kill = False

    def __init__(self, *args, **kwargs):
        self.app = pg.mkQApp()
        self.win = pg.QtGui.QMainWindow()
        self.win.resize(1000, 800)
        if "title" in kwargs:
            self.win.setWindowTitle(kwargs["title"])
            del kwargs["title"]
        pg.ImageView.__init__(self, 
                              self.win,
                              view=pg.PlotItem())
        self.setImage(*args, **kwargs)
        self.win.setCentralWidget(self)
        for m in ["resize"]:
            setattr(self, m, getattr(self.win, m))
        self.setPredefinedGradient("flame")
        self.win.show()

    def add_plot(self, *args, **kwargs):
        return self.getView().plot(*args, **kwargs)

    def add_line(self, *args, **kwargs):
        p = kwargs["position"]
        del kwargs["position"]
        line = pg.InfiniteLine(*args, **kwargs)
        line.setPos([p,p])
        self.getView().addItem(line)
        return line

    def keyPressEvent(self, ev):
        if ev.key() == 82: # r button
            print("'r' pushed, reject triggered")
            self.reject = True
            self.app.quit()
        if ev.key() == 32: # spacebar
            print("spacebar pushed, continuing to next dataset")
            self.app.quit()
        if ev.key() == 81: # q button
            print("'q' pushed, quitting program")
            self.quit = True
            self.app.quit()
        if ev.key() == 75:
            print("killing program")
            self.app.closeAllWindows()
            self.app.quit()
            self.kill = True
            del(self.app)

    def set_mask(self, mask, color=None):
        d = mask
        if color is None:
            color = (255, 255, 255, 20)
        mask_rgba = np.zeros((d.shape[0], d.shape[1], 4))
        r = np.zeros_like(d)
        r[d == 0] = color[0]
        g = np.zeros_like(d)
        g[d == 0] = color[1]
        b = np.zeros_like(d)
        b[d == 0] = color[2]
        t = np.zeros_like(d)
        t[d == 0] = color[3]
        mask_rgba[:, :, 0] = r
        mask_rgba[:, :, 1] = g
        mask_rgba[:, :, 2] = b
        mask_rgba[:, :, 3] = t
        im = pg.ImageItem(mask_rgba)
        self.getView().addItem(im)




if __name__ == "__main__":
    # "/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/logs"
    base_directory = "/home/kkarpos/rawdata_temp/microjets/581_64"
    # base_directory = "/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/20200625/500_79"

    datapath = os.path.join(base_directory)
    savepath = os.path.join(base_directory, "analyzed_data")
    os.makedirs(savepath, exist_ok=True)
    logpath = ""
    # logpath = "/bioxfel/data/kirianlab/asu-backup/xfel1/data/rawdata/microjets/2020/logs"
    wait_time = 10 # seconds
    analyzer = JetAnalysis(datapath=datapath, savepath=savepath, 
                            logpath=logpath, wait_time=wait_time)
    analyzer.start_live_analysis()
