import sys
import os
import glob

import numpy as np
import pylab as plt
import pandas as pd
import imageio 
import h5py
import json
import scipy
from skimage import filters, morphology, measure, feature

import pyqtgraph as pg

import filters_playground as filt
from regionprops import JetRegionProps

import utils



class JetAnalysis():

    r"""A class to analyze data from the Kirian Lab microjet testing station
    """


    # Image stack properties
    _image_stack_raw = None
    _image_metadata = None
    _n_frames = None
    _image_shape = None

    # Stack cleanup
    _stack_filtered = None
    _stack_thresholded = None
    _stack_processed = None
    _droplet_stack_labeled = None


    # droplet, jet, and nozzle regions
    _nozzle_tip_position = None
    _stack_nozzle_cropped = None
    _nozzle_region = None
    _jet_region_idx = None
    _jet_region = None
    _droplet_region = None

    # calculated properties
    _jet_length = None
    _jet_diameter = None
    _jet_speed = None
    _jet_speed_err = None
    _correlation_matrix = None
    _correlation_projection = None
    _drop_dispersion_list = None
    _drop_dispersion_avg = None
    _drop_dispersion_err = None

    # To Do: Move all inputs to a general config, add a validation function
    def __init__(self, image_stack, image_metadata, 
                    filter_method:str='sobel', threshold_method:str='otsu',
                    remove_small_objects:int=5, binary_closing_iterations:int=5,
                    region_properties=None, crop_nozzle_tip:bool=True):

        # =============================
        # User defined properties
        # =============================

        self._image_stack_raw = image_stack
        self._image_metadata = image_metadata
        self.filter_method = filter_method
        self.threshold_method = threshold_method

        # =============================
        # Extracted Stack Information
        # =============================

        # return the number of frames in the video
        self._n_frames = np.shape(self._image_stack_raw)[0] 
        self._image_shape = np.shape(self._image_stack_raw)[1:3] # should return (1024, 512), or whatever your resolution is

        self._check_shapes() # ensure the image stack is in the correct orientation

        # =============================
        # Stack Cleanup
        # =============================

        # filter out the background using an edge detection algorithm
        self._stack_filtered = self._filter_stack(self._image_stack_raw, filter_method=self.filter_method)
        # threshold the image 
        self._stack_thresholded = self._threshold_stack(self._stack_filtered, threshold_method=self.threshold_method)
        # make it binary and clean it up
        self._stack_processed = self._apply_processing_methods(self._stack_thresholded, 
                                                                remove_small_objects=remove_small_objects, 
                                                                binary_closing_iterations=binary_closing_iterations)
        # remove the nozzle tip
        self._nozzle_tip_position = self._find_stack_nozzle_tip(self._stack_processed)
        self._stack_nozzle_cropped = self._remove_nozzle_tip()
        self._nozzle_region = self._get_nozzle_image()
        # remove the jet region (want droplets only)
        self._jet_region_idx = self._find_stack_jet_region(self._stack_nozzle_cropped)
        self._jet_region = self._get_jet_image()
        self._droplet_region = self._remove_jet_region()

        # label the droplet stack
        self._droplet_stack_labeled = self._label_stack(self._droplet_region)


        # =============================
        # Calculated Properties
        # =============================

        self._jet_length = self._get_jet_length()

        self._stack_regions_info = self._get_regions_info()

        # calculate correlation matrix and the diagonal projection
        self._correlation_matrix = self._get_correlation_matrix()
        self._correlation_projection = self._get_correlation_projection()

        # calculate droplet translation in terms of pixels
        self._droplet_translation = self._get_droplet_translaton()

        # get calculated jet parameters
        self._jet_speed = self._get_jet_speed()
        self._jet_speed_err = self._get_jet_speed_err()
        self._jet_diameter = self._get_jet_diameter()

        # droplet dispersion stuff
        dd = self._get_droplet_dispersion()
        self._drop_dispersion_list = dd[0]
        self._drop_dispersion_avg = dd[1]
        self._drop_dispersion_err = dd[2]



    # =============================
    # Class Properties
    # =============================

    # ----------------------------------------------------------
    # extracted information from user-inputted image stack
    # ----------------------------------------------------------

    @property
    def image_stack_raw(self):
        return self._image_stack_raw

    @property
    def image_metadata(self):
        return self._image_metadata

    @property
    def n_frames(self):
        return self._n_frames

    @property
    def image_shape(self):
        return self._image_shape

    # ----------------------------------------------------------
    # stack cleanup
    # ----------------------------------------------------------

    @property
    def stack_filtered(self):
        return self._stack_filtered

    @property
    def stack_thresholded(self):
        return self._stack_thresholded

    @property
    def stack_processed(self):
        return self._stack_processed

    @property
    def stack_droplets_labeled(self):
        return self._droplet_stack_labeled

    # ----------------------------------------------------------
    # cropping different regions
    # ----------------------------------------------------------

    @property
    def nozzle_tip_position(self):
        return self._nozzle_tip_position

    @property
    def jet_region_position(self):
        return self._jet_region_idx

    @property
    def droplets_image(self):
        return self._droplet_region

    @property
    def jet_image(self):
        return self._jet_region

    @property
    def nozzle_image(self):
        return self._nozzle_region

    # ----------------------------------------------------------
    # calculated properties
    # ----------------------------------------------------------

    @property
    def jet_length(self):
        return self._jet_length

    @property
    def jet_speed(self):
        return self._jet_speed

    @property
    def jet_speed_err(self):
        return self._jet_speed_err

    @property
    def correlation_matrix(self):
        return self._correlation_matrix

    @property
    def correlation_projection(self):
        return self._correlation_projection

    @property
    def jet_speed(self):
        return self._jet_speed

    @property
    def jet_diameter(self):
        return self._jet_diameter

    @property
    def droplet_dispersion_list(self):
        return self._drop_dispersion_list

    @property
    def droplet_dispersion_avg(self):
        return self._drop_dispersion_avg

    @property
    def droplet_dispersion_err(self):
        return self._drop_dispersion_err

    # =============================
    # Class Methods
    # =============================

    def _check_shapes(self):
        r"""
            Not sure why, but sometimes the image shapes read as 512 x 1024
            rather than 1024 x 512. This code is built around the latter shape...
        """
        x, y = self._image_shape
        imst = self._image_stack_raw
        if x < y:
            print("Need to fix the shapes! Swaping axis 1 and 2.")
            imnew = np.swapaxes(imst, 1, 2)
            self._image_stack_raw = imnew
            self._image_shape = np.shape(self._image_stack_raw)[1:3]


    # ----------------------------------------------------------
    # stack cleanup
    # ----------------------------------------------------------

    def _filter_stack(self, stack, filter_method:str='sobel'):
        r""" Detects edges and filters background
            
            Arguments:
                stack: The images that make up your movie. This will also work on a single image.
                erode (bool): Will perform a morphological erosion step to further filter the background

            Returns:
                a filtered stack of the same shape as the input stack
        """
        if filter_method == 'sobel':
            stack = [filters.sobel(s) for s in stack]
        else:
            raise KeyError("filter_method can only handle 'sobel' for now. Other algrothims will be added eventually.")
        return np.array(stack)

    def _threshold_stack(self, stack, threshold_method:str='otsu'):
        r""" Will threshold an image stack using the provided method
            
            Arguments:
                stack (array): the image stack, np.ndarray or not
                threshold_method (str): the thresholding method to use
                    TO DO: I don't want to waste time adding other methods in for now, 
                            update this to either take a user-defined method, or call in 
                            in a different method (with proper parameters) from the
                            filters_playground.py script. 

            Returns:
                (ndarray): thresholded image stack
        """
        if threshold_method != 'otsu':
            raise ValueError("Only 'otsu' is implemented for now.")
            # To Do: add in options to switch between other thresholding methods
        thresh_stack = filt.threshold_otsu(stack)
        return np.array(thresh_stack)

    def _apply_processing_methods(self, stack, 
                                    remove_small_objects:int=10, 
                                    binary_closing_iterations:int=3,
                                    footprint='default'):
        r"""Apply morphology methods to the stack, gathering a host of connected component parameters

            Arguments:
                stack: Your image stack, it should be filtered and thresholded before running this function
                remove_small_objects (int): 
        """
        if footprint == 'default':
            footprint = [[0,0,0,0],
                         [0,0,0,0],
                         [1,1,1,1],
                         [0,0,0,0],
                         [0,0,0,0]]
        # first remove all the small activated pixels
        sclean = np.array([morphology.remove_small_objects(s, remove_small_objects) for s in self.stack_thresholded])
        # apply a binary closing method to fill in droplets
        sdil = np.array([scipy.ndimage.binary_closing(s, structure=footprint, 
                                                        iterations=binary_closing_iterations) for s in sclean])
        return sdil 

    # ----------------------------------------------------------
    # methods to split jet into droplet, jet, and nozzle regions
    # ----------------------------------------------------------

    def _remove_nozzle_tip(self):
        r"""Will remove the nozzle tip from a stack of images

            Arguments:
                stack: your processed image

            Returns:
                the cropped image stack
        """
        stack = self._stack_processed.copy()
        ntip_idx = self._nozzle_tip_position
        for i, idx in enumerate(ntip_idx):
            stack[i,int(idx):,:] = 0
        return stack

    def _find_stack_nozzle_tip(self, stack):
        ntip_idx = np.zeros(len(stack), dtype=int)
        lim = [0.02, 0.05, 0.07, 0.1]
        # sometimes the threshold for the nozzle tip finder doesn't work at low numbers
        # this for loop tries the lowest threshold value first
        # if it doesn't work, it'll increase the value until it works
        # idk what will happen if it reaches 0.1, it'll probably crash.
        # FIX ME: Revisit this and make it more robust...
        for i, im in enumerate(stack):
            for j, m in enumerate(lim):
                try:
                    ntip_idx[i] = self._find_nozzle_tip_position(im=im, lim=lim[j])
                except:
                    pass

        # if the max index value above is less than, say, half the image
        # that's indicative of no nozzle tip (i.e., the code thinks the 
        # jet end is the nozzle tip)
        # the following code checks for this and attempts to fix it
        idmax = np.max(ntip_idx)
        if idmax < self._image_shape[1] * 0.6:
            ntip_idx = np.array([self._image_shape[1] for i in range(len(stack))], dtype=int)
        # if the max is not less than that value, the nozzle tip might be 
        # close enough to the edge to confuse the code
        else:
            for i, idx in enumerate(ntip_idx):
                if idx < np.max(ntip_idx)*0.9:
                    ntip_idx[i] = idmax
        return ntip_idx

    def _find_nozzle_tip_position(self, im, lim:float=0.02):
        x = np.sum(im, axis=0)
        xi = np.where(x < lim*np.max(x))
        a = np.sum(im[:,xi[0]], axis=1)
        ntip_idx = self._find_longest_nonzero_subset(a)#[0]
        return ntip_idx[0]

    def _remove_jet_region(self):
        r"""Will remove the jetfrom a stack of images

            Arguments:
                stack: your processed image

            Returns:
                the cropped image stack
        """
        stack = self._stack_nozzle_cropped.copy()
        jet_idx = self._jet_region_idx
        for i, idx in enumerate(jet_idx):
            stack[i,int(idx[0]):,:] = 0
        cidx = self._cleanup_droplet_region(stack)
        return stack[:,:,cidx]

    def _cleanup_droplet_region(self, stack):
        s = np.sum(stack, axis=0)
        sx = np.sum(s, axis=0)
        sxl = self._find_longest_nonzero_subset(im=sx)
        return sxl

    def _find_stack_jet_region(self, stack):
        jet_idx = np.zeros((len(stack),4), dtype=int)
        for i, im in enumerate(stack):
            x0, x1, y0, y1 = self._find_jet_region(im=im)
            jet_idx[i] = x0, x1, y0, y1
        return jet_idx

    def _find_jet_region(self, im):
        r""" Locates the jet in an image stack

            Arguments:
                stack: your processed image

            Returns:
                the indices associated with the jet
        """
        x = np.sum(im, axis=1)
        jet_idx = self._find_longest_nonzero_subset(x)
        y = np.sum(im, axis=0)
        jet_idy = self._find_longest_nonzero_subset(y)
        return jet_idx[0], jet_idx[-1], jet_idy[0], jet_idy[-1]

    def _find_longest_nonzero_subset(self, im):

        max_subset = []
        current_max_subset = []
        for i, n in enumerate(im):
            if n > 0:
                current_max_subset.append(i)
                if len(current_max_subset) > len(max_subset):
                    max_subset = current_max_subset
            else:
                current_max_subset = []
        return max_subset

    def _get_jet_image(self):
        stack = self._stack_processed.copy()
        jr = self._jet_region_idx
        for i, im in enumerate(stack):
            stack[i,:jr[i][0],:] = 0
            stack[i,jr[i][1]:,:] = 0
            stack[i,:,:jr[i][2]] = 0
            stack[i,:,jr[i][3]:] = 0
        return stack

    def _get_nozzle_image(self):
        stack = self._stack_processed.copy()
        nr = self._nozzle_tip_position
        for i, im in enumerate(stack):
            stack[i,:nr[i],:] = 0
        return stack

    # ----------------------------------------------------------
    # jet speed methods
    # ----------------------------------------------------------

    def _label_stack(self, stack):
        r""" Labels an image stack from use with the regionprops stuff

            Returns:
                (ndarray): the labeled image stack
        """
        return np.array([measure.label(sf, background=0) for sf in stack])

    def _get_regions_info(self):
        reg_info = {}
        for i, r in enumerate(self._droplet_stack_labeled):
            try:
                jrp = JetRegionProps(r)
                reg_info[i] = jrp.frame_info
            except TypeError:
                print(f"RegionProps failed on frame {i}")
        return reg_info

    def _get_correlation_matrix(self):
        r""" Calculates a correlation matrix to get jet length
        """
        cc_mat = np.zeros((self._image_shape[0], self._image_shape[0]))
        p1i = self._get_p1_stack()
        p1_prev = None
        for i, im in enumerate(p1i):
            p1 = p1i[i]
            if p1_prev is not None:
                op = np.outer(p1, p1_prev)
                if i % 2 == 1:
                    cc_mat -= op
                else:
                    cc_mat += op
            p1_prev = p1
        return cc_mat

    def _shear(self, matrix):
        r"""
            A special matrix shear operation for analyzing
            the jet cross correlations
        """
        arr = np.empty_like(matrix)
        fast_scan, slow_scan = matrix.shape
        for i in range(slow_scan):
            for j in range(fast_scan):
                arr[i, j] = matrix[i, (j + i) % fast_scan]
        return arr

    def _get_correlation_projection(self):
        ccmp = np.mean(self._shear(self._correlation_matrix), axis=0)
        w = np.where(ccmp == np.max(ccmp))[0][0]
        slc = np.s_[max(0, w - 5):min(w + 5, len(ccmp))]
        return ccmp, slc

    def _get_p1_stack(self):
        region = self._stack_regions_info
        try:
            reg = [region[i]['droplet_fit_line']['p1'].tolist() for i,r in enumerate(region)]
        except:
            reg = []
            for i,r in enumerate(region):
                try:
                    reg.append(region[i]['droplet_fit_line']['p1'].tolist())
                except:
                    print(f"Frame {i} failed...")
                    if i == 0:
                        raise ValueError("Something is wrong with the droplet fitting...")
                    reg.append(region[i-1]['droplet_fit_line']['p1'].tolist())
        return reg

    def _get_droplet_translaton(self):
        ccmp, slc = self._correlation_projection
        num = np.sum(np.arange(len(ccmp))[slc] * ccmp[slc])
        den = np.sum(ccmp[slc])
        return num / den


    # ----------------------------------------------------------
    # Calculated jet properties
    # ----------------------------------------------------------

    def _get_jet_length(self):
        jetlen_list = [props[1]-props[0] for props in self._jet_region_idx]
        avg = np.mean(jetlen_list) * self._image_metadata['pixel_size'] * 1e-6
        err = np.std(jetlen_list) * self._image_metadata['pixel_size'] * 1e-6
        return avg, err

    def _get_jet_speed(self):
        speed = self._droplet_translation * self._image_metadata['pixel_size'] * 1e-6 / (self._image_metadata['delay'] * 1e-9)
        return speed

    def _get_jet_speed_err(self):
        ccmp = self._correlation_projection[0]
        noise = np.median(np.abs(ccmp[1:-2] - ccmp[2:-1]))
        return noise

    def _get_jet_diameter(self):
        alpha = 1e-9 / 60 # ul/min to m^3 / sec scale factor
        a = (self._image_metadata['liquid_flow']*alpha)/(np.pi * self._jet_speed) # units = m^2
        jet_diameter = 2*np.sqrt(a)  # meters
        return jet_diameter


    # ----------------------------------------------------------
    # droplet dispersion methods
    # ----------------------------------------------------------

    def _fit_jet(self):
        x, y = np.meshgrid(np.arange(self._image_shape[0]), np.arange(self._image_shape[1]), indexing='ij')
        fit_params = np.zeros((len(self._jet_region),2))
        for i, im in enumerate(self._jet_region):
            wjet = np.where(im.flat == 1)
            xj, yj = x.flat[wjet], y.flat[wjet]
            slope, yint = np.polyfit(xj, yj, 1)
            fit_params[i] = slope, yint
        return fit_params

    def _get_droplet_dispersion(self):
        jet_fit = self._fit_jet()
        drop_centroids = {k:v['droplet_centroids'] for k,v in self._stack_regions_info.items()}
        th_list = []
        for i, im in enumerate(jet_fit):
            x0 = self._jet_region_idx[i,1]
            y0 = jet_fit[i,0]*drop_centroids[i][0,:,0] + jet_fit[i,1]
            dx = drop_centroids[i][0,:,0] - x0 # distance to droplet
            dy = drop_centroids[i][0,:,1] - y0 
            theta = np.arctan(dy/dx)
            th_list.append(theta)
        th_list = np.concatenate(th_list)
        th_list = np.abs(th_list)
        # thidx = np.where(th_list < np.mean(th_list)+2*np.std(th_list))
        return th_list, np.mean(th_list), np.std(th_list)



    # ----------------------------------------------------------
    # Output dictionaries
    # ----------------------------------------------------------

    def _calculated_data_dict(self):
        data = {"jet_length": self._jet_length[0],
                "jet_length_err": self._jet_length[1],
                "jet_diameter": self._jet_diameter, 
                "jet_diameter_err": None, 
                "jet_speed": self._jet_speed,
                "jet_speed_err": self._jet_speed_err, 
                "jet_deviation": None, # the angle of the jet from the nozzle normal
                "jet_deviation_err": None, # the variability in jet deviation across all frames
                "jet_stability": None, # how do we quantify stability? Maybe just stick with the errors above?
                "droplet_dispersion": self._drop_dispersion_avg,
                "droplet_dispersion_err": self._drop_dispersion_err 
                }
        return data

    def to_dict(self):
        data = self._calculated_data_dict()
        data.update(self._image_metadata)
        return data


    # ----------------------------------------------------------
    # h5 output for saving all the analyzed data
    # ----------------------------------------------------------

    def _images_dict(self):
        r""" The image stacks to save in the analysis h5 file
        """
        data = {#"raw_stack": self._image_stack_raw,
                # "filtered_stack": self._stack_filtered,
                # "thresholded_stack": self._stack_thresholded,
                #"processed_stack": self._stack_processed,
                "stack_sdev": self._stack_sdev,
                "correlation_matrix": self._correlation_matrix
                }
        return data

    def _h5_single_group(self, h5, data, group_name):
        r""" A method for saving a single group in an h5 file
        """
        try:
            group = h5.create_group(group_name)
        except ValueError:
            group = h5['{}'.format(group_name)]

        for k, v in data.items():
            if v is None:
                v = False
            f = group_name+'/'+f"{k}"
            h5[f] = v

    def to_h5(self, save_path:str=None):
        r"""Saves all the data to an h5 file
        """

        if save_path is None:
            raise ValueError("save path required!")

        d = os.path.dirname(save_path)
        os.makedirs(d, exist_ok=True)
        try:
            h5 = h5py.File(save_path, 'a')
        except ValueError:
            h5 = h5py.File(save_path, 'w')

        self._h5_single_group(h5=h5, data=self._image_metadata, group_name="metadata")
        self._h5_single_group(h5=h5, data=self._images_dict(), group_name="2D_images")
        self._h5_single_group(h5=h5, data=self._calculated_data_dict(), group_name="calculated_data")
        # self._h5_single_group(h5=h5, data=self._stack_regions_info, group_name="regionprops_info")

        h5.close()



































