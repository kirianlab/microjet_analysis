import sys
import os
import glob

import numpy as np
import pylab as plt
import matplotlib as mpl
import pandas as pd
import imageio 
import h5py
import json
import scipy

import utils

# This class might not need to be a class, I just wanted a way to keep things consistent

class PlotJetData():

    r"""A class to plot the analyzed data from the Kirian Lab microjet testing station

        Things to determine and general questions:

        Notes:
            1) This only works for a single nozzle type - any nozzle comparisons should be custom 
    """

    _stack_data = None
    _nozzle_id = None

    # data lists
    _jet_speeds = None
    _liquid_flow = None
    _gas_flow = None
    _jet_diameter = None

    # make plots
    # _lvg_js = None

    def __init__(self, stack_data, nozzle_id:str=""):

        self._stack_data = stack_data
        self._nozzle_id = nozzle_id

        # split up the data
        self._data_dict_to_lists()



    @property
    def stack_data(self):
        return self._stack_data

    @property
    def nozzle_id(self):
        return self._nozzle_id

    @property
    def jet_speeds(self):
        return self._jet_speeds

    @property
    def liquid_flow(self):
        return self._liquid_flow

    @property
    def gas_flow(self):
        return self._gas_flow

    @property
    def jet_diameter(self):
        return self._jet_diameter




    def _data_dict_to_lists(self):
        r"""Reorganizes data for plotting

            Given the stack_data dictionary, will reorganize the data
            into multiple lists.
        """
        self._jet_speeds = [v['jet_speed'] for k,v in self._stack_data.items()]
        self._liquid_flow = [v['liquid_flow'] for k,v in self._stack_data.items()]
        self._gas_flow = [v['gas_flow'] for k,v in self._stack_data.items()]
        self._jet_diameter = [v['jet_width']*1e6 for k,v in self._stack_data.items()]
        self._jet_length = [v['jet_length']*1e6 for k,v in self._stack_data.items()]



    def config_liq_gas_jetspeed(self):
        r""" Default config for the liquid vs gas jetspeed plot
        """
        plot_config = {
            'title': self._nozzle_id,  # Title of the plot
            'x': self._liquid_flow,
            'x_label': 'Liquid Flow (ul/min)',  # What to label the x coordinate
            'x_range': (0,54),  # Ranges (use None if not sure)
            'y': self._gas_flow,
            'y_label': 'Gas Flow (mg/min)',
            'y_range': (0,60),
            'z': self._jet_speeds,
            'z_label': 'Jet Speed (m/s)',
            'z_range': (0,120),
            'level_step': 5,  # One contour per step
            'marker_size_is_jet_diameter': True
            }
        return plot_config

    def config_liq_gas_jetlength(self):
        r""" Default config for the liquid vs gas jetspeed plot
        """
        plot_config = {
            'title': self._nozzle_id,  # Title of the plot
            'x': self._liquid_flow,
            'x_label': 'Liquid flow (ul/min)',  # What to label the x coordinate
            'x_range': (0,54),  # Ranges (use None if not sure)
            'y': self._gas_flow,
            'y_label': 'Gas flow (mg/min)',
            'y_range': (0,60),
            'z': self._jet_length,
            'z_label': 'Jet Length (um)',
            'z_range': (300,600),
            'level_step': 5,  # One contour per step
            'marker_size_is_jet_diameter': True
            }
        return plot_config

    def _make_contour(self, ranges, data, level_step):
        r""" Prepares a basic contour plot given data

            Returns:

        """

        # Make a contour plot of irregularly spaced data 
        # coordinate via interpolation on a grid.
        n_levels = int((ranges[2][1]-ranges[2][0])/level_step)
        levels = np.linspace(ranges[2][0], ranges[2][1], int(n_levels+1))

        # Create grid values first.
        xi = np.linspace(0, np.max(data[0]), 200)
        yi = np.linspace(0, np.max(data[1]), 200)
        Xi, Yi = np.meshgrid(xi, yi)

        # Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
        interpolator = mpl.tri.LinearTriInterpolator(mpl.tri.Triangulation(data[0], data[1]), data[2])
        zi = interpolator(Xi, Yi)

        return xi, yi, zi, levels

    def _make_phase_diagram(self, ranges, data, contours, levels, 
                            xlabel:str=None, ylabel:str=None, zlabel:str=None, markersize=2,
                            save_path:str=None, figure_dpi:int=100, display_phase_diagram:bool=False):

        fig, ax = plt.subplots(1,1, dpi=figure_dpi, tight_layout=True)
        # contour  = ax.contour(contours[0], contours[1], contours[2], levels=levels, linewidths=0.3, colors='k')
        contourf = ax.contourf(contours[0], contours[1], contours[2], levels=levels, cmap="RdBu_r")
        fig.colorbar(contourf, ax=ax, label=zlabel)

        cmap = mpl.cm.get_cmap('RdBu_r')
        for i in range(len(data[0])):
            ax.scatter(data[0][i], data[1][i], markersize[i], marker='o', 
                        color=cmap((data[2][i]-ranges[2][0])/(ranges[2][1]-ranges[2][0])),
                        edgecolors=(0, 0, 0), linewidths=0.3)
        ax.set(xlim=ranges[0], ylim=ranges[1])

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        fig.suptitle(self._nozzle_id)

        if save_path is not None:
            print("Saving diagram here: \n\t")
            sp = os.path.join(save_path)
            print(sp)
            plt.savefig(sp)
        if display_phase_diagram:
            plt.draw()



    def plot_phase_digram(self, config=None, save_path:str=None, display_phase_diagram:bool=False, figure_dpi:int=100):
        r""" Plots a liquid-vs-gas phase diagram, where the colorbar is the jet speed
        """
        if config is None:
            raise KeyError("Must provide a config!")

        xr = config['x_range']
        yr = config['y_range']
        zr = config['z_range']

        x = config['x']
        y = config['y']
        z = config['z']

        if config['marker_size_is_jet_diameter']:
            s = np.array(self._jet_diameter)
        else:
            s = 10

        xi, yi, zi, levels = self._make_contour(ranges=(xr, yr, zr), data=(x,y,z), level_step=config['level_step'])
        self._make_phase_diagram(ranges=(xr, yr, zr), data=(x,y,z), contours=(xi,yi,zi),  
                                    xlabel=config['x_label'], ylabel=config['y_label'], zlabel=config['z_label'],
                                    markersize=s, levels=levels, figure_dpi=figure_dpi, 
                                    display_phase_diagram=display_phase_diagram)


    def show_all_figures(self):
        plt.show()



































