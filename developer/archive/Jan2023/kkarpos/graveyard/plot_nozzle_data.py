import numpy as np
import pylab as plt
import glob



files = glob.glob('/home/kkarpos/Nozzles/data/analysis/20200811/564_175_water/*.h5.jet_analyzer.npz')

nozzle = '564_175'
cap_line = '1.5m'
sample = 'Water'
nozzle_ID = '75um'
medium = 'Vacuum'



gas = []
liq = []
vel = []
dia = []
pix = []
for i in files:
    data = np.load(i)

    jet_crop = data['jet_crop']
    droplet_threshold = data['droplet_threshold']
    cc_matrix = data['cc_matrix']
    cc_matrix_projection = data['cc_matrix_projection']
    droplet_translation = data['droplet_translation']
    back_even = data['back_even']
    back_odd = data['back_odd']
    stack_transpose = data['stack_transpose']
    gas_flow = data['gas_flow']
    liquid_flow = data['liquid_flow']
    pixel_size = data['pixel_size']
    delay = data['delay']

    jet_speed = droplet_translation*(pixel_size*1e-6)/(delay*1e-9)
    jet_diameter = 2*np.sqrt(jet_speed / (np.pi * liquid_flow))

    gas.append(gas_flow)
    liq.append(liquid_flow)
    vel.append(jet_speed)
    dia.append(jet_diameter)
    pix.append(pixel_size)





gas = np.array(gas)
liq = np.array(liq)
vel = np.array(vel)
dia = np.array(dia)

vel_outliers = np.where(vel > 1.5*vel.mean())
new_vel = np.delete(vel, vel_outliers[0])
new_gas_vel = np.delete(gas, vel_outliers[0])
new_liq_vel = np.delete(liq, vel_outliers[0])


dia_outliers = np.where(dia > 1.5*dia.mean())
new_dia = np.delete(dia, dia_outliers[0])
new_gas_dia = np.delete(gas, dia_outliers[0])
new_liq_dia = np.delete(liq, dia_outliers[0])


# ===================
# Plots
# ===================

# # Gas/Liq Phase Diagram
# plt.figure(1)
# plt.scatter(gas, liq)
# plt.xlabel('Gas Flow Rate (mg/min)')
# plt.ylabel('Liquid Flow Rate (uL/min)')
# plt.title('Nozzle: %s, Capillary Length: %s, \n Sample: %s, Nozzle ID: %s, Medium: %s' % (nozzle, cap_line, sample, nozzle_ID, medium))


# # Jet Speed v Gas -- Add 
# plt.figure(2)
# plt.scatter(gas, vel)
# plt.xlabel('Gas Flow Rate (mg/min)')
# plt.ylabel('Jet Speed (m/s)')
# plt.title('Nozzle: %s, Capillary Length: %s, \n Sample: %s, Nozzle ID: %s, Medium: %s' % (nozzle, cap_line, sample, nozzle_ID, medium))



# # Jet Length v Gas
# # plt.figure(3)
# # plt.plot(v, g)

# # Jet Diameter v Gas 
# plt.figure(4)
# plt.scatter(gas, dia)
# plt.xlabel('Gas Flow Rate (mg/min)')
# plt.ylabel('Jet Diameter ($\mu$m????)') #CHECK UNITS
# plt.title('Nozzle: %s, Capillary Length: %s, \n Sample: %s, Nozzle ID: %s, Medium: %s' % (nozzle, cap_line, sample, nozzle_ID, medium))



plt.figure(5)
cm = plt.cm.get_cmap('tab20c')
plt.scatter(new_gas_dia, new_dia, c=new_liq_dia, cmap=cm)
cbar = plt.colorbar()
cbar.set_label('Liquid Flow [$\mu$L/min]')
plt.grid(alpha=.3)
plt.ylabel('Jet Diameter [$\mu$m]')
plt.xlabel('Gas Flow Rate [mg/min]')
plt.title('Nozzle: %s, Capillary Length: %s, \n Nozzle ID: %s, Medium: %s' % (nozzle, cap_line, nozzle_ID, medium))
plt.text(np.min(new_gas_dia), np.max(new_dia)+0.025,s='Sample: %s' % sample, bbox=dict(facecolor='grey', alpha=0.2))
plt.ylim(np.min(new_dia)-0.05, np.max(new_dia)+0.5)



plt.figure(6)
cm = plt.cm.get_cmap('tab20c')
plt.scatter(new_gas_vel, new_vel, c=new_liq_vel, cmap=cm)
cbar = plt.colorbar()
cbar.set_label('Liquid Flow [$\mu$L/min]')
plt.grid(alpha=.3)
plt.ylabel('Jet Speed [m/s]')
plt.xlabel('Gas Flow Rate [mg/min]')
plt.title('Nozzle: %s, Capillary Length: %s, \n Nozzle ID: %s, Medium: %s' % (nozzle, cap_line, nozzle_ID, medium))
plt.text(np.min(new_gas_vel), np.max(new_vel),s='Sample: %s' % sample, bbox=dict(facecolor='grey', alpha=0.2))
plt.ylim(np.min(new_vel)-0.5, np.max(new_vel)+5)


plt.show()