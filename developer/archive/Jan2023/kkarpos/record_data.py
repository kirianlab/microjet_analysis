import tempfile
# from time import sleep
import os
import json
import datetime
import sys

sys.path.append('D:\\microjet-analysis\hardware_interfaces')
sys.path.append('D:\\microjet-analysis\jet_analysis')

from hardware.bronkhorst import Bronkhorst
from hardware.sensirion import Sensirion
from shimadzu_pump import shimadzu_driver as hplc
import pyqtgraph as pg
import numpy as np
from analyze_jet import *

pg.mkQApp()

config = {'sensirion_serial_number': 'FTSTT5NA',
          'sensirion_lowflow': 'FTXEF0AHA',
          'bronkhorst_com_port': 'COM3'}

#
#
pathogen = lambda path: os.path.normpath(os.path.abspath(os.path.expanduser(path)))
#
status_path = ''

#FIXME: Update status paths to save/read in the repo, rather than on the desktop.
#           Reasoning: These are txt files that trigger recordings, they do
#                      not need to be on the desktop.

##################################################
# Set up Bronkhorst gas flow meter/controller
##################################################
print("Setting up the Bronkhorst", flush=True)
#
# set the status file path
gas_status_path = pathogen("~/Desktop/sensor_status_gas.txt")
#
# # gather all the sensors available and initialize the class
bronkhorst = Bronkhorst(com_port=config['bronkhorst_com_port'])


def write_bronkhorst_status(avg_n_points: int = 10, setpoint: float = None):
    r"""
        FIXME: Add documentation here!
    """
    data = bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
    # while data is None:
    #     data =  bronkhorst.data(avg_n_points=avg_n_points, setpoint=setpoint)
    with open(gas_status_path, 'w') as f:
        json.dump(data, f, indent=4)  # sort_keys=True


def set_gas(flow):
    bronkhorst.setpoint = flow


##################################################
# Set up Shimadzu liqid flow controller
##################################################
print("Setting up the HPLC pump", flush=True)

# FIXME: Update this to use the shimadzu.py script, it keeps things in the same units

# set the status file path and connect to the pump
hplc_status_path = pathogen("~/Desktop/sensor_status_hplc.txt")
hplc = hplc.ShimadzuCbm20('192.168.200.99')
hplc.login('raklab', 'sky2Blue')


def write_hplc_status():
    now = datetime.datetime.now()
    timestamp = now.timestamp()  # machine readable
    timestamp_iso = now.isoformat(sep=" ")  # human   readable
    data = hplc.get_all()

    data['timestamp'] = timestamp
    data['timestamp_iso'] = timestamp_iso
    data['flow_units'] = 'ml/min'
    data['reading'] = data['flow'] * 1e3
    data['reading_units'] = 'ul/min'
    if data['pressure_unit'] != 1:
        raise ValueError("Is the HPLC pressure unit psi?")
    data['pressure_units'] = 'psi'

    with open(hplc_status_path, 'w') as f:
        json.dump(data, f, indent=4)


def set_liquid(flow, start_pump=True):  # This is ul/min
    hplc.set('flow', flow * 1e-3)
    hplc.start()
    write_hplc_status()


##################################################
# Set up Sensirion liquid flow meter
##################################################
print("Setting up the Sensirion pump", flush=True)

# set the status file path
sensirion_status_path = pathogen("~/Desktop/sensor_status_liq.txt")

# gather all the sensors available and initialize the class
sensirion = Sensirion(serial_number=config['sensirion_serial_number'])


def write_sensirion_status(avg_n_points: int = 10):
    data = sensirion.data(avg_n_points=avg_n_points)
    # while data is None:
    #     data = sensirion.data(avg_n_points=avg_n_points, wait_time=wait_time)
    with open(sensirion_status_path, 'w') as f:
        json.dump(data, f, indent=4)  # sort_keys=True


###################################################
# Set up interface for fastcam recording
###################################################
print("Setting up the FastCAM", flush=True)
record_file = os.path.join(tempfile.gettempdir(), "ody_record")
print(record_file, flush=True)

# import asyncio
def record():
    f = open(record_file, 'w')
    f.close()
    while os.path.isfile(record_file):
        sleep(0.2)
    print("recording done")


####################################################
# Record a range of liquid and gas flow rates
####################################################

def write_status():
    write_bronkhorst_status()
    write_sensirion_status()
    write_hplc_status()


def record_range(liquid_flows, gas_flows, analysis, equilibrate_time: int = 10):
    print("\n\n Starting parameter sweep:", flush=True)
    count = 0
    for l in liquid_flows:
        # set the liquid flow rate
        set_liquid(l)
        print(f'Equilibrating liquid... waiting {equilibrate_time} seconds', flush=True)
        sleep(equilibrate_time)
        for g in gas_flows:
            # set the gas flow rate
            set_gas(g)
            print('liquid =', l, ', gas = ', g, flush=True)

            # write the current gas and liq flow configurations for recording
            write_status()  # FIXME: write loop to check how much time has passed

            # record a movie
            print('recording video...', flush=True)
            record()

            print('analyzing...', flush=True)
            analysis.start_analysis()


# if __name__ == "__main__":

set_gas(30)
set_liquid(20)

# if sys.platform == "win32":
#     pass
# else:
#     raise ValueError("You're not on Windows, this won't work.")

basepath = "C:\\Users\\raklab\\Desktop\\NanoscribeNozzles"
datapath = os.path.join(basepath, "2022\\20220203\\testest123")
savepath = os.path.join(datapath, "analysis")
# logpath = "C:\\Users\\raklab\\Desktop\\NanoscribeNozzles\\2021\\logs"
logpath = ""
waittime = 10
glob_this = "*.h5"
pd = True
nozzle_id = datapath.split(os.sep)[-1]

configs = {'datapath': datapath,
           'savepath': savepath,
           'logpath': logpath,
           'wait_time': waittime,
           'globstr': glob_this,
           'reuse_previous': False,
           'interactive': False,
           'save_data': True,
           'is_darkfield': False,
           'display': False,
           'thresh_sigma': 6,
           'reuse': False,
           'jet_crop': None,
           'droplet_threshold': None,
           'quality': None,
           'nozzle_id': nozzle_id,
           'confirm_results': False}

analyzer = JetAnalysis(configs)


record_range(liquid_flows=[20], gas_flows=[15, 16, 18], analysis=analyzer, equilibrate_time=1)

hplc.stop()
