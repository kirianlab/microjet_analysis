# First install miniconda
import sys
import os
import numpy as np  # conda install numpy
from numpy.fft import fft, ifft
from pyqtgraph import QtGui, QtCore
import pyqtgraph as pg  # conda install -c conda-forge pyqtgraph
from pyqtgraph.Qt import QtWidgets as qwgt
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
from skimage.filters import threshold_otsu, threshold_local
from microjet_analysis import gdvn, dataio


class Main(qwgt.QMainWindow):
    r""" QMainWindow subclass.  Overrides keyPressEvent, sets window size to 2/3 of primary screen size. """
    def __init__(self, par=None, *args, **kwargs):
        r""" This should not be necessary, but is presently used to configure key-press events."""
        app = pg.mkQApp()
        super().__init__(*args, **kwargs)
        self.par = par
        s = app.primaryScreen().size()
        f = 2 / 3  # Make the main window fill this fraction of the screen
        self.setGeometry(int(s.width()*(1-f)/2), int(s.height()*(1-f)/2), int(s.width()*f), int(s.height()*f))
    def keyPressEvent(self, ev):
        r""" Overrides a built-in method. """
        if self.par is not None:
            self.par.key_pressed(ev)


class InteractiveGUI:
    jetana = None
    def __init__(self, filepath=None, config=None):
        self.app = pg.mkQApp()
        self.main_window = Main(par=self)
        self.main_window.setWindowTitle("GDVN Analyzer")
        k = {'show_histogram': True, 'aspect_locked': False}
        self.rawview = gdvn.GDVNView(title='Raw', xlabel='X', ylabel='Y', **k)
        self.grid_layout = qwgt.QGridLayout()
        self.grid_widget = qwgt.QWidget()
        self.grid_widget.setLayout(self.grid_layout)
        self.main_widget = qwgt.QWidget()
        self.main_widget.setLayout(self.grid_layout)
        # self.layout.addWidget(self.grid_widget)
        self.rawview = gdvn.GDVNView(title='Raw Images', xlabel='X', ylabel='Y', **k)
        # self.view1 = gdvn.GDVNView(title='Raw', xlabel='X', ylabel='Y', **k)
        self.grid_layout.addWidget(self.rawview, 0, 0)
        self.threshview = gdvn.GDVNView(title='Thresholded', xlabel='X', ylabel='Y', **k)
        self.grid_layout.addWidget(self.threshview, 1, 1)
        self.filtview = gdvn.GDVNView(title='Filtered', xlabel='X', ylabel='Y', **k)
        self.grid_layout.addWidget(self.filtview, 0, 1)
        # Link the frame numbers across multiple ImageViews
        # self.threshview.sigTimeChanged.connect(self.rawview.setCurrentIndex)
        self.rawview.sigTimeChanged.connect(self.threshview.setCurrentIndex)
        self.rawview.sigTimeChanged.connect(self.filtview.setCurrentIndex)
        self.threshview.view.setXLink(self.rawview.view)
        self.threshview.view.setYLink(self.rawview.view)
        self.filtview.view.setXLink(self.rawview.view)
        self.filtview.view.setYLink(self.rawview.view)
        # self.plot1 = pg.PlotWidget()
        # self.plot1.setLabels(title='Droplet Centroids', bottom='X', left='Y')
        # self.plot1.setXLink(self.rawview.view)
        # self.plot1.setYLink(self.rawview.view)
        # self.grid_layout.addWidget(self.plot1, 1, 0)
        # self.plot2 = pg.PlotWidget()
        # self.grid_layout.addWidget(self.plot2, 1, 1)
        self.main_window.setCentralWidget(self.main_widget)
        # Some basic dropdown menu options:
        self.menubar = qwgt.QMenuBar()
        self.main_window.setMenuBar(self.menubar)
        file_menu = self.menubar.addMenu('File')
        open_action = qwgt.QAction('Open file...', self.main_window)
        open_action.triggered.connect(self.open_data_file_dialog)
        file_menu.addAction(open_action)
        # Statusbar so we know what is going on
        self.statusbar = self.main_window.statusBar()
        self.main_window.show()
        self.app.processEvents()
        self.set_status('Ready.')
        if filepath is not None:
            self.load_file(filepath)
    def open_data_file_dialog(self):
        r""" Select a file to open. """
        opt = qwgt.QFileDialog.Options()
        f, t = qwgt.QFileDialog.getOpenFileName(self.main_window, "Load data", os.getcwd(), "Data (*.h5)", options=opt)
        self.set_status(f'Loading file {f}...')
        if f:
            self.load_file(f)
    def set_status(self, message):
        self.statusbar.showMessage(message)
        self.app.processEvents()
    def load_file(self, filepath):
        self.set_status(f'Loading data {filepath}...')
        stack, metadata = dataio.h5_metadata_version(filepath)
        self.set_status('Processing data...')
        self.jetana = gdvn.JetAnalysis(stack, metadata)
        self.set_status('Displaying results...')
        self.update_display()
        self.main_window.setWindowTitle(f"GDVN Analyzer ({filepath})")
        self.set_status('Ready')
    def update_display(self):
        ja = self.jetana
        self.rawview.set_jetana(ja)
        self.rawview.setImage(ja._image_stack_raw)
        # self.view1.set_jetana(ja)
        # self.view1.setImage(ja._stack_filtered)
        self.threshview.set_jetana(ja)
        self.threshview.setImage(ja._stack_thresholded.astype(int))
        self.filtview.setImage(ja._stack_filtered)
        # dc = ja.droplet_centroids
        # x = np.concatenate([d[0] for d in dc])
        # y = np.concatenate([d[1] for d in dc])
        # self.plot1.plot(x, y, clear=True, pen=None, symbolBrush=(0, 255, 0, 50), symbolSize=5, symbolPen=None)
        self.app.processEvents()
    def start(self):
        r""" Execute the application. """
        self.app.exec_()
    def key_pressed(self, ev):
        r""" Handling of key-press events. """
        key = ev.key()
        if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
            self.process_data()
            self.display()


if __name__ == '__main__':
    thisfile = r'../rawdata/mh_coarse_b2_n1/001.h5'
    from pathlib import Path
    thisfile = Path("/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/microjets/2022/20220929/ros_ipvisio_01_analysis/data/")
    interact = InteractiveGUI(filepath=thisfile)
    interact.start()
