import sys
import os
import numpy as np  # conda install numpy
from numpy.fft import fft, ifft
from pyqtgraph import QtGui, QtCore
import pyqtgraph as pg  # conda install -c conda-forge pyqtgraph
from pyqtgraph.Qt import QtWidgets as qwgt
from skimage.transform import hough_line, hough_line_peaks  # conda install -c anaconda scikit-image
from skimage.filters import threshold_otsu, threshold_local
from microjet_analysis import gdvn, dataio




class Main(qwgt.QMainWindow):
    r""" QMainWindow subclass.  Overrides keyPressEvent, sets window size to 3/4 of primary screen size. """
    def __init__(self, par=None, *args, **kwargs):
        r""" This should not be necessary, but is presently used to configure key-press events."""
        app = pg.mkQApp()
        super().__init__(*args, **kwargs)
        self.par = par
        s = app.primaryScreen().size()
        f = 2/3  # Make the main window fill this fraction of the screen
        self.setGeometry(int(s.width()*(1-f)/2), int(s.height()*(1-f)/2), int(s.width()*f), int(s.height()*f))
    def keyPressEvent(self, ev):
        r""" Overrides a built-in method. """
        if self.par is not None:
            self.par.key_pressed(ev)








class AnalysisViewer:

    jetana = None

    def __init__(self, filepath=None, config=None):
        self.config = config

        # Make the main GUI window
        self.app = pg.mkQApp()
        self.movie_main_window = Main(par=self)
        self.movie_main_window.setWindowTitle("Movie Viewer")

        self.ana_main_window = Main(par=self)
        self.ana_main_window.setWindowTitle("GDVN Analysis")

        # Define some GUI kwargs
        k = {'show_histogram': True, 'aspect_locked': False}

        # make the frame viewers
        self.movie_layout = qwgt.QGridLayout()
        self.movie_widget = qwgt.QWidget()
        self.movie_widget.setLayout(self.movie_layout)
        self.movie_main_widget = qwgt.QWidget()
        self.movie_main_widget.setLayout(self.movie_layout)

        # Set the movie display panels
        self.rawview = gdvn.GDVNView(title='Normalized Raw', xlabel='X', ylabel='Y', **k)
        self.movie_layout.addWidget(self.rawview, 0, 0)
        self.threshview = gdvn.GDVNView(title='Thresholded', xlabel='X', ylabel='Y', **k)
        self.movie_layout.addWidget(self.threshview, 1, 1)
        self.filtview = gdvn.GDVNView(title='Filtered', xlabel='X', ylabel='Y', **k)
        self.movie_layout.addWidget(self.filtview, 0, 1)
        self.procview = gdvn.GDVNView(title='Processed', xlabel='X', ylabel='Y', **k)
        self.movie_layout.addWidget(self.procview, 1, 0)

        self.movie_main_window.setCentralWidget(self.movie_main_widget)
        self.menubar = qwgt.QMenuBar()
        self.movie_main_window.setMenuBar(self.menubar)
        file_menu = self.menubar.addMenu('File')
        open_action = qwgt.QAction('Open file...', self.movie_main_window)
        open_action.triggered.connect(self.open_data_file_dialog)
        file_menu.addAction(open_action)
        # Statusbar so we know what is going on
        self.statusbar = self.movie_main_window.statusBar()
        self.movie_main_window.show()


        linkthese = [self.rawview, self.threshview, self.filtview, self.procview]
        for a in linkthese:
            for b in linkthese:
                if a != b:
                    a.sigTimeChanged.connect(b.setCurrentIndex)
                    a.view.setXLink(b.view)
                    a.view.setYLink(b.view)


        # make the analysis viewers
        self.ana_layout = qwgt.QGridLayout()
        self.ana_widget = qwgt.QWidget()
        self.ana_widget.setLayout(self.ana_layout)
        self.ana_main_widget = qwgt.QWidget()
        self.ana_main_widget.setLayout(self.ana_layout)

        # set the analysis display planels
        self.corrview = gdvn.GDVNView(title='Correlation', xlabel='X', ylabel='Y', levels=(0,1), **k)
        self.corrplot = self.corrview.add_plot(pen=(255, 0, 0))
        self.ana_layout.addWidget(self.corrview, 0, 0)
        names = ['Mean', 'Standard Deviation', 'Minimum', 'Maximum', 'Processed Mean']
        self.statsview = gdvn.GDVNView(title='Statistics', xlabel='X', ylabel='Y', frame_names=names, **k)
        self.ana_layout.addWidget(self.statsview, 1, 1)

        self.areahistview = gdvn.GDVNView(title="Droplet Area Histogram", xlabel=r"Area [sq um]", ylabel="Counts", **k)
        self.areaplot = self.areahistview.add_plot(pen=(255, 0, 0))
        self.ana_layout.addWidget(self.areahistview, 1, 0)

        self.jetstabilityview = gdvn.GDVNView(title="Jet Stability", xlabel=r"Liquid Flow [ul/min]", ylabel="Gas Flow [mg/min]")
        self.jsplot = self.jetstabilityview.add_plot(pen=(255, 0, 0))
        self.ana_layout.addWidget(self.jetstabilityview, 0, 1)

        self.ana_main_window.setCentralWidget(self.ana_main_widget)
        self.ana_main_window.setMenuBar(self.menubar)
        # Statusbar so we know what is going on
        # self.statusbar = self.ana_main_window.statusBar()
        self.ana_main_window.show()





        self.app.processEvents()
        self.set_status('Ready.')
        if filepath is not None:
            self.load_file(filepath)


    def open_data_file_dialog(self):
        r""" Select a file to open. """
        opt = qwgt.QFileDialog.Options()
        f, t = qwgt.QFileDialog.getOpenFileName(self.movie_main_window, "Load data", os.getcwd(), "Data (*.h5)", options=opt)
        self.set_status(f'Loading file {f}...')
        if f:
            self.load_file(f)
    def set_status(self, message):
        self.statusbar.showMessage(message)
        self.app.processEvents()
    def load_file(self, filepath):
        self.set_status(f'Loading data {filepath}...')
        stack, metadata = dataio.h5_metadata_version(filepath)
        self.set_status('Processing data...')
        self.jetana = gdvn.JetAnalysis(stack, metadata, config=self.config)
        self.set_status('Displaying results...')
        self.update_display()
        self.movie_main_window.setWindowTitle(f"GDVN Analyzer ({filepath})")
        self.ana_main_window.setWindowTitle(f"GDVN Analyzer ({filepath})")
        self.set_status('Ready')



    def update_display(self):
        ja = self.jetana

        # set the movie images
        self.rawview.set_jetana(ja)
        self.rawview.setImage(ja._image_stack_raw)
        self.threshview.set_jetana(ja)
        self.threshview.setImage(ja._stack_thresholded.astype(int))
        self.filtview.setImage(ja._stack_filtered)
        self.procview.setImage(ja._stack_processed.astype(int))

        # set the correlation matrix and projection
        p = ja.correlation_projection[0]
        corr = ja._correlation_matrix
        corr *= np.sign(np.max(p))
        n = p.shape[0]
        p -= np.min(p)
        p *= n/np.max(p)
        self.corrplot.setData(np.arange(n), p)
        self.corrview.setImage(corr)
        self.corrview.show()

        # set the frame statistics
        s = np.array([ja._stack_mean_image,
                      ja._stack_sdev_image,
                      np.min(ja._image_stack_raw, axis=0),
                      np.max(ja._image_stack_raw, axis=0),
                      np.mean(ja._stack_processed, axis=0)])
        self.statsview.setImage(s)
        self.statsview.show()

        # set the droplet area histogram
        ahist, abins = ja._histogram_droplet_areas()
        abins *= ja._image_metadata['pixel_size']
        self.areaplot.setData(abins[:-1], ahist)
        # self.areahistview.setImage(ahist)
        self.areahistview.show()

        # set the jet stability plot
        x = np.linspace(0, 2*np.pi, 1000)
        y = np.sin(x)
        self.jsplot.setData(x, y)
        self.jetstabilityview.show()

        self.app.processEvents()


    def start(self):
        r""" Execute the application. """
        self.app.exec_()
    def key_pressed(self, ev):
        r""" Handling of key-press events. """
        key = ev.key()
        if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
            self.process_data()
            self.display()









if __name__ == '__main__':

    config = {'max_frames': 10000000,
                'threshold_method': 'otsu_clipped',
                'fill_holes': True,
                'normalize_raw': True,
                'do_filter': True,
                'filter_method': 'gausshp',
                'filter_parameters': {'sigma': 10},
                'remove_small_objects': 15,
                'binary_closing_iterations': 3,
                'label_method': 'watershed',
                'test': 'test'
                }


    thisfile = r"/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/20220620/mh_cut2_b4_n2/057.h5"
    thisfile = r"/Users/kkarpos/Desktop/gdvn_tests/002.h5"

    ana = AnalysisViewer(filepath=thisfile, config=config)
    ana.start()





























































