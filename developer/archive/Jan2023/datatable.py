import os
import glob
import pandas as pd
import numpy as np
import h5py

pd.set_option('display.max_rows', 100)

config = {"basepath": "/Volumes/GoogleDrive/Shared drives/Kirian Lab/Small Data/mh_nozzles/",
                "date": "20220620",
                "nozzle_id": "mh_cut2_b4_n2",
                "save": False
            }




def analysis_dataframe(config, save:bool=True, savepath:str=None):
    r""" Converts the saved analysis into a run-wide csv file for easy viewing

        Args:
            config (dict): The input parameters
            save (bool): Whether to save the DataFrame as a csv file

    """

    # required_keys = ["basepath", "date", "nozzle_id"]
    # for k,v in config.items():
    #     if k not in required_keys:
    #         raise KeyError(f"Key {k} missing! Required_keys: {required_keys}")

    path = os.path.join(config['basepath'], config['date'], config['nozzle_id']+"_analysis", "data")
    datapaths = np.sort(glob.glob(os.path.join(path, "*.h5")))

    dat = {'datapoint': [],
            'date': [],
            'time': [],
            'year': [],
            'delay': [],
            'pixel_size': [],
            'liquid_flow': [],
            'liquid_flow_units': [],
            'gas_flow': [],
            'gas_flow_units': [],
            'jet_speed': [],
            'jet_speed_err': [],
            'droplet_dispersion': [],
            'droplet_dispersion_err': [],
            'jet_deviation': [],
            'jet_deviation_err': [],
            'jet_diameter': [],
            'jet_diameter_err': [],
            'jet_length': [],
            'jet_length_err': [],
            'angle_units': []
            }

    for dp in datapaths:
        dat["datapoint"].append(dp.split(os.sep)[-1])
        f = h5py.File(dp, 'r')
        for k,v in f['metadata'].items():
            dat[k].append(v[()])
        for k,v in f['calculated_data'].items():
            dat[k].append(v[()])
        f.close()

    df = pd.DataFrame(dat)

    if save:
        if savepath is None:
            savepath = os.path.join(config['basepath'],
                                    config['date'],
                                    config['nozzle_id']+"_analysis",
                                    f"{config['nozzle_id']}.csv")
        df.to_csv(savepath, index=False)

    return df

df = analysis_dataframe(config, save=config['save'])














