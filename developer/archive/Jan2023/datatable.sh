#!/bin/bash

dir=$1

for f in $dir/*.txt; do
  echo $f $(grep 'liquid =' $f) $(grep 'gas    =' $f) "${dir}_analysis/data/"
done