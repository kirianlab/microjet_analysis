import os
import argparse
from microjet_analysis import gdvn, dataio
parser = argparse.ArgumentParser(description='Process GDVN data.')
parser.add_argument('filepaths', type=str, nargs='+',
                    help='Path to data file.')
parser.add_argument('--no_analysis', action='store_true',
                    help='Set flag to skip data analysis steps.')
parser.add_argument('--view', action='store_true',
                    help='View the analysis.')
parser.add_argument('--savedir', type=str, default='',
                    help='Directory in which files will be saved')
parser.add_argument('--savedir_replace', type=str, default='',
                    help='Create save directory by replacing substring of raw data path.'  
                         'Example: --savedir_replace=rawdata,tempdata')
parser.add_argument('--reload', action='store_true',
                    help='If results already exist, reload them and skip data analysis steps.')
parser.add_argument('--overwrite_results', action='store_true',
                    help='Overwrite previous results file.')
args = parser.parse_args()
for filepath in args.filepaths:
    print('File path:', filepath)
    savepath = None
    jetana = None
    if args.savedir_replace:
        _from = args.savedir_replace.split(',')[0]
        _to = args.savedir_replace.split(',')[1]
        savepath = filepath.replace(_from, _to)
        savepath = os.path.splitext(savepath)[0] + '_results.h5'
        if savepath == filepath:
            raise ValueError('Save path cannot be the same as file path')
        os.makedirs(os.path.split(savepath)[0], exist_ok=True)
    if args.savedir:
        savepath = os.path.join(args.savedir, os.path.split(filepath)[1])
        savepath = os.path.splitext(savepath)[0] + '_results.h5'
        if savepath == filepath:
            raise ValueError('Save path cannot be the same as file path')
        os.makedirs(os.path.split(savepath)[0], exist_ok=True)
    if savepath is not None:
        print('Save path:', savepath)
    if args.reload:
        if os.path.exists(savepath):
            raise ValueError('The JetAnalysis.from_h5 method needs to be implemented')
    if jetana is None:
        print('Loading raw data')
        stack, metadata = dataio.h5_metadata_version(filepath)
        print('Processing raw data')
        jetana = gdvn.JetAnalysis(stack, metadata)
    if args.view:
        print('Viewing data')
        gdvn.gdvn_view(stack, jetana=jetana, title=filepath)
    if savepath is not None:
        print('Saving results')
        jetana.to_h5(savepath, overwrite=args.overwrite_results)
    print('Done')
