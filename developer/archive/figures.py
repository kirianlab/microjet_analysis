import sys
import os
import glob

import numpy as np
import pylab as plt
import matplotlib as mpl
import imageio 
import h5py
import json
import pandas
import pandas as pd
import scipy.stats as stats



class DataPlotter:

    _df = None
    _nozzle_id = None

    def __init__(self, dataframe=None, csv:str=None, nozzle_id:str=None):
        r""" A microjet analysis data plotter class.

            TO DO: add method to splice together multiple csv files

            Args: 
                dataframe (pandas dataframe): a pre-existing pandas dataframe with all the analyzed data
                csv (str): a full file path to a csv with already analyzed data
        """

        if (csv is not None) and (dataframe is None):
            self._df = pd.read_csv(csv)
        elif (csv is None) and (dataframe is not None):
            self._df = dataframe
        elif (csv is None) and (dataframe is None):
            raise ValueError("dataframe and csv cannot both be None")
        elif (csv is not None) and (dataframe is not None):
            raise ValueError("dataframe and csv cannot both be None")

        if nozzle_id is not None:
            self._nozzle_id = nozzle_id

    @property
    def dataframe(self):
        return self._df

    @property
    def nozzle_id(self):
        return self._nozzle_id

    @property
    def df(self):
        return self._df

    def water_params(self):
        water = {'rho': 998, # kg/m^3 mass density at 20C
                    'rho_units': 'kg/m^3',
                    'mu': 1e-3, # kg/ms
                    'mu_units': 'kg/ms',
                    'sigma': 0.072, # N/m
                    'sigma_units': 'N/m'
                    }
        return water

    def helium_params(self):
        helium = {'rho': 0.1786,
                    'rho_units': 'kg/m^3',
                    }
        return helium

    def set_nozzle_id(self, nid:str):
        r""" Sets the nozzle ID for plotting purposes.

            Args:
                nid (|str|): The nozzle ID you wish to use for the plots.

            Returns:
                None
        """
        self._nozzle_id = nid

    def reynolds(self, Q, R, rho, mu):
        r"""Calculates the Reynolds number of a substance

            Args:
                Q (float): volumetric flow rate
                R (float): The liquid jet diameter
        """
        num = rho * Q
        den = np.pi * R * mu
        return num / den

    def weber(self, Q, R, rho, sigma):
        num = rho * Q**2
        den = np.pi**2 * R**3 * sigma
        return num / den

    def capillary(self, v, mu, sigma):
        num = mu * v
        den = sigma * np.sqrt(2)
        return num / den

    def gas_vs_velocity(self, df=None, n:int=11, show=True, zscore:float=2.2,
                            savepath:str=None, nozzle_id:str=None):
        r""" 
            Plots gas mass flow rate vs jet speed. At high liquid flow rates
            (~20ul/min and up), the data should behave according to the 
            Bernoulli equation. Low liquid flow rates are outside of the assumptions
            used in the equation. This plot should show that high liquid
            flow rates reach some asymptotic behavior in jet speeds, 
            with the low liquid flow rates being substantially way from that 
            asymptote. 

            Args:
                df (|dataframe|): your dataframe. If None, will use the classes dataframe.
                n (|int|): The number of steps to separate the liquid flow rates by,
                            default is 11.
                show (|bool|): Choose whether to show the plot or not
                savepath (|str|): Where you would like to save the data.
                                    If None, the method will not save the figure.
        """

        if df is None:
            df = self._df

        if nozzle_id is None:
            nozzle_id = self._nozzle_id

        # gas vs velocity
        gas = df['gas_flow']
        liq = df['liquid_flow']
        vel = df['jet_speed']

        ls = np.linspace(np.floor(liq.min()), np.ceil(liq.max()), n)

        fig, ax = plt.subplots(1,1, dpi=150)
        colors = mpl.cm.rainbow(np.linspace(0, 1, len(ls)))

        for i in range(len(ls)-1):
            ddf = df[(liq >= ls[i]) & (liq < ls[i+1])].sort_values('liquid_flow')
            ddf['zscore'] = stats.zscore(ddf['jet_speed'])
            ddf = ddf[np.abs(ddf['zscore']) < zscore]
            if ddf.empty:
                pass
            else:
                z = np.polyfit(ddf['gas_flow'], ddf['jet_speed'], 2)
                f = np.poly1d(z)
                x_new = np.linspace(ddf['gas_flow'].min(), ddf['gas_flow'].max(), 50)
                y_new = f(x_new)
                ax.plot(ddf['gas_flow'], ddf['jet_speed'], '.', color=colors[i])
                label = rf"[{ls[i]:0.2f}, {ls[i+1]:0.2f})"
                ax.plot(x_new, y_new, color=colors[i], alpha=0.5, label=label)

        ax.set_xlabel("Mass Flow Rate [mg/min]")
        ax.set_ylabel("Measured Jet Speed [m/s]")
        ax.grid(alpha=0.5)
        ax.legend(fontsize=5, ncol=3,
                    title=r"Liquid Flow Rate [$\mu$L/min]", title_fontsize='x-small')
        fig.suptitle(nozzle_id)

        if savepath is not None:
            plt.savefig(savepath)
        if show:
            plt.show()

    def reynolds_vs_weber(self, df=None, n:int=11, show=True, zscore:float=2.2,
                            savepath:str=None, nozzle_id:str=None):
        r""" 
            ADD FILTERING

            Args:
                df (|dataframe|): your dataframe. If None, will use the classes dataframe.
                n (|int|): The number of steps to separate the liquid flow rates by,
                            default is 11.
                show (|bool|): Choose whether to show the plot or not
                savepath (|str|): Where you would like to save the data.
                                    If None, the method will not save the figure.
        """

        if df is None:
            df = self._df

        if nozzle_id is None:
            nozzle_id = self._nozzle_id

        liq = df['liquid_flow']
        lmin, lmax = np.min(liq), np.max(liq)
        ls = np.linspace(np.floor(lmin), np.ceil(lmax), 11)

        fig, ax = plt.subplots(1,1, figsize=(8,5))#, dpi=150)

        norm = mpl.colors.Normalize(vmin=lmin, vmax=lmax)
        clb = fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap='rainbow'), ax=ax)
        clb.ax.set_ylabel(r'Liquid Flow Rate [$\mu$L/min]',fontsize=12)

        colors = mpl.cm.rainbow(np.linspace(0, 1, len(ls)))
        for i in range(len(ls)-1):
            ddf = df[(liq >= ls[i]) & (liq < ls[i+1])].sort_values('liquid_flow')
            d = ddf['jet_diameter'].to_numpy()
            re = self.reynolds(ddf['liquid_flow'] * 1.6667e-11, d, 
                    self.water_params()['rho'], self.water_params()['mu'])
            we = self.weber(ddf['liquid_flow'] * 1.6667e-11, d, 
                    self.water_params()['rho'], self.water_params()['sigma'])
            label = rf"({ls[i]:0.2f}, {ls[i+1]:0.2f})  ,  ({d[0]*1e6:0.2f}, {d[-1]*1e6:0.2f})"
            ax.plot([], [], '.', color=colors[i], label=label, markersize=np.mean(ddf['jet_diameter'])*1e6)
            for d in ddf['jet_diameter'].to_numpy():
                ax.plot(re, we, '.', color=colors[i], markersize=2*d*1e6)

        ax.set_xlabel("Liquid Jet Reynolds Number")
        ax.set_ylabel("Liquid Jet Weber Number")
        ax.grid(alpha=0.5)
        lt = r"Liquid Flow Rate Range ($\mu$L/min), Jet Diameter ($\mu$m)"

        fig.suptitle(nozzle_id)

        if savepath is not None:
            plt.savefig(savepath)
        if show:
            plt.show()








if __name__ == '__main__':

    import glob, os

    # filepath = "../developer/kkarpos/figure_tests/565_63_water.csv"
    # filepath = "../developer/kkarpos/figure_tests/79V1-DB-50G-50L-221206-04_glycerol_25.csv"
    # filepath = "../developer/kkarpos/figure_tests/79M_AA_50L_100G.csv"
    filepath = "/Users/kkarpos/Desktop/NozzleData/79_water/"

    csvs = glob.glob(os.path.join(filepath, "*.csv"))

    df = pd.read_csv(csvs[0])
    df['nozzle_id'] = csvs[0].split('/')[-1].split('_')[0]
    for i, d in enumerate(csvs):
        dm = pd.read_csv(d)
        dm['nozzle_id'] = d.split('/')[-1].split('_')[0]
        df = pd.concat([df, dm])

    df = df.reset_index(drop=True)


    dp = DataPlotter(dataframe=df)

    # dp.set_nozzle_id("")

















