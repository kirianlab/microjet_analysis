# Microjet Analysis Repository

This git repository contains various python scripts to analyze liquid microjet data, 
as shown in our publication

Karpos, K. et al. Comprehensive characterization of gas dynamic virtual nozzles for 
x-ray free-electron laser experiments. Structural Dynamics 11, 064302 (2024).

https://doi.org/10.1063/4.0000262

We are in the process of documenting the microjet_analysis package.  Hopefully you
will soon see improvements to this readme.  In the meantime, it is best if you
run the Jupyter notebooks found in the examples directory.